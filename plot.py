import matplotlib.pyplot as plt
from utils import *
import pickle,glob

plt.rc('lines', linewidth=2)
plt.rc('pdf', fonttype=42)
plt.rc('ps', fonttype=42)
plt.rc('font', size=15)

def plot_num_actions(filepaths,names,savePath=None,speedup='Push',barchart=False,width=6):
    fig=plt.figure()
    ax=fig.add_subplot(1,1,1)
    filepaths=[glob.glob(paths) for paths in filepaths]
    
    #plot
    xlabel='$N$'
    actionsList=[]
    objectsList=[]
    ax.set_ylabel('#Actions')
    for name,files in zip(names,filepaths):
        actions=[]
        objects=[]
        for idf,f in enumerate(files):
            args,traj,_,_,transferHistory=pickle.load(open(f,'rb'))
            nrA=len(transferHistory)
            if 'seed' in args:
                xlabel='Problem Instance'
                nrO=args['seed']+1
            else: nrO=len(traj[0])-1 if isinstance(traj[0],list) else idf
            #insert
            id=0
            while id<len(objects) and objects[id]<nrO:
                id+=1
            actions=actions[0:id]+[nrA]+actions[id:]
            objects=objects[0:id]+[nrO]+objects[id:]
        #plot
        actionsList.append(actions)
        objectsList.append(objects)
        if not barchart:
            ax.plot(objects,actions,'o-',label=name)
    ax.legend(loc='best')
    ax.set_xlabel(xlabel)
    
    #plot speedup
    if barchart:
        #ax.set_ylabel('SpeedUp')
        ax.bar([i-width/2 for i in objectsList[0]],actionsList[-1],width=width,label=names[1])
        ax.bar([i+width/2 for i in objectsList[0]],actionsList[ 0],width=width,label=names[0])
        ax.legend(loc='best')
    elif speedup!='' and len(actionsList)==2:
        ax2=ax.twinx()
        ax2.set_ylabel('SpeedUp')
        speedups=op.div(actionsList[0],actionsList[-1])
        ax2.set_ylim(min(speedups)*0.5,max(speedups)*1.5)
        ax2.plot(objectsList[0],speedups,'o-',label='SpeedUp With %s'%speedup,color='brown')
        ax2.legend(loc='best')
        
    plt.title('%s - #Actions'%xlabel)
    if savePath is not None:
        plt.savefig(savePath)
    plt.show()
    
def plot_transits(filepaths,names,savePath=None,speedup='Push',withTransfer=False,barchart=False,width=6):
    fig=plt.figure()
    ax=fig.add_subplot(1,1,1)
    filepaths=[glob.glob(paths) for paths in filepaths]
    
    #plot
    xlabel='$N$'
    actionsList=[]
    objectsList=[]
    ax.set_ylabel('Transit')
    for name,files in zip(names,filepaths):
        actions=[]
        objects=[]
        for idf,f in enumerate(files):
            args,traj,_,transitHistory,transferHistory=pickle.load(open(f,'rb'))
            nrA=sum(transitHistory)
            if withTransfer:
                nrA+=sum(transferHistory)
            if 'seed' in args:
                xlabel='Problem Instance'
                nrO=args['seed']+1
            else: nrO=len(traj[0])-1 if isinstance(traj[0],list) else idf
            #insert
            id=0
            while id<len(objects) and objects[id]<nrO:
                id+=1
            actions=actions[0:id]+[nrA]+actions[id:]
            objects=objects[0:id]+[nrO]+objects[id:]
        #plot
        actionsList.append(actions)
        objectsList.append(objects)
        if not barchart:
            ax.plot(objects,actions,'o-',label=name)
    ax.legend(loc='best')
    ax.set_xlabel(xlabel)
    
    #plot speedup
    if barchart:
        #ax.set_ylabel('SpeedUp')
        ax.bar([i-width/2 for i in objectsList[0]],actionsList[-1],width=width,label=names[1])
        ax.bar([i+width/2 for i in objectsList[0]],actionsList[ 0],width=width,label=names[0])
        ax.legend(loc='best')
    elif speedup!='' and len(actionsList)==2:
        ax2=ax.twinx()
        ax2.set_ylabel('SpeedUp')
        speedups=op.div(actionsList[0],actionsList[-1])
        ax2.set_ylim(min(speedups)*0.5,max(speedups)*1.5)
        ax2.plot(objectsList[0],speedups,'o-',label='SpeedUp With %s'%speedup,color='brown')
        ax2.legend(loc='best')
        
    plt.title('%s - Transit'%xlabel)
    if savePath is not None:
        plt.savefig(savePath)
    plt.show()
    
def adjustFigAspect(fig,aspect=1):
    '''
    Adjust the subplot parameters so that the figure has the correct
    aspect ratio.
    '''
    xsize,ysize = fig.get_size_inches()
    minsize = min(xsize,ysize)
    xlim = .4*minsize/xsize
    ylim = .4*minsize/ysize
    if aspect < 1:
        xlim *= aspect
    else:
        ylim /= aspect
    fig.subplots_adjust(left=.5-xlim,
                        right=.5+xlim,
                        bottom=.5-ylim,
                        top=.5+ylim)
    
def sample_average(xss,yss,xi):
    ret=0.
    for xlist,ylist in zip(xss,yss):
        #first
        if xlist[0]>xi:
            ret+=ylist[0]
            continue
        #determine id
        id=0
        while id<len(xlist)-1 and xlist[id]<xi:
            id+=1
        #last
        if id==len(xlist)-1:
            ret+=ylist[-1]
        else:   #middle
            alpha=(xi-xlist[id])/(xlist[id+1]-xlist[id])
            ret+=ylist[id]*(1-alpha)+ylist[id+1]*alpha
    #average
    return ret/len(xss)
    
def plot_cost_reduces(filepaths,names,savePath=None,withTransfer=False):
    fig=plt.figure()
    ax=fig.add_subplot(1,1,1)
    filepaths=[glob.glob(paths) for paths in filepaths]
    plt.rc('lines', linewidth=4)
    
    NR_SAMPLE=1000
    ax.set_ylabel('Cost')
    for name,files in zip(names,filepaths):
        xss=[]
        yss=[]
        for idf,f in enumerate(files):
            traj,ops,costHistory,transitHistory,transferHistory=pickle.load(open(f,'rb'))
            transitHistoryAccum=[0]+[sum(transitHistory[0:i]) for i in range(1,len(transitHistory)+1)]
            transferHistoryAccum=[0]+[sum(transferHistory[0:i]) for i in range(1,len(transitHistory)+1)]
            if withTransfer:
                transitHistoryAccum=[transitHistoryAccum[i]+transferHistoryAccum[i] for i in range(len(transitHistoryAccum))]
            #store
            xss.append(transitHistoryAccum)
            yss.append(costHistory)
        #average
        maxTrans=max([x[-1] for x in xss])
        x=[maxTrans*i/NR_SAMPLE for i in range(NR_SAMPLE)]
        y=[sample_average(xss,yss,xi) for xi in x]
        ax.plot(x,y,label=name)
    
    adjustFigAspect(fig, aspect=3.)
    ax.set_xlabel('Transit+Transfer')
    plt.legend(loc='best')
    plt.title('Cost - Transit+Transfer')
    if savePath is not None:
        plt.savefig(savePath)
    plt.show()

def plot_cost(filepaths,names,savePath=None):
    fig=plt.figure()
    ax=fig.add_subplot(1,1,1)
    for file,name in zip(filepaths,names):
        traj,ops,costHistory,transitHistory,transferHistory=pickle.load(open(file,'rb'))
        ax.plot([i for i,_ in enumerate(costHistory)],costHistory,label=name)
    ax.set_xlabel('Action Id')
    ax.set_ylabel('Cost')
    plt.legend(loc='best')
    plt.title('Cost - Action Id')
    if savePath is not None:
        plt.savefig(savePath)
    plt.show()

def plot_transit(filepaths,names,savePath=None,withTransfer=False):
    fig=plt.figure()
    ax=fig.add_subplot(1,1,1)
    for file,name in zip(filepaths,names):
        traj,ops,costHistory,transitHistory,transferHistory=pickle.load(open(file,'rb'))
        transitHistoryAccum=[0]+[sum(transitHistory[0:i]) for i in range(1,len(transitHistory)+1)]
        transferHistoryAccum=[0]+[sum(transferHistory[0:i]) for i in range(1,len(transitHistory)+1)]
        if withTransfer:
            transitHistoryAccum=[transitHistoryAccum[i]+transferHistoryAccum[i] for i in range(len(transitHistoryAccum))]
        ax.plot(transitHistoryAccum,costHistory,label=name)
    ax.set_xlabel('Transit')
    ax.set_ylabel('Cost')
    plt.legend(loc='best')
    plt.title('Cost - Transit')
    if savePath is not None:
        plt.savefig(savePath)
    plt.show()

if __name__=='__main__':
    paths=["tmpSaveGreedy.txt","tmpSaveReceding.txt"]
    names=["Greedy","Receding Horizon"]
    plot_cost(paths,names)
    plot_transit(paths,names)