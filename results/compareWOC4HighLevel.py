import os,shutil,sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from plannerRun import run_example
from plot import plot_num_actions,plot_transits

if __name__=='__main__':
    NTest=10
    objFrac=0.2
    objSz=0.025
    path='compareWOC4HighLevelResults'
    #generate data
    if not os.path.exists(path):
        os.mkdir(path)
    for i in range(NTest):
        planner='grasp_push_continuous_recedingHorizonMerged'
        if not os.path.exists('%s/H1%d.dat'%(path,i)):
            run_example(nrCluster=4,planner=planner,horizon=1,objFrac=objFrac,scaleCamera=2.5,seed=i,savePath='%s/H1%d.dat'%(path,i),bbt=((0.25,-0.5,0),(0.75,0.5,0)))
        if not os.path.exists('%s/H2%d.dat'%(path,i)):
            run_example(nrCluster=4,planner=planner,horizon=2,objFrac=objFrac,scaleCamera=2.5,seed=i,savePath='%s/H2%d.dat'%(path,i),bbt=((0.25,-0.5,0),(0.75,0.5,0)))
        if not os.path.exists('%s/H3%d.dat'%(path,i)):
            run_example(nrCluster=4,planner=planner,horizon=3,objFrac=objFrac,scaleCamera=2.5,seed=i,savePath='%s/H3%d.dat'%(path,i),bbt=((0.25,-0.5,0),(0.75,0.5,0)))
    #plot data
    plot_num_actions(['%s/H1[0-9].dat'%path,'%s/H3[0-9].dat'%path],['H=1','H=3'],savePath='%s/actions.pdf' %path,speedup='High-Level',barchart=True,width=0.3)
    plot_transits   (['%s/H1[0-9].dat'%path,'%s/H3[0-9].dat'%path],['H=1','H=3'],savePath='%s/transits.pdf'%path,speedup='High-Level',barchart=True,width=0.3)