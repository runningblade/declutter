import os,shutil,sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from plannerRun import run_example
from plot import plot_num_actions,plot_transits

if __name__=='__main__':
    NTest=10
    objFrac=0.2
    objSz=0.025
    path='compareWOCPushResults'
    #generate data
    if not os.path.exists(path):
        os.mkdir(path)
    for i in range(NTest):
        nrObj=int((200-50)*i/(NTest-1)+50)
        plannerO='grasp'
        plannerW='grasp_push_continuous_greedy'
        if not os.path.exists('%s/grasp%d.dat'%(path,i)):
            run_example(nrCluster=-2,planner=plannerO,objFrac=objFrac,scaleCamera=2.5,objSz=objSz,nrObj=nrObj,savePath='%s/grasp%d.dat'%(path,i),bbt=((0.25,-0.5,0),(0.75,0.5,0)))
        if not os.path.exists('%s/graspPush%d.dat'%(path,i)):
            run_example(nrCluster=-2,planner=plannerW,objFrac=objFrac,scaleCamera=2.5,objSz=objSz,nrObj=nrObj,savePath='%s/graspPush%d.dat'%(path,i),bbt=((0.25,-0.5,0),(0.75,0.5,0)))
    #plot data
    plot_num_actions(['%s/grasp[0-9].dat'%path,'%s/graspPush[0-9].dat'%path],['Grasp','Grasp+Push'],savePath='%s/actions.pdf'%path,barchart=True)
    plot_transits   (['%s/grasp[0-9].dat'%path,'%s/graspPush[0-9].dat'%path],['Grasp','Grasp+Push'],savePath='%s/transits.pdf'%path,barchart=True)