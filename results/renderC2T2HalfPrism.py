import os,shutil,sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from plannerRun import run_example

if __name__=='__main__':
    objFrac=0.5
    objSz=0.05
    nrObj=70
    cube=False
    bbt=((0.3,-0.4,0),(0.75,0.4,0))
    vis=run_example(nrCluster=2,planner='grasp_push_continuous_recedingHorizonMerged',horizon=3,    \
                    seed=0,objFrac=objFrac,scaleCamera=None,objSz=objSz,nrObj=nrObj,cube=cube,bbt=bbt)
    vis.run()