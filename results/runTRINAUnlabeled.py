import os,shutil,sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from plannerRun import run_example
from sensor import SensorEmulator

if __name__=='__main__':
    objFrac=0#.5
    objSz=0.035
    nrObj=20
    dx=-0.05
    dx2=-0.1
    dy=0.
    vis=run_example(planner='grasp_push_continuous_recedingHorizon',horizon=3,useTRINA="software",    \
                    seed=1,objFrac=objFrac,scaleCamera=None,objSz=objSz,nrObj=nrObj,bbt=((-0.125+dx2,-0.3,0),(0.15+dx2,0.3,0)),  \
                    nrCluster=2,bbc0=((-0.25+dx,0.2-dy,0),(0.15+dx,0.6-dy,0)),bbc1=((-0.25+dx,-0.6+dy,0),(0.15+dx,-0.2+dy,0)),  \
                    objInit="move_to_table",jointPush=True)
    
    vis.planNonStop=True
    vis.playSpeed=1
    vis.run()