import os,shutil,sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from plannerRun import run_example

if __name__=='__main__':
    objFrac=0.
    objSz=0.05
    nrObj=70
    cube=0.5
    bbt=((0.3,-0.5,0),(0.75,0.5,0))
    vis=run_example(nrCluster=-2,planner='grasp_pushBD_continuous_recedingHorizon',horizon=3,    \
                    seed=1,objFrac=objFrac,scaleCamera=None,objSz=objSz,nrObj=nrObj,cube=cube,bbt=bbt,  \
                    bbc0=((0.45,-0.75),(0.75,-0.25)),objInit="move_to_table",
                    bbc1=((0.45, 0.25),(0.75, 0.75)))
    vis.run()