import os,shutil,sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from plannerRun import run_example
from sensor import SensorEmulator

if __name__=='__main__':
    objFrac=0.5
    objSz=0.05
    nrObj=20
    dx=0.05
    dx2=-0.1
    dy=-0.05
    mode="hardware"
    vis=run_example(planner='grasp_push_continuous_recedingHorizonMerged',horizon=5,useTRINA=mode,    \
                    seed=1,objFrac=objFrac,scaleCamera=None,objSz=objSz,nrObj=nrObj,bbt=((-0.125+dx2,-0.3,0),(0.15+dx2,0.3,0)),  \
                    nrCluster=-2 if objFrac>0. else 2,bbc0=((-0.15+dx,0.2-dy,0),(0.15+dx,0.6-dy,0)),bbc1=((-0.15+dx,-0.6+dy,0),(0.15+dx,-0.2+dy,0)),  \
                    objInit="move_to_table",jointPush=False)
    
    objs=[vis.world.rigidObject(i) for i in range(vis.world.numRigidObjects())]
    if mode=="hardware":
        vis.sensor=SensorEmulator(vis.world,vis.table,objs,vis.planner.sim,res=(4416//2,1242),ignoreId=objFrac==0.)
        #vis.sensor.close_motion()
    
    if hasattr(vis,"sensor"): #Intrinsic
        import numpy as np
        fx=1055.28
        fy=1054.69
        cx=1126.61
        cy=636.138
        k1=-0.0426844
        k2=0.0117943
        k3=-0.00548354
        p1=0.000242741
        p2=-0.000475926
        vis.sensor.mtx = np.array([[fx,0,cx], [0,fy,cy], [0,0,1]])
        vis.sensor.dist = np.array([k1,k2,p1,p2,k3])
        
    if hasattr(vis,"sensor"): #Extrinsic
        vis.sensor.extrinsic = vis.sensor.get_remote_transform()
        print("DETECTED EXTRINSIC: %s"%str(vis.sensor.extrinsic))
        
    vis.planNonStop=True
    vis.playSpeed=1
    vis.run()