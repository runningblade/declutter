import os,shutil,sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from plannerRun import run_example

if __name__=='__main__':
    objFrac=0.
    objSz=0.05
    nrObj=100
    bbt=((0.15,-0.6,0),(0.75,0.6,0))
    vis=run_example(nrCluster=2,planner='grasp_pushBD_continuous_recedingHorizonMerged',horizon=3,  \
                    seed=1,objFrac=objFrac,scaleCamera=None,objSz=objSz,nrObj=nrObj,bbt=bbt)
    vis.run()