import os,shutil,sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from plannerRun import run_example
from plot import plot_num_actions,plot_transits,plot_cost_reduces

if __name__=='__main__':
    NTest=10
    objFrac=0.2
    objSz=0.025
    path='compareWOCPushCostReduceResults'
    #generate data
    if not os.path.exists(path):
        os.mkdir(path)
    for i in range(NTest):
        plannerO='grasp'
        plannerW='grasp_push_continuous_greedy'
        if not os.path.exists('%s/grasp%d.dat'%(path,i)):
            run_example(nrCluster=2,planner=plannerO,objFrac=objFrac,scaleCamera=2.5,seed=i,objSz=objSz,savePath='%s/grasp%d.dat'%(path,i),bbt=((0.25,-0.5,0),(0.75,0.5,0)))
        if not os.path.exists('%s/graspPush%d.dat'%(path,i)):
            run_example(nrCluster=2,planner=plannerW,objFrac=objFrac,scaleCamera=2.5,seed=i,objSz=objSz,savePath='%s/graspPush%d.dat'%(path,i),bbt=((0.25,-0.5,0),(0.75,0.5,0)))
    #plot data
    plot_cost_reduces(['%s/grasp[0-9].dat'%path,'%s/graspPush[0-9].dat'%path],['Grasp','Grasp+Push'],savePath='%s/costReduce.pdf'%path)
    