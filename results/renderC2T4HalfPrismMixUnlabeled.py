import os,shutil,sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from plannerRun import run_example

if __name__=='__main__':
    objFrac=0.
    objSz=0.05
    nrObj=50
    cube=0.5
    dx1=0.2
    dx2=0.2
    bbt=((0.5,-0.5,0),(0.85,0.5,0))
    vis=run_example(nrCluster=4,planner='grasp_push_continuous_recedingHorizonMerged',horizon=3,    \
                    seed=0,objFrac=objFrac,scaleCamera=None,objSz=objSz,nrObj=nrObj,cube=cube,bbt=bbt,  \
                    bbc0=(( 0.65+dx1,-0.55-dx2),( 0.95+dx1,-0.05-dx2)),objInit="move_to_table",
                    bbc1=(( 0.65+dx1, 0.05+dx2),( 0.95+dx1, 0.55+dx2)),
                    bbc2=(( 0.25-dx1,-0.55-dx2),( 0.55-dx1,-0.05-dx2)),
                    bbc3=(( 0.25-dx1, 0.05+dx2),( 0.55-dx1, 0.55+dx2)))
    vis.run()