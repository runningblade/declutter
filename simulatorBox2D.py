from costFunction import CostDistToRegionQuadratic,OptimalTransportCost
from costFunctionFast import CostDistToRegionQuadraticFast
from reachableTraj import ReachableTrajTx90Robotiq
from reachableTRINA import ReachableTRINA
import simulator,Box2D.b2 as b2
from polygon2D import Polygon2D
from klampt.vis import gldraw
from klampt import Simulator
import OpenGL.GL as gl
from scene import *
import simulator
import executor

class SimulatorBox2D(simulator.Simulator):
    def __init__(self,cost,world,reachableTraj,friction=0.3,damping=10.0,robotBaseBox2D=False,dt=0.001):
        if world is None:
            return
        simulator.Simulator.__init__(self,cost,world,reachableTraj,dt=dt)
        self.friction=friction
        self.damping=damping
        #create rigid objects in Box2D
        self.objectsBox2D=[]
        self.fixturesBox2D=[]
        self.shapesBox2D=[]
        self.worldBox2D=b2.world(gravity=(0,0),doSleep=True)
        for i in range(world.numRigidObjects()):
            body,fixture,shape=self.add_object(world.rigidObject(i))
            self.objectsBox2D.append(body)
            self.fixturesBox2D.append(fixture)
            self.shapesBox2D.append(shape)
        self.sim=Simulator(world)
        self.executor=executor.KlamptExecutor(self.sim)
        #create pusher
        eeL=(self.world.robot(0),self.reachableTraj.get_pusher().eeLid)
        self.objectPusherBox2D,self.fixturePusherBox2D,self.shapePusherBox2D=self.add_object(eeL,kinematic=True)
        self.dirPusherBox2D=SimulatorBox2D.longest_angle(self.shapePusherBox2D)
        #create robot base
        if robotBaseBox2D:
            eeB=self.world.robot(0).link(0)
            self.bodyBase,self.fixtureBase,_=self.add_object(eeB,kinematic=True)
        else: self.bodyBase=self.fixtureBase=None
            
    @staticmethod
    def longest_angle(poly):
        maxLen=0
        maxLenId=-1
        for i in range(len(poly.vss)):
            length=op.norm(poly.edge(i))
            if length>maxLen:
                maxLen=length
                maxLenId=i
        n=perp(poly.edge(maxLenId))
        n=op.div(n,op.norm(n))
        return n
            
    def add_object(self,rigid,kinematic=False,skin=0.003):
        if isinstance(rigid,Polygon2D):
            poly=rigid
        elif isinstance(rigid,tuple):
            poly=Polygon2D(project_convex_mesh_to_2D_pusher(rigid[0],rigid[1]))
            rigid=rigid[0].link(rigid[1])
        else: 
            poly=Polygon2D(project_convex_mesh_to_2D(rigid.geometry()))
        #add body
        R,t=rigid.getTransform()
        angle=op.atan2_safe(R[1],R[0])
        if kinematic:
            body=self.worldBox2D.CreateKinematicBody(position=(t[0],t[1]),angle=angle)
            fixture=body.CreatePolygonFixture(vertices=poly.vss,density=1,friction=self.friction,radius=skin)
        else:
            body=self.worldBox2D.CreateDynamicBody(allowSleep=False,position=(t[0],t[1]),angle=angle)
            fixture=body.CreatePolygonFixture(vertices=poly.vss,density=1,friction=self.friction,radius=skin)
            body.linearDamping=self.damping
            body.angularDamping=self.damping
        body.sleepingAllowed=False
        body.color=rigid.appearance().getColor()
        body.name=rigid.getName()
        return body,fixture,poly
    
    def relax(self,thres=1e-6,maxIter=10000):
        #run box2D simulation
        it=0
        while it<maxIter:
            it+=1
            #before
            infoBefore=[]
            for i in range(self.world.numRigidObjects()):
                body=self.objectsBox2D[i]
                infoBefore+=[body.position[0],body.position[1],body.angle]
            #sim
            self.simulate()
            #after
            infoAfter=[]
            for i in range(self.world.numRigidObjects()):
                body=self.objectsBox2D[i]
                infoAfter+=[body.position[0],body.position[1],body.angle]
            #compare
            diff=op.norm(op.sub(infoBefore,infoAfter))
            print("diff=%f"%diff)
            if diff<thres:
                break
        self.sync_back()
    
    def execute(self,dof0,dof,move_pusher_away=False):
        if self.executor is None:
            #get height of object
            objectBBHigh=0.0
            for i in range(self.world.numRigidObjects()):
                object=self.world.rigidObject(i)
                objectBBHigh=max(objectBBHigh,compute_bb_tight(object.geometry())[1][2])
        
            self.world.robot(0).setConfig(dof)
            pusher=self.reachableTraj.get_pusher()
            pusherBBLow=get_robot_bb(self.world.robot(0),pusher.eeLid)[0][2]
            if pusherBBLow<objectBBHigh:
                #in this case, we will simulate using box2D
                body=self.objectPusherBox2D
                if move_pusher_away:
                    #set pusher position
                    body.position=(INFINITY_NUMBER,INFINITY_NUMBER)
                    body.angle=0.
                    
                    #set pusher velocity
                    body.linearVelocity=(0.,0.)
                    body.angularVelocity=0.
                else:
                    #set pusher position
                    gpos=self.reachableTraj.get_pusher().global_pos()[1]
                    gangle=self.reachableTraj.get_pusher().global_angle()
                    body.position=(gpos[0],gpos[1])
                    body.angle=gangle
                    
                    #set pusher velocity
                    self.world.robot(0).setConfig(dof0)
                    gpos0=self.reachableTraj.get_pusher().global_pos()[1]
                    gangle0=self.reachableTraj.get_pusher().global_angle()
                    while abs(gangle-gangle0-math.pi*2)<abs(gangle-gangle0):
                        gangle0+=math.pi*2
                    while abs(gangle-gangle0+math.pi*2)<abs(gangle-gangle0):
                        gangle0-=math.pi*2
                    body.linearVelocity=tuple(op.mul(op.sub(gpos,gpos0)[0:2],1/self.dt))
                    body.angularVelocity=op.mul(gangle-gangle0,1/self.dt)
                
                #run box2D simulation
                self.simulate()
                self.sync_back()
                #set robot pose
                self.world.robot(0).setConfig(dof)
            else:
                self.world.robot(0).setConfig(dof)
            return self.dt
        else: 
            return self.executor.execute(dof,self.dt)
        
    def simulate(self,vel_iters=10,pos_iters=20):
        self.worldBox2D.Step(self.dt,vel_iters,pos_iters)
    
    def draw_polygon_Box2D(self,obj,fixture,fill,margin):
        vss=[obj.transform*v for v in fixture.shape.vertices]
        if fill:
            gl.glBegin(gl.GL_TRIANGLES)
            for i in range(1,len(vss)-1):
                a=vss[0]
                b=vss[i]
                c=vss[i+1]
                gl.glVertex3f(a[0],a[1],margin)
                gl.glVertex3f(b[0],b[1],margin)
                gl.glVertex3f(c[0],c[1],margin)
            gl.glEnd()
        else:
            gl.glBegin(gl.GL_LINES)
            for i in range(len(vss)):
                a=vss[i]
                b=vss[(i+1)%len(vss)]
                gl.glVertex3f(a[0],a[1],margin)
                gl.glVertex3f(b[0],b[1],margin)
            gl.glEnd()
            
    def draw_circle_Box2D(self,obj,fixture,fill,margin):
        gl.glPushMatrix()
        pos=obj.transform().position
        glTranslatef(pos[0],pos[1],0)
        draw_circle(fill,fixture.shape.radius,margin)
        gl.glPopMatrix()
            
    def draw_object_Box2D(self,obj,fixture,fill=False,margin=0.001):
        if fixture.shape.type==2:
            self.draw_polygon_Box2D(obj,fixture,fill,margin)
        elif fixture.shape.type==0:
            self.draw_circle_Box2D(obj,fixture,fill,margin)
        else: assert False

    def display(self):
        #objects
        for obj,fixture in zip(self.objectsBox2D,self.fixturesBox2D):
            gldraw.setcolor(obj.color[0],obj.color[1],obj.color[2],obj.color[3])
            self.draw_object_Box2D(obj,fixture,True)
            
        #pusher
        obj=self.objectPusherBox2D
        fixture=self.fixturePusherBox2D
        gldraw.setcolor(PUSHER_COLOR[0],PUSHER_COLOR[1],PUSHER_COLOR[2])
        self.draw_object_Box2D(obj,fixture,True)
        
        #base
        if self.bodyBase is not None:
            gldraw.setcolor(PUSHER_COLOR[0],PUSHER_COLOR[1],PUSHER_COLOR[2])
            self.draw_object_Box2D(self.bodyBase,self.fixtureBase,True)
    
    def sync_back(self):
        #sync-back
        for i in range(self.world.numRigidObjects()):
            body=self.objectsBox2D[i]
            R,t=self.world.rigidObject(i).getTransform()
            t=(body.position[0],body.position[1],t[2])
            R=[0.0 for id in range(9)]
            R[0]=R[4]=op.cos_safe(body.angle)
            R[1]=R[3]=op.sin_safe(body.angle)
            R[3]*=-1
            R[8]=1.0
            self.world.rigidObject(i).setTransform(R,t)
    
    def sync(self,move_pusher_away=True):
        #objects
        for i in range(self.world.numRigidObjects()):
            R,t=self.world.rigidObject(i).getTransform()
            angle=op.atan2_safe(R[1],R[0])
            body=self.objectsBox2D[i]
            
            #position0=body.position
            #angle0=body.angle
            body.position=(t[0],t[1])
            body.angle=angle
            body.linearVelocity=(0,0)
            body.angularVelocity=0
    
        #pusher
        body=self.objectPusherBox2D
        gpos=self.reachableTraj.get_pusher().global_pos()[1]
        gangle=self.reachableTraj.get_pusher().global_angle()
        #position0=body.position
        #angle0=body.angle
        if move_pusher_away:
            body.position=(INFINITY_NUMBER,INFINITY_NUMBER)
            body.angle=0
        else:
            body.position=(gpos[0],gpos[1])
            body.angle=gangle
        body.linearVelocity=(0,0)
        body.angularVelocity=0
    
    def pss(self):
        pss=[]
        for obj in self.objectsBox2D:
            pss.append((obj.position.x,obj.position.y))
        return pss
    
    def css(self):
        return [int(obj.name) for obj in self.objectsBox2D]
    
    def get_state(self):
        state=[]
        for obj in self.objectsBox2D:
            state.append(((obj.position.x,obj.position.y),obj.angle))
        #pusher
        state.append(self.world.robot(0).getConfig())
        return state
        
    def set_state(self,state):
        for obj,s in zip(self.objectsBox2D,state[0:len(state)-1]):
            obj.linearVelocity=(0,0)
            obj.angularVelocity=0
            obj.position=(s[0][0],s[0][1])
            obj.angle=s[1]
        #pusher
        body=self.objectPusherBox2D
        body.position=(INFINITY_NUMBER,INFINITY_NUMBER)
        body.angle=0
        self.world.robot(0).setConfig(state[-1])
    
    def get_shapes(self,emulated_pos_noise=None):
        shapes=[]
        for obj,shape in zip(self.objectsBox2D,self.shapesBox2D):
            if emulated_pos_noise is not None:
                emulated_pos_x=random.uniform(-emulated_pos_noise,emulated_pos_noise)
                emulated_pos_y=random.uniform(-emulated_pos_noise,emulated_pos_noise)
                shapes.append(shape.transform((emulated_pos_x,emulated_pos_y),obj.angle))
            else:
                shapes.append(shape.transform((obj.position.x,obj.position.y),obj.angle))
        return shapes
    
    def stopped(self,thresLinearVelocity=0.0001,thresAngularVelocity=0.0001):
        for obj in self.objectsBox2D:
            if abs(obj.linearVelocity[0])>thresLinearVelocity:
                return False
            if abs(obj.linearVelocity[1])>thresLinearVelocity:
                return False
            if abs(obj.angularVelocity)>thresAngularVelocity:
                return False
        return True
    
    def push(self,s,pss,speed,trajDOF):
        self.set_state(s)
        cost0=self.calc_cost()
        
        #phase1
        traj=[]
        for i in range(1,len(pss)):
            v=op.sub(pss[i],pss[i-1])
            time=op.norm(v)/speed
            v=op.mul(v,speed/max(op.norm(v),1e-6))
            self.objectPusherBox2D.position=(pss[i-1][0],pss[i-1][1])
            cosVal=op.dot(v,self.dirPusherBox2D)
            sinVal=op.dot(v,[-self.dirPusherBox2D[1],self.dirPusherBox2D[0]])
            angle=math.atan2(sinVal,cosVal)
            while abs(angle-math.pi)<abs(angle):
                angle-=math.pi
            while abs(angle+math.pi)<abs(angle):
                angle+=math.pi
            self.objectPusherBox2D.angle=angle
            self.objectPusherBox2D.linearVelocity=(v[0],v[1])
            self.objectPusherBox2D.angularVelocity=0
            t=0
            while t<time:
                self.simulate()
                traj.append(self.get_state())
                t+=self.dt
        
        #phase2
        self.objectPusherBox2D.position=(pss[-1][0],pss[-1][1])
        self.objectPusherBox2D.linearVelocity=(0,0)
        self.objectPusherBox2D.angularVelocity=0
        while not self.stopped():
            self.simulate()
            traj.append(self.get_state())
        
        state1=self.get_state()
        cost1=self.calc_cost()
        return state1,cost0-cost1,traj
    
    def grasp(self,s,oid,t):
        self.set_state(s)
        cost0=self.calc_cost()
        
        obj=self.objectsBox2D[oid]
        obj.linearVelocity=(0,0)
        obj.angularVelocity=0
        obj.position=(t[0],t[1])
        
        state1=self.get_state()
        cost1=self.calc_cost()
        return state1,cost0-cost1,[state1]
    
    def pusher_shape(self):
        return self.shapePusherBox2D
    
    def keyboardfunc(self,c,x,y):
        if c==b'h':
            self.objectPusherBox2D.linearVelocity+=(0, 1)
        elif c==b'k':
            self.objectPusherBox2D.linearVelocity+=(0,-1)
        elif c==b'u':
            self.objectPusherBox2D.linearVelocity+=( 1,0)
        elif c==b'j':
            self.objectPusherBox2D.linearVelocity+=(-1,0)
        elif c==b'y':
            self.objectPusherBox2D.angularVelocity+=10
        elif c==b'i':
            self.objectPusherBox2D.angularVelocity-=10
    
if __name__=='__main__':
    useTRINA=False
    if useTRINA:
        world,table,robot,robot_path=make_scene_TRINA()
        bbt=((-0.25,-0.5,0),(0.25,0.5,0))#table
        create_boxes_same_size(world,50,0.05,0.5,bbt,robot,0.004,"move_to_table")
        cost1=CostDistToRegionQuadraticFast(((-0.75,-0.65),(-0.5,-0.4)))
        cost2=CostDistToRegionQuadraticFast(((-0.75,0.4),(-0.5,0.65)))
    else:
        world,table,robot,robot_path=make_scene_tx90()
        bbt=((0.25,-0.5,0),(0.75,0.5,0))#table
        create_boxes_same_size(world,50,0.05,0.5,bbt,robot,0.004,"move_to_table")
        cost1=CostDistToRegionQuadraticFast(((0.25,-0.35),(0.5,-0.1)))
        cost2=CostDistToRegionQuadraticFast(((0.25,0.1),(0.5,0.35)))
    
    cost=OptimalTransportCost([cost1,cost2],[[12,13],[13,12]])
    if useTRINA:
        reachableTraj=ReachableTRINA(world,table,robot,robot_path,0.3)
    else: reachableTraj=ReachableTrajTx90Robotiq(world,table,robot,robot_path,0.3)
    reachableTraj.build_grid(0.025)
    reachableTraj.drawEE=False
    
    sim=SimulatorBox2D(cost,world,reachableTraj,robotBaseBox2D=True)
    simulator.SimulatorGLVisualizer(world,table,robot,sim,scaleCamera=1.5).run()