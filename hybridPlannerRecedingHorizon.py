from polygon2D import Polygon2D
from planner import *

class HybridPlannerRecedingHorizon(Planner):
    def __init__(self,sim,subPlanners,level=3,mergeAction=True):
        Planner.__init__(self,sim)
        self.subPlanners=subPlanners
        self.mergeAction=mergeAction
        self.level=level
        #minimal acceptable improvement
        self.minCostReduce=sim.get_shapes()[0].diameter()/10
    
    def plan(self,debugInfo):
        self.bestLeaf=None
        self.bestLeafCost=None
        self.state0=self.sim.get_state()
        self.id0=self.sim.set_state(self.state0)
        
        #depth first search
        stack=[]
        numLeaves=0
        stack+=self.pick_actions(False)
        while len(stack)>0:
            action=stack[-1]
            stack=stack[0:len(stack)-1]
            if debugInfo:
                print("HybridPlannerRecedingHorizon stackSize=%d actionLevel=%s!"%(len(stack),"None" if action is None else str(action.level)))
            if action is None:
                continue
            if action.costReduce<self.minCostReduce:
                if debugInfo:
                    print("HybridPlannerRecedingHorizon costReduce(%f) small than %f!"%(action.costReduce,self.minCostReduce))
                continue
            
            #check cost
            numLeaves+=1
            costReduce=self.cost_reduce(action)
            involveTransit=True
            if involveTransit:
                transit=self.transit(action)
            else: transit=1.0
            if self.bestLeaf is None or costReduce/transit>self.bestLeafCost:
                if debugInfo:
                    print("HybridPlannerRecedingHorizon updatedAction=(%f,%f)!"%(costReduce,transit))
                self.bestLeaf=action
                self.bestLeafCost=costReduce/transit
            
            #depth first search
            if action.level<self.level:
                stack+=self.pick_actions(False,action)
        
        #take only first action
        if self.bestLeaf is not None:
            if debugInfo:
                print("HybridPlannerRecedingHorizon explored %d leaf nodes, cumulativeCostReduction=%f!"%(numLeaves,self.bestLeafCost))
            self.bestAction=self.bestLeaf
            while self.bestAction.parent is not None:
                self.bestAction.parent.next=self.bestAction
                self.bestAction=self.bestAction.parent
            return self.bestAction
        else: 
            if debugInfo:
                print("HybridPlannerRecedingHorizon empty!")
            return None
        
    def pick_actions(self,debug,parent=None):
        ret=None
        
        for planner in self.subPlanners:
            if parent is not None:
                self.sim.set_state(parent.state1)
            else: self.sim.set_state(self.state0)
            actions=planner.branch(debug)
            if ret is None:
                ret=actions
            elif self.mergeAction:
                for i in range(len(ret)):
                    if ret[i] is None:
                        ret[i]=actions[i]
                    elif actions[i] is None:
                        pass
                    elif ret[i].costReduce<actions[i].costReduce:
                        ret[i]=actions[i]
            else: ret+=actions
                
        #set parent/depth
        for action in ret:
            if action is not None:
                action.parent=parent
                action.level=parent.level+1 if parent is not None else 1
        return ret
            
    def cost_reduce(self,action):
        costReduce=0.
        while action is not None:
            costReduce+=action.costReduce
            action=action.parent
        return costReduce
    
    def transit(self,action):
        actions=[]
        while action is not None:
            actions=[action]+actions
            action=action.parent
        
        ret=1e-3
        last=self.id0
        for i in range(len(actions)):
            posBeg=op.mul(actions[i].idss[0],self.sim.reachableTraj.get_pusher().res)
            posEnd=op.mul(actions[i].idss[-1],self.sim.reachableTraj.get_pusher().res)
            ret+=op.norm(op.sub(posBeg,last))
            last=posEnd
        return ret
    
    def get_push_actions(self):
        for planner in self.subPlanners:
            if planner.get_push_actions() is not None:
                return planner.get_push_actions()
        return None