rm -rdf *.user
rm -rdf *.pyc
rm -rdf tmp*
rm -rdf allCases*
rm -rdf gurobi.log
rm -rdf objPose.dat
rm -rdf test_wrench
rm -rdf __pycache__
git add --all
git commit -m "$1"
git push -u origin master
git tag "$2"
git push --tags

