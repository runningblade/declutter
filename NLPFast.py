from symbolicFast import (NLPFunction,NLPVariable,NLPLinearExpression,NLPQuadraticExpression)
from utils import *
import knitro as ktr
import numpy as np

def get_gurobi(output=False,timeLimit=60):
    def get_gurobi_template():
        M=op.gp.Model("Impulse")
        M.setParam("LogToConsole",1 if output else 0)
        M.setParam("LogToFile","")
        if timeLimit>0:
            M.setParam("TimeLimit",timeLimit)
        return M
    return get_gurobi_template

def get_knitro(output=False,checkDeriv=False):
    def get_knitro_template():
        M=NLPSolver()
        M.outlev=3 if output else 0
        M.checkDeriv=checkDeriv
        return M
    return get_knitro_template

class NLPSolver:
    def __init__(self):
        self.outlev=0
        self.ftol=1e-15
        self.checkDeriv=False
        #binary
        self.vss=[]
        self.vbinaries=[]
        self.vlowers=[]
        self.vuppers=[]
        self.inits=[]
        #constraint
        self.cexprs=[]
        self.clowers=[]
        self.cuppers=[]
        #objective
        self.obj=None
        self.objSol=None
        self.objSense=ktr.KN_OBJGOAL_MAXIMIZE
        #solution
        self.sol=None
        self.nStatus=None
        
    def add_var(self,lb=0.0,ub=INFINITY_NUMBER,vtype=None,init=None):
        self.vss.append(NLPVariable(len(self.vlowers)))
        if vtype is None:
            self.vbinaries.append(False)
        else:
            assert vtype==op.GRB.BINARY or vtype==op.GRB.CONTINUOUS
            self.vbinaries.append(vtype==op.GRB.BINARY)
        self.vlowers.append(max(lb,-INFINITY_NUMBER))
        self.vuppers.append(min(ub,INFINITY_NUMBER))
        self.inits.append(0.0 if init is None else init)
        return self.vss[-1]
    
    def addVar(self,lb=0.0,ub=INFINITY_NUMBER,vtype=None,init=None):
        return self.add_var(lb,ub,vtype,init)
    
    def add_con(self,expr=None,lb=0.0,ub=INFINITY_NUMBER):
        assert expr is not None
        if op.is_float(expr):
            assert expr>=lb and expr<=ub
        elif len(expr.vars())==0:
            v=expr.eval(None)
            assert v>=lb and v<=ub
        else:
            self.cexprs.append(expr)
            self.clowers.append(max(lb,-INFINITY_NUMBER))
            self.cuppers.append(min(ub,INFINITY_NUMBER))
        return len(self.clowers)-1
    
    def addConstr(self,expr=None):
        name=expr.name
        if name=="eq":
            return self.add_con(expr.args[1]-expr.args[0],ub=0.0)
        elif name=="le":
            return self.add_con(expr.args[1]-expr.args[0])
        elif name=="ge":
            return self.add_con(expr.args[0]-expr.args[1])
        else: assert False
    
    def get_value(self,var):
        assert self.sol is not None
        return var.x
    
    def set_objective_maximize(self,expr):
        self.objSense=ktr.KN_OBJGOAL_MAXIMIZE
        if expr is None or op.is_float(expr):
            self.obj=None
        else: self.obj=expr
            
    def set_objective_minimize(self,expr):
        self.objSense=ktr.KN_OBJGOAL_MINIMIZE
        if expr is None or op.is_float(expr):
            self.obj=None
        else: self.obj=expr
    
    def setObjective(self,obj,sense=op.GRB.MAXIMIZE):
        if sense==op.GRB.MAXIMIZE:
            self.set_objective_maximize(obj)
        elif sense==op.GRB.MINIMIZE:
            self.set_objective_minimize(obj)
        else: assert False
    
    def objective_value(self):
        return self.objSol
    
    def getObjective(self):
        return self.obj
    
    def get_init(self):
        for vid in range(len(self.vss)):
            if hasattr(self.vss[vid],"start"):
                self.inits[vid]=self.vss[vid].start
        return self.inits
    
    def optimize(self):
        #create a new Knitro solver instance
        self.get_init()
        try:
            kc=ktr.KN_new()
        except:
            print("Failed to find a valid license.")
            quit()
        
        #set variables
        ids=ktr.KN_add_vars(kc,len(self.vss))
        ktr.KN_set_var_lobnds(kc,ids,self.vlowers)
        ktr.KN_set_var_upbnds(kc,ids,self.vuppers)
        ktr.KN_set_var_primal_init_values(kc,ids,self.inits)
        ktr.KN_set_var_types(kc,xTypes=[ktr.KN_VARTYPE_BINARY if b else ktr.KN_VARTYPE_CONTINUOUS for b in self.vbinaries])
        
        #set constraints
        ids=ktr.KN_add_cons(kc,len(self.cexprs))
        ktr.KN_set_con_lobnds(kc,ids,self.clowers)
        ktr.KN_set_con_upbnds(kc,ids,self.cuppers)
        self.optimize_formulation(kc)
        
        #set objective
        ktr.KN_set_obj_goal(kc,self.objSense)
        cb=ktr.KN_add_eval_callback(kc,evalObj=self.objReduced is not None,
                                    indexCons=self.reducedIndices,
                                    funcCallback=self.callbackEvalFC)
        ktr.KN_set_cb_grad(kc,cb,
                           objGradIndexVars=self.objVars,
                           jacIndexCons=self.cbjacIndexCons,
                           jacIndexVars=self.cbjacIndexVars,
                           gradCallback=self.callbackEvalGA)
        ktr.KN_set_int_param(kc,ktr.KN_PARAM_BAR_FEASIBLE,ktr.KN_BAR_FEASIBLE_GET_STAY)
        if self.checkDeriv:
            ktr.KN_set_int_param(kc,ktr.KN_PARAM_DERIVCHECK,ktr.KN_DERIVCHECK_FIRST)
        ktr.KN_set_int_param(kc,ktr.KN_PARAM_OUTLEV,self.outlev)
        ktr.KN_set_double_param(kc,ktr.KN_PARAM_FTOL,self.ftol)
        ktr.KN_solve(kc)
        
        #get solution
        self.nStatus,self.objSol,self.sol,_=ktr.KN_get_solution(kc)
        if self.optimal():
            for i in range(len(self.vss)):
                self.vss[i].x=self.sol[i]   #compatible with gurobi
        else:
            for i in range(len(self.vss)):
                self.vss[i].x=0.0
        self.status=op.GRB.OPTIMAL if self.optimal() else None
        if self.obj is not None:
            self.obj.x=self.objSol
        
    def callbackEvalFC(self,kc,cb,evalRequest,evalResult,userParams):
        if evalRequest.type!=ktr.KN_RC_EVALFC:
            print("*** callbackEvalFC incorrectly called with eval type %d"%evalRequest.type)
            return -1
        x=evalRequest.x
        try:
            if self.objReduced is not None:
                evalResult.obj=self.objReduced.eval(x)
            for i in range(len(self.reducedCexprs)):
                evalResult.c[i]=self.reducedCexprs[i].eval(x)
            return 0
        except RuntimeError:
            return ktr.KN_RC_EVAL_ERR
    
    def callbackEvalGA(self,kc,cb,evalRequest,evalResult,userParams):
        if evalRequest.type!=ktr.KN_RC_EVALGA:
            print("*** callbackEvalGA incorrectly called with eval type %d"%evalRequest.type)
            return -1
        x=evalRequest.x
        try:
            if self.objReduced is not None:
                off=0
                res=self.objReduced.jac(x)
                for i in sorted(res.keys()):
                    evalResult.objGrad[off]=res[i]
                    off+=1
            off=0
            for expr in self.reducedCexprs:
                res=expr.jac(x)
                for i in sorted(res.keys()):
                    evalResult.jac[off]=res[i]
                    off+=1
            return 0
        except RuntimeError:
            return ktr.KN_RC_EVAL_ERR
        
    def optimize_formulation(self,kc):
        self.objReduced=None
        if self.obj is not None:
            self.objReduced=NLPSolver.reduce(kc,self.obj)
            if self.objReduced is not None:
                self.objVars=sorted(self.objReduced.vars())
            else: self.objVars=None
        self.reducedCexprs=[]
        self.reducedIndices=[]
        for i in range(len(self.cexprs)):
            #expr=self.cexprs[i]
            expr=NLPSolver.reduce(kc,self.cexprs[i],i)
            if expr is not None:
                self.reducedCexprs.append(expr)
                self.reducedIndices.append(i)
        #count jacobian
        self.cbjacIndexCons=[]
        self.cbjacIndexVars=[]
        for i,c in enumerate(self.reducedCexprs):
            vars=sorted(c.vars())
            self.cbjacIndexCons+=[self.reducedIndices[i] for v in vars]
            self.cbjacIndexVars+=vars
        
    def optimal(self):
        return self.nStatus is not None and ((self.nStatus<=-100 and self.nStatus>=-199) or self.nStatus>=0)

    @staticmethod
    def reduce(kc,expr,cid=None):
        if isinstance(expr,NLPVariable):
            #linear
            if cid is None:
                ktr.KN_add_obj_linear_struct(kc,[expr.id],[1.0])
            else: ktr.KN_add_con_linear_struct(kc,cid,[expr.id],[1.0])
            #reduced
            return None
        elif isinstance(expr,NLPLinearExpression):
            #constant
            if cid is None:
                ktr.KN_add_obj_constant(kc,expr.const)
            else: ktr.KN_add_con_constants(kc,cid,expr.const)
            #linear
            rows,vals=expr.get_linear()
            if cid is None:
                ktr.KN_add_obj_linear_struct(kc,rows,vals)
            else: ktr.KN_add_con_linear_struct(kc,cid,rows,vals)
            #reduced
            return None
        elif isinstance(expr,NLPQuadraticExpression):
            #constant
            if cid is None:
                ktr.KN_add_obj_constant(kc,expr.const)
            else: ktr.KN_add_con_constants(kc,cid,expr.const)
            #linear
            rows,vals=expr.get_linear()
            if cid is None:
                ktr.KN_add_obj_linear_struct(kc,rows,vals)
            else: ktr.KN_add_con_linear_struct(kc,cid,rows,vals)
            #quadratic
            rows,cols,vals=expr.get_quadratic()
            if cid is None:
                ktr.KN_add_obj_quadratic_struct(kc,rows,cols,vals)
            else: ktr.KN_add_con_quadratic_struct(kc,cid,rows,cols,vals)
            #reduced
            return None
        else: return expr

def solve_example_QP():
    NLP=NLPSolver()
    x0=NLP.add_var()
    x1=NLP.add_var()
    x2=NLP.add_var(lb=-3,ub=2)
    NLP.set_objective_minimize(0.5*(x0**2+x1**2+x2**2)+11*x0+x2)
    NLP.addConstr(-6*x2<=5)
    NLP.optimize()
    print("Solution: ",x0.x,x1.x,x2.x)

def solve_example_QCQP():
    NLP=NLPSolver()
    x0=NLP.add_var(init=2)
    x1=NLP.add_var(init=2)
    x2=NLP.add_var(init=2)
    NLP.set_objective_minimize(1000-x0**2-2*x1**2-x2**2-x0*x1-x0*x2)
    NLP.addConstr(8*x0+14*x1+7*x2==56)
    NLP.addConstr(x0**2+x1**2+x2**2>=25)
    NLP.optimize()
    print("Solution: ",x0.x,x1.x,x2.x)

def solve_example_HS15():
    NLP=NLPSolver()
    x0=NLP.add_var(lb=-INFINITY_NUMBER,init=-2)
    x1=NLP.add_var(lb=-INFINITY_NUMBER,init=1)
    NLP.set_objective_minimize(100*(x1-x0**2)**2+(1-x0)**2)
    NLP.addConstr(x0*x1>=1)
    NLP.addConstr(x0+x1**2>=0)
    NLP.addConstr(x0<=0.5)
    NLP.optimize()
    print("Solution: ",x0.x,x1.x)

def solve_example_barrier():
    NLP=NLPSolver()
    x0=NLP.add_var(lb=-INFINITY_NUMBER,init=1)
    NLP.set_objective_minimize(-op.log_safe(x0)+x0*1000.0)
    NLP.addConstr(x0>=0)
    NLP.optimize()
    print("Solution: ",x0.x)

if __name__=='__main__':
    solve_example_QP()
    solve_example_QCQP()
    solve_example_HS15()
    solve_example_barrier()