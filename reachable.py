from klampt.model.collide import WorldCollider
from scipy.optimize import minimize,least_squares
from planner import Action
from klampt.vis import gldraw
from scene import make_scene_tx90
from BVHTree import draw_bb
from visualizer import *
from NLPUtils import *
from utils import *
import numpy as np
import pyDeclutter,random,os,pickle,time

class Reachable:
    def __init__(self,world,table,robot,robot_path,height,eeLid,eeLocalPos,eeLocalDir,optDOF,additionalCheck=None,margin=0.01):
        self.lid=0
        self.table=table
        self.robot=robot
        self.robot_path=robot_path
        self.height=height
        self.margin=margin
        self.thresMinimizer=0.001
        self.lmt=robot.getJointLimits()
        self.dof=self.robot.getConfig()
        for i in range(self.robot.numLinks()):
            l=self.robot.link(i)
            print("Link: %s Lower: %f Upper: %f!"%(l.getName(),self.lmt[0][i],self.lmt[1][i]))
        self.robot.setConfig(self.dof)
        #end-effector
        self.eeLid=eeLid
        self.eeLocalPos=eeLocalPos
        self.eeLocalDir=eeLocalDir
        self.drawEE=True
        self.drawCell=False
        self.drawText=True
        self.drawBB=False
        #mask-opt-vars
        self.optDOF=optDOF
        if eeLid not in self.optDOF:
            self.optDOF.append(eeLid)
        self.coll=WorldCollider(world)
        self.exclusion_coll=set()
        self.additionalCheck=additionalCheck
        #self.debug_IK_grad()
    
    def range(self,lid):
        return self.lmt[1][lid]-self.lmt[0][lid]
    
    def keyboardfunc(self,c,x,y):
        r=self.range(self.lid)
        slice_edit=10
        if c==b'1':
            self.lid-=1
            if self.lid<0:
                self.lid+=self.robot.numLinks()
            #print('editing link: %d'%self.lid)
        elif c==b'2':
            self.lid+=1
            if self.lid>=self.robot.numLinks():
                self.lid-=self.robot.numLinks()
            #print('editing link: %d'%self.lid)
        elif c==b'3':
            if math.isinf(r):
                return
            self.dof[self.lid]-=r/slice_edit if r>0 else 1/slice_edit
            if r>0:
                self.dof[self.lid]=max(self.dof[self.lid],self.lmt[0][self.lid])
            #print('link%d=%f'%(self.lid,self.dof[self.lid]))
            self.robot.setConfig(self.dof)
        elif c==b'4':
            if math.isinf(r):
                return
            self.dof[self.lid]+=r/slice_edit if r>0 else 1/slice_edit
            if r>0:
                self.dof[self.lid]=min(self.dof[self.lid],self.lmt[1][self.lid])
            #print('link%d=%f'%(self.lid,self.dof[self.lid]))
            self.robot.setConfig(self.dof)
        elif c==b'5':
            self.dof=[0.0]*len(self.lmt[0])
            self.robot.setConfig(self.dof)
        elif c==b'6':
            x,obj=self.solve_IK_random_restart(self.random_target_pos())
        elif c==b'7':
            x,obj=self.solve_IK_random_restart(self.random_target_pos(),[0,0,-1])
        elif c==b'8':
            self.drawEE=not self.drawEE
        elif c==b'9':
            self.drawCell=not self.drawCell
        elif c==b'0':
            self.drawText=not self.drawText
        elif c==b'-':
            self.drawBB=not self.drawBB
        elif c==b'=':
            #we randomly walk on the graph
            milliseconds=int(round(time.time()*1000))
            random.seed(milliseconds)
            gridx=self.grid[random.choice(list(self.grid))]
            assert isinstance(gridx,tuple) and len(gridx)==2
            self.set_opt_DOF(gridx[0])
        elif c==b'v':
            self.action=Action(None,0,0)
            self.action.test_error()
            self.drawEE=True
        elif c=='up':
            if not hasattr(self,"showGridId"):
                self.showGridId=self.grid.keys()[0]
            if (self.showGridId[0]+1,self.showGridId[1]+0) in self.grid:
                self.showGridId=(self.showGridId[0]+1,self.showGridId[1]+0)
            self.set_opt_DOF(self.grid[self.showGridId][0])
        elif c=='down':
            if not hasattr(self,"showGridId"):
                self.showGridId=self.grid.keys()[0]
            if (self.showGridId[0]-1,self.showGridId[1]+0) in self.grid:
                self.showGridId=(self.showGridId[0]-1,self.showGridId[1]+0)
            self.set_opt_DOF(self.grid[self.showGridId][0])
        elif c=='left':
            if not hasattr(self,"showGridId"):
                self.showGridId=self.grid.keys()[0]
            if (self.showGridId[0]+0,self.showGridId[1]+1) in self.grid:
                self.showGridId=(self.showGridId[0]+0,self.showGridId[1]+1)
            self.set_opt_DOF(self.grid[self.showGridId][0])
        elif c=='right':
            if not hasattr(self,"showGridId"):
                self.showGridId=self.grid.keys()[0]
            if (self.showGridId[0]+0,self.showGridId[1]-1) in self.grid:
                self.showGridId=(self.showGridId[0]+0,self.showGridId[1]-1)
            self.set_opt_DOF(self.grid[self.showGridId][0])
    
    def display_screen(self,glProg):
        gldraw.setcolor(TEXT_COLOR[0],TEXT_COLOR[1],TEXT_COLOR[2])
        if self.drawText:
            glProg.draw_text((0,12),"Editing link%d=%f"%(self.lid,self.dof[self.lid]))
            glProg.draw_text((0,24),"DOF=%s"%str(self.dof))
    
    def display(self):
        if self.drawEE:
            gldraw.setcolor(EE_DIRECTION_COLOR[0],EE_DIRECTION_COLOR[1],EE_DIRECTION_COLOR[2])
            x0=se3.apply(self.robot.link(self.eeLid).getTransform(),self.eeLocalPos)
            x1=se3.apply(self.robot.link(self.eeLid).getTransform(),op.add(self.eeLocalPos,self.eeLocalDir))
            v=op.sub(x1,x0)
            gldraw.hermite_curve(x0,v,x1,v)
            
        if self.drawCell and hasattr(self,"grid"):
            margin=0.1
            gldraw.setcolor(REACHABILITY_GRID_COLOR[0],REACHABILITY_GRID_COLOR[1],REACHABILITY_GRID_COLOR[2])
            for id in self.cell:
                gldraw.box([(id[0]+0)*self.res,(id[1]+0)*self.res,self.height],
                           [(id[0]+1)*self.res,(id[1]+1)*self.res,self.height+margin])
            
        if self.drawEE and hasattr(self,"gridBounds"):
            gldraw.setcolor(EE_DIRECTION_COLOR[0],EE_DIRECTION_COLOR[1],EE_DIRECTION_COLOR[2])
            gl.glBegin(gl.GL_LINES)
            for line in self.gridBounds:
                for i in range(len(line.coords.xy[0])):
                    gl.glVertex3f(line.coords.xy[0][i]*self.res,line.coords.xy[1][i]*self.res,self.height)
            gl.glEnd()
            if hasattr(self,"action"):
                gldraw.setcolor(EE_DIRECTION_COLOR[0],EE_DIRECTION_COLOR[1],EE_DIRECTION_COLOR[2])
                gl.glBegin(gl.GL_LINES)
                gl.glVertex3f(self.action.idss[ 0][0]*self.res,self.action.idss[ 0][1]*self.res,self.height)
                gl.glVertex3f(self.action.idss[-1][0]*self.res,self.action.idss[-1][1]*self.res,self.height)
                gl.glEnd()
            else:
                pos=self.global_pos()[1]
                id=op.mul(pos[0:2],1/self.res)
                dir=perp(self.robot.link(self.eeLid).getTransform()[0][0:2])
                f,b=Reachable.range_intersect(self,id,dir)
                if f is not None and b is not None:
                    f*=self.res
                    b*=self.res
                    gl.glBegin(gl.GL_LINES)
                    gl.glVertex3f(pos[0]+dir[0]*f,pos[1]+dir[1]*f,pos[2])
                    gl.glVertex3f(pos[0]-dir[0]*b,pos[1]-dir[1]*b,pos[2])
                    gl.glEnd()
                
        if self.drawBB:
            for lid in range(self.robot.numLinks()):
                bb=get_link_bb(self.robot,lid)
                draw_bb(bb,self.margin)
            
    def idle(self):
        if self.drawCell and hasattr(self,"grid"):
            #we randomly walk on the graph
            milliseconds=int(round(time.time()*1000))
            random.seed(milliseconds)
            gridx=self.grid[random.choice(list(self.grid))]
            assert isinstance(gridx,tuple) and len(gridx)==2
            self.set_opt_DOF(gridx[1])#[0])
    
    def get_DOF(self,x):
        dof=[i for i in self.dof]
        for i,DOFId in enumerate(self.optDOF):
            dof[DOFId]=x[i]
        return dof
    
    def set_opt_DOF(self,x,dt=None):
        for i,DOFId in enumerate(self.optDOF):
            self.dof[DOFId]=x[i]
        dof0=self.robot.getConfig()
        self.robot.setConfig(self.dof)
        if dt is None:
            self.robot.setVelocity([0]*len(self.dof))
        else:
            v=op.mul(op.sub(self.dof,dof0),1/dt)
            self.robot.setVelocity(v)
    
    def get_opt_DOF(self,x):
        return [x[DOFId] for DOFId in self.optDOF]
    
    def sync(self):
        x=self.get_opt_DOF(self.robot.getConfig())
        self.set_opt_DOF(x)
    
    def set_collision(self,links,enable):
        for d in self.optDOF:
            assert d not in links
            for d2 in links:
                if not enable:
                    self.exclusion_coll.add((d,d2))
                    self.exclusion_coll.add((d2,d))
                else:
                    self.exclusion_coll.discard((d,d2))
                    self.exclusion_coll.discard((d2,d))
    
    def global_pos(self,plusDir=False):
        Rt=self.robot.link(self.eeLid).getTransform()
        if plusDir:
            lPos=op.add(self.eeLocalPos,self.eeLocalDir)
        else: lPos=self.eeLocalPos
        return lPos,se3.apply(Rt,lPos)
    
    def global_angle(self):
        #print(self.robot.link(self.eeLid).getName())
        Rt=self.robot.link(self.eeLid).getTransform()
        return atan2_safe(Rt[0][1],Rt[0][0])
    
    def objLS(self,x,eeTargetPos,eeTargetDir=None):
        self.set_opt_DOF(x)
        diff=op.sub(self.global_pos()[1],eeTargetPos)
        if eeTargetDir is None:
            return diff
        
        diff2=op.sub(self.global_pos(True)[1],op.add(eeTargetPos,eeTargetDir))
        if eeTargetDir is not None:
            return diff+diff2
    
    def gradLS(self,x,eeTargetPos,eeTargetDir=None):
        self.set_opt_DOF(x)
        lpos,gpos=self.global_pos()
        diff=op.sub(gpos,eeTargetPos)
        jacEEPos=self.robot.link(self.eeLid).getJacobian(lpos)[3:6]
        if eeTargetDir is None:
            return [[row[i] for i in self.optDOF] for row in jacEEPos]
        
        lpos,gpos=self.global_pos(True)
        diff=op.sub(gpos,op.add(eeTargetPos,eeTargetDir))
        jacEEPos2=self.robot.link(self.eeLid).getJacobian(lpos)[3:6]
        if eeTargetDir is not None:
            return [[row[i] for i in self.optDOF] for row in jacEEPos+jacEEPos2]
    
    def obj(self,x,eeTargetPos,eeTargetDir=None):
        self.set_opt_DOF(x)
        diff=op.sub(self.global_pos()[1],eeTargetPos)
        E=op.dot(diff,diff)/2
        if eeTargetDir is None:
            return E
        
        diff=op.sub(self.global_pos(True)[1],op.add(eeTargetPos,eeTargetDir))
        E+=op.dot(diff,diff)/2
        if eeTargetDir is not None:
            return E
        
    def grad(self,x,eeTargetPos,eeTargetDir=None):
        self.set_opt_DOF(x)
        lpos,gpos=self.global_pos()
        diff=op.sub(gpos,eeTargetPos)
        jacEEPos=self.robot.link(self.eeLid).getJacobian(lpos)[3:6]
        G=op.mul(jacEEPos[0],diff[0])
        G=op.add(G,op.mul(jacEEPos[1],diff[1]))
        G=op.add(G,op.mul(jacEEPos[2],diff[2]))
        if eeTargetDir is None:
            return [G[i] for i in self.optDOF]
        
        lpos,gpos=self.global_pos(True)
        diff=op.sub(gpos,op.add(eeTargetPos,eeTargetDir))
        jacEEPos=self.robot.link(self.eeLid).getJacobian(lpos)[3:6]
        G=op.add(G,op.mul(jacEEPos[0],diff[0]))
        G=op.add(G,op.mul(jacEEPos[1],diff[1]))
        G=op.add(G,op.mul(jacEEPos[2],diff[2]))
        if eeTargetDir is not None:
            return [G[i] for i in self.optDOF]
    
    def debug_IK_grad(self,nrTrial=10):
        DELTA=1e-6
        eeTargetPos=[random.uniform(-1,1) for i in range(3)]
        eeTargetDir=[random.uniform(-1,1) for i in range(3)]
        for i in range(nrTrial):
            x=self.random_opt_DOF()
            dx=self.random_opt_DOF()
            
            obj=self.obj(x,eeTargetPos,eeTargetDir)
            grad=self.grad(x,eeTargetPos,eeTargetDir)
            obj2=self.obj(op.add(x,op.mul(dx,DELTA)),eeTargetPos,eeTargetDir)
            gradXRef=op.dot(grad,dx)
            gradXNumeric=(obj2-obj)/DELTA
            print("gradXRef=%f gradXErr=%f"%(gradXRef,gradXRef-gradXNumeric))
    
            obj=self.objLS(x,eeTargetPos,eeTargetDir)
            grad=self.gradLS(x,eeTargetPos,eeTargetDir)
            obj2=self.objLS(op.add(x,op.mul(dx,DELTA)),eeTargetPos,eeTargetDir)        
            gradXRef=op.matvecmul(grad,dx)
            gradXNumeric=op.mul(op.sub(obj2,obj),1/DELTA)
            print("gradXLSRef=%f gradXLSErr=%f"%(op.norm(gradXRef),op.norm(op.sub(gradXRef,gradXNumeric))))
    
    def solve_IK(self,x,eeTargetPos,eeTargetDir=None,maxDelta=None,noEELid=False):
        if maxDelta is None:
            bd=[(self.lmt[0][i],self.lmt[1][i]) for i in self.optDOF]
        else:
            bd=[(x[id]-maxDelta,x[id]+maxDelta) for id,i in enumerate(self.optDOF)]
        if noEELid:
            eeId=self.optDOF.index(self.eeLid)
            bd[eeId]=(x[eeId]-1e-3,x[eeId]+1e-3)
        
        #opt
        USE_LS=True
        if USE_LS:
            def objIK(x):
                return np.array(self.objLS(x.tolist(),eeTargetPos,eeTargetDir))
            def gradIK(x):
                return np.array(self.gradLS(x.tolist(),eeTargetPos,eeTargetDir))
            res=least_squares(x0=np.array(x),fun=objIK,jac=gradIK,bounds=([bdi[0] for bdi in bd],[bdi[1] for bdi in bd]),gtol=0,xtol=0)
            return res.x.tolist(),self.obj(res.x.tolist(),eeTargetPos,eeTargetDir)
        else:
            def objIK(x):
                return self.obj(x.tolist(),eeTargetPos,eeTargetDir)
            def gradIK(x):
                return np.array(self.grad(x.tolist(),eeTargetPos,eeTargetDir))
            res=minimize(x0=np.array(x),fun=objIK,jac=gradIK,method="L-BFGS-B",bounds=bd,gtol=0,xtol=0)
            return res.x.tolist(),objIK(res.x)
    
    def solve_IK_random(self,eeTargetPos,eeTargetDir=None):
        return self.solve_IK(self.random_opt_DOF(),eeTargetPos,eeTargetDir)
    
    def solve_IK_random_restart(self,eeTargetPos,eeTargetDir=None,nrTrial=100):
        x=None
        obj=None
        for i in range(nrTrial):
            x,obj=self.solve_IK_random(eeTargetPos,eeTargetDir)
            if self.is_valid(x,obj):
                print("Successful!")
                return x,obj
        return x,obj
    
    def is_valid(self,x,obj,objColl=True):
        if obj is not None and obj>self.thresMinimizer:
            return False
        if self.additionalCheck is not None:
            if not self.additionalCheck(self,x):
                return False
        for c in self.coll.robotSelfCollisions(self.robot):
            if (c[0].index,c[1].index) not in self.exclusion_coll:
                return False
        if objColl:
            colls=[c for c in self.coll.robotObjectCollisions(self.robot)]
            if len(colls)>0:
                return False
        return True
    
    def random_opt_DOF(self,range=1):
        return [random.uniform(self.lmt[0][i],self.lmt[1][i])*range for i in self.optDOF]
    
    def random_target_pos(self,range=0.5):
        bbt=get_object_bb(self.table)
        return [random.uniform(bbt[0][0],bbt[1][0])*range,random.uniform(bbt[0][1],bbt[1][1])*range,self.height]
    
    def compute_height_touchdown(self,x):
        self.set_opt_DOF(x)
        bb=get_robot_bb(self.robot,link0=self.eeLid)
        self.height_touchdown=bb[0][2]
        if self.height_touchdown<=0:
            print("Height too small!")
            assert False
        self.height_touchdown=self.height-self.height_touchdown+self.margin
    
    def get_save_path_grid(self,suffix):
        import hashlib
        data=open(self.robot_path,'r').read()
        md5=hashlib.md5(data).hexdigest()
        return DATA_PATH+"/data/"+self.robot.getName()+"_ReachableGrasp_"+md5+suffix+".dat"
    
    def corner_cell(self,id,cells):
        assert id not in cells
        dir=[(-1,-1),(1,-1),(-1,1),(1,1)]
        for d in dir:
            d10In=tuple(op.add(id,(d[0],  0 ))) in cells
            d01In=tuple(op.add(id,(  0 ,d[1]))) in cells
            d11In=tuple(op.add(id,(d[0],d[1]))) in cells
            if (not d10In) and (not d01In) and d11In:
                #print("Rejected corner case!")
                return True
        return False
    
    def build_grid(self,res,nrTrial=100,suffix="",onlyHeight=False):
        #load
        savePath=self.get_save_path_grid(suffix)
        print(savePath)
        if os.path.exists(savePath):
            file=open(savePath,"rb")
            self.dof,self.res,self.grid,self.cell=pickle.load(file)
        else:
            #build grid
            self.res=res
            self.grid=None
            self.cell=None
            nrFail=0
            while nrFail<nrTrial:
                x,obj=self.solve_IK_random(self.random_target_pos(),[0,0,-1])
                if not self.is_valid(x,obj):
                    continue
                if not self.add_grid(x,onlyHeight):
                    nrFail+=1
                else: nrFail=0
                if onlyHeight and hasattr(self,"height_touchdown"):
                    return
            #save
            print("Saving reachable grid!")
            assert self.grid is not None
            file=open(savePath,"wb")
            pickle.dump((self.dof,self.res,self.grid,self.cell),file)
        #construct shapely bounds
        self.build_grid_bounds()
            
    def grid_id(self,x):
        if isinstance(x,tuple) or isinstance(x,list):
            assert len(x)==2
            return (self.grid_id(x[0]),self.grid_id(x[1]))
        else: return int(op.floor_safe(x/self.res))
                
    def x_in_grid(self,x):
        self.set_opt_DOF(x)
        id=self.grid_id(self.global_pos()[1][0:2])
        return id in self.cell
    
    def id_in_grid(self,id):
        id=(op.floor_safe(id[0]),op.floor_safe(id[1]))
        return id in self.cell
           
    def new_grid(self,initx,interval=20,noEELid=True,onlyHeight=False):
        grid={}
        visited={}
        cells=[]
        isInit=True
        maxDelta=1.0
        self.set_opt_DOF(initx)
        stack=[(self.grid_id(self.global_pos()[1][0:2]),(initx,initx))]
        #print(stack)
        while len(stack)>0:
            id,initx01=stack[-1]
            stack.pop()
            if id in visited:
                continue
            if self.corner_cell(id,cells):
                continue
            visited[id]=True
            
            #solve IK for each corner point
            cell={}
            succ=True
            for i in range(4):
                idi=[id[0],id[1]]
                gpos=[id[0]*self.res,id[1]*self.res,self.height]
                if (i&1)>0:
                    idi[0]+=1
                    gpos[0]+=self.res
                if (i&2)>0:
                    idi[1]+=1
                    gpos[1]+=self.res
                #this corner point has been solved for
                idi=tuple(idi)
                if idi in grid:
                    for d in range(2):
                        if op.norm(op.sub(grid[idi][d],initx01[d]))>maxDelta*len(self.optDOF):
                            succ=False
                    continue
                #try solve
                xi,obji=self.solve_IK(initx01[0],gpos,[0,0,-1],maxDelta,noEELid)
                if not self.is_valid(xi,obji):
                    succ=False
                #compute touchdown distance
                if not hasattr(self,"height_touchdown"):
                    self.compute_height_touchdown(xi)
                    if onlyHeight:
                        return None,None
                #try reach down
                gpos[2]=self.height_touchdown
                xj,objj=self.solve_IK(initx01[1],gpos,[0,0,-1],None if isInit else maxDelta,noEELid)
                #if not self.is_valid(xj,objj):
                #    succ=False
                if objj>self.thresMinimizer:
                    succ=False
                #check succ
                if succ==False:
                    break
                else: cell[idi]=(xi,xj)
                
            #expand grid cells if succ
            if succ==True:
                cells.append(id)
                grid.update(cell)
                stack.append(((id[0]-1,id[1]),grid[(id[0]  ,id[1]  )]))
                stack.append(((id[0]+1,id[1]),grid[(id[0]+1,id[1]  )]))
                stack.append(((id[0],id[1]-1),grid[(id[0]  ,id[1]  )]))
                stack.append(((id[0],id[1]+1),grid[(id[0]  ,id[1]+1)]))
                if len(cells)%20==0:
                    print("Created %d cells!"%len(cells))
                isInit=False
        if len(cells)>0:
            print("Added grid with %d cells!"%len(cells))
            return grid,cells
        else: return None,None
                
    def add_grid(self,initx,onlyHeight=False):
        #do not build if already in grid
        if self.grid is not None:
            if self.x_in_grid(initx):
                return False
        #build
        grid,cell=self.new_grid(initx,onlyHeight=onlyHeight)
        if grid is None:
            return False
        elif self.grid is None:
            self.grid=grid
            self.cell=cell
            self.check_grid_all()
            return True
        elif len(self.grid)<len(grid):
            self.grid=grid
            self.cell=cell
            self.check_grid_all()
            return True
        return False

    def build_grid_bounds(self):
        import shapely.geometry as geom
        
        #edges
        edges=set()
        for id in self.cell:
            if (id[0],id[1]-1) not in self.cell:
                edges.add(((id[0],id[1]),(id[0]+1,id[1])))
            if (id[0]+1,id[1]) not in self.cell:
                edges.add(((id[0]+1,id[1]),(id[0]+1,id[1]+1)))
            if (id[0],id[1]+1) not in self.cell:
                edges.add(((id[0]+1,id[1]+1),(id[0],id[1]+1)))
            if (id[0]-1,id[1]) not in self.cell:
                edges.add(((id[0],id[1]+1),(id[0],id[1])))
        
        #create geometry
        self.gridBounds=geom.MultiLineString(list(edges))

    def range_intersect(self,id,dir):
        if isinstance(id,list) and isinstance(dir,list):
            return [Reachable.range_intersect(self,idi,diri) for idi,diri in zip(id,dir)]
        elif not self.id_in_grid(id):
            return 0.0,0.0
        else:
            import shapely.geometry as geom
            dir=op.mul(dir,1/op.norm(dir))
            extent=op.mul(dir,float(len(self.cell)))
            
            forward=self.gridBounds.intersection(geom.LineString([id,op.add(id,extent)]))
            if isinstance(forward,geom.Point):
                geoms=[forward]
            else: geoms=forward
            forward=INFINITY_NUMBER
            for l in geoms:
                if len(l.coords.xy)==0:
                    forward=None
                    break
                pt=(l.coords.xy[0][0],l.coords.xy[1][0])
                dist=op.dot(op.sub(pt,id),dir)
                if dist<forward:
                    forward=dist
                
            backward=self.gridBounds.intersection(geom.LineString([id,op.sub(id,extent)]))
            if isinstance(backward,geom.Point):
                geoms=[backward]
            else: geoms=backward
            backward=INFINITY_NUMBER
            for l in geoms:
                if len(l.coords.xy)==0:
                    backward=None
                    break
                x=(l.coords.xy[0][0],l.coords.xy[1][0])
                dist=op.dot(op.sub(id,x),dir)
                if dist<backward:
                    backward=dist
            
            if forward is None or backward is None:
                return 0.0,0.0
            else: return forward,backward
    
    def height_ratio(self,hObj):
        heightAll=0.0
        numberCounted=0.0
        tmpDOF=self.robot.getConfig()
        for k,v in self.grid.items():
            self.set_opt_DOF(v[0])
            high=get_robot_bb(self.robot,self.eeLid)[0][2]
            self.set_opt_DOF(v[1])
            low=get_robot_bb(self.robot,self.eeLid)[0][2]
            height=(high-hObj)/(high-low)
            heightAll+=height
            numberCounted+=1
            break
        self.robot.setConfig(tmpDOF)
        heightMean=heightAll/numberCounted
        return heightMean
    
    def get_pusher(self):
        return self
    
    def get_gripper(self):
        return self
    
    def check_grid(self,id,noEELid=True):
        nrDOF=len(self.optDOF)-1 if noEELid else len(self.optDOF)
        diffmean=0.0
        diffmax=0.0
        cmax=None
        for c in self.cell:
            diff0=op.norm(op.sub(self.grid[(c[0]+0,c[1]+0)][id],self.grid[(c[0]+1,c[1]+0)][id])[:nrDOF])
            diff1=op.norm(op.sub(self.grid[(c[0]+0,c[1]+0)][id],self.grid[(c[0]+0,c[1]+1)][id])[:nrDOF])
            diff2=op.norm(op.sub(self.grid[(c[0]+1,c[1]+1)][id],self.grid[(c[0]+1,c[1]+0)][id])[:nrDOF])
            diff3=op.norm(op.sub(self.grid[(c[0]+1,c[1]+1)][id],self.grid[(c[0]+0,c[1]+1)][id])[:nrDOF])
            diffmean+=diff0+diff1+diff2+diff3
            diffmaxc=max(max(diff0,diff1),max(diff2,diff3))
            if diffmaxc>diffmax:
                diffmax=diffmaxc
                cmax=c
        diffmean/=len(self.cell)*4
        #print(op.sub(self.grid[(cmax[0]+0,cmax[1]+0)][id],self.grid[(cmax[0]+1,cmax[1]+0)][id])[:nrDOF])
        #print(op.sub(self.grid[(cmax[0]+0,cmax[1]+0)][id],self.grid[(cmax[0]+0,cmax[1]+1)][id])[:nrDOF])
        #print(op.sub(self.grid[(cmax[0]+1,cmax[1]+1)][id],self.grid[(cmax[0]+1,cmax[1]+0)][id])[:nrDOF])
        #print(op.sub(self.grid[(cmax[0]+1,cmax[1]+1)][id],self.grid[(cmax[0]+0,cmax[1]+1)][id])[:nrDOF])
        print("Grid-v%d: maxDiff=%f meanDiff=%f"%(id,diffmax,diffmean))
        self.showGridId=cmax
        return cmax
        
    def check_grid_all(self):
        self.check_grid(0)
        self.check_grid(1)
    
    def check_grid_idle(self,id):
        if not hasattr(self,"parity"):
            self.parity=0
        cid=self.check_grid(0)
        if self.parity==0:
            self.set_opt_DOF(self.grid[(cid[0]+0,cid[1]+0)][id])
        if self.parity==1:
            self.set_opt_DOF(self.grid[(cid[0]+1,cid[1]+0)][id])
        if self.parity==2:
            self.set_opt_DOF(self.grid[(cid[0]+1,cid[1]+1)][id])
        if self.parity==3:
            self.set_opt_DOF(self.grid[(cid[0]+0,cid[1]+1)][id])
        self.parity=(self.parity+1)%4
    
        diff0=op.norm(op.sub(self.grid[(cid[0]+0,cid[1]+0)][id],self.grid[(cid[0]+1,cid[1]+0)][id]))
        diff1=op.norm(op.sub(self.grid[(cid[0]+1,cid[1]+0)][id],self.grid[(cid[0]+1,cid[1]+1)][id]))
        diff2=op.norm(op.sub(self.grid[(cid[0]+1,cid[1]+1)][id],self.grid[(cid[0]+0,cid[1]+1)][id]))
        diff3=op.norm(op.sub(self.grid[(cid[0]+0,cid[1]+1)][id],self.grid[(cid[0]+0,cid[1]+0)][id]))
        print("Check grid: diff0=%f diff1=%f diff2=%f diff3=%f!"%(diff0,diff1,diff2,diff3))
    
class ReachableFast(Reachable):
    def __init__(self,world,table,robot,robot_path,height,eeLid,eeLocalPos,eeLocalDir,optDOF,additionalCheck=None,margin=0.01):
        Reachable.__init__(self,world,table,robot,robot_path,height,eeLid,eeLocalPos,eeLocalDir,optDOF,additionalCheck,margin)
        
    def build_grid_bounds(self):
        Reachable.build_grid_bounds(self)
        iss=[pyDeclutter.Vec2i(id[0],id[1]) for id in self.cell]
        self.gridBoundsFast=pyDeclutter.Environment2D(iss)
    
    def range_intersect(self,id,dir):
        if isinstance(id,list) and isinstance(dir,list):
            lss=[]
            for idi,diri in zip(id,dir):
                diri=op.mul(diri,INFINITY_NUMBER/op.norm(diri))
                lss.append(pyDeclutter.LineSeg2D(pyDeclutter.Vec2(idi[0],idi[1]),pyDeclutter.Vec2(idi[0]+diri[0],idi[1]+diri[1])))
            ab=self.gridBoundsFast.rangeBatched(lss,False)
            return [(abi[0],abi[2]) for abi in ab]
        else:
            dir=op.mul(dir,INFINITY_NUMBER/op.norm(dir))
            l=pyDeclutter.LineSeg2D(pyDeclutter.Vec2(id[0],id[1]),pyDeclutter.Vec2(id[0]+dir[0],id[1]+dir[1]))
            ab=self.gridBoundsFast.range(l,False,-2)
            return ab[0],ab[2]
    
    def debug_range_intersect(self,d=100,N=100):
        Reachable.build_grid_bounds(self)
        pss=[(random.uniform(-d,d),random.uniform(-d,d)) for i in range(N)]
        dss=[(random.uniform(-d,d),random.uniform(-d,d)) for i in range(N)]
        a=Reachable.range_intersect(self,pss,dss)
        b=self.range_intersect(pss,dss)
        err=0
        for ai,bi in zip(a,b):
            if abs(ai[0])+abs(ai[1])>0 or abs(bi[0])+abs(bi[1])>0:
                err+=abs(ai[0]-bi[0])
                err+=abs(ai[1]-bi[1])
        print("err: ",err)
    
class ReachableTx90Robotiq(ReachableFast):
    def __init__(self,world,table,robot,robot_path,height):
        def additionalCheck(reachable,x):
            reachable.set_opt_DOF(x)
            bb1=get_link_bb_ctr(reachable.robot,1)
            bb2=get_link_bb_ctr(reachable.robot,2)
            bb3=get_link_bb_ctr(reachable.robot,3)
            return bb2[2]>bb1[2] and bb3[2]>bb1[2]
        Reachable.__init__(self,world,table,robot,robot_path,height,6,[0,0,0],[0,0,1],[1,2,3,4,5],additionalCheck=additionalCheck)
    
class GLVisualizerReachable(GLVisualizer):
    def __init__(self,world,table,robot,reachable):
        GLVisualizer.__init__(self,world,table,robot,None)
        self.reachable=reachable
        
    def keyboardfunc(self,c,x,y):
        GLVisualizer.keyboardfunc(self,c,x,y)
        self.reachable.keyboardfunc(c,x,y)
        
    def display_screen(self):
        GLVisualizer.display_screen(self)
        self.reachable.display_screen(self)
        
    def display(self):
        GLVisualizer.display(self)
        self.reachable.display()
        
    def idle(self):
        GLVisualizer.idle(self)
        self.reachable.idle()    
        
if __name__=='__main__':
    world,table,robot,robot_path=make_scene_tx90()
    reachable=ReachableTx90Robotiq(world,table,robot,robot_path,0.3)
    reachable.build_grid(0.025)
    reachable.check_grid_all()
    reachable.debug_range_intersect()
    GLVisualizerReachable(world,table,robot,reachable).run()
