from klampt import SimBody
from utils import *

class ObjectCarry:
    def __init__(self,grasp,link):
        self.id0=None
        self.t0=None
        self.tO0=None
        self.id1=None
        self.t1=None
        self.tO1=None
        self.grasp=grasp
        self.link=link
        self.sim=None
        
    def set_obj_pose(self,tR,isRelease=False):
        R,t=se3.mul(tR,se3.mul(se3.inv(self.t0),self.tO0))
        if isRelease:
            #respect the fact that objects are moving on a table so apply only 2D transformation
            t=[t[0],t[1],self.tO0[1][2]]
        self.grasp.setTransform(R,t)
        if self.sim is not None:
            sbody=self.sim.body(self.grasp)
            sbody.setTransform(R,t)
            sbody.setVelocity([0,0,0],[0,0,0])
            sbody.enableDynamics(False)
            sbody.enable(False)
        
    def release(self):
        if self.tO1 is not None:
            self.set_obj_pose(self.tO1,True)
        else: self.set_obj_pose(self.t1,True)
        if self.sim is not None:
            sbody=self.sim.body(self.grasp)
            sbody.enableDynamics(True)
            sbody.enable(True)
    
    def update(self,i,stage,cond=False):
        if self.grasp is None:
            return 0
        elif stage==2:
            return 2
        elif stage==1:
            if i>=self.id1 or cond:
                self.release()
                return 2
            else: 
                self.set_obj_pose(self.link.getTransform())
                return 1
        else:
            assert stage==0
            if i>=self.id0 or cond:
                self.set_obj_pose(self.link.getTransform())
                return 1
            else: return 0
    
    def robot(self):
        return self.link.robot()
    
    def robot_bb(self):
        return get_robot_bb(self.link.robot(),self.link.index)
    
    def grasp_bb(self):
        return get_object_bb(self.grasp)