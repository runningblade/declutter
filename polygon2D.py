from visualizer import GLVisualizer
from klampt.vis import gldraw
from utils import *
from scene import *
import gmpy2,random

def draw_elem(elem,margin,res=5):
    gl.glBegin(gl.GL_LINES)
    if len(elem)==2:
        a=elem[0]
        b=elem[1]
        gl.glVertex3f(a[0],a[1],margin)
        gl.glVertex3f(b[0],b[1],margin)
    else:
        assert len(elem)==4
        ctr=elem[0]
        rad=elem[1]
        theta1=elem[2]
        theta2=elem[3]
        nrSeg=int(op.ceil_safe((theta2-theta1)*180/math.pi/res))
        for i in range(nrSeg):
            ang1=interp_1d(theta1,theta2,float(i+0)/nrSeg)
            ang2=interp_1d(theta1,theta2,float(i+1)/nrSeg)
            a=(op.cos_safe(ang1)*rad,op.sin_safe(ang1)*rad)
            b=(op.cos_safe(ang2)*rad,op.sin_safe(ang2)*rad)
            gl.glVertex3f(a[0]+ctr[0],a[1]+ctr[1],margin)
            gl.glVertex3f(b[0]+ctr[0],b[1]+ctr[1],margin)
    gl.glEnd()

class Polygon2D:
    def __init__(self,vss,colinearThres=0.0):
        import shapely.geometry as geom
        if isinstance(vss,geom.Polygon):
            self.hull=vss.convex_hull
        else:
            self.hull=geom.MultiPoint(vss).convex_hull
        #vss
        vhull=self.hull.exterior.coords.xy
        vhull=list(zip(vhull[0].tolist(),vhull[1].tolist()))
        vhull.pop()
        self.vss=vhull[::-1]
        assert self.is_convex()
        
    def is_convex(self,eps=0.0):
        for i in range(len(self.vss)):
            a=self.edge(i-1)
            b=self.edge(i)
            if wedge(a,b)<eps:
                return False
        return True
    
    def vertex(self,i):
        while i<0:
            i+=len(self.vss)
        while i>=len(self.vss):
            i-=len(self.vss)
        return self.vss[i]
    
    def edge(self,i):
        return op.sub(self.vertex(i+1),self.vertex(i))
    
    def longest_edge(self):
        return max([op.norm(self.edge(i)) for i in range(len(self.vss))])
    
    def shortest_edge(self):
        return min([op.norm(self.edge(i)) for i in range(len(self.vss))])
    
    def dist_to(self,pt,qtype=gmpy2.mpq,ftype=float):
        pt=(qtype(pt[0]),qtype(pt[1]))
        zero=qtype(0)
        one=qtype(1)
        closest=None
        distSqr=None
        dndx=None
        #vertex
        for i in range(len(self.vss)):
            c=self.vertex(i)
            c=[qtype(c[0]),qtype(c[1])]
            pt2c=op.sub(pt,c)
            distSqrI=op.dot(pt2c,pt2c)
            if distSqr is None or distSqrI<distSqr:
                closest=c
                distSqr=distSqrI
                dndx=[[one,zero],[zero,one]]
                distNormSqr=op.dot(pt2c,pt2c)
                dndx[0]=op.sub(dndx[0],op.mul(pt2c,pt2c[0]/distNormSqr))
                dndx[1]=op.sub(dndx[1],op.mul(pt2c,pt2c[1]/distNormSqr))
                side=0 if (pt2c[0]==zero and pt2c[1]==zero) else 1
        #facet
        for i in range(len(self.vss)):
            a=self.vertex(i)
            b=self.vertex(i+1)
            a=[qtype(a[0]),qtype(a[1])]
            b=[qtype(b[0]),qtype(b[1])]
            dir=op.sub(b,a)
            pt2a=op.sub(pt,a)
            pt2b=op.sub(pt,b)
            dirNormSqr=op.dot(dir,dir)
            dist=op.dot(pt2a,dir)
            if zero<dist and dist<dirNormSqr:
                c=interp_1d(a,b,dist/dirNormSqr)
                pt2c=op.sub(pt,c)
                distSqrI=op.dot(pt2c,pt2c)
                if distSqr is None or distSqrI<distSqr:
                    closest=c
                    distSqr=distSqrI
                    dndx=[[zero,zero],[zero,zero]]
                    side=op.dot(perp(dir),pt2c)
                    side=0 if side==zero else (-1 if side<zero else 1)
        #reconstruct multiple stuff
        pt2c=op.sub(pt,closest)
        distSqr=op.dot(pt2c,pt2c)
        pt2c,dndx=Polygon2D.safe_normalize(pt2c,dndx,zero)
        dist=op.sqrt_safe(distSqr)
        if side<0:
            dist*=-1
            pt2c=op.mul(pt2c,-1)
        return  ftype(dist),                            \
                [ftype(closest[0]),ftype(closest[1])],  \
                [ftype(pt2c[0]),ftype(pt2c[1])],        \
                [[ftype(dndx[0][0]),ftype(dndx[0][1])], \
                 [ftype(dndx[1][0]),ftype(dndx[1][1])]],\
                side
    
    def minkowskiSumCircle(self,rad):
        bds=[]
        for i in range(len(self.vss)):
            a=self.edge(i-1)
            b=self.edge(i)
            ap=perp(a)
            bp=perp(b)
            #edge
            ea=op.add(self.vertex(i-1),op.mul(ap,rad/op.norm(ap)))
            eb=op.add(self.vertex(i)  ,op.mul(ap,rad/op.norm(ap)))
            bds.append((ea,eb))
            #arc
            arc=(self.vertex(i),rad,atan2_zero2pi(ap[1],ap[0]),atan2_zero2pi(bp[1],bp[0]))
            if arc[3]<arc[2]:
                arc=(arc[0],arc[1],arc[2],arc[3]+math.pi*2)
            bds.append(arc)
        return bds
        
    def intersect_ray(self,a,b):
        import shapely.geometry as geom
        line=geom.LineString([a,b])
        inter=self.hull.intersection(line)
        return inter.is_empty
    
    def intersect_ray_vert(self,a,b):
        import shapely.geometry as geom
        line=geom.LineString([a,b])
        inter=self.hull.intersection(line)
        if inter.is_empty:
            return None
        else: 
            coords=inter.coords.xy
            return zip(coords[0].tolist(),coords[1].tolist())
        
    def intersect_ray_alpha(self,a,b):
        import shapely.geometry as geom
        line=geom.LineString([a,b])
        inter=self.hull.intersection(line)
        if inter.is_empty:
            return None
        else: 
            coords=inter.coords.xy
            verts=zip(coords[0].tolist(),coords[1].tolist())
            return [op.norm(op.sub(v,a)) for v in verts]
        
    def intersect_bb(self,bb):
        return self.intersect_poly(Polygon2D([(bb[0][0],bb[0][1]),(bb[1][0],bb[0][1]),(bb[1][0],bb[1][1]),(bb[0][0],bb[1][1])]))
    
    def intersect_poly(self,poly):
        return self.hull.intersects(poly.hull)
    
    def intersect(self,other):
        if isinstance(other,tuple):
            return self.intersect_bb(other)
        elif isinstance(other,Polygon2D):
            return self.intersect_poly(other)
        else: assert False
        
    def compute_bb(self):
        self.bb=empty_bb(2)
        for v in self.vss:
            self.bb=union_bb(self.bb,list(v))
        return self.bb
            
    def diameter(self):
        ret=0
        for i in self.vss:
            for j in self.vss:
                ret=max(op.norm(op.sub(i,j)),ret)
        return ret
            
    def draw(self,margin=0.001,fill=False,raw=False):
        if fill:
            #center
            c=[0.0,0.0]
            for v in self.vss:
                c=op.add(c,v)
            c=op.mul(c,1.0/len(self.vss))
            #solve triangle
            if not raw:
                gl.glBegin(gl.GL_TRIANGLES)
            for i in range(len(self.vss)):
                a=self.vertex(i)
                b=self.vertex(i+1)
                gl.glVertex3f(c[0],c[1],margin)
                gl.glVertex3f(a[0],a[1],margin)
                gl.glVertex3f(b[0],b[1],margin)
            if not raw:
                gl.glEnd()
        else:
            if not raw:
                gl.glBegin(gl.GL_LINES)
            for i in range(len(self.vss)):
                a=self.vertex(i)
                b=self.vertex(i+1)
                gl.glVertex3f(a[0],a[1],margin)
                gl.glVertex3f(b[0],b[1],margin)
            if not raw:
                gl.glEnd()
        
    def debug_gradient(self):
        bb=self.compute_bb()
        bb=expand_bb(bb,max(op.sub(bb[1],bb[0])))
        x00=(bb[0][0],bb[0][1])
        x10=(bb[1][0],bb[0][1])
        x11=(bb[1][0],bb[1][1])
        x01=(bb[0][0],bb[1][1])
        
        DELTA=1e-6
        dp=(random.uniform(-1,1),random.uniform(-1,1))
        p=interp_2d(x00,x10,x11,x01,(random.uniform(0,1),random.uniform(0,1)))
        d1,_,n ,dndx,_=self.dist_to(p)
        d2,_,n2,_   ,_=self.dist_to(op.add(p,op.mul(dp,DELTA)))
        dndd=op.add(op.mul(dndx[0],dp[0]),op.mul(dndx[1],dp[1]))
        print("d ",op.dot(n,dp),(d2-d1)/DELTA-op.dot(n,dp))
        print("n0",dndd[0],(n2[0]-n[0])/DELTA-dndd[0])
        print("n1",dndd[1],(n2[1]-n[1])/DELTA-dndd[1])
        
    def transform(self,pos,theta):
        cosTheta=op.cos_safe(theta)
        sinTheta=op.sin_safe(theta)
        import shapely.affinity as affinity
        affine=[cosTheta,-sinTheta,sinTheta,cosTheta,pos[0],pos[1]]
        poly=Polygon2D(affinity.affine_transform(self.hull,affine))
        poly.pos=pos
        poly.theta=theta
        return poly
        
    @staticmethod
    def safe_normalize(pt,dndx,zero):
        if pt[0]!=zero or pt[1]!=zero:
            #well-condition
            coef=1/(abs(pt[0])+abs(pt[1]))
            pt=op.mul(pt,coef)
            dndx=[op.mul(dndx[0],coef),op.mul(dndx[1],coef)]
            #multiply
            coef=1/op.norm(pt)
            pt=op.mul(pt,coef)
            dndx=[op.mul(dndx[0],coef),op.mul(dndx[1],coef)]
        return pt,dndx
    
    @staticmethod
    def randomize(np):
        vss=[]
        for i in range(np):
            vss.append((random.uniform(-1,1),random.uniform(-1,1)))
        return Polygon2D(compute_convex_2D(vss))
    
class Polygon2DGLVisualizer(GLVisualizer):
    def __init__(self,polygon,world,table,robot,rad):
        GLVisualizer.__init__(self,world,table,robot,None)
        self.msum=polygon.minkowskiSumCircle(rad)
        self.polygon=polygon
        self.drawElem=True
    
    def keyboardfunc(self,c,x,y):
        GLVisualizer.keyboardfunc(self,c,x,y)
        if c==b'1':
            self.drawElem=not self.drawElem
        elif c==b'2':
            self.shape_a=Polygon2D.randomize(5)
            self.shape_b=Polygon2D.randomize(5)
            self.vert_a=(random.uniform(-1,1),random.uniform(-1,1))
            self.vert_b=(random.uniform(-1,1),random.uniform(-1,1))
    
    def keyboardupfunc(self,c,x,y):
        GLVisualizer.keyboardupfunc(self,c,x,y)
        
    def display(self):
        margin=0.001
        gldraw.setcolor(OBJECT_COLOR[0],OBJECT_COLOR[1],OBJECT_COLOR[2])
        self.polygon.draw(margin)
        
        #draw shape
        if hasattr(self,"shape_a") and hasattr(self,"shape_b"):
            inter=self.shape_a.intersect(self.shape_b)
            if inter:
                gldraw.setcolor(SPECIAL_OBJECT_COLOR[0],SPECIAL_OBJECT_COLOR[1],SPECIAL_OBJECT_COLOR[2])
            else: gldraw.setcolor(OBJECT_COLOR[0],OBJECT_COLOR[1],OBJECT_COLOR[2])
            self.shape_a.draw(margin)
            self.shape_b.draw(margin)
            
            verts=self.shape_a.intersect_ray_vert(self.vert_a,self.vert_b)
            if verts is not None:
                gldraw.setcolor(SPECIAL_OBJECT_COLOR[0],SPECIAL_OBJECT_COLOR[1],SPECIAL_OBJECT_COLOR[2])
            else: gldraw.setcolor(OBJECT_COLOR[0],OBJECT_COLOR[1],OBJECT_COLOR[2])
            gl.glBegin(gl.GL_LINES)
            gl.glVertex3f(self.vert_a[0],self.vert_a[1],margin)
            gl.glVertex3f(self.vert_b[0],self.vert_b[1],margin)
            gl.glEnd()
            
            if verts is not None:
                gl.glPointSize(CONTACT_PATCH_POINT_SIZE)
                gl.glBegin(gl.GL_POINTS)
                for v in verts:
                    gl.glVertex3f(v[0],v[1],margin)
                gl.glEnd()
        else:
            #draw minkowski sum
            if self.drawElem:
                for elem in self.msum:
                    draw_elem(elem,margin)
            #draw dist
            res=20
            bb=self.polygon.compute_bb()
            bb=expand_bb_eps(bb,1)
            gl.glBegin(gl.GL_LINES)
            for i in range(res):
                for j in range(res):
                    pos00=(bb[0][0],bb[0][1])
                    pos10=(bb[1][0],bb[0][1])
                    pos11=(bb[1][0],bb[1][1])
                    pos01=(bb[0][0],bb[1][1])
                    p=interp_2d(pos00,pos10,pos11,pos01,(float(i)/(res-1),float(j)/(res-1)))
                    d,c,n,dndx,s=self.polygon.dist_to(p)
                    if s>0: gldraw.setcolor(COST_DISTANCE_COLOR[0],COST_DISTANCE_COLOR[1],COST_DISTANCE_COLOR[2])
                    else: gldraw.setcolor(1-COST_DISTANCE_COLOR[0],1-COST_DISTANCE_COLOR[1],1-COST_DISTANCE_COLOR[2])
                    gl.glVertex3f(p[0],p[1],margin)
                    gl.glVertex3f(c[0],c[1],margin)
            gl.glEnd()
    
if __name__=='__main__':
    world,table,robot=make_scene(True,5,5)
    polygon=Polygon2D(((0,0),(1,0),(1,1),(0,1)))
    for i in range(100):
        polygon.debug_gradient()
    Polygon2DGLVisualizer(polygon,world,table,robot,0.5).run()