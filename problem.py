from klampt.model.collide import WorldCollider
from klampt import WorldModel,Geometry3D
from visualizer import *
from utils import *
import random

def type_list(nr,type_frac):
    if type_frac is not None:
        type=[1]*int(nr*type_frac)
        while len(type)<nr:
            type.append(0)
        random.shuffle(type)
        return type
    else: 
        return None

def is_valid_box(world,box,bbt,robot,bss):
    #on table
    if bbt is not None:
        obb=box.geometry().getBB()
        for i in range(8):
            pos=[obb[0][d] if (i&(1<<d))==0 else obb[1][d] for d in range(3)]
            for d in range(2):
                if pos[d]<bbt[0][d] or pos[d]>bbt[1][d]:
                    return False
    #no collision with robot
    coll=WorldCollider(world)
    colls=[c for c in coll.robotObjectCollisions(robot,box)]
    if len(colls)>0:
        return False
    #no collision with other boxes
    for b in bss:
        colls=[c for c in coll.objectObjectCollisions(b,box)]
        if len(colls)>0:
            return False
    return True

def move_to_center(world,box,robot,bss,R,h,l,margin):
    h0=h
    while True:
        mid=op.mul(op.add(l,h),0.5)
        box.setTransform(R,mid)
        if is_valid_box(world,box,None,robot,bss):
            h=mid
        else: l=mid
        if op.norm(op.sub(h,l))<margin:
            break
    #move away by margin
    mid2h=op.sub(h0,mid)
    len=op.norm(mid2h)
    if len>margin:
        mid2h=op.mul(mid2h,margin/len)
        h=op.add(mid,mid2h)
    else: h=h0
    box.setTransform(R,h)

def create_boxes(world,size_list,type_list,table,robot,margin,dist,nr_trial=10000,cube=True):
    bss=[]
    if isinstance(table,tuple):
        bbt=table
    else: bbt=get_object_bb(table)
    bbr=get_robot_bb(robot)
    if size_list is None:
        #use existing objects
        size_list=[]
        for i in range(world.numRigidObjects()):
            r=world.rigidObject(i)
            r.setTransform([1,0,0,0,1,0,0,0,1],[0,0,0])
            bb=r.geometry().getBBTight()
            size_list.append(op.sub(bb[1],bb[0]))
    for i,sz in enumerate(size_list):
        if world.numRigidObjects()>i and (world.rigidObject(i).getName()=="0" or world.rigidObject(i).getName()=="1"):
            box=world.rigidObject(i)
        else:
            #create object
            l=Geometry3D()
            if (isinstance(cube,int) and cube>0) or (isinstance(cube,float) and i>cube*len(size_list)):
                l.loadFile(DATA_PATH+"/data/objects/cube.off")
            else:
                l.loadFile(DATA_PATH+"/data/objects/prism.off")
            #local transform
            s=([sz[0],0,0,0,sz[1],0,0,0,sz[2]],[0,0,0])
            t=([1,0,0,0,1,0,0,0,1],[-sz[0]/2,-sz[1]/2,margin])
            Rt=se3.mul(t,s)
            l.transform(Rt[0],Rt[1])
            box=world.makeRigidObject("")
            box.geometry().set(l)
            box.appearance().setColor(OBJECT_COLOR[0],OBJECT_COLOR[1],OBJECT_COLOR[2])
        #label type
        if type_list is not None:
            assert len(type_list)==len(size_list)
            if type_list[i]==1:
                box.appearance().setColor(SPECIAL_OBJECT_COLOR[0],SPECIAL_OBJECT_COLOR[1],SPECIAL_OBJECT_COLOR[2])
                box.setName("1")
            else: box.setName("0")
        else: box.setName("0")
        #global transform
        R=None
        t=[0,0,0]
        found=False
        for i in range(nr_trial):
            theta=random.uniform(-math.pi,math.pi)
            t[0]=random.uniform(bbt[0][0],bbt[1][0])
            t[1]=random.uniform(bbt[0][1],bbt[1][1])
            R=[ op.cos_safe(theta),op.sin_safe(theta),0,  \
               -op.sin_safe(theta),op.cos_safe(theta),0,  \
                                 0,                 0,1]
            box.setTransform(R,t)
            if is_valid_box(world,box,bbt,robot,bss):
                found=True
                if dist=="move_to_robot":
                    t0=[(bbr[0][0]+bbr[1][0])/2,(bbr[0][1]+bbr[1][1])/2,0]
                    move_to_center(world,box,robot,bss,R,t,t0,margin)
                elif dist=="move_to_table":
                    t0=[(bbt[0][0]+bbt[1][0])/2,(bbt[0][1]+bbt[1][1])/2,0]
                    move_to_center(world,box,robot,bss,R,t,t0,margin)
                break
        assert found
        bss.append(box)
    return bss

def create_boxes_same_size(world,nr,size,type_frac,table,robot,margin,dist,nr_trial=10000,cube=True):
    if op.is_float(size):
        size=[size]*3
    assert margin>0
    return create_boxes(world,[size]*nr,type_list(nr,type_frac),table,robot,margin,dist,nr_trial,cube=cube)