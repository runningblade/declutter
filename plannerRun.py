from reachable import ReachableTx90Robotiq
from reachableTRINA import ReachableTRINA
from reachableTraj import ReachableTrajTx90Robotiq,GLVisualizerReachable
from costFunctionFast import CostDistToRegionQuadraticFast
from costFunction import OptimalTransportCost
from simulatorBox2D import SimulatorBox2D
from simulatorODE import SimulatorODE
from executor import KlamptExecutor
from klampt import Simulator
from scene import *

#planners
from planner import Action,PlannerGLVisualizer
from pushPlannerContinuous import PushPlannerContinuous
from pushPlannerDiscrete import PushPlannerDiscrete
from graspPlannerDiscrete import GraspPlannerDiscrete
from hybridPlannerGreedy import HybridPlannerGreedy
from hybridPlannerRecedingHorizon import HybridPlannerRecedingHorizon

def get_capacity_labeled(world,nrRegion):
    idss=[]
    for i in range(world.numRigidObjects()):
        world.rigidObject(i).setName(str(i))
        idss.append(i)
    import random
    random.shuffle(idss)
    
    capacity=[[0 for i in range(world.numRigidObjects())] for i in range(nrRegion)]
    for i in range(world.numRigidObjects()):
        capacity[i%nrRegion][idss[i]]+=1
        if i%nrRegion==0:
            world.rigidObject(idss[i]).appearance().setColor(OBJECT_COLOR[0],OBJECT_COLOR[1],OBJECT_COLOR[2])
        elif i%nrRegion==1:
            world.rigidObject(idss[i]).appearance().setColor(SPECIAL_OBJECT_COLOR[0],SPECIAL_OBJECT_COLOR[1],SPECIAL_OBJECT_COLOR[2])
        elif i%nrRegion==2:
            world.rigidObject(idss[i]).appearance().setColor(SPECIAL2_OBJECT_COLOR[0],SPECIAL2_OBJECT_COLOR[1],SPECIAL2_OBJECT_COLOR[2])
        elif i%nrRegion==3:
            world.rigidObject(idss[i]).appearance().setColor(SPECIAL3_OBJECT_COLOR[0],SPECIAL3_OBJECT_COLOR[1],SPECIAL3_OBJECT_COLOR[2])
        else: assert False
    return capacity

def get_capacity(world,nrRegion):
    nrCategory=[]
    for i in range(world.numRigidObjects()):
        category=int(world.rigidObject(i).getName())
        while category>=len(nrCategory):
            nrCategory.append(0)
        nrCategory[category]+=1
    
    capacity=[]
    for i in range(nrRegion):
        if i<nrRegion-1:
            capacity.append([c//nrRegion for c in nrCategory])
        else: capacity.append([c-(c//nrRegion)*(nrRegion-1) for c in nrCategory])
    return capacity

def run_planner(world,table,robot,robot_path,cost,planner,useBox2D,scaleCamera,margin=0.004,speed=1.5,speedMove=0.5,minLevel=2,fullHorizon=False,offline=True,horizon=3,jointPush=False):
    useDynamics=True
    if "tx90" in robot.getName():
        reachableTraj=ReachableTrajTx90Robotiq(world,table,robot,robot_path,0.3)
        useDynamics=False
        robotBase=True
    elif "Anthrax" in robot.getName():
        reachableTraj=ReachableTRINA(world,table,robot,robot_path,0.35)
        useDynamics=False
        robotBase=False
    else: raise RuntimeError("Unknown robot!")
    reachableTraj.build_grid(0.025)
    reachableTraj.drawEE=False
    
    if useBox2D:
        sim=SimulatorBox2D(cost,world,reachableTraj,robotBaseBox2D=robotBase,dt=0.001 if "tx90" in robot.getName() else 0.01)
    else: sim=SimulatorODE(cost,world,reachableTraj)
    if useDynamics:
        set_simulator_margin(world,sim.sim,margin)
        sim.relax()
    else:
        sim.sim=None
        sim.executor=None
    if scaleCamera is not None:
        assert useBox2D
        
    if planner=='debug_action_error':
        act=Action(sim,0,0)
        reachable=ReachableTx90Robotiq(world,table,robot,0.3)
        reachable.build_grid(0.025)
        reachable.drawEE=False
        reachable.build_grid_bounds()
        vis=GLVisualizerReachable(world,table,robot,reachable)
        return vis
    elif planner=='grasp':
        planner=GraspPlannerDiscrete(sim,speed,speedMove)
    elif planner=='push_discrete':
        planner=PushPlannerDiscrete(sim,speed,speedMove,minLevel)
    elif planner=='push_continuous':
        planner=PushPlannerContinuous(sim,speed,speedMove,joint=jointPush)
    elif planner=='pushBD_continuous':
        planner=PushPlannerContinuous(sim,speed,speedMove,joint=jointPush,entireSet=False)
    elif planner=='grasp_push_discrete_greedy':
        subPlanners=[]
        subPlanners.append(GraspPlannerDiscrete(sim,speed,speedMove))
        subPlanners.append(PushPlannerDiscrete(sim,speed,speedMove,minLevel))
        planner=HybridPlannerGreedy(sim,subPlanners)
    elif planner=='grasp_push_continuous_greedy':
        subPlanners=[]
        subPlanners.append(GraspPlannerDiscrete(sim,speed,speedMove))
        subPlanners.append(PushPlannerContinuous(sim,speed,speedMove,joint=jointPush))
        planner=HybridPlannerGreedy(sim,subPlanners)
    elif planner=='grasp_pushBD_continuous_greedy':
        subPlanners=[]
        subPlanners.append(GraspPlannerDiscrete(sim,speed,speedMove))
        subPlanners.append(PushPlannerContinuous(sim,speed,speedMove,joint=jointPush,entireSet=False))
        planner=HybridPlannerGreedy(sim,subPlanners)
    elif planner=='grasp_push_continuous_recedingHorizon':
        subPlanners=[]
        subPlanners.append(GraspPlannerDiscrete(sim,speed,speedMove))
        subPlanners.append(PushPlannerContinuous(sim,speed,speedMove,joint=jointPush))
        planner=HybridPlannerRecedingHorizon(sim,subPlanners,mergeAction=False,level=horizon)
    elif planner=='grasp_pushBD_continuous_recedingHorizon':
        subPlanners=[]
        subPlanners.append(GraspPlannerDiscrete(sim,speed,speedMove))
        subPlanners.append(PushPlannerContinuous(sim,speed,speedMove,entireSet=False,joint=jointPush))
        planner=HybridPlannerRecedingHorizon(sim,subPlanners,mergeAction=False,level=horizon)
    elif planner=='grasp_push_continuous_recedingHorizonMerged':
        subPlanners=[]
        subPlanners.append(GraspPlannerDiscrete(sim,speed,speedMove))
        subPlanners.append(PushPlannerContinuous(sim,speed,speedMove,joint=jointPush))
        planner=HybridPlannerRecedingHorizon(sim,subPlanners,mergeAction=True,level=horizon)
    elif planner=='grasp_pushBD_continuous_recedingHorizonMerged':
        subPlanners=[]
        subPlanners.append(GraspPlannerDiscrete(sim,speed,speedMove))
        subPlanners.append(PushPlannerContinuous(sim,speed,speedMove,entireSet=False,joint=jointPush))
        planner=HybridPlannerRecedingHorizon(sim,subPlanners,mergeAction=True,level=horizon)
    else: assert False

    if offline:
        return planner
    else: return PlannerGLVisualizer(world,table,robot,planner,scaleCamera=scaleCamera,fullHorizon=fullHorizon)
    
def debug_save(args,traj,ops,savePath):
    costHistory=[]
    transitHistory=[]
    transferHistory=[]
    for op in ops:
        op.sim.set_state(op.state0)
        costHistory.append(op.sim.calc_cost())
        transitHistory.append(op.transit_cost())
        transferHistory.append(op.transfer_cost())
    #last one
    if len(ops)>0:
        op.sim.set_state(ops[-1].state1)
        costHistory.append(op.sim.calc_cost())
    #pickle
    import pickle
    pickle.dump((args,traj,costHistory,transitHistory,transferHistory),open(savePath,'wb'))
    print("costHistory=%s!"%str(costHistory))
    print("transitHistory=%s!"%str(transitHistory))
    print("transferHistory=%s!"%str(transferHistory))
    print("Saved to %s!"%savePath)
    
def run_example(**args):
    if "useTRINA" in args:
        if args["useTRINA"]=="hardware":
            world,table,robot,robot_path=make_scene_TRINA_CALIBRATED()
        else: world,table,robot,robot_path=make_scene_TRINA()
        dx=0.7
    else: 
        tableSize=args["tableSize"] if "tableSize" in args else 5 
        world,table,robot,robot_path=make_scene_tx90(tableSize,tableSize)
        dx=0.0
    bbt=args["bbt"] if "bbt" in args else ((-0.1-dx,-0.5,0),(0.4-dx,0.5,0))
    objFrac=args["objFrac"] if "objFrac" in args else 0.2
    objSz=args["objSz"] if "objSz" in args else 0.05
    nrObj=args["nrObj"] if "nrObj" in args else 50
    random.seed(args["seed"] if "seed" in args else 0)
    objInit=args["objInit"] if "objInit" in args else ""
    horizon=args["horizon"] if "horizon" in args else 3
    margin=args["margin"] if "margin" in args else 0.006
    cube=args["cube"] if "cube" in args else True
    jointPush=args["jointPush"] if "jointPush" in args else False
    
    if "nrCluster" not in args:
        args["nrCluster"]=1
    if abs(args["nrCluster"])==4:
        bbc0=args["bbc0"] if "bbc0" in args else (( 0.75-dx,-1.00),(1.25-dx,-0.50))
        bbc1=args["bbc1"] if "bbc1" in args else (( 0.75-dx, 0.50),(1.25-dx, 1.00))
        bbc2=args["bbc2"] if "bbc2" in args else ((-0.25-dx,-1.00),(0.25-dx,-0.50))
        bbc3=args["bbc3"] if "bbc3" in args else ((-0.25-dx, 0.50),(0.25-dx, 1.00))
        create_boxes_same_size(world,nrObj,objSz,objFrac,bbt,robot,margin,objInit,cube=cube)
        if args["nrCluster"]<0:
            capacity=get_capacity_labeled(world,4)
        else: capacity=get_capacity(world,4)
        cost1=CostDistToRegionQuadraticFast(bbc0)
        cost2=CostDistToRegionQuadraticFast(bbc1)
        cost3=CostDistToRegionQuadraticFast(bbc2)
        cost4=CostDistToRegionQuadraticFast(bbc3)
        cost=OptimalTransportCost([cost1,cost2,cost3,cost4],capacity,frequentUpdate=True)
    elif abs(args["nrCluster"])==2:
        bbc0=args["bbc0"] if "bbc0" in args else ((-0.25-dx,-1.00),(0.25-dx,-0.50))
        bbc1=args["bbc1"] if "bbc1" in args else ((-0.25-dx, 0.50),(0.25-dx, 1.00))
        create_boxes_same_size(world,nrObj,objSz,objFrac,bbt,robot,margin,objInit,cube=cube)
        if args["nrCluster"]<0:
            capacity=get_capacity_labeled(world,2)
        else: capacity=get_capacity(world,2)
        cost1=CostDistToRegionQuadraticFast(bbc0)
        cost2=CostDistToRegionQuadraticFast(bbc1)
        cost=OptimalTransportCost([cost1,cost2],capacity,frequentUpdate=True)
    elif abs(args["nrCluster"])==1:
        bbc0=args["bbc0"] if "bbc0" in args else ((0.5-dx,-0.25),(1.0-dx,0.25))
        create_boxes_same_size(world,nrObj,objSz,.0,bbt,robot,margin,objInit,cube=cube)
        cost=CostDistToRegionQuadraticFast(bbc0)
    else: raise RuntimeError("Unsupported nrCluter=%d!"%args["nrCluster"])
    
    minLevel=args["minLevel"] if "minLevel" in args else 2
    useBox2D=args["useBox2D"] if "useBox2D" in args else True
    scaleCamera=args["scaleCamera"] if "scaleCamera" in args else 1.5
    fullHorizon=args["fullHorizon"] if "fullHorizon" in args else True
    planner=args["planner"] if "planner" in args else 'grasp_push_continuous_recedingHorizonMerged'

    #running
    vis=run_planner(world,table,robot,robot_path,cost,planner,useBox2D,scaleCamera,margin=margin/2,   \
                    minLevel=minLevel,fullHorizon=fullHorizon,offline="savePath" in args,horizon=horizon,
                    jointPush=jointPush)
    if "loadPath" in args:
        import pickle
        assert "savePath" not in args
        assert os.path.exists(args["loadPath"])
        _,traj,_,_,_=pickle.load(open(args["loadPath"],'rb'))
        vis.currTraj=traj
        vis.currTime=0
    elif "savePath" in args:
        traj,ops=vis.plan_trajectory(debugInfo=True)
        debug_save(args,traj,ops,args["savePath"])
    else:
        vis.bbt=bbt
    return vis
    
if __name__=='__main__':
    run_example(useTRINA=True,nrCluster=2,tableSize=2,planner='grasp_push_continuous_greedy',scaleCamera=None)
    #run_example(nrCluster=2,planner='grasp_push_continuous_greedy',scaleCamera=None,bbt=((0.25,-0.5,0),(0.75,0.5,0)))
    #run_example(nrCluster=2,planner='grasp_push_continuous_recedingHorizonMerged',horizon=1,savePath="tmpSaveGreedy.txt")
    #run_example(nrCluster=2,planner='grasp_push_continuous_recedingHorizonMerged',horizon=3,savePath="tmpSaveReceding.txt")
    