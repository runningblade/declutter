from costFunction import *
from utils import *
import pyDeclutter,random

class CostDistToRegionQuadraticFast(CostDistToRegionQuadratic):
    def __init__(self,sss=None):
        CostDistToRegionQuadratic.__init__(self,sss)
    
    def add_triangle(self,t0,t1,t2):
        CostDistToRegionQuadratic.add_triangle(self,t0,t1,t2)
        self.init_cpp()
        
    def add_quad(self,q0,q1,q2,q3):
        CostDistToRegionQuadratic.add_quad(self,q0,q1,q2,q3)
        self.init_cpp()
    
    def add_bb(self,minC,maxC):
        CostDistToRegionQuadratic.add_bb(self,minC,maxC)
        self.init_cpp()
        
    def calc_costs(self,pss):
        ppss=[pyDeclutter.Vec2(pt[0],pt[1]) for pt in pss]
        dcpss=self.poly.distanceBatched(ppss)
        return [d[0]**2 for d in dcpss]
        
    def calc_cost(self,pss,category=None):
        return sum(self.calc_costs(pss))
    
    def calc_cost_dist(self,pss,category=None):
        ppss=[pyDeclutter.Vec2(pt[0],pt[1]) for pt in pss]
        dcpss=self.poly.distanceBatched(ppss)
        return sum([d[0]**2 for d in dcpss]),[(p,(dcp[1][0],dcp[1][1])) for p,dcp in zip(pss,dcpss)]
        
    def get_directional_cost(self,p,d,id=None):
        if isinstance(p,list) and isinstance(d,list):
            lss=[]
            for pi,di in zip(p,d):
                di=op.mul(di,INFINITY_NUMBER/op.norm(di))
                lss.append(pyDeclutter.LineSeg2D(pyDeclutter.Vec2(pi[0],pi[1]),pyDeclutter.Vec2(pi[0]+di[0],pi[1]+di[1])))
            dirs=self.poly.buildDirectionalCostBatched(lss)
            return [self.to_directional_cost(l,d) for l,d in zip(lss,dirs)]
        else:
            d=op.mul(d,INFINITY_NUMBER/op.norm(d))
            l=pyDeclutter.LineSeg2D(pyDeclutter.Vec2(p[0],p[1]),pyDeclutter.Vec2(p[0]+d[0],p[1]+d[1]))
            dir=self.poly.buildDirectionalCost(l)
            return self.to_directional_cost(l,dir)
        
    def to_directional_cost(self,l,d):
        ret=DirectionalCost(None,None,None)
        ret.a=(l._a[0],l._a[1])
        ret.dir=(l._dir[0],l._dir[1])
        ret.b=(l._b[0],l._b[1])
        ret.cases=[]
        for c in d._case:
            cost=DirectionalCostCase(None,None,None,None)
            cost.alpha0=c._alpha0
            cost.alpha1=c._alpha1
            cost.a=c._a
            cost.b=c._b
            cost.c=c._c
            cost.type=b'I' if c.getType()==0 else b'V' if c.getType()==1 else b'E'
            ret.cases.append(cost)
        return ret
    
    def debug_cost(self,d=10,N=100):
        pss=[(random.uniform(-d,d),random.uniform(-d,d)) for i in range(N)]
        a=self.calc_cost(pss)
        b=CostDistToRegionQuadratic.calc_cost(self,pss)
        print("cpp: ",a," python: ",b)
        
        err=0
        a,ac=self.calc_cost_dist(pss)
        b,bc=CostDistToRegionQuadratic.calc_cost_dist(self,pss)
        for pcpa,pcpb in zip(ac,bc):
            err+=op.norm(op.sub(pcpa[0],pcpb[0]))
            err+=op.norm(op.sub(pcpa[1],pcpb[1]))
        print("cpp: ",a," python: ",b," err: ",err)
    
    def debug_directional_cost(self,d=10,N=100):
        pss=[(random.uniform(-d,d),random.uniform(-d,d)) for i in range(N)]
        dss=[(random.uniform(-d,d),random.uniform(-d,d)) for i in range(N)]
        a=self.get_directional_cost(pss,dss)
        b=CostDistToRegionQuadratic.get_directional_cost(self,pss,dss)
        errs=[]
        for ai,bi in zip(a,b):
            err=0
            err+=op.norm(op.sub(ai.a,bi.a))
            err+=op.norm(op.sub(ai.dir,bi.dir))
            err+=op.norm(op.sub(ai.b,bi.b))
            assert len(ai.cases)==len(bi.cases)
            for ca,cb in zip(ai.cases,bi.cases):
                err+=abs(ca.alpha0-cb.alpha0)
                err+=abs(ca.alpha1-cb.alpha1)
                err+=abs(ca.a-cb.a)
                err+=abs(ca.b-cb.b)
                err+=abs(ca.c-cb.c)
            errs.append(err)
        print("err: ",errs)
    
    def init_cpp(self):
        assert len(self.pss)==1
        vss=[pyDeclutter.Vec2(pt[0],pt[1]) for pt in self.pss[0].vss]
        self.poly=pyDeclutter.Polygon2D(vss,INFINITY_NUMBER)
    
if __name__=='__main__':
    cost=CostDistToRegionQuadraticFast()
    cost.add_bb([0,0],[1,1])
    cost.debug_cost()
    cost.debug_directional_cost()