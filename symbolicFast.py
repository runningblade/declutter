import NLPUtils as op
from utils import *
import copy

def max_(a,b):
    return NLPFunctionMax((a,b))

def min_(a,b):
    return NLPFunctionMin((a,b))

def sqrt(expr):
    return NLPFunctionSqrt((expr,))

def sin(a):
    return NLPFunctionSin((a,))

def cos(a):
    return NLPFunctionCos((a,))

def asin(a):
    return NLPFunctionAsin((a,))

def acos(a):
    return NLPFunctionAcos((a,))

def log(a):
    return NLPFunctionLog(a)

def add_jac(res,k,v):
    if k in res:
        res[k]+=v
    else: res[k]=v

def merge_jac(resa,resb):
    ret=dict()
    for k,v, in resa.items():
        add_jac(ret,k,v)
    for k,v, in resb.items():
        add_jac(ret,k,v)
    return ret

def debug_jac(expr,x,delta=1e-6):
    val0=expr.eval(x)
    res=expr.jac(x)
    if op.is_float(val0):
        val0=[val0]
        res=[res]
    for i in range(len(val0)):
        for k,v in res[i].items():
            x2=copy.deepcopy(x)
            x2[k]+=delta
            val1=expr.eval(x2)
            if op.is_float(val1):
                val1=[val1]
            jval=(val1[i]-val0[i])/delta
            print("Jac(%d,%d): %f, Err: %f"%(i,k,v,v-jval))

class NLPFunction:
    def __init__(self,args,name):
        self.args=args
        self.name=name
    
    def __add__(self,expr):
        if op.is_float(expr) and expr==0.0:
            return self
        else: return NLPFunctionAdd((self,expr))
    
    def __radd__(self,expr):
        if op.is_float(expr) and expr==0.0:
            return self
        else: return NLPFunctionAdd((expr,self))
        
    def __sub__(self,expr):
        if op.is_float(expr) and expr==0.0:
            return self
        else: return NLPFunctionSub((self,expr))
    
    def __rsub__(self,expr):
        return NLPFunctionSub((expr,self))
        
    def __neg__(self):
        return NLPFunctionNeg((self,))
        
    def __mul__(self,expr):
        if op.is_float(expr) and expr==1.0:
            return self
        else: return NLPFunctionMul((self,expr))
    
    def __rmul__(self,expr):
        if op.is_float(expr) and expr==1.0:
            return self
        else: return NLPFunctionMul((expr,self))
        
    def __div__(self,expr):
        if op.is_float(expr) and expr==1.0:
            return self
        else: return NLPFunctionDiv((self,expr))
    
    def __rdiv__(self,expr):
        return NLPFunctionDiv((expr,self))

    def __pow__(self,b):
        assert op.is_float(b)
        return NLPFunctionPowConstExp((self,),b)
      
    def __str__(self):
        ret=self.name+"("
        for i in range(len(self.args)):
            ret+=("" if i==0 else ",")+str(self.args[i])
        ret+=")"
        return ret
        
    def __eq__(self,other):
        return NLPFunctionEQ((self,other))

    def __le__(self,other):
        return NLPFunctionLE((self,other))
    
    def __ge__(self,other):
        return NLPFunctionGE((self,other))  

    def vars(self):
        ret=set()
        for a in self.args:
            if not op.is_float(a):
                ret=ret.union(a.vars())
        return ret
        
    def eval(self,x):
        raise NotImplementedError("Function %s does not support evaluate!"%self.name)
    
    def getValue(self):
        return self.x
    
    def jac(self,x,res=None,coef=None):
        raise NotImplementedError("Function %s does not support jacobian!"%self.name)

    def eval_safe(self,id,x):
        if op.is_float(self.args[id]):
            return self.args[id]
        else: return self.args[id].eval(x)
        
    def jac_safe(self,id,x,res=None,coef=None):
        if op.is_float(self.args[id]):
            return res
        else: return self.args[id].jac(x,res,coef)

class NLPFunctionAdd(NLPFunction):
    def __init__(self,args):
        NLPFunction.__init__(self,args,"add")
        assert len(self.args)==2
       
    def __str__(self):
        return "("+str(self.args[0])+")+("+str(self.args[1])+")"
    
    def eval(self,x):
        return self.eval_safe(0,x)+self.eval_safe(1,x)
    
    def jac(self,x,res=None,coef=None):
        if res is None:
            res=dict()
        self.jac_safe(0,x,res,coef)
        self.jac_safe(1,x,res,coef)
        return res

class NLPFunctionSub(NLPFunction):
    def __init__(self,args):
        NLPFunction.__init__(self,args,"sub")
        assert len(self.args)==2
       
    def __str__(self):
        return "("+str(self.args[0])+")-("+str(self.args[1])+")"
    
    def eval(self,x):
        return self.eval_safe(0,x)-self.eval_safe(1,x)
    
    def jac(self,x,res=None,coef=None):
        if res is None:
            res=dict()
        self.jac_safe(0,x,res,coef)
        self.jac_safe(1,x,res,-1.0 if coef is None else -coef)
        return res
 
class NLPFunctionMul(NLPFunction):
    def __init__(self,args):
        NLPFunction.__init__(self,args,"mul")
        assert len(self.args)==2
       
    def __str__(self):
        return "("+str(self.args[0])+")*("+str(self.args[1])+")"
    
    def eval(self,x):
        return self.eval_safe(0,x)*self.eval_safe(1,x)
    
    def jac(self,x,res=None,coef=None):
        if res is None:
            res=dict()
        c=self.eval_safe(1,x)
        self.jac_safe(0,x,res,c*(1.0 if coef is None else coef))
        c=self.eval_safe(0,x)
        self.jac_safe(1,x,res,c*(1.0 if coef is None else coef))
        return res
   
class NLPFunctionDiv(NLPFunction):
    def __init__(self,args):
        NLPFunction.__init__(self,args,"div")
        assert len(self.args)==2
       
    def __str__(self):
        return "("+str(self.args[0])+")/("+str(self.args[1])+")"
    
    def eval(self,x):
        return self.eval_safe(0,x)/self.eval_safe(1,x)
    
    def jac(self,x,res=None,coef=None):
        if res is None:
            res=dict()
        c0=self.eval_safe(1,x)
        self.jac_safe(0,x,res,(1.0 if coef is None else coef)/c0)
        c=-self.eval_safe(0,x)/c0**2
        self.jac_safe(1,x,res,c*(1.0 if coef is None else coef))
        return res

class NLPFunctionNeg(NLPFunction):
    def __init__(self,args):
        NLPFunction.__init__(self,args,"neg")
        assert len(self.args)==1
       
    def __str__(self):
        return "-("+str(self.args[0])+")"
    
    def eval(self,x):
        return -self.eval_safe(0,x)
    
    def jac(self,x,res=None,coef=None):
        if res is None:
            res=dict()
        self.jac_safe(0,x,res,-1.0 if coef is None else -coef)
        return res

class NLPFunctionSqrt(NLPFunction):
    def __init__(self,args):
        NLPFunction.__init__(self,args,"sqrt")
        assert len(self.args)==1
       
    def __str__(self):
        return "sqrt("+str(self.args[0])+")"
    
    def eval(self,x):
        return op.sqrt_safe(self.eval_safe(0,x))
    
    def jac(self,x,res=None,coef=None):
        if res is None:
            res=dict()
        c=0.5/op.sqrt_safe(self.eval_safe(0,x))
        self.jac_safe(0,x,res,c*(1.0 if coef is None else coef))
        return res

class NLPFunctionSin(NLPFunction):
    def __init__(self,args):
        NLPFunction.__init__(self,args,"sin")
        assert len(self.args)==1
       
    def __str__(self):
        return "sin("+str(self.args[0])+")"
    
    def eval(self,x):
        return op.sin_safe(self.eval_safe(0,x))
    
    def jac(self,x,res=None,coef=None):
        if res is None:
            res=dict()
        c=op.cos_safe(self.eval_safe(0,x))
        self.jac_safe(0,x,res,c*(1.0 if coef is None else coef))
        return res
    
class NLPFunctionCos(NLPFunction):
    def __init__(self,args):
        NLPFunction.__init__(self,args,"cos")
        assert len(self.args)==1
       
    def __str__(self):
        return "cos("+str(self.args[0])+")"
    
    def eval(self,x):
        return op.cos_safe(self.eval_safe(0,x))
    
    def jac(self,x,res=None,coef=None):
        if res is None:
            res=dict()
        c=-op.sin_safe(self.eval_safe(0,x))
        self.jac_safe(0,x,res,c*(1.0 if coef is None else coef))
        return res

class NLPFunctionAsin(NLPFunction):
    def __init__(self,args):
        NLPFunction.__init__(self,args,"asin")
        assert len(self.args)==1
       
    def __str__(self):
        return "asin("+str(self.args[0])+")"
    
    def eval(self,x):
        return op.asin_safe(self.eval_safe(0,x))
    
    def jac(self,x,res=None,coef=None):
        if res is None:
            res=dict()
        c=1/op.sqrt_safe(1-self.eval_safe(0,x)**2)
        self.jac_safe(0,x,res,c*(1.0 if coef is None else coef))
        return res
    
class NLPFunctionAcos(NLPFunction):
    def __init__(self,args):
        NLPFunction.__init__(self,args,"acos")
        assert len(self.args)==1
       
    def __str__(self):
        return "acos("+str(self.args[0])+")"
    
    def eval(self,x):
        return op.acos_safe(self.eval_safe(0,x))
    
    def jac(self,x,res=None,coef=None):
        if res is None:
            res=dict()
        c=-1/op.sqrt_safe(1-self.eval_safe(0,x)**2)
        self.jac_safe(0,x,res,c*(1.0 if coef is None else coef))
        return res

class NLPFunctionLog(NLPFunction):
    def __init__(self,args):
        NLPFunction.__init__(self,args,"log")
        assert len(self.args)==1
       
    def __str__(self):
        return "log("+str(self.args[0])+")"
    
    def eval(self,x):
        return op.log_safe(self.eval_safe(0,x))
    
    def jac(self,x,res=None,coef=None):
        if res is None:
            res=dict()
        c=self.eval_safe(0,x)
        if c<=0:
            raise RuntimeError("Negative value for log!")
        self.jac_safe(0,x,res,(1.0 if coef is None else coef)/c)
        return res

class NLPFunctionPowConstExp(NLPFunction):
    def __init__(self,args,exp):
        NLPFunction.__init__(self,args,"powConstExp")
        assert len(self.args)==1
        self.exp=exp

    def __str__(self):
        return str(self.args[0])+"**"+str(self.exp)
    
    def eval(self,x):
        return self.eval_safe(0,x)**self.exp
    
    def jac(self,x,res=None,coef=None):
        if res is None:
            res=dict()
        c=self.exp*self.eval_safe(0,x)**(self.exp-1)
        self.jac_safe(0,x,res,c*(1.0 if coef is None else coef))
        return res

class NLPFunctionMax(NLPFunction):
    def __init__(self,args):
        NLPFunction.__init__(self,args,"max")
        assert len(self.args)==2
       
    def __str__(self):
        return "max("+str(self.args[0])+","+str(self.args[1])+")"
    
    def eval(self,x):
        ca=self.eval_safe(0,x)
        cb=self.eval_safe(1,x)
        return ca if ca>cb else cb
    
    def jac(self,x,res=None,coef=None):
        if res is None:
            res=dict()
        ca=self.eval_safe(0,x)
        cb=self.eval_safe(1,x)
        if ca>cb:
            self.jac_safe(0,x,res,coef)
        else: self.jac_safe(1,x,res,coef)
        return res

class NLPFunctionMin(NLPFunction):
    def __init__(self,args):
        NLPFunction.__init__(self,args,"min")
        assert len(self.args)==2
       
    def __str__(self):
        return "min("+str(self.args[0])+","+str(self.args[1])+")"
    
    def eval(self,x):
        ca=self.eval_safe(0,x)
        cb=self.eval_safe(1,x)
        return ca if ca<cb else cb
    
    def jac(self,x,res=None,coef=None):
        if res is None:
            res=dict()
        ca=self.eval_safe(0,x)
        cb=self.eval_safe(1,x)
        if ca<cb:
            self.jac_safe(0,x,res,coef)
        else: self.jac_safe(1,x,res,coef)
        return res   

class NLPFunctionEQ(NLPFunction):
    def __init__(self,args):
        NLPFunction.__init__(self,args,"eq")
        assert len(self.args)==2
        
    def __str__(self):
        return str(self.args[0])+"=="+str(self.args[1])
        
class NLPFunctionLE(NLPFunction):
    def __init__(self,args):
        NLPFunction.__init__(self,args,"le")
        assert len(self.args)==2
        
    def __str__(self):
        return str(self.args[0])+"<="+str(self.args[1])
    
class NLPFunctionGE(NLPFunction):
    def __init__(self,args):
        NLPFunction.__init__(self,args,"ge")
        assert len(self.args)==2
        
    def __str__(self):
        return str(self.args[0])+">="+str(self.args[1])

class NLPFunctionCat(NLPFunction):
    def __init__(self,args):
        NLPFunction.__init__(self,args,"cat")
       
    def __str__(self):
        ret="cat("
        for i in range(len(self.args)):
            ret+=str(self.args[i])+("," if i<len(self.args)-1 else "")
        ret+=")"
        return ret
    
    def eval(self,x):
        ret=[]
        for i in range(len(self.args)):
            val=self.eval_safe(i,x)
            if op.is_float(val):
                ret.append(val)
            else: ret+=val
        return ret
    
    def jac(self,x,res=None,coef=None):
        if res is None:
            res=[]
        for i in range(len(self.args)):
            val=self.jac_safe(i,x,None,None if coef is None else coef[i])
            if isinstance(val,list):
                res+=val
            else: res.append(val)
        return res

class NLPFunctionWedge(NLPFunction):
    def __init__(self,args):
        NLPFunction.__init__(self,args,"wedge")
       
    def __str__(self):
        return "wedge("+str(self.args[0])+","+str(self.args[1])+")"
    
    def eval(self,x):
        a=self.eval_safe(0,x)
        b=self.eval_safe(1,x)
        return wedge(a,b)
    
    def jac(self,x,res=None,coef=None):
        if res is None:
            res=dict()
        #a
        c=self.eval_safe(1,x)
        resa=self.jac_safe(0,x,None,op.mul([c[1],-c[0]],1.0 if coef is None else coef))
        res=merge_jac(res,resa[0])
        res=merge_jac(res,resa[1])
        #b
        c=self.eval_safe(0,x)
        resb=self.jac_safe(1,x,None,op.mul([-c[1],c[0]],1.0 if coef is None else coef))
        res=merge_jac(res,resb[0])
        res=merge_jac(res,resb[1])
        return res

class NLPVariable(NLPFunction):
    def __init__(self,id):
        NLPFunction.__init__(self,None,"variable")
        self.id=id

    def __add__(self,expr):
        if op.is_float(expr):
            return NLPLinearExpression(self)+expr
        elif isinstance(expr,NLPVariable):
            return NLPLinearExpression(self)+NLPLinearExpression(expr)
        elif isinstance(expr,NLPLinearExpression):
            return expr+self
        elif isinstance(expr,NLPQuadraticExpression):
            return expr+self
        else: return NLPFunctionAdd((self,expr))
        
    def __radd__(self,expr):
        return self.__add__(expr)
        
    def __sub__(self,expr):
        if op.is_float(expr):
            return NLPLinearExpression(self)-expr
        elif isinstance(expr,NLPVariable):
            return NLPLinearExpression(self)-NLPLinearExpression(expr)
        elif isinstance(expr,NLPLinearExpression):
            return expr.__rsub__(self)
        elif isinstance(expr,NLPQuadraticExpression):
            return expr.__rsub__(self)
        else: return NLPFunctionSub((self,expr))
        
    def __rsub__(self,expr):
        if op.is_float(expr):
            return expr-NLPLinearExpression(self)
        elif isinstance(expr,NLPVariable):
            return NLPLinearExpression(expr)-NLPLinearExpression(self)
        elif isinstance(expr,NLPLinearExpression):
            return expr-self
        elif isinstance(expr,NLPQuadraticExpression):
            return expr-self
        else: return NLPFunctionSub((expr,self))
        
    def __neg__(self):
        return self*-1.0
        
    def __mul__(self,expr):
        if op.is_float(expr):
            return NLPLinearExpression(self)*expr
        elif isinstance(expr,NLPVariable):
            return NLPQuadraticExpression(self)*expr
        elif isinstance(expr,NLPLinearExpression):
            return NLPQuadraticExpression(expr)*self
        else: return NLPFunctionMul((self,expr))
        
    def __rmul__(self,expr):
        return self.__mul__(expr)
        
    def __div__(self,expr):
        if op.is_float(expr):
            return NLPLinearExpression(self)/expr
        else: return NLPFunctionDiv((self,expr))

    def __rdiv__(self,expr):
        return NLPFunctionDiv((expr,self))

    def __pow__(self,b):
        if b==1:
            return NLPLinearExpression(self)
        elif b==2:
            return NLPLinearExpression(self)**2
        elif b==0.5:
            return NLPFunctionSqrt(NLPLinearExpression(self))
        else: 
            assert op.is_float(b)
            return NLPFunctionPowConstExp((self,),b)

    def __str__(self):
        return "C"+str(self.id)

    def __eq__(self,other):
        return NLPFunctionEQ((self,other))

    def __le__(self,other):
        return NLPFunctionLE((self,other))
    
    def __ge__(self,other):
        return NLPFunctionGE((self,other))

    def vars(self):
        return set([self.id])
        
    def eval(self,x):
        return x[self.id]
        
    def getValue(self):
        return self.x
    
    def jac(self,x,res=None,coef=None):
        if res is None:
            res=dict()
        add_jac(res,self.id,1.0 if coef is None else coef)
        return res
        
class NLPLinearExpression(NLPFunction):
    def __init__(self,init=None):
        NLPFunction.__init__(self,None,"linear")
        self.const=0.0
        self.coefs=dict()
        if init is None:
            return
        elif op.is_float(init):
            self.const=init
        elif isinstance(init,NLPVariable):
            self.coefs[init.id]=1.0
        elif isinstance(init,NLPLinearExpression):
            self.const=init.const
            self.coefs=copy.deepcopy(init.coefs)
        else: assert False
        
    def __add__(self,expr):
        if op.is_float(expr):
            return copy.deepcopy(self).add_constant(expr)
        elif isinstance(expr,NLPVariable):
            return copy.deepcopy(self).add_linear_expression(NLPLinearExpression(expr))
        elif isinstance(expr,NLPLinearExpression):
            return copy.deepcopy(self).add_linear_expression(expr)
        elif isinstance(expr,NLPQuadraticExpression):
            return expr+self
        else: return NLPFunctionAdd((self,expr))
        
    def __radd__(self,expr):
        return self.__add__(expr)
        
    def __sub__(self,expr):
        if op.is_float(expr):
            return copy.deepcopy(self).add_constant(-expr)
        elif isinstance(expr,NLPVariable):
            return copy.deepcopy(self).sub_linear_expression(NLPLinearExpression(expr))
        elif isinstance(expr,NLPLinearExpression):
            return copy.deepcopy(self).sub_linear_expression(expr)
        elif isinstance(expr,NLPQuadraticExpression):
            return expr.__rsub__(self)
        else: return NLPFunctionSub((self,expr))
        
    def __rsub__(self,expr):
        if op.is_float(expr):
            return NLPLinearExpression(expr)-self
        elif isinstance(expr,NLPVariable):
            return NLPLinearExpression(expr)-self
        elif isinstance(expr,NLPLinearExpression):
            return expr-self
        elif isinstance(expr,NLPQuadraticExpression):
            return expr-self
        else: return NLPFunctionSub((expr,self))
        
    def __neg__(self):
        return self*-1.0
        
    def __mul__(self,expr):
        if op.is_float(expr):
            return copy.deepcopy(self).mul_constant(expr)
        elif isinstance(expr,NLPVariable):
            return self.mul_linear_expression(NLPLinearExpression(expr))
        elif isinstance(expr,NLPLinearExpression):
            return self.mul_linear_expression(expr)
        else: return NLPFunctionMul((self,expr))
        
    def __rmul__(self,expr):
        return self.__mul__(expr)
    
    def __div__(self,expr):
        if op.is_float(expr):
            return copy.deepcopy(self).mul_constant(1.0/expr)
        else: return NLPFunctionDiv((self,expr))
        
    def __rdiv__(self,expr):
        return NLPFunctionDiv((expr,self))
        
    def __pow__(self,b):
        if b==1:
            return copy.deepcopy(self)
        elif b==2:
            return self*self
        elif b==0.5:
            return NLPFunctionSqrt(NLPLinearExpression(self))
        else: 
            assert op.is_float(b)
            return NLPFunctionPowConstExp((self,),b)

    def __str__(self):
        ret=str(self.const)
        for k,v in self.coefs.items():
            ret+=("+" if v>=0 else "")+str(v)+"*C"+str(k)
        return ret
        
    def __eq__(self,other):
        return NLPFunctionEQ((self,other))

    def __le__(self,other):
        return NLPFunctionLE((self,other))
    
    def __ge__(self,other):
        return NLPFunctionGE((self,other))

    def get_linear(self):
        rows=[]
        vals=[]
        for k,v in self.coefs.items():
            rows.append(k)
            vals.append(v)
        return rows,vals

    def vars(self):
        return set([id for id in self.coefs.keys()])
        
    def eval(self,x):
        ret=self.const
        for id in self.coefs.keys():
            ret+=self.coefs[id]*x[id]
        return ret
        
    def getValue(self):
        return self.x
    
    def jac(self,x,res=None,coef=None):
        if res is None:
            res=dict()
        for k,v in self.coefs.items():
            add_jac(res,k,v if coef is None else v*coef)
        return res
        
    def add_linear_expression(self,expr):
        self.const+=expr.const
        for k,v in expr.coefs.items():
            self.add_linear(k,v)
        return self
        
    def sub_linear_expression(self,expr):
        self.const-=expr.const
        for k,v in expr.coefs.items():
            self.add_linear(k,-v)
        return self

    def mul_linear_expression(self,expr):
        ret=NLPQuadraticExpression()
        #const
        ret.const=self.const*expr.const
        #linear: self
        for k,v in self.coefs.items():
            ret.add_linear(k,v*expr.const)
        for k,v in expr.coefs.items():
            ret.add_linear(k,v*self.const)
        #quadratic
        for k,v in self.coefs.items():
            for k2,v2 in expr.coefs.items():
                ret.add_quadratic(k,k2,v*v2)
        return ret
    
    def add_constant(self,expr):
        self.const+=expr
        return self
        
    def add_linear(self,k,v):
        if k in self.coefs:
            self.coefs[k]+=v
        else: self.coefs[k]=v
        
    def mul_constant(self,coef):
        self.const*=coef
        for k in self.coefs.keys():
            self.coefs[k]*=coef
        return self
    
class NLPQuadraticExpression(NLPFunction):
    def __init__(self,init=None):
        NLPFunction.__init__(self,None,"quadratic")
        self.const=0.0
        self.coefLs=dict()
        self.coefQs=dict()  #only lower triangular
        if init is None:
            return
        elif op.is_float(init):
            self.const=init
        elif isinstance(init,NLPVariable):
            self.coefLs[init.id]=1.0
        elif isinstance(init,NLPLinearExpression):
            self.const=init.const
            self.coefLs=copy.deepcopy(init.coefs)
        elif isinstance(init,NLPQuadraticExpression):
            self.const=init.const
            self.coefLs=copy.deepcopy(init.coefLs)
            self.coefQs=copy.deepcopy(init.coefQs)
        else: assert False
    
    def __add__(self,expr):
        if op.is_float(expr):
            return copy.deepcopy(self).add_constant(expr)
        elif isinstance(expr,NLPVariable):
            return copy.deepcopy(self).add_linear_expression(NLPLinearExpression(expr))
        elif isinstance(expr,NLPLinearExpression):
            return copy.deepcopy(self).add_linear_expression(expr)
        elif isinstance(expr,NLPQuadraticExpression):
            return copy.deepcopy(self).add_quadratic_expression(expr)
        else: return NLPFunctionAdd((self,expr))
        
    def __radd__(self,expr):
        return self.__add__(expr)
        
    def __sub__(self,expr):
        if op.is_float(expr):
            return copy.deepcopy(self).add_constant(-expr)
        elif isinstance(expr,NLPVariable):
            return copy.deepcopy(self).sub_linear_expression(NLPLinearExpression(expr))
        elif isinstance(expr,NLPLinearExpression):
            return copy.deepcopy(self).sub_linear_expression(expr)
        elif isinstance(expr,NLPQuadraticExpression):
            return copy.deepcopy(self).sub_quadratic_expression(expr)
        else: return NLPFunctionSub((self,expr))
        
    def __rsub__(self,expr):
        if op.is_float(expr):
            return NLPQuadraticExpression(expr)-self
        elif isinstance(expr,NLPVariable):
            return NLPQuadraticExpression(expr)-self
        elif isinstance(expr,NLPLinearExpression):
            return NLPQuadraticExpression(expr)-self
        elif isinstance(expr,NLPQuadraticExpression):
            return expr-self
        else: return NLPFunctionSub((expr,self))
        
    def __neg__(self):
        return self*-1.0
        
    def __mul__(self,expr):
        if op.is_float(expr):
            return copy.deepcopy(self).mul_constant(expr)
        else: return NLPFunctionMul((self,expr))
        
    def __rmul__(self,expr):
        return self.__mul__(expr)
    
    def __div__(self,expr):
        if op.is_float(expr):
            return copy.deepcopy(self).mul_constant(1.0/expr)
        else: return NLPFunctionDiv((self,expr))
       
    def __rdiv__(self,expr):
        return NLPFunctionDiv((expr,self))
       
    def __pow__(self,b):
        if b==1:
            return copy.deepcopy(self)
        elif b==2:
            return NLPFunctionPowConstExp((self,),2)
        elif b==0.5:
            return NLPFunctionSqrt(NLPLinearExpression(self))
        else: 
            assert op.is_float(b)
            return NLPFunctionPowConstExp((self,),b)

    def __str__(self):
        ret=str(self.const)
        for k,v in self.coefLs.items():
            ret+=("+" if v>=0 else "")+str(v)+"*C"+str(k)
        for k,v in self.coefQs.items():
            for k2,v2 in v.items():
                ret+=("+" if v2>=0 else "")+str(v2)+"*C"+str(k)+"*C"+str(k2)
        return ret
         
    def __eq__(self,other):
        return NLPFunctionEQ((self,other))

    def __le__(self,other):
        return NLPFunctionLE((self,other))
    
    def __ge__(self,other):
        return NLPFunctionGE((self,other))

    def get_linear(self):
        rows=[]
        vals=[]
        for k,v in self.coefLs.items():
            rows.append(k)
            vals.append(v)
        return rows,vals

    def get_quadratic(self):
        rows=[]
        cols=[]
        vals=[]
        for k,v in self.coefQs.items():
            for k2,v2 in v.items():
                rows.append(k)
                cols.append(k2)
                vals.append(v2)
        return rows,cols,vals

    def vars(self):
        ret=set([id for id in self.coefLs.keys()])
        for k,v in self.coefQs.items():
            for k2,v2 in v.items():
                ret.add(k)
                ret.add(k2)
        return ret
        
    def eval(self,x):
        ret=self.const
        for id in self.coefLs.keys():
            ret+=self.coefLs[id]*x[id]
        for k,v in self.coefQs.items():
            for k2,v2 in v.items():
                ret+=x[k]*x[k2]*v2
        return ret
        
    def getValue(self):
        return self.x
    
    def jac(self,x,res=None,coef=None):
        if res is None:
            res=dict()
        for k,v in self.coefLs.items():
            add_jac(res,k,v if coef is None else v*coef)
        for k,v in self.coefQs.items():
            for k2,v2 in v.items():
                if k==k2:
                    add_jac(res,k,x[k2]*(2*v2 if coef is None else 2*v2*coef))
                else:
                    add_jac(res,k,x[k2]*(v2 if coef is None else v2*coef))
                    add_jac(res,k2,x[k]*(v2 if coef is None else v2*coef))
        return res
    
    def add_linear_expression(self,expr):
        self.const+=expr.const
        for k,v in expr.coefs.items():
            self.add_linear(k,v)
        return self
        
    def sub_linear_expression(self,expr):
        self.const-=expr.const
        for k,v in expr.coefs.items():
            self.add_linear(k,-v)
        return self

    def add_quadratic_expression(self,expr):
        self.const+=expr.const
        for k,v in expr.coefLs.items():
            self.add_linear(k,v)
        for k,v in expr.coefQs.items():
            for k2,v2 in v.items():
                self.add_quadratic(k,k2,v2)
        return self
        
    def sub_quadratic_expression(self,expr):
        self.const-=expr.const
        for k,v in expr.coefLs.items():
            self.add_linear(k,-v)
        for k,v in expr.coefQs.items():
            for k2,v2 in v.items():
                self.add_quadratic(k,k2,-v2)
        return self

    def add_constant(self,expr):
        self.const+=expr
        return self
        
    def add_linear(self,k,v):
        if k in self.coefLs:
            self.coefLs[k]+=v
        else: self.coefLs[k]=v
        
    def add_quadratic(self,k,k2,v):
        if k>k2:
            k,k2=k2,k
        if k in self.coefQs:
            c=self.coefQs[k]
        else: c=self.coefQs[k]=dict()
        if k2 in c:
            c[k2]+=v
        else: c[k2]=v
    
    def mul_constant(self,coef):
        self.const*=coef
        for k in self.coefLs.keys():
            self.coefLs[k]*=coef
        for k,v in self.coefQs.items():
            for k2 in v.keys():
                self.coefQs[k][k2]*=coef
        return self
    
def debug_linear_expression():
    a=NLPVariable(0)
    b=NLPVariable(1)
    c=NLPVariable(2)
    
    print("\nVariableOp")
    print(a+1)
    print(1+a)
    print(a-1)
    print(1-a)
    print(a*2)
    print(2*a)
    print(a/2)
    print(2/a)
    
    print("\nLinearExpressionOp")
    exprL=a*0.5/0.25+b*0.3/0.6-c*0.36
    print(exprL+1)
    print(1+exprL)
    print(exprL-1)
    print(1-exprL)
    print(exprL*2)
    print(2*exprL)
    print(exprL/2)
    print(2/exprL)
    print(exprL)
    print(exprL**1)
    print(exprL*2.0/4.0)
    print(exprL*2.0/4.0-exprL)
    print(exprL*2.0/4.0-exprL-a)
    print(exprL*2.0/4.0+exprL)
    print(exprL*2.0/4.0+exprL+a)
    
    print("\nInequalityOp")
    print(exprL==0.0)
    print(exprL<=0.0)
    print(exprL>=0.0)
    #eval
    debug_jac(exprL,[0.1,0.2,0.3])
    
def debug_quadratic_expression():
    a=NLPVariable(0)
    b=NLPVariable(1)
    c=NLPVariable(2)
    
    print("\nQuadraticExpressionOp")
    exprL=a*0.5/0.25+b*0.3/0.6-c*0.36
    exprL2=exprL*exprL
    print(exprL2+1)
    print(1+exprL2)
    print(exprL2-1)
    print(1-exprL2)
    print(exprL2*2)
    print(2*exprL2)
    print(exprL2/2)
    print(2/exprL2)
    print(exprL)
    print(exprL**2)
    print(exprL2)
    print(exprL2*2.0/4.0)
    print(exprL2*2.0/4.0-exprL-exprL2)
    print(exprL2*2.0/4.0-exprL-exprL2-a)
    print(exprL2*2.0/4.0+exprL+exprL2)
    print(exprL2*2.0/4.0+exprL+exprL2+a)
    #eval
    debug_jac(exprL2,[0.1,0.2,0.3])
    
def debug_function_expression():
    a=NLPVariable(0)
    b=NLPVariable(1)
    c=NLPVariable(2)
    exprL=a*0.5/0.25+b*0.3/0.6-c*0.36
    exprR=a*0.2+b*0.7/0.6-c*0.1
    exprL2=exprL*exprL
    exprL4=exprL2*exprL2
    exprR2=exprR*exprR
    exprR4=exprR2*exprR2
    
    print("\nGeneralOp")
    debug_jac(exprL4,[0.1,0.2,0.3])
    debug_jac(-exprL4,[0.1,0.2,0.3])
    debug_jac(sqrt(exprR4)+exprL4-min_(exprL4,exprR2)+max_(exprL2,exprR4)+cos(exprL2),[0.1,0.2,0.3])
    debug_jac((sqrt(exprR4)+exprL4**3.2)/(min_(exprL4,exprR2)+max_(exprL2,exprR4))+sin(exprL2),[0.1,0.2,0.3])
    
    print("\nVectorOp")
    cat1=NLPFunctionCat((exprL2,exprR4))
    cat2=NLPFunctionCat((exprL4,exprR2))
    debug_jac(cat1,[0.1,0.2,0.3])
    debug_jac(cat2,[0.1,0.2,0.3])
    debug_jac(NLPFunctionWedge((cat1,cat2)),[0.1,0.2,0.3])
    debug_jac(NLPFunctionCat((cat1,cat2)),[0.1,0.2,0.3])
    
if __name__=='__main__':
    debug_linear_expression()
    debug_quadratic_expression()
    debug_function_expression()