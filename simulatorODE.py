from costFunction import OptimalTransportCost
from costFunctionFast import CostDistToRegionQuadraticFast
from reachableTraj import ReachableTrajTx90Robotiq
from reachableTRINA import ReachableTRINA
from polygon2D import Polygon2D
from klampt.vis import gldraw
from klampt import Simulator
import OpenGL.GL as gl
from scene import *
import simulator
import executor

class SimulatorODE(simulator.Simulator):
    EPS_EDGE_LENGTH=0.001
    
    def __init__(self,cost,world,reachableTraj):
        simulator.Simulator.__init__(self,cost,world,reachableTraj)
        #record rigid objects
        self.objects=[]
        self.shapes=[]
        for i in range(world.numRigidObjects()):
            self.objects.append(world.rigidObject(i))
            self.shapes.append(Polygon2D(project_convex_mesh_to_2D(world.rigidObject(i).geometry())))
        self.sim=Simulator(world)
        self.executor=executor.KlamptExecutor(self.sim)
        self.shapePusherBox2D=Polygon2D(project_convex_mesh_to_2D_pusher(self.world.robot(0),self.reachableTraj.get_pusher().eeLid))
     
    def execute(self,dof0,dof,move_pusher_away=False):
        if self.executor is None:
            raise RuntimeError("SimulatorODE cannot execute without executor!")
        else: return self.executor.execute(dof,self.dt)
        
    def simulate(self):
        self.sim.simulate(self.dt)
    
    def display(self):
        self.world.drawGL()
    
    def dRt2dwv(self,R0,t0,R,t):
        v=op.mul(op.sub(t,t0),1/self.dt)
        expw=so3.mul(R,so3.inv(R0))
        w=so3.rotation_vector(expw)
        return w,v
    
    def sync(self):
        self.executor.execute(self.world.robot(0).getConfig(),None)
    
    def pss(self):
        pss=[]
        for obj in self.objects:
            _,t=obj.getTransform()
            pss.append((t[0],t[1]))
        return pss
    
    def css(self):
        return [int(obj.getName()) for obj in self.objects]
    
    def get_state(self):
        state=[]
        for o in self.objects:
            state.append(o.getTransform())
        #robot
        stateRobot=[]
        for i in range(self.world.robot(0).numLinks()):
            l=self.sim.body(self.world.robot(0).link(i))
            stateRobot.append(l.getTransform())
        stateRobot.append(self.world.robot(0).getConfig())
        state.append(stateRobot)
        return state
        
    def set_state(self,state):
        for i in range(len(self.objects)):
            body=self.sim.body(self.objects[i])
            body.setTransform(state[i][0],state[i][1])
            body.setVelocity([0]*3,[0]*3)
            body.enableDynamics(True)
            body.enable(True)
        #robot
        stateRobot=state[-1]
        for i in range(self.world.robot(0).numLinks()):
            l=self.sim.body(self.world.robot(0).link(i))
            l.setTransform(stateRobot[i][0],stateRobot[i][1])
            l.setVelocity([0]*3,[0]*3)
            l.enableDynamics(True)
            l.enable(True)
        self.executor.execute(stateRobot[-1])
        self.world.robot(0).setConfig(stateRobot[-1])
        self.world.robot(0).setVelocity([0]*len(stateRobot[-1]))
        state.append(stateRobot)
        
    def get_shapes(self):
        shapes=[]
        for o,shape in zip(self.objects,self.shapes):
            R,t=o.getTransform()
            shapes.append(shape.transform((t[0],t[1]),atan2_zero2pi(R[1],R[0])))
        return shapes
        
    def stopped(self,thresLinearVelocity=0.001,thresAngularVelocity=0.001):
        for obj in self.objects:
            _,t=obj.getTransform()
            if t[2]<0:
                continue
            angularVelocity,linearVelocity=obj.getVelocity()
            if op.norm(linearVelocity)>thresLinearVelocity:
                return False
            if op.norm(angularVelocity)>thresAngularVelocity:
                return False
        return True
    
    def push(self,s,pss,speed,trajDOF):
        self.set_state(s)
        cost0=self.calc_cost()
        
        #phase1
        traj=[]
        t=0
        while t<trajDOF[1][-1]:
            _,_,dof=self.reachableTraj.search_traj_time(trajDOF,t)
            dtActual=self.executor.execute(self.reachableTraj.get_DOF(dof),self.dt)
            traj.append(self.get_state())
            t+=dtActual
        
        #phase2
        _,_,dof=self.reachableTraj.search_traj_time(trajDOF,trajDOF[1][-1])
        self.executor.execute(self.reachableTraj.get_DOF(dof))
        while not self.stopped():
            self.simulate()
            traj.append(self.get_state())
        
        state1=self.get_state()
        cost1=self.calc_cost()
        return state1,cost0-cost1,traj
    
    def grasp(self,s,oid,t):
        self.set_state(s)
        cost0=self.calc_cost()
        
        obj=self.objects[oid]
        body=self.sim.body(obj)
        body.setVelocity([0,0,0],[0,0,0])
        R,tt=body.getTransform()
        tt=(t[0],t[1],tt[2])
        body.setTransform(R,tt)
        body.enableDynamics(True)
        body.enable(True)
        
        state1=self.get_state()
        cost1=self.calc_cost()
        return state1,cost0-cost1,[state1]
    
    def pusher_shape(self):
        return self.shapePusherBox2D
    
if __name__=='__main__':
    useTRINA=True
    if useTRINA:
        world,table,robot,robot_path=make_scene_TRINA()
        bbt=((-0.25,-0.5,0),(0.25,0.5,0))#table
        create_boxes_same_size(world,50,0.05,0.5,bbt,robot,0.004,"move_to_table")
        cost1=CostDistToRegionQuadraticFast(((-0.75,-0.65),(-0.5,-0.4)))
        cost2=CostDistToRegionQuadraticFast(((-0.75,0.4),(-0.5,0.65)))
    else:
        world,table,robot,robot_path=make_scene_tx90()
        bbt=((0.25,-0.5,0),(0.75,0.5,0))#table
        create_boxes_same_size(world,50,0.05,0.5,bbt,robot,0.004,"move_to_table")
        cost1=CostDistToRegionQuadraticFast(((0.25,-0.35),(0.5,-0.1)))
        cost2=CostDistToRegionQuadraticFast(((0.25,0.1),(0.5,0.35)))
        
    cost=OptimalTransportCost([cost1,cost2],[[12,13],[13,12]])
    if useTRINA:
        reachableTraj=ReachableTRINA(world,table,robot,robot_path,0.3)
    else: reachableTraj=ReachableTrajTx90Robotiq(world,table,robot,robot_path,0.3)
    reachableTraj.build_grid(0.025)
    reachableTraj.drawEE=False
    
    sim=SimulatorODE(cost,world,reachableTraj)
    set_simulator_margin(world,sim.sim,0.001)
    simulator.SimulatorGLVisualizer(world,table,robot,sim,scaleCamera=None).run()