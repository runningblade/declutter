import glfw,cv2
import cv2.aruco as aruco
from visualizer import TABLE_COLOR
import matplotlib.pyplot as plt
import matplotlib as mpl
from OpenGL import GL
from utils import *
import numpy as np
from klampt.sim.simulation import SensorEmulator

class ObjectRenderer:
    def __init__(self, obj, texs):
        self.obj = obj
        #id=int(obj.getName())
        #self.tex = texs[id]
        t = obj.geometry().getTriangleMesh()
        
        #create vertex array
        bb = empty_bb()
        for vi in range(len(t.vertices)//3):
            bb = union_bb(bb, [t.vertices[vi*3+0],t.vertices[vi*3+1],t.vertices[vi*3+2]])
        self.bb = bb
        self.verts=[bb[0][0],bb[0][1],bb[0][2],
                    bb[1][0],bb[0][1],bb[0][2],
                    bb[0][0],bb[1][1],bb[0][2],
                    bb[1][0],bb[1][1],bb[0][2],
                    bb[0][0],bb[0][1],bb[1][2],
                    bb[1][0],bb[0][1],bb[1][2],
                    bb[0][0],bb[1][1],bb[1][2],
                    bb[1][0],bb[1][1],bb[1][2]]
        self.verts=np.array(self.verts).astype(GL.GLfloat)
        
        #create coords
        self.coords=[0.0,0.0,
                     1.0,0.0,
                     0.0,1.0,
                     1.0,1.0,
                     0.0,0.0,
                     1.0,0.0,
                     0.0,1.0,
                     1.0,1.0,]
        self.coords=np.array(self.coords).astype(GL.GLfloat)
        
        #create index array
        self.indices=[0,2,3,
                      0,3,1,
                      4,5,7,
                      4,7,6,
                      0,1,5,
                      0,5,4,
                      2,6,7,
                      2,7,3,
                      1,3,7,
                      1,7,5,
                      0,4,6,
                      0,6,2]
        self.indices=np.array(self.indices).astype(GL.GLuint)
            
    def render(self):
        GL.glEnable(GL.GL_TEXTURE_2D)
        GL.glBindTexture(GL.GL_TEXTURE_2D,self.tex)
        GL.glPolygonMode(GL.GL_FRONT_AND_BACK,GL.GL_FILL)
        
        GL.glColor3f(1,1,1)
        GL.glEnableClientState(GL.GL_VERTEX_ARRAY)
        GL.glEnableClientState(GL.GL_TEXTURE_COORD_ARRAY)
        GL.glVertexPointer(3, GL.GL_FLOAT, 0, self.verts)
        GL.glTexCoordPointer(2, GL.GL_FLOAT, 0, self.coords)
        GL.glDrawElements(GL.GL_TRIANGLES, len(self.indices), GL. GL_UNSIGNED_INT, self.indices)
        GL.glDisableClientState(GL.GL_VERTEX_ARRAY)
        GL.glDisableClientState(GL.GL_TEXTURE_COORD_ARRAY)
        SensorEmulator.report_GL_error()

class SensorEmulator:
    ARUCO_DICT=aruco.DICT_ARUCO_ORIGINAL
    
    def __init__(self, world, table, objs, sim, res=(1024,768), resMarker=512, tableMult=2, forceObjCategory=2, ignoreId=True):
        self.ignoreId=ignoreId
        self.world=world
        self.table=table
        self.objs=objs
        self.sim=sim
        self.markers=forceObjCategory
        for obj in objs:
            self.markers=max(self.markers,int(obj.getName())+1)
        if self.markers>=forceObjCategory:
            #self.markers=forceObjCategory
            self.objsLabeled0=[int(obj.getName()) for obj in objs if obj.appearance().getColor()[2]>0.5]
            self.objsLabeled1=[int(obj.getName()) for obj in objs if obj.appearance().getColor()[2]<0.5]
        self.markers+=4  #additional 4 markers for 4 corners of table
        self.tableMult=tableMult
        
        glfw.init()
        self.res=res
        glfw.window_hint(glfw.VISIBLE, glfw.FALSE)
        self.wnd=glfw.create_window(res[0],res[1],"",None,None)
        glfw.make_context_current(self.wnd)
        self.init_FBO()

        #create marker
        self.texs=[]
        self.resMarker=resMarker
        if False:
            for m in range(self.markers):
                tex=GL.glGenTextures(1)
                img,resMarkerWithBorder=SensorEmulator.get_aruco(m+1,resMarker)
                imgRGB=[]
                for r in range(resMarkerWithBorder):
                    for c in range(resMarkerWithBorder):
                        for d in range(3):
                            imgRGB.append(float(img[r,c])/255)
                GL.glBindTexture(GL.GL_TEXTURE_2D, tex)
                GL.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGB, resMarkerWithBorder, resMarkerWithBorder, 0, GL.GL_RGB, GL.GL_FLOAT, np.array(imgRGB))
                GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
                GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR); 
                GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE);
                GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE);
                SensorEmulator.report_GL_error()
                self.texs.append(tex)
            
        self.objsRender=[ObjectRenderer(obj,self.texs) for obj in objs]
        #camera intrinsic parameter
        self.mtx = None
        self.dist = None
        self.init_TRINA()
        
    def __del__(self):
        glfw.destroy_window(self.wnd)
        glfw.terminate()
        self.proxyCamera=None
        self.proxyMotion=None

    def init_TRINA(self):
        import xmlrpc.client,socket,json
        socket.setdefaulttimeout(10)
        #proxyCamera
        try:
            self.proxyCamera=xmlrpc.client.ServerProxy("http://10.0.242.158:8040/")
        except Exception as e:
            self.proxyCamera=None
        #proxyMotion
        try:
            self.proxyMotion=xmlrpc.client.ServerProxy("http://10.0.242.158:8080/")
            self.proxyMotion.startServer('Physical',['left_limb','right_limb'],'bubonic')
        except Exception as e:
            self.proxyMotion=None

    def init_FBO(self):
        #create a color buffer
        self.TARGET = GL.glGenTextures(1)
        GL.glBindTexture(GL.GL_TEXTURE_2D, self.TARGET)
        GL.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA32F, self.res[0], self.res[1], 0, GL.GL_RGBA, GL.GL_UNSIGNED_BYTE, None)
        GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST);
        GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST); 
        GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE);
        GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE);
        SensorEmulator.report_GL_error()
        
        self.DEPTH = GL.glGenRenderbuffers(1)
        GL.glBindRenderbuffer(GL.GL_RENDERBUFFER, self.DEPTH)
        GL.glRenderbufferStorage(GL.GL_RENDERBUFFER, GL.GL_DEPTH_COMPONENT32, self.res[0], self.res[1])
        SensorEmulator.report_GL_error()

        #create a frame buffer
        self.FBO = GL.glGenFramebuffers(1)
        GL.glBindFramebuffer(GL.GL_FRAMEBUFFER, self.FBO)
        SensorEmulator.report_GL_error()
        
    def print_board(self, board):
        aruco_dict = aruco.Dictionary_get(SensorEmulator.ARUCO_DICT)
        x, y, lenSqr, lenMarker, nImage, wR, alpha = board
        boardDef = aruco.CharucoBoard_create(x, y, lenSqr, lenMarker, aruco_dict)
        
        import pint
        ureg = pint.UnitRegistry()
        imboard = boardDef.draw((self.resMarker*x, self.resMarker*y))
        imgRGB = np.stack((imboard,imboard,imboard),2)
        ix = (x*lenSqr*ureg.meter).to(ureg.inch)._magnitude
        iy = (y*lenSqr*ureg.meter).to(ureg.inch)._magnitude
        
        import PIL
        imgPIL=PIL.Image.fromarray(imgRGB)
        imgPIL.save("../board.png",dpi=(int(imgRGB.shape[0]/ix),int(imgRGB.shape[1]/iy)))
    
    def print_markers(self, prefix="", pageSz=(8.3,11.7), border=32, circle=0):
        import pint
        ureg = pint.UnitRegistry()
        for m in range(self.markers):
            img,resMarkerWithBorder=SensorEmulator.get_aruco(m+1,self.resMarker)
            if circle>0:
                border=img.shape[0]//4
            #channel 1
            imgb1=np.full((img.shape[0]+border*2,img.shape[1]+border*2), 255, dtype=np.uint8)
            if circle>0:
                SensorEmulator.draw_circles(imgb1,circle)
            imgb1[border:img.shape[0]+border,border:img.shape[1]+border]=img
            #channel 2
            imgb2=np.full((img.shape[0]+border*2,img.shape[1]+border*2), 0, dtype=np.uint8)
            if circle>0:
                SensorEmulator.draw_circles(imgb2,circle)
            imgb2[border:img.shape[0]+border,border:img.shape[1]+border]=img
            bb = self.objsRender[0].bb
            sz = bb[1][0]-bb[0][0]
            if m>=self.markers-4:
                sz*=self.tableMult
            sz*=(img.shape[0]+border*2)/img.shape[0]
            dpi=int(imgb1.shape[0]/(sz*ureg.meter).to(ureg.inch)._magnitude)
            
            nrX=0
            while (sz*(nrX+1)*ureg.meter).to(ureg.inch)._magnitude<pageSz[0]:
                ix = (sz*(nrX+1)*ureg.meter).to(ureg.inch)._magnitude
                nrX += 1
            nrY=0
            while (sz*(nrY+1)*ureg.meter).to(ureg.inch)._magnitude<pageSz[1]:
                iy = (sz*(nrY+1)*ureg.meter).to(ureg.inch)._magnitude
                nrY += 1
            imgb1 = np.vstack(tuple([imgb1]*nrY))
            imgb1 = np.hstack(tuple([imgb1]*nrX))
            imgb2 = np.vstack(tuple([imgb2]*nrY))
            imgb2 = np.hstack(tuple([imgb2]*nrX))
            imgRGB = np.stack((imgb1,imgb2,imgb2),2)
                
            import PIL
            imgPIL=PIL.Image.fromarray(imgRGB)
            imgPIL.save("../"+prefix+"marker%d.png"%(m+1),dpi=(dpi,dpi))
    
    def infer_size_marker(self, size_of_marker):
        if size_of_marker is None:
            bb = self.objsRender[0].bb
            size_of_marker = (bb[1][0]-bb[0][0])*7/9
        return size_of_marker
        
    def fetch_sensor(self, extrinsic, intrinsic, drawFunc=None, table_color=TABLE_COLOR):
        GL.glEnable(GL.GL_DEPTH_TEST)
        GL.glDepthFunc(GL.GL_LESS)
        
        #setup render target
        GL.glBindFramebuffer(GL.GL_FRAMEBUFFER, self.FBO)
        GL.glFramebufferTexture(GL.GL_FRAMEBUFFER, GL.GL_COLOR_ATTACHMENT0, self.TARGET, 0)
        GL.glFramebufferRenderbuffer(GL.GL_FRAMEBUFFER, GL.GL_DEPTH_ATTACHMENT, GL.GL_RENDERBUFFER, self.DEPTH)
        SensorEmulator.report_GL_error()
        
        #setup matrix
        target,pos,up=extrinsic
        SensorEmulator.set_view_matrix(target, pos, up)
        fov,zNear,zFar=intrinsic
        SensorEmulator.set_projection_matrix(fov, float(self.res[0])/float(self.res[1]), zNear, zFar)
        
        #render mesh
        r,g,b=table_color
        GL.glClearColor(r,g,b, 1)
        GL.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT)
        if callable(drawFunc):
            drawFunc()
        else:
            self.render_table_corners()
            for obj in self.objsRender:
                self.render_obj(obj)
        SensorEmulator.report_GL_error()
            
        #clear
        GL.glBindFramebuffer(GL.GL_FRAMEBUFFER, 0)
        SensorEmulator.report_GL_error()
        return self.get_color()
    
    def fetch_calibrate_images(self, extrinsic, intrinsic, board, table_color=TABLE_COLOR, show=False):
        aruco_dict = aruco.Dictionary_get(SensorEmulator.ARUCO_DICT)
        x, y, lenSqr, lenMarker, nImage, wR, alpha = board
        boardDef = aruco.CharucoBoard_create(x, y, lenSqr, lenMarker, aruco_dict)
        if not hasattr(self,"boardTex"):
            imboard = boardDef.draw((self.resMarker*x, self.resMarker*y))
            if show:
                plt.imshow(imboard)
                plt.show()
            self.boardTex = GL.glGenTextures(1)
            imgRGB = np.stack((imboard,imboard,imboard),2)
            GL.glBindTexture(GL.GL_TEXTURE_2D, self.boardTex)
            GL.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGB, self.resMarker*x, self.resMarker*y, 0, GL.GL_RGB, GL.GL_FLOAT, np.array(imgRGB))
            GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
            GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR); 
            GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE);
            GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE);
            SensorEmulator.report_GL_error()
            
        images=[]
        for i in range(nImage):
            #translate0
            T0=np.identity(4,np.float64)
            T0[0,3]=-x*lenSqr/2
            T0[1,3]=-y*lenSqr/2
            #rotate along Z
            R0=SensorEmulator.R_w([0,0,random.uniform(-math.pi,math.pi)])
            #rotate to dir
            R1=SensorEmulator.R_a_b((0,0,1),np.subtract(extrinsic[1],extrinsic[0]))
            #rotate random
            R2=SensorEmulator.R_w([random.uniform(-wR,wR),random.uniform(-wR,wR),random.uniform(-wR,wR)])
            #translate1
            T1=np.identity(4,np.float64)
            T1[:3,3]=np.add(np.multiply(extrinsic[0],(1-alpha)),np.multiply(extrinsic[1],alpha))
            #install
            RT=np.matmul(T1,np.matmul(np.matmul(np.matmul(R2,R1),R0),T0))
            #object render
            def drawFunc():
                #enable texture
                GL.glEnable(GL.GL_TEXTURE_2D)
                GL.glBindTexture(GL.GL_TEXTURE_2D,self.boardTex)
                GL.glPolygonMode(GL.GL_FRONT_AND_BACK,GL.GL_FILL)
                #draw the quad
                GL.glColor3f(1,1,1)
                GL.glBegin(GL.GL_QUADS)
                #0
                GL.glTexCoord2f(0,0)
                GL.glVertex3f(*SensorEmulator.transform(RT,(0,0,0)))
                #1
                GL.glTexCoord2f(1,0)
                GL.glVertex3f(*SensorEmulator.transform(RT,(x*lenSqr,0,0)))
                #2
                GL.glTexCoord2f(1,1)
                GL.glVertex3f(*SensorEmulator.transform(RT,(x*lenSqr,y*lenSqr,0)))
                #3
                GL.glTexCoord2f(0,1)
                GL.glVertex3f(*SensorEmulator.transform(RT,(0,y*lenSqr,0)))
                GL.glEnd()
            img=self.fetch_sensor(extrinsic, intrinsic, drawFunc, table_color)
            if show:
                plt.imshow(img)
                plt.show()
            images.append(img)
        return images, boardDef
        
    def calibrate_camera(self, extrinsic, intrinsic, board, table_color=TABLE_COLOR, show=False, images=None, boardDef=None):
        if extrinsic is None and hasattr(self, 'extrinsic'):
            extrinsic=self.extrinsic
        if images is None or boardDef is None:
            print("CALIBRATING AS OPENGL-EMULATOR!")
            images, boardDef = self.fetch_calibrate_images(extrinsic, intrinsic, board, table_color, show)
        elif os.path.exists("calibrated.dat"):
            import pickle
            print("READING CALIBRATED!")
            images,self.mtx,self.dist=pickle.load(open("calibrated.dat","rb"))
            print("mtx=%s dist=%s"%(str(self.mtx),str(self.dist)))
        elif isinstance(images,str) and images=="capture":
            print("CAPTURING!")
            images=[]
            # Open the ZED camera
            cap = cv2.VideoCapture(0)
            if cap.isOpened() == 0:
                exit(-1)
            cap.set(cv2.CAP_PROP_FRAME_WIDTH, self.res[0]*2)
            cap.set(cv2.CAP_PROP_FRAME_HEIGHT,self.res[1])
            while len(images)<board[4]:
                # Get a new frame from camera
                for i in range(10):
                    retval,frame=cap.read()
                # Extract left and right images from side-by-side
                left_right_image=np.split(frame,2,axis=1)
                #cv2.imshow("left", left_right_image[0])
                images.append(left_right_image[0])
                if show:
                    plt.imshow(images[-1])
                    plt.show()
        else: 
            print("CALIBRATING IN REAL-WORLD!")
            
        allCorners, allIds, imsize = SensorEmulator.read_chessboards(images, boardDef)
        ret=SensorEmulator.calibrate_camera_cv2(allCorners, allIds, imsize, boardDef)
        if show:
            for m in images:
                mu=cv2.undistort(m,ret[1],ret[2],None)
                plt.subplot(121)
                plt.imshow(m)
                plt.title("Raw image")
                plt.axis("off")
                plt.subplot(122)
                plt.imshow(mu)
                plt.title("Corrected image")
                plt.axis("off")
                plt.show()
        self.mtx = ret[1]
        self.dist = ret[2]
        
        #pickle
        import pickle
        pickle.dump((images,self.mtx,self.dist),open("calibrated.dat","wb"))
        
    def detect_aruco(self, frame, size_of_marker=None):
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        aruco_dict = aruco.Dictionary_get(SensorEmulator.ARUCO_DICT)
        parameters = aruco.DetectorParameters_create()
        #parameters.cornerRefinementWinSize=100
        #parameters.cornerRefinementMaxIterations=300
        #parameters.cornerRefinementMethod=aruco.CORNER_REFINE_CONTOUR
        corners, ids, rejectedImgPoints = aruco.detectMarkers(gray, aruco_dict, parameters=parameters, cameraMatrix=self.mtx, distCoeff=self.dist)
        size_of_marker = self.infer_size_marker(size_of_marker)
        if self.mtx is not None and self.dist is not None and size_of_marker is not None:
            rvecs, tvecs = [], []
            for c,id in zip(corners,ids):
                rvec, tvec, trash = aruco.estimatePoseSingleMarkers([c], size_of_marker*self.tableMult if id>self.markers-4 else size_of_marker, self.mtx, self.dist)
                rvecs.append(rvec[0,0,:])
                tvecs.append(tvec[0,0,:])
                #print(tvec[0,0,:])
        else: rvecs, tvecs = None, None
        return corners, ids, rvecs, tvecs#, rejectedImgPoints
    
    def detect_aruco_global(self, extrinsic, frame, size_of_marker=None):
        if extrinsic is None and hasattr(self, 'extrinsic'):
            extrinsic=self.extrinsic
        axis = frame.copy()
        corners, ids, rvecs, tvecs = self.detect_aruco(frame, size_of_marker)
        size_of_marker = self.infer_size_marker(size_of_marker)
        ret = [(id[0],SensorEmulator.to_global(extrinsic,rvec,tvec,size_of_marker if id[0]-1<self.markers-4 else 0)) for id,rvec,tvec in zip(ids,rvecs,tvecs)]
        for c,i,rvec,tvec in zip(corners,ids,rvecs,tvecs):
            axis = aruco.drawAxis(axis, self.mtx, self.dist, rvec, tvec, size_of_marker)
        return ret, axis
    
    def debug_detect_aruco(self, extrinsic, intrinsic, show=False, img=None, relax=False):
        if extrinsic is None and hasattr(self, 'extrinsic'):
            extrinsic=self.extrinsic
        if img is None:
            print("DETECTING AS OPENGL-EMULATOR (%s)!"%("Sync" if self.sim is not None else "No-Sync"))
            img = self.fetch_sensor(extrinsic, intrinsic)
        elif isinstance(img,str) and img=="capture":
            cap = cv2.VideoCapture(0)
            if cap.isOpened() == 0:
                exit(-1)
            cap.set(cv2.CAP_PROP_FRAME_WIDTH, self.res[0]*2)
            cap.set(cv2.CAP_PROP_FRAME_HEIGHT,self.res[1])
            for i in range(10):
                retval,frame=cap.read()
            # Extract left and right images from side-by-side
            left_right_image=np.split(frame,2,axis=1)
            #cv2.imshow("left", left_right_image[0])
            img=left_right_image[0]
        elif isinstance(img,str) and img=="TRINA":
            img=self.get_remote_sensor()
        else: 
            print("DETECTING IN REAL-WORLD (%s)!"%("Sync" if self.sim is not None else "No-Sync"))
        globals, axis = self.detect_aruco_global(extrinsic, img)
        if show:
            plt.imshow(axis)
            plt.show()
        
        #check error
        nrDetected=0
        for id,pose in globals:
            if id>self.markers-4:
                #this is table corner, continue
                continue
            else: nrDetected+=1
            minErr=None
            minObj=None
            for obj in self.objs:
                R,t=obj.getTransform()
                err=np.linalg.norm(np.subtract(t[:2],pose[:2,3]))
                if minErr is None or err<minErr:
                    minErr=err
                    minObj=obj
            print("Id=%d, PredictedId=%d, errT=%f"%(int(minObj.getName())+1,id,minErr))
        print("DETECTED %d/%d objects!"%(nrDetected,len(self.objs)))
                
        #check whether this is labeled
        maxId=0
        for obj in self.objs:
            maxId=max(maxId,int(obj.getName())+1)
        #this is labeled
        if maxId==len(self.objs):
            idAll=0
            newGlobals=[]
            for id,pose in globals:
                if id==1:
                    newGlobals.append((self.objsLabeled0[idAll]+1,pose))
                    idAll+=1
            idAll=0
            for id,pose in globals:
                if id==2:
                    newGlobals.append((self.objsLabeled1[idAll]+1,pose))
                    idAll+=1
            globals=newGlobals
        
        #sync-simulator
        if self.sim is not None:
            objCategories = [[] for i in range(self.markers-4)]
            for obj in self.objs:
                objCategories[int(obj.getName())].append(obj)
            for id,pose in globals:
                id-=1
                if id>=self.markers-4:
                    #this is table corner, continue
                    id=-1
                    continue
                elif len(objCategories[id])==0:
                    if self.ignoreId:
                        id=0
                if len(objCategories[id])==0:
                    raise RuntimeError("Incorrect id detected!")
                minObj=objCategories[id][-1]
                R,t=minObj.getTransform()
                theta=math.atan2(pose[1,0],pose[0,0])
                R[0]=math.cos(theta)
                R[1]=math.sin(theta)
                R[3]=-math.sin(theta)
                R[4]=math.cos(theta)
                t=[pose[0,3],pose[1,3],t[2]]
                minObj.setTransform(R,t)
                objCategories[id]=objCategories[id][0:len(objCategories[id])-1]
            self.sim.sync(move_pusher_away=True)
            if relax:
                self.sim.relax()
        
        #draw frame
        if show and (img is None):
            bb = self.objsRender[0].bb
            sz = (bb[1][0]-bb[0][0])
            def drawFunc():
                self.render_table_corners()
                for obj in self.objsRender:
                    self.render_obj(obj)
                GL.glDisable(GL.GL_DEPTH_TEST)
                for id,pose in globals:
                    GL.glLineWidth(0.01*self.res[0])
                    GL.glDisable(GL.GL_TEXTURE_2D)
                    GL.glBegin(GL.GL_LINES)
                    GL.glColor3f(1,0,0)
                    GL.glVertex3f(pose[0,3],pose[1,3],pose[2,3])
                    GL.glVertex3f(pose[0,3]+pose[0,0]*sz,pose[1,3]+pose[1,0]*sz,pose[2,3]+pose[2,0]*sz)
                    GL.glColor3f(0,1,0)
                    GL.glVertex3f(pose[0,3],pose[1,3],pose[2,3])
                    GL.glVertex3f(pose[0,3]+pose[0,1]*sz,pose[1,3]+pose[1,1]*sz,pose[2,3]+pose[2,1]*sz)
                    GL.glColor3f(0,0,1)
                    GL.glVertex3f(pose[0,3],pose[1,3],pose[2,3])
                    GL.glVertex3f(pose[0,3]+pose[0,2]*sz,pose[1,3]+pose[1,2]*sz,pose[2,3]+pose[2,2]*sz)
                    GL.glEnd()
            img = self.fetch_sensor(extrinsic, intrinsic, drawFunc)
            plt.imshow(img)
            plt.show()
       
    def debug_detect_aruco_extrinsic(self, extrinsic, intrinsic, img=None):
        if img is None:
            print("DETECTING AS OPENGL-EMULATOR!")
            img = self.fetch_sensor(extrinsic, intrinsic)
        else: print("DETECTING IN REAL-WORLD!")
        
        #detect cameraPos and supposed globalPos
        cameraPos = [None, None, None, None]
        globals, _ = self.detect_aruco_global(None, img)
        for id,pose in globals:
            id-=1
            if id>=self.markers-4:
                cameraPos[id-(self.markers-4)] = pose
        globalPos = self.render_table_corners()
        
        #solve for global camera transform
        A = np.zeros((3,4))
        B = np.zeros((3,4))
        for i in range(4):
            A[:,i] = cameraPos[i][:3,3]
            B[:,i] = globalPos[i]
        
        Am = A-np.expand_dims(np.mean(A,1),1)
        Bm = B-np.expand_dims(np.mean(B,1),1)
        U, S, Vt = np.linalg.svd(np.matmul(Bm,Am.T))
        SDiag = np.identity(3,dtype=np.float64)
        SDiag[2,2] = -np.linalg.det(np.matmul(U,Vt))
        R = np.matmul(U,np.matmul(SDiag,Vt))
        pos = np.mean(B,1) - np.matmul(R,np.mean(A,1))
        err = np.matmul(R,A)+np.expand_dims(pos,1)-B
        #print(err)
        #exit(-1)
        up = R[:3,1]
        target = R[:3,2]+pos
        extrinsic_detected = (tuple(target.tolist()), tuple(pos.tolist()), tuple(up.tolist()))
        if extrinsic is not None:
            dir=SensorEmulator.normalize(np.subtract(extrinsic[0],extrinsic[1]))
            print("CAMERA POS: (%f,%f,%f) DETECTED: (%f,%f,%f)"%    \
                  (extrinsic[1][0],extrinsic[1][1],extrinsic[1][2],pos[0],pos[1],pos[2]))
            print("CAMERA UP: (%f,%f,%f) DETECTED: (%f,%f,%f)"%    \
                  (extrinsic[2][0],extrinsic[2][1],extrinsic[2][2],up[0],up[1],up[2]))
            print("CAMERA DIR: (%f,%f,%f) DETECTED: (%f,%f,%f)"%    \
                  (dir[0],dir[1],dir[2],R[0,2],R[1,2],R[2,2]))
        return extrinsic_detected
       
    def render_table_corners(self):
        bb = self.objsRender[0].bb
        sz = (bb[1][0]-bb[0][0])*self.tableMult/2
        
        bb = get_object_bb(self.table)
        bb = ((bb[0][0]+sz,bb[0][1]+sz), (bb[1][0]-sz,bb[1][1]-sz))
        
        positions=[]
        for i in range(4):
            GL.glEnable(GL.GL_TEXTURE_2D)
            GL.glBindTexture(GL.GL_TEXTURE_2D,self.texs[self.markers-4+i])
            GL.glPolygonMode(GL.GL_FRONT_AND_BACK,GL.GL_FILL)
            
            x = bb[1][0] if (i&1)==1 else bb[0][0]
            y = bb[1][1] if (i&2)==2 else bb[0][1]
            positions.append([x,y,0])
        
            GL.glColor3f(1,1,1)
            GL.glBegin(GL.GL_QUADS)
            GL.glTexCoord2f(0,0)
            GL.glVertex2f(x-sz,y-sz)
            GL.glTexCoord2f(1,0)
            GL.glVertex2f(x+sz,y-sz)
            GL.glTexCoord2f(1,1)
            GL.glVertex2f(x+sz,y+sz)
            GL.glTexCoord2f(0,1)
            GL.glVertex2f(x-sz,y+sz)
            GL.glEnd()
            SensorEmulator.report_GL_error()
        return positions
     
    def render_obj(self, obj):
        GL.glMatrixMode(GL.GL_MODELVIEW)
        GL.glPushMatrix()
        R,t=obj.obj.getTransform()
        MT = np.matrix([[R[0], R[3], R[6], t[0]],
                        [R[1], R[4], R[7], t[1]],
                        [R[2], R[5], R[8], t[2]],
                        [0.0 , 0.0 , 0.0 , 1.0]])
        GL.glMultMatrixf(MT.flatten('F').astype(GL.GLfloat))
        obj.render()
        GL.glPopMatrix()
        
    def get_color(self):
        GL.glBindTexture(GL.GL_TEXTURE_2D, self.TARGET)
        str = GL.glGetTexImage(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA, GL.GL_UNSIGNED_BYTE)
        GL.glBindTexture(GL.GL_TEXTURE_2D, 0)
        data = np.fromstring(str, dtype=np.uint8)
        return data.reshape((self.res[1],self.res[0],4))

    def get_remote_transform(self):
        if self.proxyCamera is None:
            raise RuntimeError("ProxyCamera is None!")
        R,t=self.proxyCamera.getCameraTransform()
        T=np.identity(4,dtype=np.float64)
        T[:3,0]=R[0:3]
        T[:3,1]=R[3:6]
        T[:3,2]=R[6:9]
        T[:3,3]=t
        
        robot=self.world.robot(0)
        if robot is not None:
            T[:3,3]+=robot.getConfig()[:3]
        return T

    def get_remote_sensor(self):
        import json
        if self.proxyCamera is None:
            raise RuntimeError("ProxyCamera is None!")
        #self.proxyCamera.startCamera()
        img=self.proxyCamera.getPicture()
        img=np.array(json.loads(img),dtype=np.uint8)[:,:,:3]
        return img

    def sync_remote_position(self,reachableTRINA=None):
        if self.proxyMotion is None:
            raise RuntimeError("proxyMotion is None!")
        L=self.proxyMotion.sensedLeftLimbPosition()
        R=self.proxyMotion.sensedRightLimbPosition()
        if reachableTRINA:
            read=L+R
            needCompensate=False
            idle=reachableTRINA.idlePoseL+reachableTRINA.idlePoseR
            DOF_COMPENSATE=[0.0 for i in range(len(idle))]
            for i in range(len(idle)):
                while abs(read[i]+math.pi*2-idle[i])<abs(read[i]-idle[i]):
                    read[i]+=math.pi*2
                    DOF_COMPENSATE[i]+=math.pi*2
                    needCompensate=True
                while abs(read[i]-math.pi*2-idle[i])<abs(read[i]-idle[i]):
                    read[i]-=math.pi*2
                    DOF_COMPENSATE[i]-=math.pi*2
                    needCompensate=True
            if needCompensate:
                reachableTRINA.compensate_DOF(DOF_COMPENSATE)
            reachableTRINA.set_opt_DOF(L+R)
        return read

    def set_remote_idle(self,reachableTRINA,hz=12.,speedInv=3.):
        self.sync_remote_position(reachableTRINA)
        
        import time
        from objectCarry import ObjectCarry
        trajRef=([reachableTRINA.idlePoseL+reachableTRINA.idlePoseR],[0.],ObjectCarry(None,None))
        traj=reachableTRINA.connect_current(trajRef,1/speedInv)
        
        #execute
        duration=1./hz
        currSpeed=1/speedInv/hz
        currTime=0.
        curri=0
        while currTime<traj[1][-1]:
            curri,_,xtarget=reachableTRINA.search_traj_time(traj,currTime,curri)
            print("Left  at %f: %s!"%(0.,xtarget[0:6 ]))
            print("Right at %f: %s!"%(0.,xtarget[6:12]))
            self.proxyMotion.setLeftLimbPositionLinear(xtarget[0:6],duration)
            self.proxyMotion.setRightLimbPositionLinear(xtarget[6:12],duration)
            time.sleep(duration)
            currTime+=currSpeed
        time.sleep(1)

    @staticmethod
    def draw_circles(img,thickness=5):
        radius=thickness
        while radius < img.shape[0]//2:
            cv2.circle(img, (img.shape[0]//2,img.shape[1]//2), radius, (0,0,255), thickness)
            radius+=thickness*2

    @staticmethod
    def R_a_b(a,b):
        R=np.identity(4,np.float64)
        a=np.multiply(a,1/max(np.linalg.norm(a),1e-8))
        b=np.multiply(b,1/max(np.linalg.norm(b),1e-8))
        w=np.cross(a,b)
        c=np.dot(a,b)
        s=np.linalg.norm(w)
        K=np.array([[    0,-w[2], w[1]],
                    [ w[2],    0,-w[0]],
                    [-w[1], w[0],   0]])
        R[:3,:3]=np.eye(3)+K+np.matmul(K,K)*(1-c)/max(s*s,1e-8)
        return R
        
    @staticmethod
    def R_w(w):
        R=np.identity(4,np.float64)
        theta=np.linalg.norm(w)
        w=np.multiply(w,1/max(theta,1e-8))
        K=np.array([[    0,-w[2], w[1]],
                    [ w[2],    0,-w[0]],
                    [-w[1], w[0],   0]])
        R[:3,:3]=np.eye(3)+K*math.sin(theta)+np.matmul(K,K)*(1-math.cos(theta))
        return R
            
    @staticmethod
    def transform(T,v):
        v=T[:3,0]*v[0]+T[:3,1]*v[1]+T[:3,2]*v[2]+T[:3,3]
        return tuple(v.tolist())
        
    @staticmethod
    def read_chessboards(images, boardDef):
        """
        Charuco base pose estimation.
        """
        aruco_dict = aruco.Dictionary_get(SensorEmulator.ARUCO_DICT)
        print("POSE ESTIMATION STARTS:")
        allCorners = []
        allIds = []
        decimator = 0
    
        for idm,im in enumerate(images):
            print("=> Processing image %d"%idm)
            if isinstance(im,str):
                frame = cv2.imread(im)
            else: frame=im
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            res = cv2.aruco.detectMarkers(gray, aruco_dict)
    
            if len(res[0])>0:
                res2 = cv2.aruco.interpolateCornersCharuco(res[0],res[1],gray,boardDef)
                if res2[1] is not None and res2[2] is not None and len(res2[1])>6 and decimator%1==0:
                    allCorners.append(res2[1])
                    allIds.append(res2[2])
    
            decimator+=1
    
        imsize = gray.shape
        print("FINISHED!")
        return allCorners,allIds,imsize
        
    @staticmethod
    def calibrate_camera_cv2(allCorners, allIds, imsize, boardDef):
        """
        Calibrates the camera using the dected corners.
        """
        print("CAMERA CALIBRATION")
        cameraMatrixInit = np.array([[ 2000.,    0., imsize[0]/2.],
                                     [    0., 2000., imsize[1]/2.],
                                     [    0.,    0.,           1.]])
    
        distCoeffsInit = np.zeros((5,1))
        flags = (cv2.CALIB_USE_INTRINSIC_GUESS + cv2.CALIB_RATIONAL_MODEL)
        (ret, camera_matrix, distortion_coefficients0,
         rotation_vectors, translation_vectors,
         stdDeviationsIntrinsics, stdDeviationsExtrinsics,
         perViewErrors) = cv2.aruco.calibrateCameraCharucoExtended(
                          charucoCorners=allCorners,
                          charucoIds=allIds,
                          board=boardDef,
                          imageSize=imsize,
                          cameraMatrix=cameraMatrixInit,
                          distCoeffs=distCoeffsInit,
                          flags=flags,
                          criteria=(cv2.TERM_CRITERIA_EPS & cv2.TERM_CRITERIA_COUNT, 10000, 1e-9))
        print("FINISHED!")
        return ret, camera_matrix, distortion_coefficients0, rotation_vectors, translation_vectors
        
    @staticmethod
    def to_global(extrinsic, rvec, tvec, tableHint=None):
        #camera space
        TCam=SensorEmulator.R_w(rvec)
        TCam[:3,3]=tvec
        
        #camera to world
        TGlobal=np.identity(4,np.float64)
        if extrinsic is not None and isinstance(extrinsic,tuple):
            TGlobal[:3,3]=extrinsic[1]
            TGlobal[:3,1]=SensorEmulator.normalize(extrinsic[2])
            TGlobal[:3,2]=SensorEmulator.normalize(np.subtract(extrinsic[0],extrinsic[1]))
            TGlobal[:3,0]=-SensorEmulator.normalize(np.cross(TGlobal[:3,1],TGlobal[:3,2]))
        else: TGlobal=extrinsic
        
        ret = np.matmul(TGlobal,TCam)
        #tableHint
        #(extrinsic[1]*(1-alpha)+ret[:3,3]*alpha)[2]=tableHint
        if extrinsic is not None and isinstance(extrinsic,tuple):
            if tableHint is not None:
                alpha=(extrinsic[1][2]-tableHint)/(extrinsic[1][2]-ret[2,3])
                ret[:3,3]=np.add(np.multiply(extrinsic[1],1-alpha),np.multiply(ret[:3,3],alpha))
        return ret

    @staticmethod
    def normalize(vec):
        return np.array(vec)/np.linalg.norm(vec)

    @staticmethod
    def set_view_matrix(target, pos, up):
        F = np.array(target) - np.array(pos)
        f = SensorEmulator.normalize(F)
        U = SensorEmulator.normalize(up)
        s = SensorEmulator.normalize(np.cross(f, U))
        u = SensorEmulator.normalize(np.cross(s, f))
        M = np.matrix(np.identity(4))
        M[:3,:3] = np.vstack([s,u,-f])
        T = np.matrix([[1.0, 0.0, 0.0, -pos[0]],
                       [0.0, 1.0, 0.0, -pos[1]],
                       [0.0, 0.0, 1.0, -pos[2]],
                       [0.0, 0.0, 0.0, 1.0]])
        MT = M*T
        GL.glMatrixMode(GL.GL_MODELVIEW)
        GL.glLoadMatrixf(MT.flatten('F').astype(GL.GLfloat))

    @staticmethod
    def set_projection_matrix(fov, ar, zNear, zFar):
        s = 1.0 / math.tan(math.radians(fov)/2.0)
        zNear *= math.cos(math.radians(fov)/2.0)
        
        sx, sy = s / ar, s
        zz = (zFar+zNear) / (zNear-zFar)
        zw = (2*zFar*zNear) / (zNear-zFar)
        P = np.matrix([[sx,0,0,0],
                       [0,sy,0,0],
                       [0,0,zz,zw],
                       [0,0,-1,0]])
        GL.glMatrixMode(GL.GL_PROJECTION)
        GL.glLoadMatrixf(P.flatten('F').astype(GL.GLfloat))
    
    @staticmethod
    def get_aruco(i, res, border=1):
        aruco_dict = aruco.Dictionary_get(SensorEmulator.ARUCO_DICT)
        border *= res//7
        img = aruco.drawMarker(aruco_dict, i, res)
        imgWithBorder = np.full((res+border*2, res+border*2), 255, dtype=np.uint8)
        imgWithBorder[border:res+border,border:res+border] = img
        return imgWithBorder, res+border*2

    @staticmethod
    def report_GL_error():
        ret=GL.glGetError()
        if ret!=GL.GL_NO_ERROR:
            raise RuntimeError("OpenGL Error: %s"%str(ret))

if __name__=='__main__':
    from scene import *
    #random.seed(0)
    useZED2=True
    world,table,robot,robot_path=make_scene_TRINA_CALIBRATED()
    bbt=((-0.25,-0.5,0),(0.25,0.5,0))#table
    objs=create_boxes_same_size(world,30,0.05,0.5,bbt,robot,0.004,"move_to_table")
    
    if True:
        from simulatorBox2D import SimulatorBox2D
        from reachableTRINA import ReachableTRINA
        from costFunctionFast import CostDistToRegionQuadraticFast,OptimalTransportCost
        cost1=CostDistToRegionQuadraticFast(((-0.75,-0.65),(-0.5,-0.4)))
        cost2=CostDistToRegionQuadraticFast(((-0.75,0.4),(-0.5,0.65)))
        cost=OptimalTransportCost([cost1,cost2],[[12,13],[13,12]])
        reachableTraj=ReachableTRINA(world,table,robot,robot_path,0.3)
        sim=SimulatorBox2D(cost,world,reachableTraj)
        reachableTraj.build_grid(0.025)
    else: sim=None
    
    board=(4, 4, 0.05, 0.04, 20, 0.5, 0.8)
    extrinsic=((0,0,0),(0,0,2),(1,0,0))
    intrinsic=(45,0.01,3)
    res=(4416//2,1242)
    sensor=SensorEmulator(world,table,objs,sim,res=res)
    sensor.set_remote_idle(reachableTraj)
    del sensor
    exit(-1)
    
    #sensor
    if False:
        img = sensor.fetch_sensor(extrinsic, intrinsic)
        plt.imshow(img)
        plt.show()
        
    #print
    if False:
        sensor.print_markers("circle_",circle=15)
        sensor.print_markers()
        #sensor.print_board(board)
        plt.show()
        del sensor
        exit(-1)
    
    #calibrate
    if useZED2:
        fx=1055.28
        fy=1054.69
        cx=1126.61
        cy=636.138
        k1=-0.0426844
        k2=0.0117943
        k3=-0.00548354
        p1=0.000242741
        p2=-0.000475926
        sensor.mtx = np.array([[fx,0,cx], [0,fy,cy], [0,0,1]])
        sensor.dist = np.array([k1,k2,p1,p2,k3])
    else:
        aruco_dict = aruco.Dictionary_get(SensorEmulator.ARUCO_DICT)
        boardDef = aruco.CharucoBoard_create(5, 5, 0.0335, 0.0335*4/5, aruco_dict)
        sensor.calibrate_camera(extrinsic, intrinsic, board, show=False, images=None, boardDef=boardDef)
    
    #calibrate global
    if useZED2:
        extrinsic = sensor.get_remote_transform()
        print("DETECTED EXTRINSIC: %s"%str(extrinsic))
    else:
        extrinsic_detected = sensor.debug_detect_aruco_extrinsic(extrinsic, intrinsic)
        print("DETECTED EXTRINSIC: %s"%str(extrinsic_detected))
    
    #debug
    from simulator import SimulatorGLVisualizer
    sensor.debug_detect_aruco(extrinsic, intrinsic, show=True, img="TRINA" if useZED2 else None)
    SimulatorGLVisualizer(world,table,robot,sim,scaleCamera=None).run()
    del sensor