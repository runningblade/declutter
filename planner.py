from klampt import RigidObjectModel,Geometry3D,TriangleMesh,GeometricPrimitive
from klampt.vis import gldraw
import OpenGL.GL as gl
from scene import *
from utils import *
import simulator
import random

class Action:
    def __init__(self,sim,speed,speedMove):
        self.trajDOFFromCurrent=None
        self.trajDOF=None
        self.idss=None
        self.sim=sim
        self.speed=speed
        self.speedMove=speedMove
        
        #state of execution
        self.currTime=0.0
        self.curri=0
        self.released=False

    def write(self):
        import pickle
        outfile=open('tmpActionError.dat','wb')
        pickle.dump((self.idss,self.speed,self.speedMove),outfile)
        
    def test_error(self):
        import pickle
        assert os.path.exists('tmpActionError.dat')
        infile=open('tmpActionError.dat','rb')
        self.idss,self.speed,self.speedMove=pickle.load(infile)
        infile.close()
        print("Action Idss: ",self.idss)
        if self.sim is not None:
            self.sim.reachableTraj.gen_traj(self.idss,self.speed,self.speedMove)

    def display(self,margin,drawShape):
        pass

    def test(self,state0,parallel=False):
        self.state1=state0
        self.costReduce=0.0
        self.traj=[]
        
    def transit_cost(self):
        id0=self.sim.set_state(self.state0)
        idss0=op.mul(self.idss[0],self.sim.reachableTraj.res)
        return op.norm(op.sub(id0,idss0))
    
    def transfer_cost(self):
        ret=0.
        for i in range(1,len(self.idss)):
            ret+=op.mul(op.norm(op.sub(self.idss[i],self.idss[i-1])),self.sim.reachableTraj.res)
        return ret
        
    def apply_state(self):
        self.sim.set_state(self.state1)

    def execute_sensor(self,sensor,hz=12.,speedInv=30.):
        import time
        from sensor import SensorEmulator
        if sensor.proxyMotion is None:
            raise RuntimeError("proxyMotion is None!")
        if self.trajDOFFromCurrent[2].grasp is not None:
            sensor.proxyMotion.openLeftRobotiqGripper() #reset
        #execute
        duration=1./hz
        currSpeed=1/speedInv/hz
        self.currTime=0.
        self.curri=0
        self.graspStage=0
        force0=0
        while self.currTime<self.trajDOFFromCurrent[1][-1]:
            self.curri,_,xtarget=self.sim.reachableTraj.search_traj_time(self.trajDOFFromCurrent,self.currTime,self.curri)
            print("Left  at %f: %s!"%(self.currTime,xtarget[0:6 ]))
            print("Right at %f: %s!"%(self.currTime,xtarget[6:12]))
            sensor.proxyMotion.setLeftLimbPositionLinear(xtarget[0:6],duration)
            sensor.proxyMotion.setRightLimbPositionLinear(xtarget[6:12],duration)
            time.sleep(duration)
            if self.trajDOFFromCurrent[2].grasp is not None:
                #additional safety check based on sensed force
                #update grasp state
                graspStageLast=self.graspStage
                self.sim.reachableTraj.set_opt_DOF(xtarget)
                ee=self.sim.reachableTraj.get_gripper().global_pos()[1]
                if ee[2]>0.32:
                    force0=abs(sensor.proxyMotion.sensedLeftEEWrench('global')[2])
                dforce=abs(sensor.proxyMotion.sensedLeftEEWrench('global')[2])-force0
                if self.graspStage==0:
                    cond=dforce>1.
                elif self.graspStage==1:
                    cond=dforce>2.
                else: cond=False
                self.graspStage=self.trajDOFFromCurrent[2].update(self.curri,self.graspStage,cond)
                if graspStageLast<self.graspStage:
                    if self.graspStage==1:
                        sensor.proxyMotion.closeLeftRobotiqGripper()
                        time.sleep(3)   #wait a while for gripper to stabilize
                    else: 
                        sensor.proxyMotion.openLeftRobotiqGripper()
                        time.sleep(3)   #wait a while for gripper to stabilize
                #if there is sensed force, we should not push the gripper further
                if cond:
                    self.skip_grasp_traj(currSpeed)
            self.currTime+=currSpeed
        time.sleep(1)

    def execute(self):
        if self.trajDOFFromCurrent is None:
            self.sim.reachableTraj.sync()
            self.trajDOFFromCurrent=self.sim.reachableTraj.connect_current(self.trajDOF,self.speed)
            #state of execution
            self.currTime=0.0
            self.curri=0
            self.graspStage=0
        _,_,xtarget0=self.sim.reachableTraj.search_traj_time(self.trajDOFFromCurrent,self.currTime,self.curri)
        self.curri,_,xtarget=self.sim.reachableTraj.search_traj_time(self.trajDOFFromCurrent,self.currTime+self.sim.dt,self.curri)
        self.currTime+=self.sim.execute(self.sim.reachableTraj.get_DOF(xtarget0),self.sim.reachableTraj.get_DOF(xtarget),self.trajDOFFromCurrent[2].grasp is not None)
        if self.trajDOFFromCurrent[2].grasp is not None:
            if self.graspStage==0:
                coll=WorldCollider(self.sim.world)
                cond=len([c for c in coll.robotObjectCollisions(self.trajDOFFromCurrent[2].robot(),self.trajDOFFromCurrent[2].grasp)])>0
            elif self.graspStage==1:
                coll=WorldCollider(self.sim.world)
                cond=len([c for c in coll.objectTerrainCollisions(self.trajDOFFromCurrent[2].grasp)])>0
            else: 
                cond=False
            self.graspStage=self.trajDOFFromCurrent[2].update(self.curri,self.graspStage,cond)
            #if there is sensed force, we should not push the gripper further
            if cond:
                self.skip_grasp_traj(self.sim.dt)
        return self.finished()
    
    def finished(self):
        if self.trajDOFFromCurrent is None:
            return True
        elif self.currTime>=self.trajDOFFromCurrent[1][-1]:
            #self.trajDOFFromCurrent=None
            self.currTime=0.0
            self.curri=0
            self.released=False
            return True
        else: return False

    def skip_grasp_traj(self,dt):
        _,_,xtarget0=self.sim.reachableTraj.search_traj_time(self.trajDOFFromCurrent,self.currTime,self.curri)
        self.sim.reachableTraj.set_opt_DOF(xtarget0)
        ee0=self.sim.reachableTraj.get_gripper().global_pos()[1]
        
        while True:
            self.currTime+=dt
            self.curri,_,xtarget0=self.sim.reachableTraj.search_traj_time(self.trajDOFFromCurrent,self.currTime,self.curri)
            self.sim.reachableTraj.set_opt_DOF(xtarget0)
            ee1=self.sim.reachableTraj.get_gripper().global_pos()[1]
            if ee1[2]>ee0[2]:
                break

class CompositeAction(Action):
    def __init__(self,subActions):
        Action.__init__(self,subActions[0].sim,subActions[0].speed,subActions[0].speedMove)
        self.subActions=subActions
    
    def test(self,state0,parallel=False):
        self.state0=state0
        self.state1=state0
        self.costReduce=0.0
        self.traj=[]
        for action in self.subActions:
            action.test(self.state1,parallel)
            self.state1=action.state1
            self.costReduce+=action.costReduce
            self.traj+=action.traj

    def execute(self):
        for i,action in enumerate(self.subActions):
            if not action.finished():
                ret=action.execute()
                return ret and i==len(self.subActions)-1
        return True
    
    def finished(self):
        for action in self.subActions:
            if not action.finished():
                return False
        return True

class Planner:
    def __init__(self,sim):
        self.drawBB=True
        self.sim=sim
        self.reachableTraj=sim.reachableTraj
    
    def keyboardfunc(self,c,x,y):
        pass
    
    def display(self,margin):
        pass
    
    def plan(self,debugInfo):
        raise NotImplementedError("This is abstract super class, use subclass!")
            
    def branch(self,debugInfo):
        raise NotImplementedError("This planner does not support branching!")
            
    def plan_trajectory(self,debugInfo,fullHorizon=True,thres=0.0001,minLingerFrm=100):
        traj=[]
        ops=[]
        while True:
            opBest=self.plan(debugInfo)
            if opBest is not None:
                ops.append(opBest)
                opBest.apply_state()
                traj+=opBest.traj
                
                #for visualization, we ensure that traj of each op is at least minLingerFrm
                nrFrm=len(opBest.traj)
                while nrFrm<minLingerFrm:
                    traj.append(traj[-1])
                    nrFrm+=1
                    
                #terminate?
                if opBest.costReduce<thres or not fullHorizon:
                    break
            else:
                if debugInfo:
                    print("Planning finished!")
                break
        return traj,ops
    
    def get_push_actions(self):
        return None
            
class PlannerGLVisualizer(simulator.SimulatorGLVisualizer):
    def __init__(self,world,table,robot,planner,scaleCamera=5.0,fullHorizon=False):
        simulator.SimulatorGLVisualizer.__init__(self,world,table,robot,planner.sim,scaleCamera=scaleCamera)
        self.planner=planner
        self.drawCostDist=False
        self.plan=False
        self.planNonStop=True
        self.playSpeed=10
        self.debugInfo=True
        self.fullHorizon=fullHorizon
        self.record=False
        self.bbt=None   #bound of objects used for reset scene
        self.actionId=0
        
        #sense
        self.sense=None
        self.sensed=False
        self.sense_once=False
        
    def cost_to_geometry(self,cost,extrude=0.001):
        if hasattr(cost,"costInners"):
            geom=Geometry3D()
            geom.setGroup()
            for i,c in enumerate(cost.costInners):
                geom.setElement(i,self.cost_to_geometry(c))
        elif hasattr(cost,"pss"):
            geom=Geometry3D()
            if len(cost.pss)>1:
                geom.setGroup()
                for i,p in enumerate(cost.pss):
                    geom.setElement(i,self.cost_to_geometry(p.vss))
            else:
                geom=Geometry3D()
                tm=TriangleMesh()
                #vss
                vss=cost.pss[0].vss
                for v in vss:
                    for vi in [v[0],v[1],-extrude]:
                        tm.vertices.append(vi)
                for v in vss:
                    for vi in [v[0],v[1], extrude]:
                        tm.vertices.append(vi)
                #edge
                nrV=len(vss)
                for i in range(nrV):
                    for ii in [i,(i+1)%nrV,((i+1)%nrV)+nrV]:
                        tm.indices.append(ii)
                    for ii in [i,((i+1)%nrV)+nrV,i+nrV]:
                        tm.indices.append(ii)
                #bottom
                for i in range(1,nrV-1):
                    for ii in [0,i+1,i]:
                        tm.indices.append(ii)
                    for ii in [0,i,i+1]:
                        tm.indices.append(ii+nrV)
                geom.setTriangleMesh(tm)
        return geom
        
    def render_action(self,action):
        #find path
        if not os.path.exists(DATA_PATH+"/tmpAnimation"):
            return
        renderPath=DATA_PATH+("/tmpAnimation/action%d"%self.actionId)
        os.mkdir(renderPath)
        self.actionId+=1
        
        #save transforms
        transforms=[self.world.rigidObject(i).getTransform() for i in range(self.world.numRigidObjects())]
        
        #render action
        j=0
        state0=self.sim.get_state()
        while action is not None:
            self.sim.set_state(action.state1)
            self.sim.sync_back()
            self.render(renderPath+("/action%d.png"%j),renderPath+("/action%d.pov"%j),action)
            
            if action is not None and hasattr(action,"next"):
                action=action.next
            else: break
            j+=1
        self.sim.set_state(state0)
        
        #load transforms
        for i in range(self.world.numRigidObjects()):
            R,t=transforms[i]
            self.world.rigidObject(i).setTransform(R,t)
        
    def render(self,fn,tfn,renderAction=None):
        from klamptToPovray import klampt_to_povray,add_light
        import vapory.vapory as vp
        if not hasattr(self,"properties"):
            self.properties={}
            #hide floor
            self.properties["floor"]={"hide":True}
            #set table material
            self.properties["table"]={"transient":False}
            self.properties["table"]["finish"]=vp.Finish('ambient',0.,'diffuse',.5,'specular',.15)
            self.properties["table"]["normal"]=vp.Normal('granite',0.2,'warp {turbulence 1}','scale',.25)
            #set block material
            self.properties["0"]=self.properties["1"]={"transient":True}
            self.properties["0"]["finish"]=self.properties["1"]["finish"]=  \
            vp.Finish('ambient',0.30,'brilliance',3,'diffuse',0.4,'metallic','specular',0.70,'roughness',1/60.,'reflection',.25)
            #avoid loading robots again and again
            for l in range(self.world.robot(0).numLinks()):
                self.properties[self.world.robot(0).link(l).getName()]={"transient":False}
            #other visual objects module
            self.worldVis=WorldModel()
            #grasp
            grasp=self.worldVis.makeRigidObject("Grasp")
            grasp.appearance().setColor(0.,1.,0.,.5)
            self.properties["Grasp"]={"hide":True}
            self.properties["Grasp"]["radius"]=0.01
            self.properties["Grasp"]["finish"]=vp.Finish('ambient',0.,'diffuse',.5,'specular',.15)
            self.properties["Grasp"]["normal"]=vp.Normal('granite',0.2,'warp {turbulence 1}','scale',.25)
            self.properties["Grasp"]["arrowLength"]=0.1
            self.properties["Grasp"]["arrowRadius"]=0.05
            #push
            push=self.worldVis.makeRigidObject("Push")
            push.appearance().setColor(1.,1.,0.,.5)
            self.properties["Push"]={"hide":True}
            self.properties["Push"]["radius"]=0.01
            self.properties["Push"]["finish"]=vp.Finish('ambient',0.,'diffuse',.5,'specular',.15)
            self.properties["Push"]["normal"]=vp.Normal('granite',0.2,'warp {turbulence 1}','scale',.25)
            self.properties["Push"]["arrowLength"]=0.1
            self.properties["Push"]["arrowRadius"]=0.05
            #cost module
            rigid=self.worldVis.makeRigidObject("Cost")
            rigid.geometry().set(self.cost_to_geometry(self.sim.cost))
            rigid.appearance().setColor(1.,1.,1.,.5)
            self.properties["Cost"]={"transient":False}
            self.properties["Cost"]["finish"]=vp.Finish('ambient',0.,'diffuse',.5,'specular',.15)
            self.properties["Cost"]["normal"]=vp.Normal('granite',0.2,'warp {turbulence 1}','scale',.25)
            self.properties["visualObjects"]=[grasp,push,rigid]
            #light
            area=0.
            light_dist=5.
            add_light(self.properties,pos=[0,0,light_dist],tgt=[0.,0.,0.],color=[1.,1.,1.],area=area)
            add_light(self.properties,pos=[ light_dist,0,light_dist],tgt=[0.,0.,0.],color=[.25,.25,.25],area=area)
            add_light(self.properties,pos=[0, light_dist,light_dist],tgt=[0.,0.,0.],color=[.25,.25,.25],area=area)
            add_light(self.properties,pos=[-light_dist,0,light_dist],tgt=[0.,0.,0.],color=[.25,.25,.25],area=area)
            add_light(self.properties,pos=[0,-light_dist,light_dist],tgt=[0.,0.,0.],color=[.25,.25,.25],area=area)
        #draw grasp action
        from graspPlannerDiscrete import GraspAction
        if (renderAction is not None) and isinstance(renderAction,GraspAction):
            grasp=self.worldVis.rigidObject("Grasp")
            prim=GeometricPrimitive()
            prim.setSegment(renderAction.from_position(0.06),renderAction.to_position(0.06))
            grasp.geometry().setGeometricPrimitive(prim)
            self.properties["Grasp"]["hide"]=False
        else: self.properties["Grasp"]["hide"]=True
        #draw push action
        from pushPlannerDiscrete import PushAction
        if (renderAction is not None) and isinstance(renderAction,PushAction):
            push=self.worldVis.rigidObject("Push")
            prim=GeometricPrimitive()
            prim.setSegment(renderAction.from_position(0.06),renderAction.to_position(0.06))
            push.geometry().setGeometricPrimitive(prim)
            self.properties["Push"]["hide"]=False
        else: self.properties["Push"]["hide"]=True
        #draw all push actions
        if (renderAction is not None) and isinstance(renderAction,list):
            i=0
            for action in renderAction:
                name="Push%d"%i
                try:
                    push=self.worldVis.rigidObject(name)
                except:
                    push=self.worldVis.makeRigidObject(name)
                push.appearance().setColor(1.,1.,0.,.5)
                prim=GeometricPrimitive()
                prim.setSegment(action.from_position(0.06),action.to_position(0.06))
                push.geometry().setGeometricPrimitive(prim)
                self.properties[name]={"hide":False}
                self.properties[name]["radius"]=0.01
                self.properties[name]["finish"]=vp.Finish('ambient',0.,'diffuse',.5,'specular',.15)
                self.properties[name]["normal"]=vp.Normal('granite',0.2,'warp {turbulence 1}','scale',.25)
                self.properties["visualObjects"].append(push)
                i+=1
        #robot
        for r in range(self.world.numRobots()):
            for i in range(self.world.robot(r).numLinks()):
                self.properties[self.world.robot(r).link(i).getName()]["hide"]=(renderAction is not None)
        #output
        self.properties["tempfile"]=tfn
        self.properties["outfile"]=fn
        ret=klampt_to_povray(self,self.world,self.properties)
        if fn is not None:
            print('Saving screen to %s'%fn)
        else: print('Saving screen to %s'%tfn)
        return ret
        
    def keyboardfunc(self,c,x,y):
        simulator.SimulatorGLVisualizer.keyboardfunc(self,c,x,y)
        self.planner.keyboardfunc(c,x,y)
        if c==b'p':
            self.plan=not self.plan
        elif c==b'o':
            self.planNonStop=not self.planNonStop
        elif c==b'[':
            self.playSpeed=max(1,self.playSpeed-1)
            print("playSpeed=%d"%self.playSpeed)
        elif c==b']':
            self.playSpeed=min(self.playSpeed+1,100)
            print("playSpeed=%d"%self.playSpeed)
        elif c==b'\\':
            self.currTime=0
        elif c==b'r':
            if hasattr(self,"currTraj") and self.fullHorizon:
                delattr(self,"currTraj")
            if not hasattr(self,"currTraj") and self.bbt is not None:
                bss=create_boxes(self.world,None,None,self.bbt,self.robot,0.004,"move_to_table")
                for b in bss:
                    R,t=b.getTransform()
                    body=self.sim.sim.body(b)
                    body.setTransform(R,t)
                    body.setVelocity((0,0,0),(0,0,0))
                self.sim.sync()
        elif c==b't' or c==b'y':
            self.plan=not hasattr(self,"currTraj")
            self.record=not self.record
            self.currTime=0
            def op():
                return self.record
            self.render_animation("plan.AVI",op,dur=0.1,render_path=DATA_PATH+"/tmpAnimation" if c==b'y' else None)
        elif c==b'z':
            self.drawCost=not self.drawCost
        elif c==b'x':
            self.drawCostDist=not self.drawCostDist
        elif c==b'v':
            self.render("screenshot.png",DATA_PATH+"/tmpAnimation/script.pov")
        elif c==b'-':
            import pickle
            pickle.dump(self.sim.get_state(),open(DATA_PATH+"/state.dat",'wb'))
        elif c==b'=':
            import pickle
            state=pickle.load(open(DATA_PATH+"/state.dat",'rb'))
            self.sim.set_state(state)
        elif c==b'n':
            if hasattr(self,"sensor"):
                del self.sensor
            exit(-1)
        elif c==b'm':
            self.sync_sensor(motion=False)
        elif c==b'/':
            self.render("allPush.png",DATA_PATH+"/allPush.pov",self.planner.get_push_actions())
            
    def keyboardupfunc(self,c,x,y):
        simulator.SimulatorGLVisualizer.keyboardupfunc(self,c,x,y)
        
    def display(self):
        if not self.simOnly:
            self.world.drawGL()
        self.sim.display()
        #planner
        self.planner.display(0.001)
        if hasattr(self,"currTraj"):
            self.sim.set_state(self.currTraj[self.currTime])
        #cost
        if self.drawCost:
            if self.drawCostDist:
                self.sim.cost.draw(self.sim.pss(),category=self.sim.css())
            else: self.sim.cost.draw()
            
    def idle(self):
        self.planner.sim.sync()
        self.handle_camera()
        self.update_animation()
        if self.simOnly:
            if hasattr(self,"currTraj"):
                self.currTime=self.currTime+self.playSpeed
                if self.currTime>=len(self.currTraj):
                    if not self.fullHorizon:
                        delattr(self,"currTraj")
                    self.record=False
                    self.currTime=len(self.currTraj)-1
            elif self.plan: 
                #we are only using simulator not robot hardware, 
                #so we plan the entire trajectory, assuming perfect sensing and execution
                currTraj,currOps=self.planner.plan_trajectory(self.debugInfo,fullHorizon=self.fullHorizon)
                if currTraj is not None and len(currTraj)>0:
                    self.currTraj=currTraj
                    self.currOps=currOps
                    self.currTime=0
                self.plan=self.planNonStop and len(currTraj)>0
        else:
            if hasattr(self,"currOp"):
                for step in range(self.playSpeed):
                    
                    finished=self.currOp.execute()
                    if finished:
                        if hasattr(self,"sensor"):
                            try:
                                self.currOp.execute_sensor(self.sensor)
                            except Exception as e:
                                print("Motion Error: %s"%str(e))
                        #self.sync_sensor()
                        break
                if finished:
                    delattr(self,"currOp")
                    self.sim.sync()
            elif self.plan:
                #we are testing on robot hardware,
                #so we plan the current push move and execute
                self.sync_sensor()
                self.sim.sync()
                x_saved=self.sim.reachableTraj.get_opt_DOF(self.robot.getConfig())
                currOp=self.planner.plan(self.debugInfo)
                self.sim.reachableTraj.set_opt_DOF(x_saved)
                self.sim.relax_onestep(True)    #it seems that klampt stores the last state of robot config and renders that version, this command avoids this behavior
                if currOp is not None:
                    self.render_action(currOp)
                    self.currOp=currOp
                else: 
                    self.record=False
                    print("Planning finished!")
                self.plan=self.planNonStop and currOp is not None
            else:
                self.sim.relax_onestep()
                
    def sync_sensor(self,motion=True):
        if hasattr(self,"sensor"):
            #get robot's real position
            if motion:
                try:
                    if not hasattr(self.sensor,'set_remote_idle_called') or self.sensor.set_remote_idle_called==False:
                        self.sensor.set_remote_idle(self.planner.reachableTraj)
                        self.sensor.set_remote_idle_called=True
                except Exception as e:
                    print("Motion Error: %s"%str(e))
                    self.sense_once=True
            #detect object
            if not self.sensed or not self.sense_once:
                try:
                    self.sensor.debug_detect_aruco(None, None, show=False, img="TRINA")
                    self.sensed=True
                except Exception as e:
                    print("Sensor Error: %s"%str(e))
                    exit(-1)
        elif self.sim.sim is None:
            self.sim.relax()
            