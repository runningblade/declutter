import symbolicFast as sym
import sparseBlock3x3 as blk
from gurobipy import GRB
import gurobipy as gp
import numpy as np
import math,gmpy2

MPFR_TYPE=type(gmpy2.mpfr(0))
MPQ_TYPE=type(gmpy2.mpq(0))

def sqrt_safe(x):
    if isinstance(x,sym.NLPFunction):
        return sym.NLPFunctionSqrt((x,))
    elif isinstance(x,MPFR_TYPE) or isinstance(x,MPQ_TYPE):
        return gmpy2.sqrt(x)
    else: return math.sqrt(x)

def sin_safe(x):
    if isinstance(x,sym.NLPFunction):
        return sym.NLPFunctionSin((x,))
    elif isinstance(x,MPFR_TYPE) or isinstance(x,MPQ_TYPE):
        return gmpy2.sin(x)
    else: return math.sin(x)

def cos_safe(x):
    if isinstance(x,sym.NLPFunction):
        return sym.NLPFunctionCos((x,))
    elif isinstance(x,MPFR_TYPE) or isinstance(x,MPQ_TYPE):
        return gmpy2.cos(x)
    else: return math.cos(x)
    
def asin_safe(x):
    if isinstance(x,sym.NLPFunction):
        return sym.NLPFunctionAsin((x,))
    elif isinstance(x,MPFR_TYPE) or isinstance(x,MPQ_TYPE):
        return gmpy2.asin(x)
    else: return math.asin(x)

def acos_safe(x):
    if isinstance(x,sym.NLPFunction):
        return sym.NLPFunctionAcos((x,))
    elif isinstance(x,MPFR_TYPE) or isinstance(x,MPQ_TYPE):
        return gmpy2.acos(x)
    else: return math.acos(x)
    
def atan2_safe(y,x):
    if isinstance(y,sym.NLPFunction) or isinstance(x,sym.NLPFunction):
        raise RuntimeError("NLP atan2 not implemented!")
    elif isinstance(x,MPFR_TYPE) or isinstance(x,MPQ_TYPE):
        return gmpy2.atan2(y,x)
    else: return math.atan2(y,x)

def floor_safe(x):
    if isinstance(x,sym.NLPFunction):
        raise RuntimeError("NLP floor not implemented!")
    elif isinstance(x,MPFR_TYPE) or isinstance(x,MPQ_TYPE):
        return gmpy2.floor(x)
    else: return math.floor(x)

def ceil_safe(x):
    if isinstance(x,sym.NLPFunction):
        raise RuntimeError("NLP ceil not implemented!")
    elif isinstance(x,MPFR_TYPE) or isinstance(x,MPQ_TYPE):
        return gmpy2.ceil(x)
    else: return math.ceil(x)

def log_safe(x):
    if isinstance(x,sym.NLPFunction):
        return sym.NLPFunctionLog((x,))
    elif isinstance(x,MPFR_TYPE) or isinstance(x,MPQ_TYPE):
        return gmpy2.log(x)
    else: return math.log(x)

def is_float(a):
    return  isinstance(a,float) or isinstance(a,np.float64) or  \
            isinstance(a,np.float128) or isinstance(a,int) or   \
            isinstance(a,MPFR_TYPE) or isinstance(a,MPQ_TYPE)

def is_scalar(a):
    return not isinstance(a,list) and not isinstance(a,tuple)

def cross(a,b):
    axb=[None,None,None]
    axb[0]=(a[1]*b[2])-(a[2]*b[1])
    axb[1]=(a[2]*b[0])-(a[0]*b[2])
    axb[2]=(a[0]*b[1])-(a[1]*b[0])
    return axb

def cross_mat(v):
    #    0,-v[2], v[1],
    # v[2],    0,-v[0],
    #-v[1],val v[0],    0;
    ret=[[0.0 for c in range(3)] for r in range(3)]
    ret[0][1]=-v[2]
    ret[0][2]= v[1]
    ret[1][0]= v[2]
    ret[1][2]=-v[0]
    ret[2][0]=-v[1]
    ret[2][1]= v[0]
    return ret

def abs_max(a):
    if is_scalar(a):
        return abs(a)
    ret=0.0
    for v in a:
        val=abs_max(v)
        if val>ret:
            ret=val
    return ret

def dot(a,b):
    if is_scalar(a):
        return a*b
    elif is_scalar(b):
        return a*b
    else:
        ret=0
        for ai,bi in zip(a,b):
            ret+=dot(ai,bi)
        return ret
    
def shape(m):
    return (len(m),len(m[0]))
    
def norm(a):
    return sqrt_safe(dot(a,a))
    
def mul(a,b):
    if a is None:
        return b
    elif b is None:
        return a
    elif is_scalar(a):
        if is_scalar(b):
            return a*b
        else: return [mul(a,bi) for bi in b]
    elif is_scalar(b):
        if is_scalar(a):
            return a*b
        else: return [mul(ai,b) for ai in a]
    else: return [mul(ai,bi) for ai,bi in zip(a,b)]
    
def div(a,b):
    if a is None:
        return b
    elif b is None:
        return a
    elif is_scalar(a):
        if is_scalar(b):
            return a/b
        else: return [div(a,bi) for bi in b]
    elif is_scalar(b):
        if is_scalar(a):
            return a/b
        else: return [div(ai,b) for ai in a]
    else: return [div(ai,bi) for ai,bi in zip(a,b)]
    
def matmul(a,b):
    assert len(a[0])==len(b)
    ret=[[0.0 for c in range(len(b[0]))] for r in range(len(a))]
    for r in range(len(a)):
        for c in range(len(b[0])):
            for k in range(len(a[0])):
                ret[r][c]+=a[r][k]*b[k][c]
    return ret

def catcol(a,b):
    assert len(a)==len(b)
    return [ai+bi for ai,bi in zip(a,b)]

def matTmul(a,b):
    assert len(a)==len(b)
    ret=[[0.0 for c in range(len(b[0]))] for r in range(len(a[0]))]
    for r in range(len(a[0])):
        for c in range(len(b[0])):
            for k in range(len(a)):
                ret[r][c]+=a[k][r]*b[k][c]
    return ret

def matvecmul(a,b):
    if isinstance(a,blk.SparseBlock3x3):
        return a.mul(b)
    assert len(a[0])==len(b)
    ret=[0.0 for r in range(len(a))]
    for r in range(len(a)):
        for c in range(len(b)):
                ret[r]+=a[r][c]*b[c]
    return ret

def matTvecmul(a,b):
    if isinstance(a,blk.SparseBlock3x3):
        return a.mulT(b)
    assert len(a)==len(b)
    ret=[0.0 for r in range(len(a[0]))]
    for r in range(len(a[0])):
        for c in range(len(b)):
                ret[r]+=a[c][r]*b[c]
    return ret

def transpose(a):
    ret=[[0.0 for c in range(len(a))] for r in range(len(a[0]))]
    for r in range(len(a)):
        for c in range(len(a[0])):
            ret[c][r]=a[r][c]
    return ret

def muldiagAB(a,b):
    return [mul(a,b[i]) for i in range(b)]

def add(a,b):
    if a is None:
        return b
    elif b is None:
        return a
    elif is_scalar(a):
        if is_scalar(b):
            return a+b
        else: return [add(a,bi) for bi in b]
    elif is_scalar(b):
        if is_scalar(a):
            return a+b
        else: return [add(ai,b) for ai in a]
    else: return [add(ai,bi) for ai,bi in zip(a,b)]

def sub(a,b):
    if a is None:
        return b
    elif b is None:
        return a
    elif is_scalar(a):
        if is_scalar(b):
            return a-b
        else: return [sub(a,bi) for bi in b]
    elif is_scalar(b):
        if is_scalar(a):
            return a-b
        else: return [sub(ai,b) for ai in a]
    else: return [sub(ai,bi) for ai,bi in zip(a,b)]
    
def add_gradient_block(r,blk,grad):
    for i in range(len(blk)):
        grad[r+i]+=blk[i]
                
def sub_gradient_block(r,blk,grad):
    for i in range(len(blk)):
        grad[r+i]-=blk[i]
    
def add_hessian_block(r,c,b,hess):
    if isinstance(hess,blk.SparseBlock3x3):
        hess.add_blk(r,c,b)
    else:
        for i in range(len(b)):
            for j in range(len(b[0])):
                hess[r+i][c+j]+=b[i][j]
                
def sub_hessian_block(r,c,b,hess):
    if isinstance(hess,blk.SparseBlock3x3):
        hess.sub_blk(r,c,b)
    else:
        for i in range(len(b)):
            for j in range(len(b[0])):
                hess[r+i][c+j]-=b[i][j]

def get_hessian_element(r,c,hess):
    if isinstance(hess,blk.SparseBlock3x3):
        return hess.get_elem(r,c)
    else: return hess[r][c]

def block_3x3(r,c,hess):
    if isinstance(hess,blk.SparseBlock3x3):
        return hess.blks[r//3][c//3]
    else: return [hess[r+0][c:c+3],hess[r+1][c:c+3],hess[r+2][c:c+3]]