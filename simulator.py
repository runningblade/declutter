from klampt import WorldModel,Geometry3D
from klampt.vis import gldraw
from visualizer import *
from utils import *
import random

class Simulator:
    def __init__(self,cost,world,reachableTraj,dt=0.01):
        self.cost=cost
        self.world=world
        self.reachableTraj=reachableTraj
        self.dt=dt
        self.sim=None
        self.executor=None
            
    def add_object(self,rigid):
        raise NotImplementedError("This is abstract super class, use subclass!")
    
    def relax(self,thresLinearVelocity=0.001,thresAngularVelocity=0.001):
        self.sync()
        if self.sim is None:
            return
        while True:
            self.sim.simulate(self.dt)
            #test stop
            finished=True
            for i in range(self.world.numRigidObjects()):
                b=self.sim.body(self.world.rigidObject(i))
                w,v=b.getVelocity()
                #print(w,v)
                if op.norm(v)>thresLinearVelocity:
                    finished=False
                if op.norm(w)>thresAngularVelocity:
                    finished=False
            if finished:
                break
        self.sync()
    
    def relax_onestep(self,sync=False):
        if self.sim is None or self.executor is None:
            return
        self.executor.execute(None,None)
        self.sim.simulate(self.dt)
        if sync:
            self.sync()
    
    def execute(self,dof0,dof,move_pusher_away=False):
        raise NotImplementedError("This is abstract super class, use subclass!")
    
    def simulate(self):
        raise NotImplementedError("This is abstract super class, use subclass!")

    def display(self):
        raise NotImplementedError("This is abstract super class, use subclass!")
    
    def sync(self):
        raise NotImplementedError("This is abstract super class, use subclass!")
    
    def pss(self):
        raise NotImplementedError("This is abstract super class, use subclass!")
    
    def css(self):
        raise NotImplementedError("This is abstract super class, use subclass!")
    
    def get_state(self):
        raise NotImplementedError("This is abstract super class, use subclass!")
        
    def set_state(self,state):
        raise NotImplementedError("This is abstract super class, use subclass!")
    
    def get_shapes(self):
        raise NotImplementedError("This is abstract super class, use subclass!")
    
    def stopped(self):
        raise NotImplementedError("This is abstract super class, use subclass!")
    
    def push(self,s,p,v,time,trajDOF):
        raise NotImplementedError("This is abstract super class, use subclass!")
    
    def grasp(self,s,oid,t):
        raise NotImplementedError("This is abstract super class, use subclass!")
    
    def random_grasp(self):
        pss=self.pss()
        oid=random.randrange(0,len(pss))
        dx=self.get_shapes()[oid].diameter()
        
        s=self.get_state()
        dx=[random.uniform(-dx,dx),random.uniform(-dx,dx)]
        t=op.add(pss[oid],dx)
        self.grasp(s,oid,t)
        
    def calc_cost(self):
        return self.cost.calc_cost(self.pss(),self.css())
    
    def pusher_shape(self):
        raise NotImplementedError("This is abstract super class, use subclass!")
    
    def keyboardfunc(self,c,x,y):
        pass
    
class SimulatorGLVisualizer(GLVisualizer):
    def __init__(self,world,table,robot,sim,scaleCamera=5.0):
        GLVisualizer.__init__(self,world,table,robot,sim)
        #s=self.sim.get_state()
        #self.sim.set_state(s)
        if isinstance(scaleCamera,float):
            self.simOnly=True
            bbr=get_robot_bb(robot)
            bbt=get_object_bb(table)
            self.look_at([0,0,bbr[1][2]],[self.get_ctr()[0],0,0],scale=scaleCamera)
        else: self.simOnly=False
        self.drawCost=True
        self.drawCostDist=True

    def get_ctr(self):
        ctr=(0,0)
        pss=self.sim.pss()
        for p in pss:
            ctr=op.add(ctr,p)
        return op.mul(ctr,1.0/len(pss))

    def keyboardfunc(self,c,x,y):
        GLVisualizer.keyboardfunc(self,c,x,y)
        if c==b'b':
            self.drawCostDist=not self.drawCostDist
        elif c==b'v':
            self.sim.reachableTraj.get_pusher().drawCell=not self.sim.reachableTraj.get_pusher().drawCell
        elif c==b't':
            s=self.sim.get_state()
            self.sim.set_state(s)
        elif c==b'g':
            self.sim.random_grasp()
        else:
            self.sim.keyboardfunc(c,x,y)

    def keyboardupfunc(self,c,x,y):
        GLVisualizer.keyboardupfunc(self,c,x,y)
        
    def display_screen(self):
        if not self.simOnly:
            self.sim.reachableTraj.display_screen(self)
            GLVisualizer.display_screen(self)
        gldraw.setcolor(TEXT_COLOR[0],TEXT_COLOR[1],TEXT_COLOR[2])
        self.draw_text((0,12*3),"Cost=%f"%self.sim.calc_cost())
        
    def display(self):
        #reachable
        if not self.simOnly:
            self.sim.reachableTraj.display()
            self.world.drawGL()
        self.sim.display()
        #cost
        if self.drawCost:
            if self.drawCostDist:
                self.sim.cost.draw(self.sim.pss(),category=self.sim.css())
            else: self.sim.cost.draw()

    def idle(self):
        self.handle_camera()
        self.update_animation()
        self.sim.reachableTraj.idle()
        if self.sim is not None:
            if not self.simOnly:
                self.sim.sync()#self.dt)
            self.sim.simulate()
            #self.sim.sync_back()
    