rm -rdf *.user
rm -rdf *.pyc
rm -rdf *.dat
rm -rdf *.AVI
rm -rdf *.png
rm -rdf *.pov
rm -rdf compareWOPushResults
rm -rdf compareWOCPushResults
rm -rdf compareWOC4HighLevelResults
rm -rdf compareWOCPushCostReduceResults
rm -rdf *.so
rm -rdf tmp*
rm -rdf allCases*
rm -rdf gurobi.log
rm -rdf objPose.dat
rm -rdf test_wrench
rm -rdf __pycache__

cd results
rm -rdf *.user
rm -rdf *.pyc
rm -rdf *.dat
rm -rdf *.AVI
rm -rdf *.png
rm -rdf *.pov
rm -rdf compareWOPushResults
rm -rdf compareWOCPushResults
rm -rdf compareWOC4HighLevelResults
rm -rdf compareWOCPushCostReduceResults
rm -rdf *.so
rm -rdf tmp*
rm -rdf allCases*
rm -rdf gurobi.log
rm -rdf objPose.dat
rm -rdf test_wrench
rm -rdf __pycache__
cd ..

cd pyDeclutter
sudo mv build dist pyDeclutter.egg-info ../../
cd ..

git add --all
git commit -m "$1"
git push -u origin $2
sudo mv ../build ../dist ../pyDeclutter.egg-info ./pyDeclutter


