from reachableTraj import *
from BVHTree import surface_area_bb
import math,pickle

def workerR(hardware,height,res,nrTrial):
    from scene import make_scene_TRINA,make_scene_TRINA_CALIBRATED
    if hardware:
        world,table,robot,robot_path=make_scene_TRINA_CALIBRATED()
    else: world,table,robot,robot_path=make_scene_TRINA()
    reachable=ReachableTRINA(world,table,robot,robot_path,height,isTraj=False,showL=False)
    reachable.reachR.set_collision(reachable.reachL.optDOF,False)
    reachable.reachR.build_grid(res,nrTrial,suffix="R")
    return None

def workerL(hardware,height,res,nrTrial):
    from scene import make_scene_TRINA,make_scene_TRINA_CALIBRATED
    if hardware:
        world,table,robot,robot_path=make_scene_TRINA_CALIBRATED()
    else: world,table,robot,robot_path=make_scene_TRINA()
    reachable=ReachableTRINA(world,table,robot,robot_path,height,isTraj=False,showL=False)
    reachable.reachL.set_collision(reachable.reachR.optDOF,False)
    reachable.reachL.build_grid(res,nrTrial,suffix="L")
    return None
    
class ReachableTRINA:
    def __init__(self,world,table,robot,robot_path,height,isTraj=True,showL=True):
        self.tableBB=get_object_bb(table)
        dof=robot.getConfig()
        if dof[1]!=0.0 or dof[3]!=0.0 or dof[4]!=0.0 or dof[5]!=0.0:
            raise RuntimeError("ReachableTRINA is not initialized using a base-symmetric pose!")
        #reachable-Left-arm
        def additionalCheckL(reachable,x):
            reachable.set_opt_DOF(x)
            bb1=get_link_bb(reachable.robot,7)[0]
            bb2=get_link_bb(reachable.robot,8)[0]
            bb3=get_link_bb(reachable.robot,9)[0]
            #bb4=get_link_bb_ctr(reachable.robot,10)
            #bb5=get_link_bb_ctr(reachable.robot,11)
            bb6=get_link_bb_ctr(reachable.robot,12)
            return bb1[2]>bb6[2] and bb2[2]>bb6[2] and bb3[2]>bb6[2] #and bb4[2]>bb6[2] and bb5[2]>bb6[2]
        if isTraj:
            self.reachL=ReachableTraj(world,table,robot,robot_path,height,12,[0,0,0],[0,0,1],[7,8,9,10,11],additionalCheck=additionalCheckL,rotDelta=math.pi/4)
        else: self.reachL=ReachableFast(world,table,robot,robot_path,height,12,[0,0,0],[0,0,1],[7,8,9,10,11],additionalCheck=additionalCheckL)
        #reachable-Right-arm, this is dummy, reachable set is not built analytically, but projected from Left-arm by symmetry
        def additionalCheckR(reachable,x):
            reachable.set_opt_DOF(x)
            bb1=get_link_bb(reachable.robot,15)[0]
            bb2=get_link_bb(reachable.robot,16)[0]
            bb3=get_link_bb(reachable.robot,17)[0]
            #bb4=get_link_bb_ctr(reachable.robot,18)
            #bb5=get_link_bb_ctr(reachable.robot,19)
            bb6=get_link_bb_ctr(reachable.robot,20)
            return bb1[2]>bb6[2] and bb2[2]>bb6[2] and bb3[2]>bb6[2] #and bb4[2]>bb6[2] and bb5[2]>bb6[2]
        if isTraj:
            self.reachR=ReachableTraj(world,table,robot,robot_path,height,20,[0,0,0],[0,0,1],[15,16,17,18,19],additionalCheck=additionalCheckR,rotDelta=math.pi/4)
        else: self.reachR=ReachableFast(world,table,robot,robot_path,height,20,[0,0,0],[0,0,1],[15,16,17,18,19],additionalCheck=additionalCheckR)
        #L2R
        self.L2R=np.zeros((self.reachL.robot.numLinks(),self.reachL.robot.numLinks()+1))
        for i in range(15):
            self.L2R[i,i]=1.0
        self.L2R[15,7 ]=-1.0
        self.L2R[16,8 ]=-1.0
        self.L2R[17,9 ]=-1.0
        self.L2R[18,10]=-1.0
        self.L2R[19,11]=-1.0
        self.L2R[16,self.reachL.robot.numLinks()]=math.pi
        self.L2R[18,self.reachL.robot.numLinks()]=math.pi
        #reachShow
        self.reachShow=self.reachL if showL else self.reachR
        
    def keyboardfunc(self,c,x,y):
        self.reachShow.keyboardfunc(c,x,y)
    
    def display_screen(self,glProg):
        self.reachShow.display_screen(glProg)
        
    def display(self):
        self.reachShow.display()
            
    def idle(self):
        #self.reachL.check_grid_idle(0)
        if isinstance(self.reachShow,ReachableTraj):
            self.idle_traj()
        else: self.reachShow.idle()
        
    def idle_traj(self):
        if not hasattr(self,"trajType"):
            self.trajType=1
        if self.reachShow.drawCell and hasattr(self.reachL,"grid") and hasattr(self.reachR,"grid"):
            if not hasattr(self,"currTraj") or self.currTraj is None:
                while True:
                    grasp=None if self.trajType==0 else True
                    #grasp=None if random.uniform(0,1)<0.5 else True
                    if grasp is None:
                        idss=[self.reachL.random_id(),self.reachL.random_id(),self.reachL.random_id()]
                    else:
                        idss=[self.reachR.random_id(),self.reachR.random_id(),self.reachR.random_id()]
                    self.sync()
                    self.currTraj=self.gen_traj(idss,0.1,0.1,directed=op.sub(idss[1],idss[0]),grasp=grasp,routeForGrasp=True)
                    if self.currTraj is not None:
                        break
                self.currTime=0.0
                self.trajType=1-self.trajType
            elif self.currTime>=self.currTraj[1][-1]:
                delattr(self,"currTraj")
                delattr(self,"currTime")
            else:
                _,_,dof=self.search_traj_time(self.currTraj,self.currTime)
                self.set_opt_DOF(dof)
                self.currTime+=0.1
        
    def search_traj_time(self,traj,time,i=None):
        return self.reachL.search_traj_time(traj=traj,time=time,i=i)
        
    def gen_traj(self,idss,speed,speedMove=None,startFromCurrent=True,directed=None,grasp=None,height=None,routeForGrasp=True):
        if grasp is not None:
            reachMain=self.reachL
            reachAux=self.reachR
            idleAux=self.idlePoseR
        else:
            reachMain=self.reachR
            reachAux=self.reachL
            idleAux=self.idlePoseL
            
        #main-algorithm
        dofMain=[reachMain.dof[i] for i in reachMain.optDOF]   #save state
        dofAux=[reachAux.dof[i] for i in reachAux.optDOF]
        def recover_state():
            if grasp:
                self.set_opt_DOF_LR(dofMain,dofAux)
            else: self.set_opt_DOF_LR(dofAux,dofMain)
        #this is a push trajectory, use Left-Arm
        traj_bundle=reachMain.gen_traj(idss=idss,speed=speed,speedMove=speedMove,startFromCurrent=False,directed=directed,grasp=grasp,height=height,routeForGrasp=routeForGrasp)
        if traj_bundle is None:
            recover_state()
            return None
        traj,time,carry=traj_bundle
        #hide aux arm
        if grasp:
            traj=[list(t)+list(idleAux) for t in traj]
        else: traj=[list(idleAux)+list(t) for t in traj]
        
        #return
        if startFromCurrent:
            recover_state()
            ret=self.connect_current((traj,time,carry),speed)
            ret=self.recover_idle(ret,speed)
            recover_state()
            return ret
        else:
            recover_state()
            ret=self.recover_idle((traj,time,carry),speed)
            recover_state()
            return ret
            
    def connect_current(self,trajRef,speed):
        dofs,times,carry=trajRef
        
        trajL,timeL=self.reachL.connect_current(list(dofs[0][0:len(self.reachL.optDOF)]),speed)
        trajR,timeR=self.reachR.connect_current(list(dofs[0][len(self.reachR.optDOF):len(self.reachR.optDOF)*2]),speed)
        
        #combine
        if carry.grasp is None:
            #move L first
            trajLR=[d+trajR[0] for d in trajL]+[trajL[-1]+d for d in trajR]
            timeLR=timeL+[t+timeL[-1] for t in timeR]
        else:
            #move R first
            trajLR=[trajL[0]+d for d in trajR]+[d+trajR[-1] for d in trajL]
            timeLR=timeR+[t+timeR[-1] for t in timeL]
            
        #data structure to compute carried object
        if carry.id0 is not None:
            carry.id0+=len(timeLR)
            carry.id1+=len(timeLR)
        #concat
        trajLR+=dofs
        timeLR+=[t+timeLR[-1] for t in times]
        #do not move EE (no need to)
        if carry.grasp is not None:
            idEE=self.reachL.optDOF.index(self.reachL.eeLid)
            for i in range(1,len(trajLR)):
                trajLR[i][idEE]=trajLR[0][idEE]
        return (trajLR,timeLR,carry)
    
    def recover_idle(self,trajRef,speed):
        dofs,times,carry=trajRef
        
        self.set_opt_DOF(dofs[-1])
        if carry.grasp is None:
            #move R
            lastL=dofs[-1][:len(dofs[-1])/2]
            trajR,time=self.reachR.connect_current(self.idlePoseR,speed)
            trajLR=[lastL+d for d in trajR]
        else:
            #move L
            lastR=dofs[-1][len(dofs[-1])/2:]
            trajL,time=self.reachL.connect_current(self.idlePoseL,speed)
            trajLR=[d+lastR for d in trajL]
        
        while time[0]==0:
            time=time[1:]
            trajLR=trajLR[1:]
        dofsLR=dofs+trajLR
        timesLR=times+[t+times[-1] for t in time]
        return (dofsLR,timesLR,carry)
        
    def get_DOF(self,x):
        dof=[i for i in self.reachL.dof]
        xL,xR=x[0:len(x)//2],x[len(x)//2:len(x)]
        for i,DOFId in enumerate(self.reachL.optDOF):
            dof[DOFId]=xL[i]
        for i,DOFId in enumerate(self.reachR.optDOF):
            dof[DOFId]=xR[i]
        return dof
        
    def set_opt_DOF(self,dof):
        self.set_opt_DOF_LR(dof[:len(dof)//2],dof[len(dof)//2:])
            
    def set_opt_DOF_LR(self,dofL=None,dofR=None):
        if dofL is not None:
            for iL,dL in enumerate(self.reachL.optDOF):
                self.reachL.dof[dL]=dofL[iL]
                self.reachR.dof[dL]=dofL[iL]
        if dofR is not None:
            for iR,dR in enumerate(self.reachR.optDOF):
                self.reachL.dof[dR]=dofR[iR]
                self.reachR.dof[dR]=dofR[iR]
        self.reachL.robot.setConfig(self.reachL.dof)
        self.reachR.robot.setConfig(self.reachR.dof)
        
    def get_opt_DOF(self,x):
        return self.reachL.get_opt_DOF(x)+self.reachR.get_opt_DOF(x)
    
    def compensate_DOF(self,DOF_COMPENSATE):
        self.idlePoseL=op.sub(self.idlePoseL,DOF_COMPENSATE[:len(DOF_COMPENSATE)//2])
        self.idlePoseR=op.sub(self.idlePoseR,DOF_COMPENSATE[len(DOF_COMPENSATE)//2:])
        
        grid={}
        for k,v in self.reachL.grid.items():
            grid[k]=(op.sub(v[0],DOF_COMPENSATE[:len(DOF_COMPENSATE)//2]),
                     op.sub(v[1],DOF_COMPENSATE[:len(DOF_COMPENSATE)//2]))
        self.reachL.grid=grid
        
        grid={}
        for k,v in self.reachR.grid.items():
            grid[k]=(op.sub(v[0],DOF_COMPENSATE[len(DOF_COMPENSATE)//2:]),
                     op.sub(v[1],DOF_COMPENSATE[len(DOF_COMPENSATE)//2:]))
        self.reachR.grid=grid
    
    def sync(self):
        x=self.get_opt_DOF(self.reachL.robot.getConfig())
        self.set_opt_DOF(x)
    
    def build_grid(self,res,nrTrial=100):
        import multiprocessing
        pool=multiprocessing.Pool(2)
        hardware="_Calibrated" in self.reachL.robot_path
        pool.apply_async(workerR,(hardware,self.reachR.height,res,nrTrial))
        pool.apply_async(workerL,(hardware,self.reachL.height,res,nrTrial))
        pool.close()
        pool.join()
        
        self.reachR.set_collision(self.reachL.optDOF,False)
        self.reachR.build_grid(res,nrTrial,suffix="R")
        self.reachL.set_collision(self.reachR.optDOF,False)
        self.reachL.build_grid(res,nrTrial,suffix="L")
        #self.reachL.check_grid_all()
        
        #build idle pose that avoids self-collisions as much as possible
        self.find_idle_pose(suffix="")
        self.register_idle_pose()
        
        #remove DOF that are still self-colliding
        self.reachL.grid={k:v for k,v in self.reachL.grid.items() if k not in self.invalidGridL}
        self.reachL.cell=[c for c in self.reachL.cell if ((c[0]  ,c[1]  ) in self.reachL.grid and   \
                                                          (c[0]+1,c[1]  ) in self.reachL.grid and   \
                                                          (c[0]  ,c[1]+1) in self.reachL.grid and   \
                                                          (c[0]+1,c[1]+1) in self.reachL.grid)]
        self.reachR.grid={k:v for k,v in self.reachR.grid.items() if (k[0],-k[1]) not in self.invalidGridL}
        self.reachR.cell=[c for c in self.reachR.cell if ((c[0]  ,c[1]  ) in self.reachR.grid and   \
                                                          (c[0]+1,c[1]  ) in self.reachR.grid and   \
                                                          (c[0]  ,c[1]+1) in self.reachR.grid and   \
                                                          (c[0]+1,c[1]+1) in self.reachR.grid)]
        self.set_opt_DOF_LR(self.idlePoseL,self.idlePoseR)
        
        #reflect left arm's grid to right arm by symmetry 
        #self.reflect_grid()
        
        #build bounds
        self.reachL.build_grid_bounds()
        self.reachR.build_grid_bounds()
        print("IdlePoseL: %s"%str(self.idlePoseL))
        print("IdlePoseR: %s"%str(self.idlePoseR))
        
    def get_save_path_idle(self,suffix=""):
        return DATA_PATH+"/data/"+self.reachL.robot.getName()+"_IdlePose"+suffix+".dat"
        
    def register_idle_pose(self):
        def dist_wrapped(a,b):
            ret=0.
            for ai,bi in zip(a,b):
                reti=ai-bi
                while abs(reti+math.pi*2)<abs(reti):
                    reti+=math.pi*2
                while abs(reti-math.pi*2)<abs(reti):
                    reti-=math.pi*2
                ret+=abs(reti)
            return ret
        
        bestDist=None
        bestPose=None
        for _,v in self.reachL.grid.items():
            dist=dist_wrapped(v[0],self.idlePoseL)
            if bestDist is None or dist<bestDist:
                bestDist=dist
                bestPose=v[0]
        self.idlePoseL=bestPose
        
        bestDist=None
        bestPose=None
        for _,v in self.reachR.grid.items():
            dist=dist_wrapped(v[0],self.idlePoseR)
            if bestDist is None or dist<bestDist:
                bestDist=dist
                bestPose=v[0]
        self.idlePoseR=bestPose
        
    def find_idle_pose(self,suffix=""):
        #find idle pose
        savePath=self.get_save_path_idle(suffix)
        if os.path.exists(savePath):
            file=open(savePath,"rb")
            self.invalidGridL,self.idlePoseL,self.idlePoseR=pickle.load(file)
        else:
            #find the set of cells in front of robot
            nrUpdated=0
            nrChecked=0
            invalidGridLOnTableBest=None
            self.invalidGridL=None
            self.idlePoseL=None
            self.idlePoseR=None
            
            #count configuration
            nrOnTable,nrOffTable=0,0
            for idL,_ in self.reachL.grid.items():
                if (idL[0],-idL[1]) not in self.reachR.grid:
                    continue
                self.set_opt_DOF_LR(self.reachL.grid[idL][0],self.reachR.grid[(idL[0],-idL[1])][0])
                gposR=self.reachR.global_pos()[1]
                onTable=gposR[0]>=self.tableBB[0][0] and gposR[0]<=self.tableBB[1][0] and \
                        gposR[1]>=self.tableBB[0][1] and gposR[1]<=self.tableBB[1][1]
                if onTable:
                    nrOnTable+=1
                else: nrOffTable+=1
            print("#grids on table=%d, #grids off table=%d!"%(nrOnTable,nrOffTable))
                
            #check all configuration
            for idL,_ in self.reachL.grid.items():
                if (idL[0],-idL[1]) not in self.reachR.grid:
                    nrChecked+=1
                    continue
                self.set_opt_DOF_LR(self.reachL.grid[idL][0],self.reachR.grid[(idL[0],-idL[1])][0])
                if len([c for c in self.reachL.coll.robotSelfCollisions(self.reachL.robot)])>0:
                    nrChecked+=1
                    continue
                #check validity
                invalidGridL=set()
                invalidGridLOnTable=set()
                for gL,_ in self.reachL.grid.items():
                    if (gL[0],-gL[1]) not in self.reachR.grid:
                        continue
                    self.set_opt_DOF_LR(self.reachL.grid[idL][0],self.reachR.grid[(gL[0],-gL[1])][0])
                    colls=[c for c in self.reachL.coll.robotSelfCollisions(self.reachL.robot)]
                    self.set_opt_DOF_LR(self.reachL.grid[idL][0],self.reachR.grid[(gL[0],-gL[1])][1])
                    colls+=[c for c in self.reachL.coll.robotSelfCollisions(self.reachL.robot)]
                    for c in colls:
                        #if left/right arm will collide at this pose, we mark this as invalid
                        if  (c[0].index in self.reachL.optDOF and c[1].index in self.reachR.optDOF) or \
                            (c[0].index in self.reachR.optDOF and c[1].index in self.reachL.optDOF):
                            invalidGridL.add(gL)
                            #if this pose is off-the-table, we don't care and avoid marking
                            gposR=self.reachR.global_pos()[1]
                            onTable=gposR[0]>=self.tableBB[0][0] and gposR[0]<=self.tableBB[1][0] and \
                                    gposR[1]>=self.tableBB[0][1] and gposR[1]<=self.tableBB[1][1]
                            if onTable:
                                invalidGridLOnTable.add(gL)
                            break
                    if invalidGridLOnTableBest is not None and len(invalidGridLOnTableBest)<len(invalidGridLOnTable):
                        break
                nrChecked+=1
                #profile
                if nrChecked%20==0:
                    print("Checked %d/%d grids, updated %d grids for idlePose, nrInvalidGridL=%s, nrInvalidGridLOnTable=%s!"%    \
                          (nrChecked,len(self.reachL.grid.keys()),nrUpdated,str(len(self.invalidGridL)),str(len(invalidGridLOnTableBest))))
                #update
                if invalidGridLOnTableBest is None:
                    nrUpdated+=1
                    invalidGridLOnTableBest=invalidGridLOnTable
                    self.invalidGridL=invalidGridL
                    self.idlePoseL=self.reachL.grid[idL][0]
                    self.idlePoseR=self.reachR.grid[(idL[0],-idL[1])][0]
                else:
                    self.set_opt_DOF_LR(self.idlePoseL)
                    sbbCurr=surface_area_bb(get_robot_bb(self.reachL.robot,self.reachL.optDOF[0]))
                    self.set_opt_DOF_LR(self.reachL.grid[idL][0])
                    sbbNew=surface_area_bb(get_robot_bb(self.reachL.robot,self.reachL.optDOF[0]))
                    if  len(invalidGridLOnTableBest)>len(invalidGridLOnTable) or    \
                        (len(invalidGridLOnTableBest)==len(invalidGridLOnTable) and sbbNew<sbbCurr):
                        nrUpdated+=1
                        invalidGridLOnTableBest=invalidGridLOnTable
                        self.invalidGridL=invalidGridL
                        self.idlePoseL=self.reachL.grid[idL][0]
                        self.idlePoseR=self.reachR.grid[(idL[0],-idL[1])][0]
            assert self.idlePoseL is not None
            file=open(savePath,"wb")
            pickle.dump((self.invalidGridL,self.idlePoseL,self.idlePoseR),file)
        
    def debug_range_intersect(self,d=100,N=100):
        self.reachL.debug_range_intersect(d,N)
        self.reachR.debug_range_intersect(d,N)
    
    def get_pusher(self):
        return self.reachR
        
    def get_gripper(self):
        return self.reachL
    
if __name__=='__main__':
    from scene import make_scene_TRINA_CALIBRATED
    world,table,robot,robot_path=make_scene_TRINA_CALIBRATED()
    reachable=ReachableTRINA(world,table,robot,robot_path,0.35,isTraj=True,showL=True)
    reachable.build_grid(0.025)
    reachable.debug_range_intersect()
    GLVisualizerReachable(world,table,robot,reachable).run()