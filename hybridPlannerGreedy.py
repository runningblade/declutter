from polygon2D import Polygon2D
from planner import *

class HybridPlannerGreedy(Planner):
    def __init__(self,sim,subPlanners):
        Planner.__init__(self,sim)
        self.subPlanners=subPlanners
        #minimal acceptable improvement
        self.minCostReduce=sim.get_shapes()[0].diameter()/10
    
    def plan(self,debugInfo):
        self.bestAction=None
        for planner in self.subPlanners:
            action=planner.plan(debugInfo)
            if action is not None:
                if hasattr(action,"costReduceWithTransit"):
                    if self.bestAction is None or action.costReduceWithTransit>self.bestAction.costReduceWithTransit:
                        self.bestAction=action
                else:
                    if self.bestAction is None or action.costReduce>self.bestAction.costReduce:
                        self.bestAction=action
        if self.bestAction is not None and self.bestAction.costReduce<self.minCostReduce:
            if debugInfo:
                print("HybridPlannerGreedy cost(%f) small than %f!"%(self.bestAction.costReduce,self.minCostReduce))
            self.bestAction=None
        return self.bestAction
    
    def get_push_actions(self):
        for planner in self.subPlanners:
            if planner.get_push_actions() is not None:
                return planner.get_push_actions()
        return None