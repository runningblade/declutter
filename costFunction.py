from visualizer import GLVisualizer
from polygon2D import Polygon2D
from klampt.vis import gldraw
import symbolicFast as sym
from scene import *
from utils import *
import sys,random,copy

class Cost:
    def calc_costs_all(self,pss):
        return [self.calc_costs(pss)]
    
    def calc_costs(self,pss):
        return [self.calc_cost([p]) for p in pss]
    
    def calc_cost(self,pss,category=None):
        raise NotImplementedError("This is abstract root class, use sub-class!")
    
    def calc_cost_dist(self,pss,category=None):
        raise NotImplementedError("This is abstract root class, use sub-class!")
        
    def draw(self,pss=None,margin=0.0005,category=None):
        if pss:
            _,dss=self.calc_cost_dist(pss,category)
            gldraw.setcolor(COST_DISTANCE_COLOR[0],COST_DISTANCE_COLOR[1],COST_DISTANCE_COLOR[2])
            gl.glBegin(gl.GL_LINES)
            for d in dss:
                gl.glVertex3f(d[0][0],d[0][1],margin)
                gl.glVertex3f(d[1][0],d[1][1],margin)
            gl.glEnd()

    def get_directional_cost(self,p,d,id=None):
        raise NotImplementedError("This is abstract root class, use sub-class!")
    
    def get_capacities(self):
        return [[INFINITY_NUMBER]]
    
class DirectionalCostCase:
    def __init__(self,p,dir,alpha0,alpha1,vert=None,normal=None):
        if p is None:
            return
        self.alpha0=alpha0
        self.alpha1=alpha1
        if vert is None and normal is None:
            self.type=b'I'
            self.a=self.b=self.c=0
        elif normal is None:
            self.type=b'V'
            p2v=op.sub(p,vert)
            self.a=op.dot(dir,dir)
            self.b=op.dot(p2v,dir)*2
            self.c=op.dot(p2v,p2v)
        else:
            self.type=b'E'
            p2vn=op.dot(op.sub(p,vert),normal)
            dn=op.dot(dir,normal)
            self.a=dn*dn
            self.b=dn*p2vn*2
            self.c=p2vn*p2vn
    
    def calc_cost(self,alpha):
        return self.a*alpha**2+self.b*alpha+self.c

    def draw(self,p,dir,margin):
        if self.type==b'I':
            gldraw.setcolor(COST_INSIDE_DIRICHLET_COLOR[0],COST_INSIDE_DIRICHLET_COLOR[1],COST_INSIDE_DIRICHLET_COLOR[2])
        elif self.type==b'V':
            gldraw.setcolor(COST_VERTEX_DIRICHLET_COLOR[0],COST_VERTEX_DIRICHLET_COLOR[1],COST_VERTEX_DIRICHLET_COLOR[2])
        elif self.type==b'E':
            gldraw.setcolor(COST_EDGE_DIRICHLET_COLOR[0],COST_EDGE_DIRICHLET_COLOR[1],COST_EDGE_DIRICHLET_COLOR[2])
        else: assert False
        gl.glBegin(gl.GL_LINES)
        v=op.add(p,op.mul(dir,self.alpha0))
        gl.glVertex3f(v[0],v[1],margin)
        v=op.add(p,op.mul(dir,self.alpha1))
        gl.glVertex3f(v[0],v[1],margin)
        gl.glEnd()

    def draw_cost(self,p,dir,interval):
        if self.type==b'I':
            gldraw.setcolor(COST_INSIDE_DIRICHLET_COLOR[0],COST_INSIDE_DIRICHLET_COLOR[1],COST_INSIDE_DIRICHLET_COLOR[2])
        elif self.type==b'V':
            gldraw.setcolor(COST_VERTEX_DIRICHLET_COLOR[0],COST_VERTEX_DIRICHLET_COLOR[1],COST_VERTEX_DIRICHLET_COLOR[2])
        elif self.type==b'E':
            gldraw.setcolor(COST_EDGE_DIRICHLET_COLOR[0],COST_EDGE_DIRICHLET_COLOR[1],COST_EDGE_DIRICHLET_COLOR[2])
        else: assert False
        nrSample=int(max(2,(self.alpha1-self.alpha0)//interval+1))
        gl.glBegin(gl.GL_LINE_STRIP)
        for i in range(nrSample):
            alpha=i*(self.alpha1-self.alpha0)/(nrSample-1)+self.alpha0
            pos=op.add(p,op.mul(dir,alpha))
            gl.glVertex3f(pos[0],pos[1],self.calc_cost(alpha))
        gl.glEnd()

    def add(self,deltaAlpha):
        ret=DirectionalCostCase(None,None,None,None)
        ret.alpha0=self.alpha0+deltaAlpha
        ret.alpha1=self.alpha1+deltaAlpha
        ret.a=self.a
        ret.b=self.b-2*self.a*deltaAlpha
        ret.c=self.a*deltaAlpha**2-self.b*deltaAlpha+self.c
        return ret

class DirectionalCost(Cost):
    def __init__(self,p,dir,poly):
        if poly is None:
            return
        dir=op.mul(dir,1/op.norm(dir))
        self.a=p
        self.dir=dir
        self.b=op.add(p,op.mul(dir,INFINITY_NUMBER))
        #build cases
        self.cases=[]
        alphasSorted=poly.intersect_dirichlet(self.a,self.b)
        lastHandle=0.0
        for handle in alphasSorted:
            vert=op.add(p,op.mul(dir,(lastHandle+handle)/2))
            vert,normal=poly.region_dirichlet(vert)
            self.cases.append(DirectionalCostCase(p,dir,lastHandle,handle,vert,normal))
            lastHandle=handle
        handle=INFINITY_NUMBER
        vert=op.add(p,op.mul(dir,(lastHandle+handle)/2))
        vert,normal=poly.region_dirichlet(vert)
        self.cases.append(DirectionalCostCase(p,dir,lastHandle,handle,vert,normal))
        
    def calc_cost(self,pss,category=None):
        assert len(pss)==1
        alpha=op.dot(op.sub(pss[0],self.p),self.dir)
        return self.calc_cost_alpha(alpha)

    def calc_cost_alpha(self,alpha):
        for case in self.cases:
            if alpha<case.alpha1:
                return case.calc_cost(alpha)
        assert False
        return 0.0
    
    def draw(self,margin=0.001):
        for case in self.cases:
            case.draw(self.a,self.dir,margin)
    
    def draw_cost(self,interval=0.1):
        for case in self.cases:
            case.draw_cost(self.a,self.dir,interval)
    
    def add(self,deltaAlpha):
        ret=DirectionalCost(None,None,None)
        ret.a=op.sub(self.a,op.mul(self.dir,deltaAlpha))
        ret.dir=self.dir
        ret.b=self.b
        ret.cases=[]
        for case in self.cases:
            ret.cases.append(case.add(deltaAlpha))
        return ret
    
class CostDistToRegion(Cost):
    def __init__(self,sss=None):
        self.pss=[]
        if sss is not None:
            if isinstance(sss,tuple):
                sss=[sss]
            for s in sss:
                assert isinstance(s,tuple)
                if len(s)==2:
                    self.add_bb(s[0],s[1])
                elif len(s)==3:
                    self.add_triangle(s[0],s[1],s[2])
                elif len(s)==4:
                    self.add_triangle(s[0],s[1],s[2],s[3])
                else: assert False
    
    def __deepcopy__(self,memo):
        if id(self) in memo:
            return memo[id(self)]
        result=CostDistToRegion()
        result.pss=copy.deepcopy(self.pss,memo)
        memo[id(self)]=result
        return result
        
    def add_triangle(self,t0,t1,t2):
        self.pss.append(Polygon2D((t0,t1,t2)))
        
    def add_quad(self,q0,q1,q2,q3):
        self.pss.append(Polygon2D((q0,q1,q2,q3)))
    
    def add_bb(self,minC,maxC):
        self.pss.append(Polygon2D([(minC[0],minC[1]),(maxC[0],minC[1]),(maxC[0],maxC[1]),(minC[0],maxC[1])]))
    
    def calc_cost(self,pss,category=None):
        c=0.0
        for p in pss:
            c+=self.dist_to_region(p)[0]
        return c
    
    def calc_cost_dist(self,pss,category=None):
        dss=[]
        c=0.0
        for p in pss:
            dp,cp,_=self.dist_to_region(p)
            if dp<0:
                dp=0.0
                cp=p
            c+=dp
            dss.append((p,cp))
        return c,dss
    
    def dist_to_region(self,p,id=None):
        d=None
        c=None
        n=None
        for poly in self.pss:
            dNew,cNew,nNew,_,_=poly.dist_to(p)
            if dNew<0.0:
                dNew=0.0
                cNew=p
                nNew=(0.0,0.0)
            if d is None or dNew<d:
                d=dNew
                c=cNew
                n=nNew
        return d,c,n
    
    def draw(self,pss=None,margin=0.0005):
        gldraw.setcolor(COST_REGION_COLOR[0],COST_REGION_COLOR[1],COST_REGION_COLOR[2])
        for p in self.pss:
            p.draw(margin,fill=True)
        Cost.draw(self,pss,margin)
    
    def get_directional_cost(self,p,d,id=None):
        raise RuntimeError("Directional cost only supported for quadratic model!")
    
class CostDistToRegionQuadratic(CostDistToRegion):
    def __init__(self,sss=None):
        CostDistToRegion.__init__(self,sss)
        self.build_dirichlet()
    
    def add_triangle(self,t0,t1,t2):
        CostDistToRegion.add_triangle(self,t0,t1,t2)
        self.build_dirichlet()
        
    def add_quad(self,q0,q1,q2,q3):
        CostDistToRegion.add_quad(self,q0,q1,q2,q3)
        self.build_dirichlet()
    
    def add_bb(self,minC,maxC):
        CostDistToRegion.add_bb(self,minC,maxC)
        self.build_dirichlet()
    
    def build_dirichlet(self):
        import shapely.geometry as geom
        for poly in self.pss:
            #edge/ray
            self.edges=[]
            self.rays=[]
            for i in range(len(poly.vss)):
                v=poly.vertex(i)
                n0=perp(poly.edge(i-1))
                n0=op.mul(n0,1/op.norm(n0))
                n1=perp(poly.edge(i))
                n1=op.mul(n1,1/op.norm(n1))
                self.edges.append(geom.LineString([poly.vertex(i),poly.vertex(i+1)]))
                self.rays.append(geom.LineString([v,op.add(v,op.mul(n0,INFINITY_NUMBER))]))
                self.rays.append(geom.LineString([v,op.add(v,op.mul(n1,INFINITY_NUMBER))]))
                
            #vert dirichlet
            self.vertDirichlet=[]
            for i in range(len(poly.vss)):
                v0=poly.vertex(i)
                n0=poly.edge(i-1)
                n0=op.mul(n0,1/op.norm(n0))
                
                v1=v0
                n1=op.mul(poly.edge(i),-1)
                n1=op.mul(n1,1/op.norm(n1))
                self.vertDirichlet.append((v0,n0,v1,n1))
                
            #edge dirichlet
            self.edgeDirichlet=[]
            for i in range(len(poly.vss)):
                v0=poly.vertex(i)
                n0=poly.edge(i)
                n0=op.mul(n0,1/op.norm(n0))
                
                v1=poly.vertex(i+1)
                n1=op.mul(n0,-1)
                n1=op.mul(n1,1/op.norm(n1))
                self.edgeDirichlet.append((v0,n0,v1,n1))
    
    def region_dirichlet(self,p):
        import shapely.geometry as geom
        if self.pss[0].hull.intersects(geom.Point(p)):
            return None,None
        for iv,dv in enumerate(self.vertDirichlet):
            if op.dot(op.sub(p,dv[0]),dv[1])>=0 and op.dot(op.sub(p,dv[2]),dv[3])>=0:
                return dv[0],None
        for iv,dv in enumerate(self.edgeDirichlet):
            if op.dot(op.sub(p,dv[0]),perp(dv[1]))>=0:
                if op.dot(op.sub(p,dv[0]),dv[1])>=0 and op.dot(op.sub(p,dv[2]),dv[3])>=0:
                    return dv[0],perp(dv[1])
        #numeric error
        return None,None
    
    def intersect_dirichlet(self,a,b):
        import shapely.geometry as geom
        line=geom.LineString([a,b])
        alphas=[]
        #ray edge
        for ie,e in enumerate(self.edges):
            inter=line.intersection(e)
            if not inter.is_empty:
                v=(inter.coords.xy[0][0],inter.coords.xy[1][0])
                alphas.append(op.norm(op.sub(v,a)))
        #ray intersection
        for ir,r in enumerate(self.rays):
            inter=line.intersection(r)
            if not inter.is_empty:
                v=(inter.coords.xy[0][0],inter.coords.xy[1][0])
                alphas.append(op.norm(op.sub(v,a)))
        #handle sorted
        return sorted(alphas)
    
    def dist_to_region(self,p,id=None):
        d,c,n=CostDistToRegion.dist_to_region(self,p,id)
        n=op.mul(n,d*2)
        d=d*d
        return d,c,n
    
    def get_directional_cost(self,p,d,id=None):
        if isinstance(p,list) and isinstance(d,list):
            return [DirectionalCost(pi,di,self) for pi,di in zip(p,d)]
        else:
            assert len(self.pss)==1
            return DirectionalCost(p,d,self)
    
    def draw_dirichlet(self,margin=0.0005):
        gldraw.setcolor(COST_DIRICHLET_COLOR[0],COST_DIRICHLET_COLOR[1],COST_DIRICHLET_COLOR[2])
        gl.glBegin(gl.GL_LINES)
        for l in self.rays:
            a=(l.coords.xy[0][0],l.coords.xy[1][0])
            gl.glVertex3f(a[0],a[1],margin)
            b=(l.coords.xy[0][1],l.coords.xy[1][1])
            gl.glVertex3f(b[0],b[1],margin)
        gl.glEnd()
    
class OptimalTransportCost(Cost):
    def __init__(self,costInners,capacities,frequentUpdate=False):
        self.costInners=costInners
        self.capacities=capacities
        self.frequentUpdate=frequentUpdate
        assert len(self.costInners)==len(self.capacities)
        
    def __deepcopy__(self,memo):
        if id(self) in memo:
            return memo[id(self)]
        result=CostDistToRegion(copy.deepcopy(self.costInners,memo),copy.deepcopy(self.capacities,memo))
        memo[id(self)]=result
        return result
        
    def calc_costs_all(self,pss):
        return [c.calc_costs(pss) for c in self.costInners]
        
    def calc_cost(self,pss,category=None):
        if not hasattr(self,"regionTag") or len(self.regionTag)!=len(pss) or self.frequentUpdate:
            self.update_region_tag(pss,category)
        c=0.0
        for i,ci in enumerate(self.costInners):
            c+=ci.calc_cost([p for p,r in zip(pss,self.regionTag) if r==i])
        return c
    
    def calc_cost_dist(self,pss,category=None):
        c=0.0
        dsss=[None for r in self.costInners]
        if not hasattr(self,"regionTag") or len(self.regionTag)!=len(pss) or self.frequentUpdate:
            self.update_region_tag(pss,category)
        for i,ci in enumerate(self.costInners):
            costi,dssi=ci.calc_cost_dist([p for p,r in zip(pss,self.regionTag) if r==i])
            dsss[i]=dssi
            c+=costi
        #re-assemble
        ret=[]
        retid=[0 for c in self.costInners]
        for ir,r in enumerate(self.regionTag):
            ret.append(dsss[r][retid[r]])
            retid[r]+=1
        return c,ret
    
    def dist_to_region(self,p,id):
        return self.costInners[self.regionTag[id]].dist_to_region(p)
    
    def update_region_tag(self,pss,category=None):
        costsAll=self.calc_costs_all(pss)
        assert category is not None
        cluster=[[i for i in range(len(pss)) if category[i]==c] for c in range(max(category)+1)]
        #solve generalized optimal transport problem:
        #min  \sum_{i,j} z_{ij}*costsAll[i][j]
        #s.t. \sum_{i} z_{ij}=1
        #     \sum_{j} z_{ij}<=self.capacities[i]
        M=op.gp.Model("OptimalTransport")
        M.setParam("LogToConsole",0)
        M.setParam("LogToFile","")
        z=[[M.addVar(vtype=op.GRB.BINARY) for j in range(len(pss))] for i in range(len(self.costInners))]
        #objective
        M.setObjective(op.dot(z,costsAll),op.GRB.MINIMIZE)
        #constraint
        for ic,c in enumerate(cluster):
            #only one region
            for j in c:
                M.addConstr(sum([z[i][j] for i in range(len(self.costInners))])==1)
            #capacity
            for i in range(len(self.costInners)):
                M.addConstr(sum([z[i][j] for j in c])<=self.capacities[i][ic])
        #solve the problem
        M.optimize()
        if M.status==op.GRB.OPTIMAL:
            #print("Optimal solution found!")
            self.regionTag=[0]*len(pss)
            for i in range(len(self.costInners)):
                for j in range(len(pss)):
                    if z[i][j].x>0.5:
                        self.regionTag[j]=i
        else:
            print("No RegionTag solution found!")
            assert False
            self.regionTag=None
    
    def draw(self,pss=None,margin=0.0005,category=None):
        for c in self.costInners:
            c.draw(None,margin)
        Cost.draw(self,pss,margin,category)
    
    def get_directional_cost(self,p,d,id=None):
        if isinstance(p,list) and isinstance(d,list):
            pss=[[pi for pi,idi in zip(p,id) if idi==ic] for ic in range(len(self.costInners))]
            dss=[[di for di,idi in zip(d,id) if idi==ic] for ic in range(len(self.costInners))]
            retss=[c.get_directional_cost(pss[ic],dss[ic]) for ic,c in enumerate(self.costInners)]
            #re-assemble
            ret=[]
            retid=[0 for c in self.costInners]
            for i in id:
                ret.append(retss[i][retid[i]])
                retid[i]+=1
            return ret
        else: return self.costInners[self.regionTag[id]].get_directional_cost(p,d)
    
    def get_capacities(self):
        return self.capacities
    
class CostDistToRegionFunction(sym.NLPFunction):
    def __init__(self,args,cost,id):
        sym.NLPFunction.__init__(self,args,"CostDistToRegion")
        assert len(args)==2
        self.cost=cost
        self.id=id
       
    def __str__(self):
        return "CostDistToRegion(["+str(self.args[0])+","+str(self.args[1])+"],"+str(self.id)+")"
    
    def eval(self,x):
        return self.cost.dist_to_region([self.eval_safe(0,x),self.eval_safe(1,x)],self.id)[0]
    
    def jac(self,x,res=None,coef=None):
        if res is None:
            res=dict()
        _,_,n=self.cost.dist_to_region([self.eval_safe(0,x),self.eval_safe(1,x)],self.id)
        for d in range(2):
            self.jac_safe(d,x,res,n[d]*(1.0 if coef is None else coef))
        return res
    
class CostGLVisualizer(GLVisualizer):
    def __init__(self,world,table,robot):
        GLVisualizer.__init__(self,world,table,robot,None)
        self.pss=[(random.uniform(-10,10),random.uniform(-10,10)) for i in range(100)]
        self.cost=CostDistToRegionQuadratic(((5,1),(7,1),(6,2)))
        
        #directional cost
        self.p=[0.0,0.0]
        self.theta=0.0
        self.dir=None
        
        self.thetaInc=False
        self.thetaDec=False
        self.xInc=False
        self.xDec=False
        self.yInc=False
        self.yDec=False
        self.speed=0.01

    def keyboardfunc(self,c,x,y):
        GLVisualizer.keyboardfunc(self,c,x,y)
        if c==b'1':
            if self.dir is None:
                self.dir=self.cost.get_directional_cost(self.p,(op.cos_safe(self.theta),op.sin_safe(self.theta)))
            else: self.dir=None
        elif c==b'2':
            deltaAlpha=0.1
            dir=self.cost.get_directional_cost(self.p,(op.cos_safe(self.theta),op.sin_safe(self.theta)))
            dirAdded=dir.add(deltaAlpha)
            
            alpha=random.uniform(0,10)
            d1=dir.calc_cost_alpha(alpha)
            d2=dirAdded.calc_cost_alpha(alpha+deltaAlpha)
            print("d1=",d1,"d2=",d2)
        elif c==b'y':
            self.thetaInc=True
        elif c==b'i':
            self.thetaDec=True
        elif c==b'h':
            self.xInc=True
        elif c==b'k':
            self.xDec=True
        elif c==b'u':
            self.yInc=True
        elif c==b'j':
            self.yDec=True

    def keyboardupfunc(self,c,x,y):
        GLVisualizer.keyboardupfunc(self,c,x,y)
        if c==b'y':
            self.thetaInc=False
        elif c==b'i':
            self.thetaDec=False
        elif c==b'h':
            self.xInc=False
        elif c==b'k':
            self.xDec=False
        elif c==b'u':
            self.yInc=False
        elif c==b'j':
            self.yDec=False
        
    def display(self):
        if self.dir is None:
            self.cost.draw(self.pss)
        else:
            self.cost.draw()
            self.cost.draw_dirichlet()
            self.dir.draw()
            self.dir.draw_cost()

    def idle(self):
        GLVisualizer.handle_camera(self)
        if self.thetaInc:
            self.theta+=math.pi/180.0
            self.dir=self.cost.get_directional_cost(self.p,(op.cos_safe(self.theta),op.sin_safe(self.theta)))
        if self.thetaDec:
            self.theta-=math.pi/180.0
            self.dir=self.cost.get_directional_cost(self.p,(op.cos_safe(self.theta),op.sin_safe(self.theta)))
        if self.xInc:
            self.p[0]-=self.speed
            self.dir=self.cost.get_directional_cost(self.p,(op.cos_safe(self.theta),op.sin_safe(self.theta)))
        if self.xDec:
            self.p[0]+=self.speed
            self.dir=self.cost.get_directional_cost(self.p,(op.cos_safe(self.theta),op.sin_safe(self.theta)))
        if self.yInc:
            self.p[1]+=self.speed
            self.dir=self.cost.get_directional_cost(self.p,(op.cos_safe(self.theta),op.sin_safe(self.theta)))
        if self.yDec:
            self.p[1]-=self.speed
            self.dir=self.cost.get_directional_cost(self.p,(op.cos_safe(self.theta),op.sin_safe(self.theta)))
    
if __name__=='__main__':
    cost1=CostDistToRegionQuadratic(((1,1),(3,1),(2,2)))
    cost2=CostDistToRegionQuadratic(((5,1),(7,1),(6,2)))
    cost=OptimalTransportCost([cost1,cost2],[2,2])
    cost.get_directional_cost(p=[(4,3),(2,6),(3,5),(6,9),(19,-8)],d=[(2,1),(-10,7),(20,5),(-9,-9),(-20,9)],id=[0,1,0,1,1])
    
    pss=[(0,0),(2,0),(4,0),(2,1.5)]
    for p in pss:
        print("CostDistToRegion at (%f,%f): %f!"%(p[0],p[1],cost1.calc_cost([p])))    
    print("OptimalTransportCost: %f!"%(cost.calc_cost(pss)))
    
    func=CostDistToRegionFunction([sym.NLPVariable(0),sym.NLPVariable(1)],cost,0)
    sym.debug_jac(func,[random.uniform(0,1),random.uniform(0,1)])
    
    world,table,robot=make_scene(True,5,5)
    CostGLVisualizer(world,table,robot).run()