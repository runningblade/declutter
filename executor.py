from klampt import WorldModel,Geometry3D,Simulator
from utils import *
import numpy as np
import copy

class Executor:
    def __init__(self,world):
        self.world=world
        
    def execute(self,x,dt):
        raise NotImplementedError("This is abstract super class, use subclass!")
    
class KlamptExecutor(Executor):
    def __init__(self,sim,velLmt=1,kP=1000,kI=0,kD=1000,margin=0.001):
        Executor.__init__(self,sim.world)
        assert self.world.numRobots()==1
        self.velLmt=velLmt
        self.sim=sim
        self.xlock=None
        self.PD=self.sim.controller(0)
        set_simulator_margin(self.world,self.sim,margin)
        
        #analyze driver
        nrDOF=len(self.world.robot(0).getConfig())
        nrDriver=self.world.robot(0).numDrivers()
        self.driver2DOFAffine=np.zeros((nrDOF,nrDriver))
        self.driver2DOFOffset=np.zeros(nrDOF)
        for d in range(nrDriver):
            driver=self.world.robot(0).driver(d)
            if driver.getType()=="normal":
                lid=driver.getAffectedLink()
                self.driver2DOFAffine[lid,d]=1.0
            elif driver.getType()=="affine":
                lss=driver.getAffectedLinks()
                scale,offset=driver.getAffineCoeffs()
                for lid,s,o in zip(lss,scale,offset):
                    self.driver2DOFAffine[lid,d]=s
                    self.driver2DOFOffset[lid]+=o
            else: 
                print("Unsupported driver type!")
                assert False
                
        #inverse
        self.DOF2DriverAffine=np.linalg.pinv(self.driver2DOFAffine)
        self.DOF2DriverOffset=-self.DOF2DriverAffine.dot(np.transpose(self.driver2DOFOffset))
        #print(self.driver2DOFAffine,self.driver2DOFOffset)
        #print(self.DOF2DriverAffine,self.DOF2DriverOffset)
        
        #PID gains
        if op.is_float(kP):
            kP=[kP]*nrDriver
        if op.is_float(kI):
            kI=[kI]*nrDriver
        if op.is_float(kD):
            kD=[kD]*nrDriver
        self.PD.setPIDGains(kP,kI,kD)
        
    def DOF2driver(self,x):
        return self.DOF2DriverAffine.dot(np.transpose(np.array(x)))+self.DOF2DriverOffset
    
    def dDOF2driver(self,x):
        return self.DOF2DriverAffine.dot(np.transpose(np.array(x)))
        
    def execute(self,x,dt=None):
        coef=1.0
        xCurr=self.world.robot(0).getConfig()
        
        #target
        if x is None:
            if self.xlock is None:
                self.xlock=self.world.robot(0).getConfig()
            x=self.xlock
        else: 
            self.xlock=copy.copy(x)
        
        #velocity    
        if dt is None:
            v=[0]*len(x)
        else:
            v=op.mul(op.sub(x,xCurr),1/dt)
        if op.norm(v)>self.velLmt:
            coef=self.velLmt/op.norm(v)
            x=interp_1d(xCurr,x,coef)
            v=op.mul(op.sub(x,xCurr),1/dt)
            #coef=0.0    #this tells trajectory searcher/tracker to stop proceed time in the reference trajectory
        
        x=self.DOF2driver(x).tolist()
        v=self.dDOF2driver(v).tolist()
        self.PD.setPIDCommand(x,v)
        if dt is not None:
            self.sim.simulate(dt)
            return dt*coef
        else:
            return None
    