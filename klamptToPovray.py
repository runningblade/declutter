from klampt import WorldModel,Geometry3D,GeometricPrimitive,VolumeGrid,PointCloud,Appearance
from klampt.vis import GLProgram,camera,gldraw
import klampt.math.vectorops as op
import klampt.math.se3 as se3
import klampt.math.so3 as so3
import math,os
if os.path.exists(".vapory"):
    import vapory.vapory as vp
else: import vapory as vp

def patch_vapory():
    for name in ['Mesh','Mesh2','VertexVectors','NormalVectors','FaceIndices']:
        if name not in dir(vp):
            setattr(vp,name,type(name,(vp.POVRayElement,object),{}))

def get_property(properties,objects,name,default=None):
    if objects is None:
        return properties[name] if name in properties else default
    elif isinstance(objects,list) and len(objects)==0:
        return properties[name] if name in properties else default
    elif isinstance(objects,tuple) and len(list(objects))==0:
        return properties[name] if name in properties else default
    else:
        for o in objects:
            if o in properties and name in properties[o]:
                return properties[o][name]
            if callable(getattr(o,"getName",None)):
                if o.getName() in properties and name in properties[o.getName()]:
                    return properties[o.getName()][name]
        return properties[name] if name in properties else default
    
def get_property_yes(properties,objects,name,default=None):
    ret=get_property(properties,objects,name,default)
    return ret is not None and (not isinstance(ret,int) or ret>0)
    
def add_light(properties,pos,tgt=None,color=[1.,1.,1.], \
              spotlight=False,radius=15.,falloff=20.,tightness=10., \
              area=0.,sample=9,adaptive=True,jitter=True):
    if 'lights' not in properties:
        properties['lights']=[]
    
    if isinstance(pos,list) and isinstance(pos[0],list):
        if isinstance(tgt,list) and isinstance(tgt[0],list):
            for p,t in zip(pos,tgt):
                add_light(properties,pos=p,tgt=t,color=color,   \
                          spotlight=spotlight,radius=radius,falloff=falloff,tightness=tightness,    \
                          area=area,sample=sample,adaptive=adaptive,jitter=jitter)
        else:
            for p in pos:
                add_light(properties,pos=p,tgt=tgt,color=color, \
                          spotlight=spotlight,radius=radius,falloff=falloff,tightness=tightness,    \
                          area=area,sample=sample,adaptive=adaptive,jitter=jitter)
    else:
        light_params=[list(pos),'color',list(color)]
        if spotlight:
            light_params+=['spotlight','radius',radius,'falloff',falloff,'tightness',tightness,'point_at',tgt]
        if area>0.:
            d=op.sub(tgt,pos)
            d=op.mul(d,1/op.norm(d))
            minId=None
            for i in range(3):
                if abs(d[i])<=abs(d[(i+1)%3]) and abs(d[i])<=abs(d[(i+2)%3]):
                    minId=i
            t0=[0,0,0]
            t0[minId]=1.
            t1=op.cross(d,t0)
            t1=op.mul(t1,1/op.norm(t1))
            t0=op.cross(d,t1)
            light_params+=['area_light',list(op.mul(t0,area)),list(op.mul(t1,area)),sample,sample,'adaptive',1,'jitter']
        properties['lights'].append(vp.LightSource(*light_params))

def create_env_light_for_bb(bb,distRatio=1.5,zdim=2,nLight=3):
    xdim=(zdim+1)%3
    ydim=(zdim+2)%3
    
    ctr=[0.,0.,0.]
    ctr[xdim]=(bb[0][xdim]+bb[1][xdim])/2
    ctr[ydim]=(bb[0][ydim]+bb[1][ydim])/2
    ctr[zdim]=(bb[1][zdim]-bb[0][zdim])*distRatio+bb[1][zdim]
    
    x=(bb[1][xdim]-bb[0][xdim])/2
    y=(bb[1][ydim]-bb[0][ydim])/2
    dist=op.norm((x,y))*(1.+distRatio)
    
    pos=[]
    for d in range(nLight):
        ang=math.pi*2*d/nLight
        ctrd=[c for c in ctr]
        ctrd[xdim]+=dist*math.cos(ang)
        ctrd[ydim]+=dist*math.sin(ang)
        pos.append(ctrd)
        
    tgt=[c for c in ctr]
    tgt[zdim]=bb[0][zdim]
    return pos,tgt

def geometry_to_povray(appearance,geometry,object,transform,properties):
    if get_property_yes(properties,[geometry,object],"hide"):
        return []
    
    #analyze appearance
    tex=get_property(properties,[geometry,object],'texture')
    if tex is None:
        tex_params=[]
        #pigment
        pigment=get_property(properties,[geometry,object],'pigment')
        if pigment is None:
            transmit=1.-appearance.getColor()[3]
            pigment=vp.Pigment(*['color',list(appearance.getColor())[0:3],
				 'transmit',get_property(properties,[geometry,object],'ambient',transmit)])
        tex_params.append(pigment)
        
        #finish
        finish=get_property(properties,[geometry,object],'finish')
        if finish is None:
            finish=vp.Finish(*['ambient',get_property(properties,[geometry,object],'ambient',0.2), \
                               'diffuse',get_property(properties,[geometry,object],'diffuse',0.7), \
                               'phong',get_property(properties,[geometry,object],'phong',1.),  \
                               'phong_size',get_property(properties,[geometry,object],'phong_size',50)])
        tex_params.append(finish)
        
        #normal
        normal=get_property(properties,[geometry,object],'normal')
        if normal is not None:
            tex_params.append(normal)
        
        #texture
        tex=vp.Texture(*tex_params)
    
    #create geometry
    ret=[]
    currentTransform=get_property(properties,[geometry,object],"transform",geometry.getCurrentTransform())
    if transform is None:
        transform=currentTransform
    else: transform=se3.mul(transform,currentTransform)
    if geometry.type()=="GeometricPrimitive":
        prim=geometry.getGeometricPrimitive()
        if get_property_yes(properties,[prim,geometry,object],"hide"):
            return ret
        if prim.type=="Point":
            rad=get_property(properties,[prim,geometry,object],"radius")
            if rad is not None:
                mesh_param=[se3.apply(transform,prim.properties[0:3]),rad]
                mesh_param.append(tex)
                mesh=vp.Sphere(*mesh_param)
                ret.append(mesh)
        elif prim.type=="Sphere":
            mesh_param=[se3.apply(transform,prim.properties[0:3]),prim.properties[3]]
            mesh_param.append(tex)
            mesh=vp.Sphere(*mesh_param)
            ret.append(mesh)
        elif prim.type=="Segment":
            rad=get_property(properties,[prim,geometry,object],"radius")
            if rad is not None:
                a=prim.properties[0:3]
                b=prim.properties[3:6]
                mesh_param=[se3.apply(transform,a),se3.apply(transform,b),rad]
                mesh_param.append(tex)
                mesh=vp.Cylinder(*mesh_param)
                ret.append(mesh)
                
                arrowLength=get_property(properties,[prim,geometry,object],"arrowLength")
                arrowRadius=get_property(properties,[prim,geometry,object],"arrowRadius")
                if (arrowLength is not None) and (arrowRadius is not None):
                    l=op.norm(op.sub(a,b))
                    n=op.div(op.sub(a,b),l)
                    l=min(arrowLength,l)
                    a=op.add(b,op.mul(n,l))
                    mesh_param=[se3.apply(transform,a),arrowRadius,se3.apply(transform,b),rad]
                    mesh_param.append(tex)
                    mesh=vp.Cone(*mesh_param)
                    ret.append(mesh)
        elif prim.type=="AABB":
            mesh_param=[se3.apply(transform,prim.properties[0:3]),se3.apply(transform,prim.properties[3:6])]
            mesh_param.append(tex)
            mesh=vp.Box(*mesh_param)
            ret.append(mesh)
    elif geometry.type()=="Group":
        for idElem in range(geometry.numElements()):
            elem=geometry.getElement(idElem)
            elem.getCurrentTransform()
            ret+=geometry_to_povray(appearance=appearance,geometry=elem,object=object,transform=transform,properties=properties)
    elif geometry.type()=="TriangleMesh":
        tm=geometry.getTriangleMesh()
        
        if get_property_yes(properties,[geometry,object],"smooth"):
            vss=[se3.apply(transform,tuple(tm.vertices[i*3:i*3+3])) for i in range(len(tm.vertices)//3)]
            iss=[tuple(tm.indices[i*3:i*3+3]) for i in range(len(tm.indices)//3)]
            mesh_param=[vp.VertexVectors(*([len(vss)]+vss)),vp.FaceIndices(*([len(iss)]+iss))]
            mesh_param.append(tex)
            mesh=vp.Mesh2(*mesh_param)
        else:
            vss=[se3.apply(transform,tuple(tm.vertices[i*3:i*3+3])) for i in range(len(tm.vertices)//3)]
            iss=[tuple(tm.indices[i*3:i*3+3]) for i in range(len(tm.indices)//3)]
            mesh_param=[vp.Triangle(vss[it[0]],vss[it[1]],vss[it[2]]) for it in iss]
            mesh_param.append(tex)
            mesh=vp.Mesh(*mesh_param)
        
        ret.append(mesh)
    elif geometry.type()=="VolumeGrid":
        from skimage import measure
        import numpy as np
        grid=geometry.getVolumeGrid()
        volume=np.reshape(np.array(list(grid.values)),tuple(grid.dims))
        spacing=[(b-a)/d for a,b,d in zip(grid.bbox[0:3],grid.bbox[3:6],grid.dims[0:3])]
        vss,iss,nss,_=measure.marching_cubes_lewiner(volume,level=0.,spacing=spacing)
        vss+=np.expand_dims(np.array(grid.bbox[0:3]).T,0)
        vss=[vss[it,:].tolist() for it in range(vss.shape[0])]
        iss=[iss[it,:].tolist() for it in range(iss.shape[0])]
        nss=[nss[it,:].tolist() for it in range(nss.shape[0])]
        mesh_param=[vp.VertexVectors(*([len(vss)]+vss)),vp.NormalVectors(*([len(nss)]+nss)),vp.FaceIndices(*([len(iss)]+iss))]
        mesh_param.append(tex)
        mesh=vp.Mesh2(*mesh_param)
        ret.append(mesh)
    elif geometry.type()=="PointCloud":
        cloud_param=[]
        cloud=geometry.getPointCloud()
        rad=get_property(properties,[cloud,geometry,object],"radius")
        for id in range(len(cloud.vertices)//3):
            cloud_param.append(vp.Sphere(cloud.vertices[id*3:id*3+3],rad))
        cloud_param.append(tex)
        mesh=vp.Union(*cloud_param)
        ret.append(mesh)
    else:
        print("Geometry (name=%s) type: %s not supported!"%(object.getName(),geometry.type()))
    return ret

def klampt_to_povray(vis,world,properties={}):
    #patch on vapory
    patch_vapory()
    
    #camera
    mat=vis.view.camera.matrix()
    pos=mat[1]
    right=mat[0][0:3]
    up=mat[0][3:6]
    dir=op.mul(mat[0][6:9],-1)
    tgt=op.add(mat[1],dir)
    #scale
    fovy=vis.view.fov*vis.view.h/vis.view.w
    fovx=math.atan(vis.view.w*math.tan(fovy*math.pi/360.)/vis.view.h)*360./math.pi
    right=op.mul(right,-float(vis.view.w)/vis.view.h)
    #camera
    camera_params=['orthographic' if vis.view.orthogonal else 'perspective',
                   'location',[pos[0],pos[1],pos[2]],
                   'look_at',[tgt[0],tgt[1],tgt[2]],
                   'right',[right[0],right[1],right[2]],
                   'up',[up[0],up[1],up[2]],
                   'angle',fovx,
                   'sky',get_property(properties,[],"sky",[0.,0.,1.])]
    camera=vp.Camera(*camera_params)
    
    #tempfile
    tempfile=get_property(properties,[],"tempfile",None)
    tempfile_path=os.path.dirname(tempfile) if tempfile is not None else '.'
    if not os.path.exists(tempfile_path):
        os.mkdir(tempfile_path)
    
    #objects
    objects=[]
    objs=[o for o in properties["visualObjects"]] if "visualObjects" in properties else []
    objs+=[world.terrain(i) for i in range(world.numTerrains())]
    objs+=[world.rigidObject(i) for i in range(world.numRigidObjects())]
    for r in range(world.numRobots()):
        objs+=[world.robot(r).link(i) for i in range(world.robot(r).numLinks())]
    for obj in objs:
        transient=get_property(properties,[obj],"transient",default=True)
        if transient:
            objects+=geometry_to_povray(obj.appearance(),obj.geometry(),obj,None,properties=properties)
        else: 
            path=tempfile_path+'/'+obj.getName()+'.pov'
            if not os.path.exists(path):
                R,t=obj.geometry().getCurrentTransform()
                obj.geometry().setCurrentTransform([1,0,0,0,1,0,0,0,1],[0,0,0])
                geom=geometry_to_povray(obj.appearance(),obj.geometry(),obj,None,properties=properties)
                if len(geom)>1:
                    file_content=vp.Union(*geom)
                elif len(geom)>0: 
                    file_content=vp.Object(*geom)
                else: file_content=None
                if file_content is not None:
                    f=open(path,'w')
                    f.write(str(file_content))
                    f.close()
                    obj.geometry().setCurrentTransform(R,t)
                else: path=None
            #include    
            if path is not None:
                R,t=obj.geometry().getCurrentTransform()
                objects.append(vp.Object('#include "%s"'%path,"matrix",R+t))
            
    #light
    if "lights" in properties:
        objects+=properties["lights"]
            
    #scene
    gsettings=[]
    scene=vp.Scene(camera=camera,
                   objects=objects,
                   included=get_property(properties,[],"included",[]),
                   global_settings=get_property(properties,[],"global_settings",[]))
    try:
        #this works with later version of vapory
        return  \
        scene.render(outfile=get_property(properties,[],"outfile",None),  \
                     width=vis.view.w,height=vis.view.h,    \
                     quality=get_property(properties,[],"quality",None),    \
                     antialiasing=get_property(properties,[],"antialiasing",0.3),    \
                     remove_temp=get_property(properties,[],"remove_temp",False),    \
                     auto_camera_angle=get_property(properties,[],"auto_camera_angle",False),    \
                     show_window=get_property(properties,[],"show_window",False),     \
                     tempfile=tempfile, \
                     includedirs=get_property(properties,[],"includedirs",None),    \
                     output_alpha=get_property(properties,[],"output_alpha",True))
    except:
        #this works with earlier version of vapory
        return  \
        scene.render(outfile=get_property(properties,[],"outfile",None),  \
                     width=vis.view.w,height=vis.view.h,    \
                     quality=get_property(properties,[],"quality",None),    \
                     antialiasing=get_property(properties,[],"antialiasing",0.3),    \
                     remove_temp=get_property(properties,[],"remove_temp",False),    \
                     auto_camera_angle=get_property(properties,[],"auto_camera_angle",False))
    
if __name__=='__main__':
    import random
    world=WorldModel()
    
    #example Floor
    floor=world.makeRigidObject("Floor")
    prim=GeometricPrimitive()
    prim.setAABB((-.1,-.1,-0.1),(.2,1.6,0.))
    floor.geometry().setGeometricPrimitive(prim)
    floor.appearance().setColor(.5,.5,.5)
    
    #example Point
    point=world.makeRigidObject("Point")
    prim=GeometricPrimitive()
    prim.setPoint((0.05,0.0,0.05))
    point.geometry().setGeometricPrimitive(prim)
    point.appearance().setColor(random.uniform(0.,1.),random.uniform(0.,1.),random.uniform(0.,1.),1.)
    
    #example Sphere
    point=world.makeRigidObject("Sphere")
    prim=GeometricPrimitive()
    prim.setSphere((0.05,0.1,0.05),0.02)
    point.geometry().setGeometricPrimitive(prim)
    point.appearance().setColor(random.uniform(0.,1.),random.uniform(0.,1.),random.uniform(0.,1.),1.)
    
    #example Segment
    point=world.makeRigidObject("Segment")
    prim=GeometricPrimitive()
    prim.setSegment((0.05,0.2,0.0),(0.05,0.2,0.1))
    point.geometry().setGeometricPrimitive(prim)
    point.appearance().setColor(random.uniform(0.,1.),random.uniform(0.,1.),random.uniform(0.,1.),1.)
    
    #example AABB
    aabb=world.makeRigidObject("AABB")
    prim=GeometricPrimitive()
    prim.setAABB((0.,0.3,0.),(0.1,0.4,0.1))
    aabb.geometry().setGeometricPrimitive(prim)
    aabb.appearance().setColor(random.uniform(0.,1.),random.uniform(0.,1.),random.uniform(0.,1.),0.5)
    
    #example TriangleMesh
    ladder=world.makeTerrain("TriangleMesh")
    mesh=Geometry3D()
    mesh.loadFile("./data/terrains/drc_ladder.off")
    mesh.transform([.033,0,0,0,.033,0,0,0,.033],[0.05,0.5,0])
    ladder.geometry().set(mesh)
    ladder.appearance().setColor(random.uniform(0.,1.),random.uniform(0.,1.),random.uniform(0.,1.),1.)
    
    #example Group
    ladders=world.makeTerrain("Group")
    ladders_geom=Geometry3D()
    ladders_geom.setGroup()
    for d in range(4):
        mesh=Geometry3D()
        mesh.loadFile("./data/terrains/drc_ladder.off")
        mesh.transform(so3.from_axis_angle(([0,0,1],math.pi/2*d)),[0,0,0])
        mesh.transform([.033,0,0,0,.033,0,0,0,.033],[0.05,0.8,0])
        ladders_geom.setElement(d,mesh)
    ladders.geometry().set(ladders_geom)
    ladders.appearance().setColor(random.uniform(0.,1.),random.uniform(0.,1.),random.uniform(0.,1.),1.)
    
    #example VolumeGrid
    sdf=world.makeTerrain("SDF")
    grid=VolumeGrid()
    grid.setBounds([0.,1.,0.],[.1,1.1,.1])
    grid.resize(32,32,32)
    for idx in range(grid.dims[0]):
        for idy in range(grid.dims[1]):
            for idz in range(grid.dims[2]):
                d=[(idx-grid.dims[0]/2)/float(grid.dims[0]/2),  \
                   (idy-grid.dims[1]/2)/float(grid.dims[1]/2),  \
                   (idz-grid.dims[2]/2)/float(grid.dims[2]/2)]
                freq=2.
                amp=.1
                thres=.5*(1.+math.sin(d[0]*math.pi*freq)*math.sin(d[1]*math.pi*freq)*math.sin(d[2]*math.pi*freq)*amp)
                grid.set(idx,idy,idz,op.norm(d)/op.norm([1.,1.,1.])-thres)
    sdf.geometry().setVolumeGrid(grid)
    sdf.appearance().setColor(random.uniform(0.,1.),random.uniform(0.,1.),random.uniform(0.,1.))
    
    #example PointCloud
    points=world.makeTerrain("PointCloud")
    cloud=PointCloud()
    for i in range(1000):
        cloud.addPoint([random.uniform(0.,0.1),random.uniform(0.,0.1),random.uniform(0.,0.1)])
    points.geometry().setPointCloud(cloud)
    points.geometry().transform([1,0,0,0,1,0,0,0,1],[0.,1.2,0.])
    points.appearance().setColor(random.uniform(0.,1.),random.uniform(0.,1.),random.uniform(0.,1.))
    
    #robot
    robot=world.loadRobot("./data/robots/tx90robotiq.rob")
    for l in range(robot.numLinks()):
        lk=robot.link(l)
        t=lk.getTransform()
        t=se3.mul(([.1,0,0,0,.1,0,0,0,.1],[0.,0.,0.]),t)
        t=se3.mul(([1,0,0,0,1,0,0,0,1],[0.05,1.5,0.]),t)
        lk.setTransform(t[0],t[1])
        lk.appearance().setColor(random.uniform(0.,1.),random.uniform(0.,1.),random.uniform(0.,1.))
    
    class GLVisualizer(GLProgram):
        def __init__(self,world):
            GLProgram.__init__(self,"Visualizer")
            self.zeroZ=True
            self.world=world
            self.init_camera()
            self.properties={}

        def look_at(self,pos,tgt,scale=None):
            cam=self.view.camera
            if scale is not None:
                cam.dist=op.norm(op.sub(tgt,pos))*scale
            cam.rot=self.get_camera_rot(op.sub(pos,tgt))
            cam.tgt=tgt
            
        def get_camera_pos(self):
            cam=self.view.camera
            z=math.sin(-cam.rot[1])
            x=math.sin(cam.rot[2])*math.cos(cam.rot[1])
            y=math.cos(cam.rot[2])*math.cos(cam.rot[1])
            pos=[x,y,z]
            return op.add(cam.tgt,op.mul(pos,cam.dist))
        
        def get_camera_rot(self,d):
            angz=math.atan2(d[0],d[1])
            angy=math.atan2(-d[2],math.sqrt(d[0]*d[0]+d[1]*d[1]))
            return [0,angy,angz]
    
        def get_camera_dir(self,zeroZ=False):
            cam=self.view.camera
            dir=op.sub(cam.tgt,self.get_camera_pos())
            if zeroZ:
                dir=(dir[0],dir[1],0)
            if op.norm(dir)>1e-6:
                dir=op.mul(dir,1/op.norm(dir))
            dir[1]*=-1
            return dir
    
        def get_left_dir(self,zeroZ=False):
            dir=op.cross([0,0,1],self.get_camera_dir())
            if zeroZ:
                dir=(dir[0],dir[1],0)
            if op.norm(dir)>1e-6:
                dir=op.mul(dir,1/op.norm(dir))
            return dir
    
        def get_bb(self):
            bb=None
            for i in range(world.numTerrains()):
                bb=self.union_bb(bb,world.terrain(i).geometry().getBBTight())
            for i in range(world.numRigidObjects()):
                bb=self.union_bb(bb,world.rigidObject(i).geometry().getBBTight())
            for r in range(world.numRobots()):
                for l in range(world.robot(r).numLinks()):
                    bb=self.union_bb(bb,world.robot(r).link(l).geometry().getBBTight())
            return bb
    
        def init_camera(self):
            bb=self.get_bb()
            pos=list(bb[1])
            tgt=list(bb[0])
            
            self.look_at(pos,tgt,2.0)
            self.moveSpd=0.005
            self.zoomSpd=1.03
            self.zoomMin=0.01
            self.zoomMax=100.0
            
            self.zoomInCam=False
            self.zoomOutCam=False
            self.forwardCam=False
            self.backCam=False
            self.leftCam=False
            self.rightCam=False
            self.raiseCam=False
            self.sinkCam=False
    
        def keyboardfunc(self,c,x,y):
            if c==b'f':
                self.init_camera()
            elif c==b'z':
                self.zeroZ=not self.zeroZ
            elif c==b'q':
                self.zoomInCam=True
            elif c==b'e':
                self.zoomOutCam=True
            elif c==b'w':
                self.forwardCam=True
            elif c==b's':
                self.backCam=True
            elif c==b'a':
                self.leftCam=True
            elif c==b'd':
                self.rightCam=True
            elif c==b' ':
                self.raiseCam=True
            elif c==b'c':
                self.sinkCam=True
            elif c=='v':
                #if self.world.numRobots()>0:
                #    self.world.robot(0).setConfig([random.uniform(-1.,1.) for d in range(self.world.robot(0).numLinks())])
                klampt_to_povray(self,self.world,self.properties)
                self.save_screen('screenshot-klampt.png')
                print('rendered!')
    
        def keyboardupfunc(self,c,x,y):
            if c==b'q':
                self.zoomInCam=False
            elif c==b'e':
                self.zoomOutCam=False
            elif c==b'w':
                self.forwardCam=False
            elif c==b's':
                self.backCam=False
            elif c==b'a':
                self.leftCam=False
            elif c==b'd':
                self.rightCam=False
            elif c==b' ':
                self.raiseCam=False
            elif c==b'c':
                self.sinkCam=False
        
        def display_screen(self):
            self.draw_text((0,12),'press v to invoke renderer!',color=(0,0,0))
        
        def display(self):
            self.world.drawGL()
    
        def handle_camera(self):
            self.view.clippingplanes=(self.view.clippingplanes[0],self.zoomMax)
            cam=self.view.camera
            moveSpd=self.moveSpd*cam.dist
            if self.zoomInCam:
                cam.dist=max(cam.dist/self.zoomSpd,self.zoomMin)
            elif self.zoomOutCam:
                cam.dist=min(cam.dist*self.zoomSpd,self.zoomMax)
            elif self.forwardCam:
                delta=op.mul(self.get_camera_dir(self.zeroZ),moveSpd)
                self.look_at(op.add(self.get_camera_pos(),delta),op.add(cam.tgt,delta))
            elif self.backCam:
                delta=op.mul(self.get_camera_dir(self.zeroZ),-moveSpd)
                self.look_at(op.add(self.get_camera_pos(),delta),op.add(cam.tgt,delta))
            elif self.leftCam:
                delta=op.mul(self.get_left_dir(self.zeroZ),moveSpd)
                self.look_at(op.add(self.get_camera_pos(),delta),op.add(cam.tgt,delta))
            elif self.rightCam:
                delta=op.mul(self.get_left_dir(self.zeroZ),-moveSpd)
                self.look_at(op.add(self.get_camera_pos(),delta),op.add(cam.tgt,delta))
            elif self.raiseCam:
                delta=(0,0,moveSpd)
                self.look_at(op.add(self.get_camera_pos(),delta),op.add(cam.tgt,delta))
            elif self.sinkCam:
                delta=(0,0,-moveSpd)
                self.look_at(op.add(self.get_camera_pos(),delta),op.add(cam.tgt,delta))
    
        def idle(self):
            self.handle_camera()
            
        def union_bb(self,bb1,bb2):
            if bb1 is None:
                return bb2
            elif bb2 is None:
                return bb1
            else:
                return ([min(a,b) for a,b in zip(bb1[0],bb2[0])],[max(a,b) for a,b in zip(bb1[1],bb2[1])])
    
    vis=GLVisualizer(world)
    vis.properties["tempfile"]="tmpPovray/__temp__.pov"
    vis.properties["outfile"]="screenshot.png"
    #set default parameter of radius
    vis.properties["radius"]=0.01
    #set a different radius for world.terrain("PointCloud")
    vis.properties["PointCloud"]={"radius":0.001}   
    #locate lights according to bounding box of the world, you can also set it by yourself
    pos,tgt=create_env_light_for_bb(vis.get_bb())   
    #add lights, turn it to pointlight by setting spotlight=False, area>0 means this is area light with soft shadow
    add_light(vis.properties,pos,tgt,spotlight=True,area=.1,color=[2.,2.,2.])   
    #change the material of floor to stone
    vis.properties["Floor"]={"hide":False}
    vis.properties["Floor"]["finish"]=vp.Finish('ambient',0.,'diffuse',.5,'specular',.15)
    vis.properties["Floor"]["normal"]=vp.Normal('granite',0.2,'warp {turbulence 1}','scale',.25)
    #change the material of VolumeGrid to metal
    vis.properties["SDF"]={}
    preset=False
    if preset:
        vis.properties["SDF"]["finish"]=vp.Finish('F_MetalB')
        #F_MetalB is a predefined material in povray, you have to include 'metal.inc' to use it
        vis.properties["included"]=['metals.inc']
    else:
        vis.properties["SDF"]["finish"]=vp.Finish('ambient',0.30,'brilliance',3,'diffuse',0.4,'metallic','specular',0.70,'roughness',1/60.,'reflection',.25)
    for l in range(robot.numLinks()):
        name=robot.link(l).getName()
        vis.properties[name]={"transient":False}
    vis.run()
