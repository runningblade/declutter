from utils import *
from scene import *
import numpy as np
import trimesh,os
import lxml.etree as ET

def xyz_rpy_2_T(xyz,rpy):
    T=trimesh.transformations.euler_matrix(rpy[0],rpy[1],rpy[2])
    T[:3,3]=xyz
    return T

def T_2_xyz_rpy(T):
    xyz=T[:3,3].tolist()
    rpy=list(trimesh.transformations.euler_from_matrix(T[:3,:3]))
    return xyz,rpy

def next_to_base(components):
    #sort handle
    def cmp(A,B):
        return A.centroid[2]-B.centroid[2]
    componentsSorted=sorted(components,key=cmpToKey(cmp))
    #componentsSorted[1].show()
    return componentsSorted[1]

def modify_base_mesh(path,deltaZ,deltaY,anglec,angle,flip=True,baseToTopZ=0.94):
    TL=np.identity(4,dtype=np.float64)
    TR=np.identity(4,dtype=np.float64)
    
    #components
    components=[]
    mesh=trimesh.exchange.load.load(path+"/base_link.STL")
    for c in trimesh.graph.connected_components(mesh.edges):
        vMap={}
        faces=[]
        #remap
        for f in mesh.faces:
            if f[0] in c:
                for d in range(3):
                    if f[d] not in vMap:
                        vMap[f[d]]=len(vMap)
                faces.append([vMap[f[0]],vMap[f[1]],vMap[f[2]]])
        #vertices
        vertices=[None for i in range(len(vMap))]
        for k,v in vMap.items():
            vertices[v]=[x for x in mesh.vertices[k]]
        components.append(trimesh.Trimesh(vertices,faces))
    
    #find rotation axis
    rotationCtr=None
    for ic,component in enumerate(components):
        ctr=(component.bounds[0]+component.bounds[1])/2
        if(abs(ctr[2])>1.0 and abs(ctr[1])<0.1):
            rotationCtr=component
            break
    
    #find component to be rotated
    leftBase,rightBase=[],[]
    leftShoulder,rightShoulder=[],[]
    leftRotCtr,rightRotCtr=None,None
    for ic,component in enumerate(components):
        if component.bounds[0][2]<1.0:
            continue
        elif component.bounds[0][1]>rotationCtr.bounds[1][1]:
            rightBase.append(component)
        elif component.bounds[1][1]<rotationCtr.bounds[0][1]:
            leftBase.append(component)
        elif component.bounds[0][1]>0.0:
            rightRotCtr=[0,component.bounds[1][1],component.bounds[1][2]]
            rightShoulder.append(component)
        elif component.bounds[1][1]<0.0:
            leftRotCtr=[0,component.bounds[0][1],component.bounds[1][2]]
            leftShoulder.append(component)
            
    #flip
    if flip:
        #flip shoulder
        rotCtr=rotationCtr.centroid
        R=trimesh.transformations.rotation_matrix(math.pi,[0,1,0])
        TRc=np.identity(4,dtype=np.float64)
        TRc[:3,3]-=rotCtr
        T=np.matmul(np.linalg.inv(TRc),np.matmul(R,TRc))
        leftShoulder=[m.apply_transform(T) for m in leftShoulder]
        rightShoulder=[m.apply_transform(T) for m in rightShoulder]
            
        #flip base
        TRc=np.identity(4,dtype=np.float64)
        TRc[:3,3]-=[leftBase[0].centroid[0],leftBase[0].centroid[1],rotationCtr.centroid[2]]
        R=trimesh.transformations.rotation_matrix(-math.pi/2,[1,0,0])
        T=np.matmul(np.linalg.inv(TRc),np.matmul(R,TRc))
        leftBase=[m.apply_transform(T) for m in leftBase]
        TR=np.matmul(T,TR)
        
        TRc=np.identity(4,dtype=np.float64)
        TRc[:3,3]-=[rightBase[0].centroid[0],rightBase[0].centroid[1],rotationCtr.centroid[2]]
        R=trimesh.transformations.rotation_matrix( math.pi/2,[1,0,0])
        T=np.matmul(np.linalg.inv(TRc),np.matmul(R,TRc))
        rightBase=[m.apply_transform(T) for m in rightBase]
        TL=np.matmul(T,TL)
            
    #deltaY
    deltaY=min(max(deltaY,0),0.09)
    TY=np.identity(4,dtype=np.float64)
    TY[1,3]=-deltaY
    TL=np.matmul(TY,TL)
    for c in rightShoulder:
        vertices=[]
        for v in c.vertices:
            if v[1]>c.bounds[0][1]+0.01:
                vertices.append([v[0],v[1]-deltaY,v[2]])
            else: vertices.append([v[0],v[1],v[2]])
        components.append(trimesh.Trimesh(vertices,c.faces))
        components.remove(c)
    TY[1,3]=deltaY
    TR=np.matmul(TY,TR)
    for c in leftShoulder:
        vertices=[]
        for v in c.vertices:
            if v[1]<c.bounds[1][1]-0.01:
                vertices.append([v[0],v[1]+deltaY,v[2]])
            else: vertices.append([v[0],v[1],v[2]])
        components.append(trimesh.Trimesh(vertices,c.faces))
        components.remove(c)
            
    #rotate component
    angle=min(max(angle,0),135)
    angle=math.pi*angle/180
    TRc=np.identity(4,dtype=np.float64)
    TRc[:3,3]-=rightRotCtr
    TRc[:3,3]+=[0,deltaY,0]
    R=trimesh.transformations.euler_matrix(angle,0,0)
    TL=np.matmul(np.linalg.inv(TRc),np.matmul(R,np.matmul(TRc,TL)))
    for c in rightBase:
        vertices=[[vri for vri in (np.matmul(R[0:3,0:3],(v-rightRotCtr))+rightRotCtr-[0,deltaY,0])] for v in c.vertices]
        components.append(trimesh.Trimesh(vertices,c.faces))
        components.remove(c)
    TRc=np.identity(4,dtype=np.float64)
    TRc[:3,3]-=leftRotCtr
    TRc[:3,3]-=[0,deltaY,0]
    R=trimesh.transformations.euler_matrix(-angle,0,0)
    TR=np.matmul(np.linalg.inv(TRc),np.matmul(R,np.matmul(TRc,TR)))
    for c in leftBase:
        vertices=[[vri for vri in (np.matmul(R[0:3,0:3],(v-leftRotCtr))+leftRotCtr+[0,deltaY,0])] for v in c.vertices]
        components.append(trimesh.Trimesh(vertices,c.faces))
        components.remove(c)
    
    #anglec
    TRc=np.identity(4,dtype=np.float64)
    TRc[:3,3]-=ctr
    anglec=math.pi*anglec/180
    R=trimesh.transformations.euler_matrix(anglec,0,0)
    Rc=np.matmul(np.linalg.inv(TRc),np.matmul(R,TRc))
            
    #find total_height_except_fetch
    if baseToTopZ is not None:
        z1=rotationCtr.bounds[1][2]
        z0=next_to_base(components).bounds[0][2]
        deltaZ+=baseToTopZ-(z1-z0)
    
    #deltaZ
    deltaZ=min(max(deltaZ,-0.23),0.23)
    TZ=np.identity(4,dtype=np.float64)
    TZ[2,3]=deltaZ
    TL=np.matmul(TZ,np.matmul(Rc,TL))
    TR=np.matmul(TZ,np.matmul(Rc,TR))
    for ic,component in enumerate(components):
        faces=component.faces
        vertices=component.vertices
        if component.bounds[0,2]>1.0:
            vertices=[([0,0,deltaZ]+(np.matmul(Rc[:3,:3],v)+Rc[:3,3])).tolist() for v in vertices]
        elif component.bounds[0,2]>0.5:
            vertices=[[v[0],v[1],v[2]+deltaZ] for v in vertices]
        components[ic]=trimesh.Trimesh(vertices,faces)
    
    #scene
    scene=trimesh.Scene()
    for ic,component in enumerate(components):
        scene.add_geometry(component)
    return scene,TL,TR
    
def modify_pusher_mesh(path,pusher,prefix,wristToEE,cameraTransform=None):
    mesh=trimesh.exchange.load.load(path+"/%s_EE_link.STL"%prefix)
    meshWrist=trimesh.exchange.load.load(path+"/%s_wrist3_link.STL"%prefix)
    box=trimesh.primitives.Box()
    box.apply_scale(np.array(list(pusher[0:3])))
    
    deltaZ=meshWrist.bounds[1][2]-box.bounds[0][2]
    pusherTransform=trimesh.transformations.rotation_matrix(pusher[4]*math.pi/180,[0,0,1])
    pusherTransform[2,3]=deltaZ+pusher[3]
    pusherTransform=np.matmul(np.linalg.inv(wristToEE),pusherTransform)
    box.apply_transform(pusherTransform)
    
    scene=trimesh.Scene()
    scene.add_geometry(mesh)
    scene.add_geometry(box)
    if cameraTransform is not None:
        camera=trimesh.exchange.load.load(path+"/SR305.STL")
        camera.apply_transform(cameraTransform)
        scene.add_geometry(camera)
    return scene
    
def modify_gripper_mesh(path,prefix,wristToEE,cameraTransform=None):
    mesh=trimesh.exchange.load.load(path+"/%s_EE_link.STL"%prefix)
    meshWrist=trimesh.exchange.load.load(path+"/%s_wrist3_link.STL"%prefix)
    gripper=trimesh.exchange.load.load(path+"/Epick_assembly.STL")

    deltaZ=meshWrist.bounds[1][2]-gripper.bounds[0][2]
    gripperTransform=np.identity(4)
    gripperTransform[:3,3]=[0,0,deltaZ]
    gripperTransform=np.matmul(np.linalg.inv(wristToEE),gripperTransform)
    gripper.apply_transform(gripperTransform)
    
    scene=trimesh.Scene()
    scene.add_geometry(mesh)
    scene.add_geometry(gripper)
    if cameraTransform is not None:
        camera=trimesh.exchange.load.load(path+"/SR305.STL")
        camera.apply_transform(cameraTransform)
        scene.add_geometry(camera)
    return scene
    
def modify_urdf(path,basePath,pusherPath,gripperPath,TL,TR,deltaZ,deltaY,angle):
    str=open(path,'r').read().encode()
    root=ET.fromstring(str)
    
    def set_mesh(e,p):
        dirname=os.path.dirname(path)
        p=p[len(dirname):]
        while p[0]=='/' or p[0]=='\\':
            p=p[1:]
        visualMesh=e.find("visual").find("geometry").find("mesh")
        visualMesh.set("filename",p)
        collisionMesh=e.find("collision").find("geometry").find("mesh")
        collisionMesh.set("filename",p)
    
    #replace mesh of base_link
    set_mesh([l for l in root.findall("link") if l.get("name")=="base_link"][0],basePath)
    
    #replace pusher
    if pusherPath is not None:
        try:
            set_mesh([l for l in root.findall("link") if l.get("name")=="right_EE_link"][0],pusherPath)
        except: raise RuntimeError("Cannot right_EE_link")
        
    #replace gripper
    if gripperPath is not None:
        try:
            set_mesh([l for l in root.findall("link") if l.get("name")=="left_EE_link"][0],gripperPath)
        except: raise RuntimeError("Cannot left_EE_link")
    
    #replace relative position: left
    leftJoint=[l for l in root.findall("joint") if l.get("name")=="base_left_base"][0].find("origin")
    xyz=[float(v) for v in leftJoint.get("xyz").split(" ")]
    rpy=[float(v) for v in leftJoint.get("rpy").split(" ")]
    T=np.matmul(TL,xyz_rpy_2_T(xyz,rpy))
    xyz,rpy=T_2_xyz_rpy(T)
    leftJoint.set("xyz","%f %f %f"%(xyz[0],xyz[1],xyz[2]))
    leftJoint.set("rpy","%f %f %f"%(rpy[0],rpy[1],rpy[2]))
    
    #replace relative position: right
    rightJoint=[l for l in root.findall("joint") if l.get("name")=="base_right_base"][0].find("origin")
    xyz=[float(v) for v in rightJoint.get("xyz").split(" ")]
    rpy=[float(v) for v in rightJoint.get("rpy").split(" ")]
    T=np.matmul(TR,xyz_rpy_2_T(xyz,rpy))
    xyz,rpy=T_2_xyz_rpy(T)
    rightJoint.set("xyz","%f %f %f"%(xyz[0],xyz[1],xyz[2]))
    rightJoint.set("rpy","%f %f %f"%(rpy[0],rpy[1],rpy[2]))
    return root
    
def get_wrist_to_EE(path):
    str=open(path,'r').read().encode()
    root=ET.fromstring(str)
    
    leftJoint=[l for l in root.findall("joint") if l.get("name")=="left_EE_tool"][0].find("origin")
    xyz=[float(v) for v in leftJoint.get("xyz").split(" ")]
    rpy=[float(v) for v in leftJoint.get("rpy").split(" ")]
    leftJoint=xyz_rpy_2_T(xyz,rpy)
    
    rightJoint=[l for l in root.findall("joint") if l.get("name")=="right_EE_tool"][0].find("origin")
    xyz=[float(v) for v in rightJoint.get("xyz").split(" ")]
    rpy=[float(v) for v in rightJoint.get("rpy").split(" ")]
    rightJoint=xyz_rpy_2_T(xyz,rpy)
    
    return leftJoint,rightJoint
    
def modify_Anthrax(path,deltaZ=0.0,deltaY=0.0,anglec=0.0,angle=0.0,
                   pusher=None,gripper=True,fullName=False,
                   leftCameraTransform=None,rightCameraTransform=None):
    if pusher is None:
        suffixPusher=""
    else: suffixPusher="_P"+"_"+str(pusher[0])+"_"+str(pusher[1])+"_"+str(pusher[2])
    if gripper is None:
        suffixGripper=""
    else: suffixGripper="_G"
    suffix="_"+str(deltaZ)+"_"+str(deltaY)+"_"+str(anglec)+"_"+str(angle)
    
    if not fullName:
        suffix=suffixPusher=suffixGripper="_Adjusted"
    if os.path.basename(path).startswith("Anthrax_lowpoly"):
        meshPathRel="/Anthrax_lowpoly"
        meshPath=os.path.dirname(path)+meshPathRel
    else: 
        meshPathRel="/Anthrax"
        meshPath=os.path.dirname(path)+meshPathRel
    
    def export_STL(scene,scenePath):
        scene.export(scenePath+".obj")
        scene=trimesh.exchange.load.load(scenePath+".obj")
        scene.export(scenePath+".STL")
        os.remove(scenePath+".obj")
        return scenePath+".STL"
    
    #modify and write to STL
    base_mesh,TL,TR=modify_base_mesh(meshPath,deltaZ,deltaY,anglec,angle)
    basePath=export_STL(base_mesh,meshPath+"/base_link"+suffix)
    
    #get wrist to EE
    leftWristToEE,rightWristToEE=get_wrist_to_EE(path)
    
    #pusher
    if pusher is None:
        pusherPath=None
    else:
        pusher_mesh=modify_pusher_mesh(meshPath,pusher,"right",rightWristToEE,rightCameraTransform)
        pusherPath=export_STL(pusher_mesh,meshPath+"/right_EE_link"+suffixPusher)
    
    #gripper
    if not gripper:
        gripperPath=None
    else:
        gripper_mesh=modify_gripper_mesh(meshPath,"left",leftWristToEE,leftCameraTransform)
        gripperPath=export_STL(gripper_mesh,meshPath+"/left_EE_link"+suffixGripper)
    
    #modify urdf
    if not fullName:
        suffixPusher=suffixGripper=""
    root=modify_urdf(path,basePath,pusherPath,gripperPath,TL,TR,deltaZ,deltaY,angle)
    pathOut=path[:len(path)-5]+suffix+suffixPusher+suffixGripper+".urdf"
    open(pathOut,'w').write(ET.tostring(root,pretty_print=True).decode())
    return pathOut
    
if __name__=='__main__':
    import pint
    ureg = pint.UnitRegistry()
    pusher=((10  *ureg.inch).to(ureg.meter)._magnitude,
            (1.75*ureg.inch).to(ureg.meter)._magnitude,
            (1.75*ureg.inch).to(ureg.meter)._magnitude,0.13,45)
    
    #modify
    right_camera_transform = ([0.009867833875554, -0.49431948695175937, 0.8692242925012816, 
                               -0.002675548800660542, -0.8692765561510516, -0.49431883472591204,\
                               -0.9999477322806525, -0.0025522041293258445, 0.009900452558469577], 
                               [-0.049899038919714755, -0.06534763981440639, -0.016062509431686134])
    left_camera_transform = ([-0.06939732886469613, -0.8820629769708032, -0.4659923984399842, 
                              -0.038975726714309905, 0.46915927983703953, -0.8822530605613572,\
                              -0.9968274191019911, 0.04306361341311882, 0.0669374463679997], 
                              [-0.038713496576159745, 0.022955546540886766, -0.051000723894234594])
    pathOut=modify_Anthrax(DATA_PATH+"/data/TRINA/robots/Anthrax_lowpoly.urdf",0.0065,0.0125,0,0,
                           pusher=pusher,gripper=True,fullName=False,
                           leftCameraTransform=klampt_to_numpy(left_camera_transform),
                           rightCameraTransform=klampt_to_numpy(right_camera_transform))
    
    #visualize
    world=WorldModel()
    robot=world.loadRobot(pathOut)
    robotRef=world.loadRobot(DATA_PATH+"/data/TRINA/robots/Bubonic.urdf")
    for l in range(robotRef.numLinks()):
        robotRef.link(l).appearance().setColor(1.,0.,0.)
    GLVisualizer(world,None,robot,None).run()