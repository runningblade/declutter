from reachable import ReachableFast,GLVisualizerReachable
from objectCarry import ObjectCarry
from klampt import Geometry3D
from klampt.vis import gldraw
from scene import make_scene_tx90
from visualizer import *
from utils import *
import numpy as np
import Box2D,random,copy

class ReachableTraj(ReachableFast):
    def __init__(self,world,table,robot,robot_path,height,eeLid,eeLocalPos,eeLocalDir,optDOF,additionalCheck=None,rotDelta=0):
        ReachableFast.__init__(self,world,table,robot,robot_path,height,eeLid,eeLocalPos,eeLocalDir,optDOF,additionalCheck)
        self.rotDelta=rotDelta
        
    def display(self):
        if self.drawEE:
            gldraw.setcolor(EE_DIRECTION_COLOR[0],EE_DIRECTION_COLOR[1],EE_DIRECTION_COLOR[2])
            x0=se3.apply(self.robot.link(self.eeLid).getTransform(),self.eeLocalPos)
            x1=se3.apply(self.robot.link(self.eeLid).getTransform(),op.add(self.eeLocalPos,self.eeLocalDir))
            v=op.sub(x1,x0)
            gldraw.hermite_curve(x0,v,x1,v)
            
    def idle(self):
        if not hasattr(self,"trajType"):
            self.trajType=0
        if self.drawCell and hasattr(self,"grid"):
            if not hasattr(self,"currTraj") or self.currTraj is None:
                while True:
                    grasp=None if self.trajType==0 else True
                    #grasp=None if random.uniform(0,1)<0.5 else True
                    idss=[self.random_id(),self.random_id(),self.random_id()]
                    self.sync()
                    self.currTraj=self.gen_traj(idss,0.1,0.1,directed=op.sub(idss[1],idss[0]),grasp=grasp)
                    if self.currTraj is not None:
                        break
                self.currTime=0.0
                self.trajType=1-self.trajType
            elif self.currTime>=self.currTraj[1][-1]:
                delattr(self,"currTraj")
                delattr(self,"currTime")
            else:
                _,_,dof=self.search_traj_time(self.currTraj,self.currTime)
                self.set_opt_DOF(dof)
                self.currTime+=0.1
    
    def search_traj_time(self,traj,time,i=None):
        dofs,times,carry=traj
        #search index
        if i is None:
            i=0
        while i<len(times)-2 and (time>times[i+1] or times[i+1]==times[i]):
            i+=1
        frac=(time-times[i])/max(times[i+1]-times[i],1e-8)
        dof=interp_1d(dofs[i],dofs[i+1],frac)
        return i,frac,dof
    
    def random_id(self):
        c=random.choice(self.cell)
        return (c[0]+random.uniform(0,1),c[1]+random.uniform(0,1))

    def index_grid(self,idx,height):
        if not self.id_in_grid(idx):
            return None,None
        id=(op.floor_safe(idx[0]),op.floor_safe(idx[1]))
        ida00,idb00=self.grid[(id[0]  ,id[1]  )]
        ida10,idb10=self.grid[(id[0]+1,id[1]  )]
        ida11,idb11=self.grid[(id[0]+1,id[1]+1)]
        ida01,idb01=self.grid[(id[0]  ,id[1]+1)]
        
        id00=interp_1d(ida00,idb00,height)
        id10=interp_1d(ida10,idb10,height)
        id11=interp_1d(ida11,idb11,height)
        id01=interp_1d(ida01,idb01,height)
        
        frac=(idx[0]-id[0],idx[1]-id[1])
        return interp_2d(id00,id10,id11,id01,frac)
    
    def index_grid_adjust(self,idx,height):
        x0=[self.dof[DOFId] for DOFId in self.optDOF] #save
        x=self.index_grid(idx,height)
        self.set_opt_DOF(x)
    
        ee0=self.global_pos()[1]
        ee1=(idx[0]*self.res,idx[1]*self.res,ee0[2])
        ret=self.solve_IK(x,ee1,[0,0,-1],0.1)[0]
        self.set_opt_DOF(x0)   #load
        return ret
        
    def add_traj(self,xfrom,xto,speed):
        if xfrom is None:
            xfrom=[self.dof[i] for i in self.optDOF]
        
        self.set_opt_DOF(xfrom)
        _,gposfrom=self.global_pos()
        
        self.set_opt_DOF(xto)
        _,gposto=self.global_pos()
        
        dist=op.norm(op.sub(gposto,gposfrom))
        return xto,dist/speed
        
    def rotate_EE(self,dirRef,x,xRef=None):
        dirRef=op.matvecmul(rot_2D(self.rotDelta),perp(dirRef))
        def compute_dir(dof):
            self.set_opt_DOF(dof)
            dir=self.robot.link(self.eeLid).getTransform()[0][0:2]
            return wedge(dir,dirRef)/max(op.norm(dir)*op.norm(dirRef),1e-6)
        def margin_from_bound(val):
            return min(val-lb,ub-val)
        #search
        maxVal=0
        maxX=None
        idEE=self.optDOF.index(self.eeLid)
        x0EE=x[idEE]
        #add or subtract choose
        delta=op.asin_safe(compute_dir(x))
        x[idEE]=x0EE+delta  
        valPlus=compute_dir(x)
        x[idEE]=x0EE-delta
        valMinus=compute_dir(x)
        if abs(valPlus)<abs(valMinus):
            x[idEE]=x0EE+delta
        else:
            x[idEE]=x0EE-delta
        #bounds
        if xRef is None:
            lb=self.lmt[0][self.eeLid]
            ub=self.lmt[1][self.eeLid]
            #leave maximal amount of margin
            while margin_from_bound(x[idEE]+math.pi)>margin_from_bound(x[idEE]):
                x[idEE]+=math.pi
            while margin_from_bound(x[idEE]-math.pi)>margin_from_bound(x[idEE]):
                x[idEE]-=math.pi
            if x[idEE]<self.lmt[0][self.eeLid] or x[idEE]>self.lmt[1][self.eeLid]:
                print("rotate_EE DOF out of joint limits!")
        else:
            #leave minimal amount of difference
            while abs(x[idEE]+math.pi-xRef[idEE])<abs(x[idEE]-xRef[idEE]):
                x[idEE]+=math.pi
            while abs(x[idEE]-math.pi-xRef[idEE])<abs(x[idEE]-xRef[idEE]):
                x[idEE]-=math.pi
            if x[idEE]<self.lmt[0][self.eeLid] or x[idEE]>self.lmt[1][self.eeLid]:
                print("rotate_EE DOF out of joint limits!")
        return x
        
    def add_traj_grid_search(self,lasttime,idfrom,idto,speed,height):
        traj=[]
        time=[]
        stamps=[]
        for d in range(2):
            if idfrom[d]<idto[d]:
                r=range(int(op.floor_safe(idfrom[d])),int(op.ceil_safe(idto[d]))+1)
            else:
                r=range(int(op.floor_safe(idto[d])),int(op.ceil_safe(idfrom[d]))+1)
            for i in r:
                if idto[d]!=idfrom[d]:
                    t=(i-idfrom[d])/(idto[d]-idfrom[d])
                    if t>0 and t<1:
                        stamps.append(t)
        stamps.append(1.0)
        stamps=sorted(stamps)
        #check every point is in grid
        for i in range(len(stamps)):
            id=interp_1d(idfrom,idto,stamps[i])
            if not self.id_in_grid(id):
                print("Id: ",id," not in grid!")
                return None,None
        for i in range(len(stamps)-1):
            alpha=(stamps[i]+stamps[i+1])/2
            id=interp_1d(idfrom,idto,alpha)
            if not self.id_in_grid(id):
                print("Id: ",id," not in grid!")
                return None,None
        #remove stamps that are too close-by
        stampsClean=[stamps[0]]
        for i in range(1,len(stamps)):
            if stamps[i]>stampsClean[-1]+0.001:
                stampsClean.append(stamps[i])
        #add to traj
        idxlast=idfrom
        for i in range(len(stampsClean)):
            idx=interp_1d(idfrom,idto,stampsClean[i])
            lasttime+=op.norm(op.sub(idx,idxlast))*self.res/speed
            traj.append(self.index_grid(idx,height))
            time.append(lasttime)
            idxlast=idx
        return traj,time
        
    def gen_traj(self,idss,speed,speedMove=None,startFromCurrent=True,directed=None,grasp=None,height=None,routeForGrasp=True):
        carry=ObjectCarry(grasp,self.robot.link(self.eeLid))
        if height is None:
            height=1
        if idss[0]==idss[-1]:
            return None
        if not self.id_in_grid(idss[0]):
            print("Id: ",idss[0]," not in grid!")
            return None
        if not self.id_in_grid(idss[-1]):
            print("Id: ",idss[-1]," not in grid!")
            return None
        #build trajectory
        dof0=[self.dof[i] for i in self.optDOF]
        traj=[self.index_grid(idss[0],0)]
        time=[0.0]
        #rotate pusher if directed
        if directed is not None:
            idDirected=len(traj)
            traj[-1]=self.rotate_EE(directed,traj[-1])
        #move down
        traji,timei=self.add_traj(None,self.index_grid_adjust(idss[0],height),speed)
        traj.append(traji)
        time.append(time[-1]+timei)
        #data structure to compute carried object
        carry.id0=len(time)-1
        carry.t0=self.robot.link(self.eeLid).getTransform()
        if grasp is not None:
            #move up
            traji,timei=self.add_traj(None,self.index_grid(idss[0],0),speed)
            traj.append(traji)
            time.append(time[-1]+timei)
        #move horizontal
        if grasp is not None:
            spd=speed if speedMove is None else speedMove
            if routeForGrasp:
                traji=self.route_on_network(traj[-1],self.index_grid(idss[-1],0))
                timei=[0.0]
                for i in range(len(traji)-1):
                    self.set_opt_DOF(traji[i])
                    p0=self.global_pos()[1]
                    self.set_opt_DOF(traji[i+1])
                    p1=self.global_pos()[1]
                    timei.append(timei[-1]+op.mul(op.norm(op.sub(p0,p1)),1/speed))
                traj+=traji
                time+=[t+time[-1] for t in timei]
            else:
                traji,timei=self.add_traj(None,self.index_grid(idss[-1],0),spd)
                traj.append(traji)
                time.append(time[-1]+timei)
        else:
            for i in range(1,len(idss)):
                h=0 if grasp is not None else height
                spd=speed if speedMove is None else speedMove
                traji,timei=self.add_traj_grid_search(time[-1],idss[i-1],idss[i],spd,h)
                if traji is None:
                    self.set_opt_DOF(dof0)   #recover state
                    return None
                traj+=traji
                time+=timei
        if grasp is not None:
            #move down
            traji,timei=self.add_traj(None,self.index_grid_adjust(idss[-1],height),speed)
            traj.append(traji)
            time.append(time[-1]+timei)
        #data structure to compute carried object
        carry.id1=len(time)-1
        carry.t1=self.robot.link(self.eeLid).getTransform()
        #move up
        traji,timei=self.add_traj(None,self.index_grid(idss[-1],0),speed)
        traj.append(traji)
        time.append(time[-1]+timei)
        #rotate pusher if directed
        if directed:
            for id in range(idDirected,len(traj)):
                traj[id]=self.rotate_EE(directed,traj[id],traj[id-1])
            
        #return
        if startFromCurrent:
            self.set_opt_DOF(dof0)   #recover state
            ret=self.connect_current((traj,time,carry),speed)
            self.set_opt_DOF(dof0)   #recover state
            return ret
        else:
            self.set_opt_DOF(dof0)   #recover state
            return (traj,time,carry)

    def connect_current(self,trajRef,speed):
        if isinstance(trajRef,tuple):
            dofs,times,carry=trajRef
        else: dofs=[trajRef]
        
        doffrom=[self.dof[i] for i in self.optDOF]
        dofto=list(dofs[0][0:len(self.optDOF)])
        traj=self.route_on_network(doffrom,dofto)
        time=[0.0]
        for i in range(len(traj)-1):
            self.set_opt_DOF(traj[i])
            p0=self.global_pos()[1]
            self.set_opt_DOF(traj[i+1])
            p1=self.global_pos()[1]
            time.append(time[-1]+op.mul(op.norm(op.sub(p0,p1)),1/speed))
        #adjust the eeLink:
        for i in range(len(traj)):
            alpha=(time[i]-time[0])/max(time[-1]-time[0],1e-8)
            traj[i][-1]=interp_1d(doffrom[-1],dofto[-1],alpha)
        if not isinstance(trajRef,tuple):
            return traj,time

        #data structure to compute carried object
        carry.id0+=len(time)
        carry.id1+=len(time)
        #concat
        traj+=dofs
        time+=[t+time[-1] for t in times]
        #do not move EE (no need to)
        if carry.grasp is not None:
            idEE=self.optDOF.index(self.eeLid)
            for i in range(1,len(traj)):
                traj[i][idEE]=traj[0][idEE]
        return (traj,time,carry)

    def build_network(self):
        if hasattr(self,"network"):
            return
        import networkx
        self.networkCellId={}
        self.networkInvCellId={}
        self.network=networkx.Graph()
        
        #terminal node
        for cc,_ in self.grid.items():
            key=(None,None,cc[0],cc[1])
            self.networkInvCellId[len(self.networkCellId)]=key
            self.networkCellId[key]=len(self.networkCellId)
            self.network.add_node(self.networkCellId[key])
        
        #travel node
        for cc,_ in self.grid.items():
            for i in [-1,0,1]:
                for j in [-1,0,1]:
                    if i!=0 or j!=0:
                        ot=(cc[0]+i,cc[1]+j)
                        if ot in self.grid.keys():
                            key=(cc[0],cc[1],ot[0],ot[1])
                            self.networkInvCellId[len(self.networkCellId)]=key
                            self.networkCellId[key]=len(self.networkCellId)
                            self.network.add_node(self.networkCellId[key])
        
        #terminal edge
        for key,id in self.networkCellId.items():
            if key[0] is None:
                continue
            self.network.add_edge(id,self.networkCellId[(None,None,key[0],key[1])],weight=1.)
            self.network.add_edge(id,self.networkCellId[(None,None,key[2],key[3])],weight=1.)
        
        #travel edge
        for key,id in self.networkCellId.items():
            if key[0] is None:
                continue
            a=(key[0],key[1])
            b=(key[2],key[3])
            for i in [-1,0,1]:
                for j in [-1,0,1]:
                    if i!=0 or j!=0:
                        ot=(b[0]+i,b[1]+j)
                        if ot in self.grid.keys():
                            d1=op.sub(b,a)
                            d2=op.sub(ot,b)
                            angle=math.acos(op.dot(d1,d2)/op.norm(d1)/op.norm(d2))*180/math.pi
                            w=op.norm(d2)+angle
                            self.network.add_edge(self.networkCellId[(a[0],a[1],b[0],b[1])],self.networkCellId[(b[0],b[1],ot[0],ot[1])],weight=w)
        
    def route_on_network(self,doffrom,dofto):
        self.build_network()
        
        idfrom,idto=None,None
        distfromBest,disttoBest=0,0
        for k,v in self.grid.items():
            distfrom=op.norm(op.sub(v[0],doffrom))
            if idfrom is None or distfrom<distfromBest:
                idfrom=k
                distfromBest=distfrom
                
            distto=op.norm(op.sub(v[0],dofto))
            if idto is None or distto<disttoBest:
                idto=k
                disttoBest=distto
        
        source=self.networkCellId[(None,None,idfrom[0],idfrom[1])]
        target=self.networkCellId[(None,None,idto[0],idto[1])]
        from networkx.algorithms.shortest_paths.weighted import single_source_dijkstra
        _,hist=single_source_dijkstra(self.network,source=source,target=target)
        hist=[(self.networkInvCellId[h][2],self.networkInvCellId[h][3]) for h in hist]
        return [doffrom]+[self.grid[h][0] for h in hist]+[dofto]
    
class ReachableTrajTx90Robotiq(ReachableTraj):
    def __init__(self,world,table,robot,robot_path,height):
        def additionalCheck(reachable,x):
            reachable.set_opt_DOF(x)
            bb1=get_link_bb_ctr(reachable.robot,1)
            bb2=get_link_bb_ctr(reachable.robot,2)
            bb3=get_link_bb_ctr(reachable.robot,3)
            return bb2[2]>bb1[2] and bb3[2]>bb1[2]
        ReachableTraj.__init__(self,world,table,robot,robot_path,height,6,[0,0,0],[0,0,1],[1,2,3,4,5],additionalCheck=additionalCheck)
    
if __name__=='__main__':
    world,table,robot,robot_path=make_scene_tx90()
    reachable=ReachableTrajTx90Robotiq(world,table,robot,robot_path,0.3)
    reachable.build_grid(0.025)
    GLVisualizerReachable(world,table,robot,reachable).run()