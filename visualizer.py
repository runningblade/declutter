from klampt import WorldModel,Geometry3D,Simulator
from klampt.vis import GLProgram,camera,gldraw
from utils import *
import os

#robot
REACHABILITY_GRID_COLOR=(0.9,0.8,0.4)
EE_DIRECTION_COLOR=(1,1,1)
#world
TABLE_COLOR=(0.858824,0.576471,0.439216)#(0.2,0.6,0.3)
PUSHER_COLOR=(0.2,0.2,0.6)
OBJECT_COLOR=(0.2,0.2,0.6)
SPECIAL_OBJECT_COLOR=(0.6,0.2,0.2)
SPECIAL2_OBJECT_COLOR=(0.2,0.6,0.2)
SPECIAL3_OBJECT_COLOR=(0.6,0.6,0.2)
FLOOR_COLOR=(0.5,0.5,0.8)
TEXT_COLOR=(0,0,0)
#cost
COST_REGION_COLOR=(1,1,1)
COST_DISTANCE_COLOR=(0.6,0.2,0.2)
COST_DIRICHLET_COLOR=SPECIAL_OBJECT_COLOR
COST_INSIDE_DIRICHLET_COLOR=(0,1,0)
COST_VERTEX_DIRICHLET_COLOR=(0,0,1)
COST_EDGE_DIRICHLET_COLOR=(1,1,0)
#planner
PUSH_ACTION_COLOR=(1,1,1)
VALID_PUSH_ACTION_COLOR=(0.6,0.2,0.2)
GRASP_ACTION_COLOR=PUSH_ACTION_COLOR
VALID_GRASP_ACTION_COLOR=VALID_PUSH_ACTION_COLOR
PUSH_GRASP_SAMPLE_POINT_SIZE=3
PUSH_GRASP_LINE_WIDTH=3
FORWARD_COLOR=(0,1,0)
BACKWARD_COLOR=(0,0,1)
OUTSIDE_COLOR=(1,1,0)
#contact patch
CONTACT_PATCH_COLOR=(0.2,0.6,0.2)
VALID_CONTACT_PATCH_COLOR=(0.6,0.2,0.2)
CONTACT_PATCH_POINT_SIZE=PUSH_GRASP_SAMPLE_POINT_SIZE
#contact force
DELTA_POSITION_COLOR=(0.2,0.2,0.6)
GROUND_CONTACT_FORCE_COLOR=(0.6,0.2,0.2)
CONTACT_FORCE_COLOR=(0.2,0.6,0.6)
CONTACT_DIR_COLOR=(0.6,0.2,0.6)
CONTACT_LINE_WIDTH=PUSH_GRASP_LINE_WIDTH

class GLVisualizer(GLProgram):
    def __init__(self,world,table,robot,sim):
        GLProgram.__init__(self,"Visualizer")
        self.zeroZ=True
        self.world=world
        self.table=table
        self.robot=robot
        self.sim=sim
        self.dt=1/60.0
        self.init_camera()

    def look_at(self,pos,tgt,scale=None):
        tgt=list(tgt)
        tgt[1]=pos[1]=0.
        cam=self.view.camera
        if scale is not None:
            cam.dist=op.norm(op.sub(tgt,pos))*scale
        cam.rot=self.get_camera_rot(op.sub(pos,tgt))
        cam.tgt=tgt
        
    def get_camera_pos(self):
        cam=self.view.camera
        z=op.sin_safe(-cam.rot[1])
        x=op.sin_safe(cam.rot[2])*op.cos_safe(cam.rot[1])
        y=op.cos_safe(cam.rot[2])*op.cos_safe(cam.rot[1])
        pos=[x,y,z]
        return op.add(cam.tgt,op.mul(pos,cam.dist))
    
    def get_camera_rot(self,d):
        angz=op.atan2_safe(d[0],d[1])
        angy=op.atan2_safe(-d[2],op.sqrt_safe(d[0]*d[0]+d[1]*d[1]))
        return [0,angy,angz]

    def get_camera_dir(self,zeroZ=False):
        cam=self.view.camera
        dir=op.sub(cam.tgt,self.get_camera_pos())
        if zeroZ:
            dir=(dir[0],dir[1],0)
        if op.norm(dir)>1e-6:
            dir=op.mul(dir,1/op.norm(dir))
        dir[1]*=-1
        return dir

    def get_left_dir(self,zeroZ=False):
        dir=op.cross([0,0,1],self.get_camera_dir())
        if zeroZ:
            dir=(dir[0],dir[1],0)
        if op.norm(dir)>1e-6:
            dir=op.mul(dir,1/op.norm(dir))
        return dir

    def init_camera(self):
        if self.robot is None:
            bb_robot=((-1.,-1.,-1.),(1.,1.,1.))
        else: bb_robot=get_robot_bb(self.robot)
        if self.table is not None:
            bb=union_bb(bb_robot,get_object_bb(self.table))
        else: bb=bb_robot
        pos=[bb[1][0],(bb[0][1]+bb[1][1])/2,bb_robot[1][2]]
        tgt=[bb[0][0],(bb[0][1]+bb[1][1])/2,bb_robot[0][2]]
        self.look_at(pos,tgt,2.0)
        self.moveSpd=0.005
        self.zoomSpd=1.03
        self.zoomMin=0.01
        self.zoomMax=100.0
        
        self.zoomInCam=False
        self.zoomOutCam=False
        self.forwardCam=False
        self.backCam=False
        self.leftCam=False
        self.rightCam=False
        self.raiseCam=False
        self.sinkCam=False
        return

    def keyboardfunc(self,c,x,y):
        if c==b'f':
            self.init_camera()
        elif c==b'z':
            self.zeroZ=not self.zeroZ
        elif c==b'q':
            self.zoomInCam=True
        elif c==b'e':
            self.zoomOutCam=True
        elif c==b'w':
            self.forwardCam=True
        elif c==b's':
            self.backCam=True
        elif c==b'a':
            self.leftCam=True
        elif c==b'd':
            self.rightCam=True
        elif c==b' ':
            self.raiseCam=True
        elif c==b'c':
            self.sinkCam=True
        elif c==b',':
            import pickle
            cam=self.view.camera
            pickle.dump((cam.dist,self.get_camera_pos(),cam.tgt),open(DATA_PATH+"/tmpCamera.dat",'wb'))
            print('Saved camera to tmpCamera.dat!')
        elif c==b'.':
            import pickle
            cam=self.view.camera
            cam.dist,pos,tgt=pickle.load(open(DATA_PATH+"/tmpCamera.dat",'rb'))
            self.look_at(pos,tgt)
            print('Loaded camera to tmpCamera.dat!')

    def keyboardupfunc(self,c,x,y):
        if c==b'q':
            self.zoomInCam=False
        elif c==b'e':
            self.zoomOutCam=False
        elif c==b'w':
            self.forwardCam=False
        elif c==b's':
            self.backCam=False
        elif c==b'a':
            self.leftCam=False
        elif c==b'd':
            self.rightCam=False
        elif c==b' ':
            self.raiseCam=False
        elif c==b'c':
            self.sinkCam=False

    def display_screen(self):
        gldraw.setcolor(TEXT_COLOR[0],TEXT_COLOR[1],TEXT_COLOR[2])
        
    def display(self):
        if self.sim is not None:
            self.sim.updateWorld()
        self.world.drawGL()

    def handle_camera(self):
        self.view.clippingplanes=(self.view.clippingplanes[0],self.zoomMax)
        cam=self.view.camera
        moveSpd=self.moveSpd*cam.dist
        if self.zoomInCam:
            cam.dist=max(cam.dist/self.zoomSpd,self.zoomMin)
        elif self.zoomOutCam:
            cam.dist=min(cam.dist*self.zoomSpd,self.zoomMax)
        elif self.forwardCam:
            delta=op.mul(self.get_camera_dir(self.zeroZ),moveSpd)
            self.look_at(op.add(self.get_camera_pos(),delta),op.add(cam.tgt,delta))
        elif self.backCam:
            delta=op.mul(self.get_camera_dir(self.zeroZ),-moveSpd)
            self.look_at(op.add(self.get_camera_pos(),delta),op.add(cam.tgt,delta))
        elif self.leftCam:
            delta=op.mul(self.get_left_dir(self.zeroZ),moveSpd)
            self.look_at(op.add(self.get_camera_pos(),delta),op.add(cam.tgt,delta))
        elif self.rightCam:
            delta=op.mul(self.get_left_dir(self.zeroZ),-moveSpd)
            self.look_at(op.add(self.get_camera_pos(),delta),op.add(cam.tgt,delta))
        elif self.raiseCam:
            delta=(0,0,moveSpd)
            self.look_at(op.add(self.get_camera_pos(),delta),op.add(cam.tgt,delta))
        elif self.sinkCam:
            delta=(0,0,-moveSpd)
            self.look_at(op.add(self.get_camera_pos(),delta),op.add(cam.tgt,delta))

    def idle(self):
        self.handle_camera()
        if self.sim is not None:
            self.sim.simulate(self.dt)
        self.update_animation()
    
    def render(self,fn,tfn):
        pass
    
    def update_animation(self):
        if not hasattr(self,"animation_fn") or self.animation_fn is None:
            return
        if hasattr(self,'render_path') and self.render_path is not None:
            tfn=self.render_path+"/frm%d.pov"%len(self.animation_frms)
            cmd=self.render(None,tfn)
            continueAnim=self.animation_op()
            if continueAnim:
                self.animation_frms.append(cmd)
            else:
                import pickle
                pickle.dump(self.animation_frms,open(self.render_path+"/cmd.dat",'wb'))
                print("Finishing saving animation to %s!"%self.render_path)
                self.animation_fn=None
                self.animation_op=None
                self.animation_dur=None
                self.animation_frms=None
        else:
            import cv2
            tmpPath="tmp.png"
            continueAnim=self.animation_op()
            self.save_screen(tmpPath,multithreaded=False)
            if continueAnim:
                self.animation_frms.append(cv2.imread(tmpPath))
            else:
                fourcc=cv2.cv.CV_FOURCC(*'XVID')
                height,width,layers=self.animation_frms[0].shape
                out=cv2.VideoWriter(self.animation_fn,fourcc,1.0/self.animation_dur,(width,height))
                for f in self.animation_frms:
                    out.write(f)
                out.release()
                if os.path.exists(tmpPath):
                    os.remove(tmpPath)
                print("Finishing saving animation to %s!"%self.animation_fn)
                self.animation_fn=None
                self.animation_op=None
                self.animation_dur=None
                self.animation_frms=None
    
    def render_animation(self,fn,op,dur=1,render_path=None):
        if hasattr(self,"animation_fn") and self.animation_fn is not None:
            return
        #print("render_path=%s"%render_path)
        self.render_path=render_path
        if self.render_path is not None:
            if os.path.exists(self.render_path):
                import shutil
                shutil.rmtree(self.render_path)
            os.mkdir(self.render_path)
        self.animation_fn=fn
        self.animation_op=op
        self.animation_dur=dur
        self.animation_frms=[]