from klampt.math import se3,so3
import OpenGL.GL as gl
import NLPUtils as op
import numpy as np
import math,os

#math
INFINITY_NUMBER=100
NUMBER_THREAD=4
DATA_PATH=os.path.dirname(os.path.abspath(__file__))

def cmpToKey(mycmp):
    'Convert a cmp= function into a key= function'
    class K(object):
        def __init__(self,obj,*args):
            self.obj=obj
        def __lt__(self,other):
            return mycmp(self.obj,other.obj)<0
        def __gt__(self,other):
            return mycmp(self.obj,other.obj)>0
        def __eq__(self,other):
            return mycmp(self.obj,other.obj)==0
        def __le__(self,other):
            return mycmp(self.obj,other.obj)<=0  
        def __ge__(self,other):
            return mycmp(self.obj,other.obj)>=0
        def __ne__(self,other):
            return mycmp(self.obj,other.obj)!=0
    return K

def draw_circle(fill,r,margin,RES=16):
    if fill:
        gl.glBegin(gl.GL_TRIANGLES)
        for i in range(RES):
            c1=op.cos_safe((i+0)*math.pi*2/RES)*r
            s1=op.sin_safe((i+0)*math.pi*2/RES)*r
            c2=op.cos_safe((i+1)*math.pi*2/RES)*r
            s2=op.sin_safe((i+1)*math.pi*2/RES)*r
            gl.glVertex3f(0 ,0 ,margin)
            gl.glVertex3f(c1,s1,margin)
            gl.glVertex3f(c2,s2,margin)
        gl.glEnd()
    else:
        gl.glBegin(gl.GL_LINES)
        for i in range(RES):
            c1=op.cos_safe((i+0)*math.pi*2/RES)*r
            s1=op.sin_safe((i+0)*math.pi*2/RES)*r
            c2=op.cos_safe((i+1)*math.pi*2/RES)*r
            s2=op.sin_safe((i+1)*math.pi*2/RES)*r
            gl.glVertex3f(c1,s1,margin)
            gl.glVertex3f(c2,s2,margin)
        gl.glEnd()

def atan2_zero2pi(y,x):
    phi=op.atan2_safe(y,x)
    if phi<0:
        phi+=math.pi*2
    return phi

def interp_1d(a,b,frac):
    if op.is_float(a):
        return a*(1-frac)+b*frac
    else:
        return op.add(op.mul(a,1-frac),op.mul(b,frac))
        
def interp_2d(x00,x10,x11,x01,frac):
    a=interp_1d(x00,x10,frac[0])
    b=interp_1d(x01,x11,frac[0])
    if op.is_float(a):
        return a*(1-frac[1])+b*frac[1]
    else: 
        return op.add(op.mul(a,1-frac[1]),op.mul(b,frac[1]))
        
def wedge(a,b):
    if op.is_float(a[0]) and op.is_float(b[0]):
        return a[0]*b[1]-a[1]*b[0]
    elif op.is_float(a[0]):
        ret=[]
        for i in range(len(b[0])):
            ret.append(wedge(a,(b[0][i],b[1][i])))
        return ret
    elif op.is_float(b[0]):
        ret=[]
        for i in range(len(a[0])):
            ret.append(wedge((a[0][i],a[1][i]),b))
        return ret
    else: assert False

def perp(a):
    return (a[1],-a[0])

def neg_perp(a):
    return (-a[1],a[0])

def inv_2x2(m):
    det=m[0][0]*m[1][1]-m[0][1]*m[1][0]
    invm=[[m[1][1],-m[0][1]],[-m[1][0],m[0][0]]]
    return op.mul(invm,1/det)

def local_to_global_2D(c,s,x,y,p):
    fx=( c*p[0])+(-s*p[1])+x
    fy=( s*p[0])+( c*p[1])+y
    return (fx,fy)

def local_to_global_2D_diff(c,s,p):
    return [[1.0,0.0,-s*p[0]-c*p[1]],
            [0.0,1.0, c*p[0]-s*p[1]]]

def global_to_local_2D(c,s,x,y,p):
    fx=( c*(p[0]-x))+( s*(p[1]-y))
    fy=(-s*(p[0]-x))+( c*(p[1]-y))
    return (fx,fy)

def global_to_local_2D_diff(c,s,x,y,p):
    p=op.sub(p,(x,y))
    return [[-c,-s,-s*p[0]+c*p[1]],
            [ s,-c,-c*p[0]-s*p[1]]]

def rot_2D(a=None,c=None,s=None):
    if a is not None:
        c=op.cos_safe(a)
        s=op.sin_safe(a)
    return [[c,-s],[s,c]]

def rot_2D_T(a=None,c=None,s=None):
    if a is not None:
        c=op.cos_safe(a)
        s=op.sin_safe(a)
    return [[c,s],[-s,c]]

def rot_2D_to_diff(r):
    c=r[0][0]
    s=r[1][0]
    return [[-s,-c],[c,-s]]

def empty_bb(dim=3):
    return ([1000]*dim,[-1000]*dim)

def union_bb(a,b):
    if isinstance(b,list):
        return union_bb(a,(b,b))
    else:
        return ([min(c[0],c[1]) for c in zip(a[0],b[0])],   \
                [max(c[0],c[1]) for c in zip(a[1],b[1])])

def expand_bb(bb,expand):
    return ([c-expand for c in bb[0]],   \
            [c+expand for c in bb[1]])

def expand_bb_eps(bb,eps):
    d=max(op.sub(bb[1],bb[0]))
    return expand_bb(bb,d*eps)

def contain_bb(bb,pt):
    for d in range(len(bb[0])):
        if pt[d]<bb[0][d] or pt[d]>bb[1][d]:
            return False
    return True

def compute_bb_tight(geom,Rt=None):
    if geom.type()=="Group":
        return geom.getBB()
        #Rtc=geom.getCurrentTransform() if Rt is None else se3.mul(Rt,geom.getCurrentTransform())
        #for i in range(geom.numElements()):
        #    e=geom.getElement(i)
        #    bb=union_bb(bb,compute_bb_tight(e,Rtc))
        #return bb
    elif geom.type()=="TriangleMesh":
        bb=empty_bb(3)
        m=geom.getTriangleMesh()
        for v in range(len(m.vertices)//3):
            vert=[m.vertices[v*3+d] for d in range(3)]
            if Rt is not None:
                vert=se3.apply(Rt,vert)
            bb=union_bb(bb,vert)
        return bb
    else: assert False

def get_link_children(robot,linkId):
    return [i for i in range(robot.numLinks()) if robot.link(i).getParent()==linkId]

def get_robot_root_link(robot):
    for i in range(robot.numLinks()):
        if str(robot.link(i).geometry().type())!='':
            return i,robot.link(i)

def get_robot_bb(robot,link0=None):
    bb=None
    if link0 is None:
        link0=get_robot_root_link(robot)[0]
    bb=compute_bb_tight(robot.link(link0).geometry(),robot.link(link0).getTransform())
    for c in get_link_children(robot,link0):
        bb=union_bb(bb,get_robot_bb(robot,c))
    return bb

def get_link_bb(robot,i):
    bb=compute_bb_tight(robot.link(i).geometry(),robot.link(i).getTransform())
    return bb

def get_link_bb_ctr(robot,i):
    bb=get_link_bb(robot,i)
    return op.mul(op.add(bb[0],bb[1]),0.5)

def get_object_bb(object):
    return compute_bb_tight(object.geometry())

def intersect_bb_bb(bb,bb2):
    for d in range(len(bb[0])):
        if bb[0][d]>bb2[1][d]:
            return False
        elif bb2[0][d]>bb[1][d]:
            return False
    return True

def intersect_edge_bb(e,bb):
    p=e[0]
    q=e[1]
    
    s=0
    t=1
    for i in range(len(bb[0])):
        D=q[i]-p[i]
        if p[i]<q[i]:
            s0=(bb[0][i]-p[i])/D
            t0=(bb[1][i]-p[i])/D
            if s0>s:s=s0
            if t0<t:t=t0
        elif p[i]>q[i]:
            s0=(bb[1][i]-p[i])/D
            t0=(bb[0][i]-p[i])/D
            if s0>s:s=s0
            if t0<t:t=t0
        elif p[i]<bb[0][i] or p[i]>bb[1][i]:
            return False
        if s>t:
            return False
    return True

def ignore_adjacent_link_collisions(robot):
    for i in range(robot.numLinks()-1):
        robot.enableSelfCollision(i,i+1,False)
    
def set_simulator_margin(world,sim,margin):
    for i in range(world.numRobots()):
        for j in range(world.robot(i).numLinks()):
            l=world.robot(i).link(j)
            b=sim.body(l)
            b.setCollisionPadding(margin)
    for i in range(world.numRigidObjects()):
        b=sim.body(world.rigidObject(i))
        b.setCollisionPadding(margin)
    for i in range(world.numTerrains()):
        b=sim.body(world.terrain(i))
        b.setCollisionPadding(margin)
        
def set_robot_dynamics_enabled(robot,sim,enabled):
    for i in range(robot.numLinks()):
        l=robot.link(i)
        b=sim.body(l)
        b.enableDynamics(enabled)

def klampt_to_numpy(T):
    ret=np.identity(4,np.float64)
    for i in range(9):
        ret[i%3,i//3]=T[0][i]
    ret[:3,3]=T[1]
    return ret

def sub_mesh(robot,linkId,trans=None):
    if trans is None:
        trans=robot.link(linkId).getTransform()
        trans=klampt_to_numpy(trans)
        trans=np.linalg.inv(trans)
        
    T=klampt_to_numpy(robot.link(linkId).getTransform())
    T=np.matmul(trans,T)    
    mesh=robot.link(linkId).geometry().getTriangleMesh()
    
    vss=[]
    nrV=len(mesh.vertices)//3
    for i in range(nrV):
        v=[mesh.vertices[i*3+j] for j in range(3)]
        v=np.matmul(T[:3,:3],v)+T[:3,3]
        vss.append(v.tolist())
    
    iss=[]
    nrI=len(mesh.indices)//3
    for i in range(nrI):
        iss.append([mesh.indices[i*3+j] for j in range(3)])
        
    for c in get_link_children(robot,linkId):
        dvss,diss=sub_mesh(robot,c,trans)
        iss+=[[i[0]+len(vss),i[1]+len(vss),i[2]+len(vss)] for i in diss]
        vss+=dvss
    return vss,iss

def compute_convex_2D(vss):
    import shapely.geometry as geom
    hull=geom.MultiPoint(vss).convex_hull.exterior.coords
    hull=list(zip(hull.xy[0].tolist(),hull.xy[1].tolist()))
    hull.pop()
    return hull[::-1]

def project_convex_mesh_to_2D(geom):
    vss=[]
    mesh=geom.getTriangleMesh()
    nrV=len(mesh.vertices)//3
    for i in range(nrV):
        vss.append([mesh.vertices[i*3+j] for j in range(2)])
    vss=compute_convex_2D(vss)
    while len(vss)>16: #this is what Box2D can take
        minLenId=-1
        minLen=INFINITY_NUMBER
        for i in range(len(vss)):
            length=op.norm(op.sub(vss[i],vss[(i+1)%len(vss)]))
            if length<minLen:
                minLen=length
                minLenId=i
        del vss[minLenId]
    return vss

def project_convex_mesh_to_2D_pusher(robot,linkId):
    #this is a very hacky method to inferring the shape of pusher
    vss,iss=sub_mesh(robot,linkId)
    
    #select vertex
    vssBottom=None
    for i in range(2):
        dir=[0.,0.,0.]
        dir[2]=1. if i%2==0 else -1.
        
        minVal=INFINITY_NUMBER
        for v in vss:
            minVal=min(minVal,op.dot(v,dir))
        vssBottom=[(-v[0],v[1]) for v in vss if op.dot(v,dir)<minVal+0.01]
        if len(vssBottom)<16:
            break
        else: vssBottom=None
    
    #summarize
    return compute_convex_2D(vssBottom)