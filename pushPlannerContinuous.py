from pushPlannerDiscrete import PushAction
from polygon2D import Polygon2D
from BVHTree import BVHTree
from costFunction import *
from planner import *
import copy,pyDeclutter

class DiscreteShape:
    def __init__(self,shape):
        vss=[pyDeclutter.Vec2(v[0],v[1]) for v in shape.vss]
        self.shapeFast=pyDeclutter.Polygon2D(vss,INFINITY_NUMBER)
        self.shape=shape
        
    def add_intersect_backward(self,a,v,vv,other):
        if self in other.intersectBackward:  #avoid infinite loop
            return
        if other not in self.intersectBackward or self.intersectBackward[other][0]>a:
            self.intersectBackward[other]=(a,v,vv)
            
    def add_intersect_forward(self,a,v,vv,other):
        if self in other.intersectForward:  #avoid infinite loop
            return
        if other not in self.intersectForward or self.intersectForward[other][0]>a:
            self.intersectForward[other]=(a,v,vv)
    
    def build_range_dir(self,dir):
        self.rangeDir=[INFINITY_NUMBER,-INFINITY_NUMBER]
        for v in self.shape.vss:
            r=op.dot(v,dir)
            self.rangeDir[0]=min(self.rangeDir[0],r)
            self.rangeDir[1]=max(self.rangeDir[1],r)
    
    def build_range_tangent(self,tangent):
        self.rangeTangent=[INFINITY_NUMBER,-INFINITY_NUMBER]
        for v in self.shape.vss:
            r=op.dot(v,tangent)
            self.rangeTangent[0]=min(self.rangeDir[0],r)
            self.rangeTangent[1]=max(self.rangeDir[1],r)
    
    @staticmethod
    def initialize_env(dshapes):
        pss=[s.shapeFast for s in dshapes]
        if not hasattr(dshapes[0],'env'):
            env=pyDeclutter.Environment2D(pss)
            for s in dshapes:
                s.env=env
        else: env=dshapes[0].env
    
    @staticmethod
    def initialize_batched(dshapes,sim,dir,tangent,all_region):
        #initialize
        pss=[]
        dss=[]
        idss=[]
        cost=sim.cost
        ndir=op.mul(dir,-1)
        for ids,s in enumerate(dshapes):
            s.intersectBackward=dict()
            s.intersectForward=dict()
            s.build_range_dir(dir)
            s.build_range_tangent(tangent)
            pss.append(s.shape.pos)
            pss.append(s.shape.pos)
            dss.append(dir)
            dss.append(ndir)
        if isinstance(cost,OptimalTransportCost):
            if not hasattr(cost,"regionTag") or cost.frequentUpdate:
                cost.update_region_tag([s.shape.pos for s in dshapes],sim.css())
            for i,s in enumerate(dshapes):
                s.regionTag=cost.regionTag[i]
                idss.append(cost.regionTag[i])
                idss.append(cost.regionTag[i])
        else:
            for i,s in enumerate(dshapes):
                s.regionTag=0
                idss.append(0)
                idss.append(0)
                
        #cost
        if all_region:
            DiscreteShape.initialize_cost_all(cost,pss,dss,dshapes)
        else: DiscreteShape.initialize_cost(cost,pss,dss,idss,dshapes)
        
        #initialize intersect    
        DiscreteShape.initialize_env(dshapes)
        vss=dshapes[0].env.dirPolygonBatched([s.shapeFast for s in dshapes],pyDeclutter.Vec2(dir[0],dir[1]))
        for pid,vid,d,poid in vss:
            if pid==poid or pid<0 or poid<0:
                continue
            elif d>0:
                v=dshapes[pid].shape.vss[vid]
                vv=op.add(v,op.mul(dir,d))
                dshapes[pid].add_intersect_forward(abs(d),v,vv,dshapes[poid])
                dshapes[poid].add_intersect_backward(abs(d),vv,v,dshapes[pid])
            elif d<0:
                v=dshapes[pid].shape.vss[vid]
                vv=op.add(v,op.mul(dir,d))
                dshapes[pid].add_intersect_backward(abs(d),v,vv,dshapes[poid])
                dshapes[poid].add_intersect_forward(abs(d),vv,v,dshapes[pid])
    
    @staticmethod
    def initialize_cost(cost,pss,dss,idss,dshapes):
        css=cost.get_directional_cost(pss,dss,idss)
        for ids,s in enumerate(dshapes):
            s.costForward=css[ids*2+0]
            s.costBackward=css[ids*2+1]
            s.cost0=s.costForward.calc_cost_alpha(0.0)

    @staticmethod
    def initialize_cost_all(cost,pss,dss,dshapes):
        for s in dshapes:
            s.costForwardRegion=[]
            s.costBackwardRegion=[]
            s.cost0Region=[]
        for idr in range(len(cost.get_capacities())):
            css=cost.get_directional_cost(pss,dss,[idr for i in pss])
            for ids,s in enumerate(dshapes):
                s.costForwardRegion.append(css[ids*2+0])
                s.costBackwardRegion.append(css[ids*2+1])
                s.cost0Region.append(s.costForwardRegion[-1].calc_cost_alpha(0.0))
    
    def dist_to(self,level):
        return min(abs(self.rangeDir[0]-level),abs(self.rangeDir[1]-level))
            
class DiscreteCase:
    SINGLE_SIDED_PUSH=True
    
    def __init__(self,sim,search,pT,pD,ssET,ssBT,ssLD,ssD,ssRD):
        self.search=search
        self.sim=sim
        self.pT=pT  #tangent pusher bound tuple
        self.pD=pD  #directional pusher bound tuple
        self.ssET=ssET  #entirely contained object set along tangent
        self.ssBT=ssBT  #boundary affected object set along tangent
        self.ssLD=ssLD  #left object set along direction
        self.ssD=ssD    #intersect object set along direction
        self.ssRD=ssRD  #right object set along direction
        #find d0 forward
        self.ssLD2d0=dict()
        for s in ssLD:
            if s not in self.ssLD2d0:
                self.ssLD2d0[s]=self.find_d0(s,True)
        #find d0 backward
        self.ssRD2d0=dict()
        for s in ssRD:
            if s not in self.ssRD2d0:
                self.ssRD2d0[s]=self.find_d0(s,False)
            
    def initialize(self,branch,safeDist):
        if self.forwardLmt<=0.5 or self.backwardLmt<=0.5:   #how much distance can we move forward/backward without hitting reachability boundary
            return False
        else:
            self.forwardLmt-=0.5
            self.backwardLmt-=0.5
            self.forwardLmt*=self.sim.reachableTraj.get_pusher().res
            self.backwardLmt*=self.sim.reachableTraj.get_pusher().res
        self.distForward,self.costReduceForward,self.segmentsForward=self.estimate_cost_reduction(True,branch=branch,safeDist=safeDist)
        self.distBackward,self.costReduceBackward,self.segmentsBackward=self.estimate_cost_reduction(False,branch=branch,safeDist=safeDist)
        if DiscreteCase.SINGLE_SIDED_PUSH:
            self.make_single_sided()
        self.estimatedCostReduce=op.add(self.costReduceForward,self.costReduceBackward)
        return True
            
    def make_single_sided(self):
        if isinstance(self.distForward,float):
            if self.costReduceForward>self.costReduceBackward:
                self.distBackward=0.
                self.costReduceBackward=0.
            else:
                self.distForward=0.
                self.costReduceForward=0.
        else:
            for ic in range(len(self.sim.cost.get_capacities())):
                if self.costReduceForward[ic]>self.costReduceBackward[ic]:
                    self.distBackward[ic]=0.
                    self.costReduceBackward[ic]=0.
                else:
                    self.distForward[ic]=0.
                    self.costReduceForward[ic]=0.
            
    def find_d0(self,s,forward,stack=[]):
        intersects=s.intersectForward if forward else s.intersectBackward
        ss=self.ssLD if forward else self.ssRD
        ss2d0=self.ssLD2d0 if forward else self.ssRD2d0
        
        minCost=s.dist_to(self.pD[0]) if forward else s.dist_to(self.pD[1]) #this is an over-estimation, set this to the distance to the pusher
        blockingS=None
        for so,a_v_vv in intersects.items():
            if so in ss:
                #assert so not in stack
                if (so not in ss2d0) and (so not in stack):
                    ss2d0[so]=self.find_d0(so,forward,stack+[s])
                if so in ss2d0:
                    cost=ss2d0[so][0]+a_v_vv[0]
                    if cost<minCost:
                        minCost=cost
                        blockingS=so
        return (minCost,blockingS)
            
    def estimate_cost_reduction(self,forward,branch,safeDist):
        ss=self.ssRD if forward else self.ssLD
        ss2d0=self.ssRD2d0 if forward else self.ssLD2d0
        lmt=self.forwardLmt if forward else self.backwardLmt
        nrRegion=len(self.sim.cost.get_capacities())
        
        #compute cost
        costIndices=[]
        costs=[]
        handles=[]
        for ids,s in enumerate(ss):
            costIndices.append(-1)
            costs.append(s.costForward.add(ss2d0[s][0]+safeDist) if forward else s.costBackward.add(ss2d0[s][0]+safeDist))
            for idc,_ in enumerate(costs[-1].cases):
                handles.append((ids,idc))
         
        #sort handle
        def get_alpha(A):
            return costs[A[0]].cases[A[1]].alpha0
        def cmp(A,B):
            return get_alpha(A)-get_alpha(B)
        handlesSorted=sorted(handles,key=cmpToKey(cmp))
        
        #enumerate cases
        segments=[]
        c0=sum([s.cost0 for ids,s in enumerate(ss)])
        minD=[0.0 for ic in range(nrRegion)] if branch else 0.0
        minCost0=[sum([s.cost0 for ids,s in enumerate(ss) if s.regionTag==ic]) for ic in range(nrRegion)] if branch else c0
        minCost=[c for c in minCost0] if branch else c0
        for i in range(len(handlesSorted)):
            alpha0=get_alpha(handlesSorted[i])
            alpha1=get_alpha(handlesSorted[i+1]) if i<len(handlesSorted)-1 else INFINITY_NUMBER
            if alpha0>=lmt:
                break
            if alpha1>=lmt:
                alpha1=lmt
            costIndices[handlesSorted[i][0]]+=1
            if branch:
                d,cost,segment=DiscreteCase.eval_min_cost_branch(alpha0,alpha1,ss,costIndices,costs,c0,nrRegion)
                segments.append(segment)
                for ic in range(nrRegion):
                    if d is not None and cost[ic]<minCost[ic]:
                        minD[ic]=d[ic]
                        minCost[ic]=cost[ic]
            else:
                d,cost,segment=DiscreteCase.eval_min_cost(alpha0,alpha1,ss,costIndices,costs)
                segments.append(segment)
                if cost<minCost:
                    minD=d
                    minCost=cost
            if alpha1>=lmt:
                break
        return minD,op.sub(minCost0,minCost),segments
    
    @staticmethod
    def eval_min_cost_branch(alpha0,alpha1,ss,costIndices,costs,c0,nrRegion):
        d0,c1,segment=DiscreteCase.eval_min_cost(alpha0,alpha1,ss,costIndices,costs)
        if c1>=c0:  #even in branch mode, the total cost should decrease
            return None,None,None
        dss,costss=([],[])
        for ic in range(nrRegion):
            a,b,c=(0.0,0.0,0.0)
            for s,idCase,cost in zip(ss,costIndices,costs):
                if s.regionTag!=ic:
                    continue
                elif idCase==-1:
                    c+=s.cost0
                else:
                    case=cost.cases[idCase]
                    a+=case.a
                    b+=case.b
                    c+=case.c
            dss.append(d0)
            costss.append(a*d0**2+b*d0+c)
        return dss,costss,segment
    
    @staticmethod
    def eval_min_cost(alpha0,alpha1,ss,costIndices,costs):
        a,b,c=(0.0,0.0,0.0)
        for s,idCase,cost in zip(ss,costIndices,costs):
            if idCase==-1:
                c+=s.cost0
            else:
                case=cost.cases[idCase]
                a+=case.a
                b+=case.b
                c+=case.c
        alpha,cost=DiscreteCase.eval_min_quadratic(alpha0,alpha1,a,b,c)
        return alpha,cost,(alpha0,alpha1,a,b,c)
    
    @staticmethod
    def eval_min_quadratic(alpha0,alpha1,a,b,c):
        if a==0:
            if b*alpha0+c<b*alpha1+c:
                return alpha0,b*alpha0+c
            else: return alpha1,b*alpha1+c
        else:
            minV=-b/(a*2)
            if minV<alpha0:
                return alpha0,a*alpha0**2+b*alpha0+c
            elif minV>alpha1:
                return alpha1,a*alpha1**2+b*alpha1+c
            else: 
                return minV,a*minV**2+b*minV+c
         
    @staticmethod
    def reachable_batched(cases):
        idss=[]
        for c in cases:
            idss.append(op.mul(c.get_pusher(0),1/c.sim.reachableTraj.get_pusher().res))
        abss=cases[0].sim.reachableTraj.get_pusher().range_intersect(idss,[cases[0].search.dir]*len(cases))
        for c,ab in zip(cases,abss):
            c.forwardLmt=ab[0]
            c.backwardLmt=ab[1]
         
    def build_action(self,state0,speed,speedMove,branch_id=None):
        dists=[0.0]
        #whether this is for branching or for planning
        distForward=self.distForward if branch_id is None else self.distForward[branch_id]
        distBackward=self.distBackward if branch_id is None else self.distBackward[branch_id]
        costReduceForward=self.costReduceForward if branch_id is None else self.costReduceForward[branch_id]
        costReduceBackward=self.costReduceBackward if branch_id is None else self.costReduceBackward[branch_id]
        
        #add action
        if distForward>distBackward:
            if costReduceForward>0:
                dists.append(distForward)
            if costReduceBackward>0:
                dists.append(-distBackward)
        else:
            if costReduceBackward>0:
                dists.append(-distBackward)
            if costReduceForward>0:
                dists.append(distForward)
        if dists.__len__()<=1:
            return Action(self.sim,speed,speedMove)
        else:
            idss=[op.mul(self.get_pusher(dist),1/self.sim.reachableTraj.get_pusher().res) for dist in dists]
            return PushAction(self.sim,idss,speed,speedMove,buildBB=False,mustExist=True)

    def display_action(self,margin,branch_id=None):
        #whether this is for branching or for planning
        distForward=self.distForward if branch_id is None else self.distForward[branch_id]
        distBackward=self.distBackward if branch_id is None else self.distBackward[branch_id]
        
        #display
        dists=[0.0]
        colors=[None]
        if distForward>distBackward:
            dists.append(distForward)
            colors.append(FORWARD_COLOR)
            dists.append(-distBackward)
            colors.append(BACKWARD_COLOR)
        else:
            dists.append(-distBackward)
            colors.append(BACKWARD_COLOR)
            dists.append(distForward)
            colors.append(FORWARD_COLOR)
        if len(dists)>1 and dists[-1]==0.0:
            dists.pop()
            colors.pop()
        gl.glBegin(gl.GL_LINES)
        for i in range(1,len(dists)):
            idfrom=self.get_pusher(dists[i-1])
            idto  =self.get_pusher(dists[i  ])
            gldraw.setcolor(colors[i][0],colors[i][1],colors[i][2])
            gl.glVertex3f(idfrom[0],idfrom[1],margin)
            gl.glVertex3f(idto  [0],idto  [1],margin)
        gl.glEnd()

    def display_cost(self,interval=0.1):
        if not hasattr(self,'segmentsForward'):
            return
        gldraw.setcolor(COST_DISTANCE_COLOR[0],COST_DISTANCE_COLOR[1],COST_DISTANCE_COLOR[2])
        
        #forward
        p0=self.get_pusher(0.0,front=True)
        gl.glBegin(gl.GL_LINE_STRIP)
        for alpha0,alpha1,a,b,c in self.segmentsForward:
            nrSample=int(max(2,(alpha1-alpha0)//interval+1))
            for i in range(nrSample):
                alpha=i*(alpha1-alpha0)/(nrSample-1)+alpha0
                pos=op.add(p0,op.mul(self.search.dir,alpha))
                gl.glVertex3f(pos[0],pos[1],a*alpha**2+b*alpha+c)
        gl.glEnd()
        
        #backward
        p0=self.get_pusher(0.0,back=True)
        gl.glBegin(gl.GL_LINE_STRIP)
        for alpha0,alpha1,a,b,c in self.segmentsBackward:
            nrSample=int(max(2,(alpha1-alpha0)//interval+1))
            for i in range(nrSample):
                alpha=i*(alpha1-alpha0)/(nrSample-1)+alpha0
                pos=op.add(p0,op.mul(self.search.dir,-alpha))
                gl.glVertex3f(pos[0],pos[1],a*alpha**2+b*alpha+c)
        gl.glEnd()

    def get_pusher(self,d,front=False,back=False):
        pD=self.pD[1] if front else self.pD[0] if back else (self.pD[0]+self.pD[1])/2
        offD=op.mul(self.search.dir,pD+d)
        
        pT=(self.pT[0]+self.pT[1])/2
        offT=op.mul(self.search.tangent,pT)
        
        return op.add(offD,offT)

class DiscreteCaseJoint(DiscreteCase):
    def __init__(self,sim,search,pT,pD,ssET,ssBT,ssLD,ssD,ssRD,shapes):
        DiscreteCase.__init__(self,sim,search,pT,pD,ssET,ssBT,ssLD,ssD,ssRD)
        self.shapesAll=shapes
        
    def estimate_cost_reduction(self,forward,branch,safeDist):
        ss=self.ssRD if forward else self.ssLD
        ss2d0=self.ssRD2d0 if forward else self.ssLD2d0
        lmt=self.forwardLmt if forward else self.backwardLmt
        nrRegion=len(self.sim.cost.get_capacities())
        category=self.sim.css()
        
        #compute cost
        costIndicesRegion=[]
        costsRegion=[]
        handles=[]
        for ids,s in enumerate(ss):
            costs_ss=s.costForwardRegion if forward else s.costBackwardRegion
            costs_ss=[c.add(ss2d0[s][0]+safeDist) for c in costs_ss]
            for idr,c in enumerate(costs_ss):
                costIndicesRegion.append(-1)
                costsRegion.append(c)
                for idc,_ in enumerate(c.cases):
                    handles.append((ids,idr,idc))
         
        #sort handle
        def get_alpha(A):
            return costsRegion[A[0]*nrRegion+A[1]].cases[A[2]].alpha0
        def cmp(A,B):
            return get_alpha(A)-get_alpha(B)
        handlesSorted=sorted(handles,key=cmpToKey(cmp))
        
        #enumerate cases
        segments=[]
        c0=sum([s.cost0Region[s.regionTag] for ids,s in enumerate(self.shapesAll)])
        minD=[0.0 for ic in range(nrRegion)] if branch else 0.0
        minCost0=[sum([s.cost0Region[s.regionTag] for ids,s in enumerate(self.shapesAll) if s.regionTag==ic]) for ic in range(nrRegion)] if branch else c0
        minCost=[c for c in minCost0] if branch else c0
        for i in range(len(handlesSorted)):
            alpha0=get_alpha(handlesSorted[i])
            alpha1=get_alpha(handlesSorted[i+1]) if i<len(handlesSorted)-1 else INFINITY_NUMBER
            if alpha0>=lmt:
                break
            if alpha1>=lmt:
                alpha1=lmt
            costIndicesRegion[handlesSorted[i][0]*nrRegion+handlesSorted[i][1]]+=1
            if branch:
                d,cost,segment=DiscreteCaseJoint.eval_min_cost_branch_joint(self.shapesAll,alpha0,alpha1,ss,costIndicesRegion,costsRegion,self.sim.cost.get_capacities(),category)
                segments.append(segment)
                for ic in range(nrRegion):
                    if d is not None and cost[ic]<minCost[ic]:
                        minD[ic]=d[ic]
                        minCost[ic]=cost[ic]
            else:
                d,cost,segment=DiscreteCaseJoint.eval_min_cost_joint(self.shapesAll,alpha0,alpha1,ss,costIndicesRegion,costsRegion,self.sim.cost.get_capacities(),category)
                if d is not None:
                    segments.append(segment)
                    if cost<minCost:
                        minD=d
                        minCost=cost
            if alpha1>=lmt:
                break
        return minD,op.sub(minCost0,minCost),segments
    
    @staticmethod
    def eval_min_cost_branch_joint(shapes,alpha0,alpha1,ss,costIndices,costs,capacities,category=None):
        distss=[]
        objss=[]
        nrRegion=len(costs)//len(ss)
        for idr in range(nrRegion):
            dist,obj,_=DiscreteCaseJoint.eval_min_cost_joint(shapes,alpha0,alpha1,ss,costIndices,costs,capacities,category,idr)
            distss.append(dist)
            objss.append(obj)
        return distss,objss,None
    
    @staticmethod
    def eval_min_cost_joint(shapes,alpha0,alpha1,ss,costIndices,costs,capacities,category=None,regionMask=None):
        nrRegion=len(costs)//len(ss)
        M=op.gp.Model("OptimalTransport")
        M.setParam("LogToConsole",0)
        M.setParam("LogToFile","")
        z=[[M.addVar(vtype=op.GRB.BINARY) for j in range(len(shapes))] for i in range(nrRegion)]
        
        #constraint
        assert category is not None
        cluster=[[i for i in range(len(shapes)) if category[i]==c] for c in range(max(category)+1)]
        for ic,c in enumerate(cluster):
            #only one region
            for j in c:
                M.addConstr(sum([z[i][j] for i in range(nrRegion)])==1)
            #capacity
            for i in range(nrRegion):
                M.addConstr(sum([z[i][j] for j in c])<=capacities[i][ic])
        
        #objective
        obj=0.
        EXPENSIVE_FORMULATION=False
        d=M.addVar(lb=alpha0,ub=alpha1)
        for idr in range(nrRegion):
            if regionMask is not None and idr!=regionMask:
                continue
            for idsAll,s in enumerate(shapes):
                #these objects will not be moved
                if s not in ss:
                    obj+=s.cost0Region[idr]*z[idr][idsAll]
                    continue
                    
                ids=None
                for idsss,sMoved in enumerate(ss):
                    if s==sMoved:
                        ids=idsss
                        break
                idCase=costIndices[ids*nrRegion+idr]
                if idCase<0:
                    #these objects can be moved by pusher will not touch it
                    obj+=s.cost0Region[idr]*z[idr][idsAll]
                    continue
                
                #pusher touch
                case=costs[ids*nrRegion+idr].cases[idCase]
                if case.a<1e-6:
                    obj+=case.c*z[idr][idsAll]
                elif EXPENSIVE_FORMULATION: #this is expensive formulation
                    r=M.addVar(lb=-INFINITY_NUMBER,ub=INFINITY_NUMBER)
                    #case 1
                    expr=(d+case.b/(2*case.a))*op.sqrt_safe(case.a)
                    expr0=case.c-case.b**2/(4*case.a)
                    c=(1-z[idr][idsAll])*INFINITY_NUMBER
                    M.addConstr(r-expr<=c)
                    M.addConstr(r-expr>=-c)
                    #case 2
                    c=z[idr][idsAll]*INFINITY_NUMBER
                    M.addConstr(r<=c)
                    M.addConstr(r>=-c)
                    obj+=r*r+expr0*z[idr][idsAll]
                else:   #this is cheaper formulation
                    d0=(alpha0+alpha1)/2
                    costValMid=(case.a*d0**2+case.b*d0+case.c)
                    obj+=costValMid*z[idr][idsAll]
        M.setObjective(obj,op.GRB.MINIMIZE)
        
        #solve the problem
        M.optimize()
        if M.status==op.GRB.OPTIMAL:
            if EXPENSIVE_FORMULATION:
                return d.x,obj.getValue(),None
            else:
                a,b,c=(0.0,0.0,0.0)
                z=[sum([i if z[i][j].x>0.5 else 0 for i in range(nrRegion)]) for j in range(len(shapes))]
                for idsAll,s in enumerate(shapes):
                    if s not in ss:
                        c+=s.cost0Region[z[idsAll]]
                        continue
                    
                    ids=None
                    for idsss,sMoved in enumerate(ss):
                        if s==sMoved:
                            ids=idsss
                            break
                    idCase=costIndices[ids*nrRegion+z[idsAll]]
                    if idCase<0:
                        #these objects can be moved by pusher will not touch it
                        c+=s.cost0Region[z[idsAll]]
                        continue
                    
                    case=costs[ids*nrRegion+z[idsAll]].cases[idCase]
                    a+=case.a
                    b+=case.b
                    c+=case.c
                alpha,cost=DiscreteCase.eval_min_quadratic(alpha0,alpha1,a,b,c)
                return alpha,cost,(alpha0,alpha1,a,b,c)
        else: return None,None,None
        
class DiscreteSearch:
    ONLY_CONSIDER_ENTIRE_SET=True
    
    def __init__(self,branch,shapes,sim,dir,pusherLength,pusherWidth,boundaryOnly=True,gPos=None,joint=False,safeDist=0.):
        self.dir=dir
        self.tangent=perp(dir)
        self.shapes=shapes
        DiscreteShape.initialize_batched(shapes,sim,dir,self.tangent,all_region=joint)
        self.sort_handles(shapes)
        self.enumerate_cases(pusherLength,pusherWidth,boundaryOnly)
        self.analyze_cases(sim,gPos,branch,joint,safeDist)
        
    def sort_handles(self,shapes):
        def cmp(A,B):
            return A[2]-B[2]
        handlesD=[]
        handlesT=[]
        for s in shapes:
            for i in range(len(s.shape.vss)):
                v=s.shape.vertex(i)
                handlesD.append((i,s,op.dot(v,self.dir)))
                handlesT.append((i,s,op.dot(v,self.tangent)))
        self.handlesSortedD=sorted(handlesD,key=cmpToKey(cmp))
        self.handlesSortedT=sorted(handlesT,key=cmpToKey(cmp))
    
    @staticmethod
    def enumerate(shapes,handlesSortedAll,length,newCase):
        #sort handle along tangent
        for s in shapes:
            s.crossedL=0
            s.crossedR=0
        handlesSorted=[]
        for h in handlesSortedAll:
            if h[1] in shapes:
                handlesSorted.append(h)
            
        #index handle along tangent
        indexR=0
        indexL=0
        LShapes=set()
        intersectShapes=set()
        entireShapes=set()
        RShapes=set(shapes)
        pusherPos=handlesSorted[0][2]-0.001
        pusherPos=(pusherPos-length,pusherPos)
        while indexL<len(handlesSorted) or indexR<len(handlesSorted):
            #left
            if indexL<len(handlesSorted):
                thresL=handlesSorted[indexL][2]
                distToL=thresL-pusherPos[0]
            else: distToL=INFINITY_NUMBER
            #right
            if indexR<len(handlesSorted):
                thresR=handlesSorted[indexR][2]
                distToR=thresR-pusherPos[1]
            else: distToR=INFINITY_NUMBER
            #update
            changedL=False
            changedIntersect=False
            changedEntire=False
            changedR=False
            lastPusherPos=pusherPos
            lastLShapes=copy.copy(LShapes)
            lastEntireShapes=copy.copy(entireShapes)
            lastIntersectShapes=copy.copy(intersectShapes)
            lastRShapes=copy.copy(RShapes)
            if distToR<distToL:
                pusherPos=(thresR-length,thresR)
                s=handlesSorted[indexR][1]
                s.crossedR+=1
                if s.crossedR==len(s.shape.vss) and s.crossedL==0:
                    entireShapes.add(s)
                    changedEntire=True
                if s.crossedR==1 and s.crossedL==0:
                    RShapes.remove(s)
                    changedR=True
                    intersectShapes.add(s)
                    changedIntersect=True
                indexR+=1
            else:
                pusherPos=(thresL,thresL+length)
                s=handlesSorted[indexL][1]
                s.crossedL+=1
                if s in entireShapes:
                    entireShapes.remove(s)
                    changedEntire=True
                if s.crossedL==len(s.shape.vss) and s.crossedR==len(s.shape.vss):
                    LShapes.add(s)
                    changedL=True
                    intersectShapes.remove(s)
                    changedIntersect=True
                indexL+=1
            #yield valid pusher pos
            if newCase(changedL,changedIntersect,changedEntire,changedR):
                yield (lastPusherPos,lastLShapes,lastIntersectShapes,lastEntireShapes,lastRShapes)
                if indexL==len(handlesSorted) and indexR==len(handlesSorted):
                    yield (pusherPos,LShapes,intersectShapes,entireShapes,RShapes)
    
    def enumerate_cases(self,pusherLength,pusherWidth,boundaryOnly):
        #enumerate all cases along tangent
        casesT=[]
        def newCaseT(cL,cI,cE,cR):
            return cE
        for pT,_,ssIT,ssET,_ in DiscreteSearch.enumerate(self.shapes,self.handlesSortedT,pusherLength,newCaseT):
            ssBT=ssIT.difference(ssET)
            #condition to add case T
            if DiscreteSearch.ONLY_CONSIDER_ENTIRE_SET and ssET.__len__()==0:
                continue
            if (not DiscreteSearch.ONLY_CONSIDER_ENTIRE_SET) and ssIT.__len__()==0:
                continue
            elif casesT.__len__()>0:
                lastSSET=casesT[-1][1]
                lastSSBT=casesT[-1][2]
                lastBetter=ssET.issubset(lastSSET)
                thisBetter=lastSSET.issubset(ssET)
                if lastBetter and thisBetter:
                    if ssBT.__len__()<lastSSBT.__len__():   #this is better by using fewer boundaries
                        casesT[-1]=(pT,ssET,ssBT)
                elif lastBetter:                            #last one is better than this one, still add
                    casesT.append((pT,ssET,ssBT))
                elif thisBetter:                            #this one is better, still add
                    casesT.append((pT,ssET,ssBT))
                else: casesT.append((pT,ssET,ssBT))         #otherwise, add
            else: casesT.append((pT,ssET,ssBT))
            
        #enumerate all cases along dir
        self.casesT=[]
        def newCaseD(cL,cI,cE,cR):
            return cL or cR
        for pT,ssET,ssBT in casesT:
            cases=[]
            for pD,ssLD,ssID,_,ssRD in DiscreteSearch.enumerate(ssET.union(ssBT),self.handlesSortedD,pusherWidth,newCaseD):
                if DiscreteSearch.ONLY_CONSIDER_ENTIRE_SET:
                    ssLD=ssLD.intersection(ssET)
                    ssRD=ssRD.intersection(ssET)
                #condition to add case D
                if boundaryOnly and ssID.__len__()>0:
                    pass
                elif ssLD.__len__()==0 and ssRD.__len__()==0:
                    pass
                elif cases.__len__()>0:
                    lastSSLD=cases[-1][1]
                    lastSSID=cases[-1][2]
                    lastSSRD=cases[-1][3]
                    lastBetter=ssLD.issubset(lastSSLD) and ssRD.issubset(lastSSRD)
                    thisBetter=lastSSLD.issubset(ssLD) and lastSSRD.issubset(ssRD)
                    if lastBetter and thisBetter:
                        if ssID.__len__()<lastSSID.__len__():   #this is better by using fewer intersections
                            cases[-1]=(pD,ssLD,ssID,ssRD)
                    elif lastBetter:                            #last one is better than this one, do nothing
                        pass
                    elif thisBetter:                            #this one is better, replace
                        cases[-1]=(pD,ssLD,ssID,ssRD)
                    else: cases.append((pD,ssLD,ssID,ssRD))     #otherwise, add
                else: cases.append((pD,ssLD,ssID,ssRD))
            #insert all cases
            self.casesT.append((pT,ssET,ssBT,cases))
    
    def analyze_cases(self,sim,gPos,branch,joint,safeDist):
        cases=[]
        casesTNew=[]
        for pT,ssET,ssBT,casesD in self.casesT:
            casesDNew=[]
            for pD,ssLD,ssD,ssRD in casesD:
                if joint:
                    case=DiscreteCaseJoint(sim,self,pT,pD,ssET,ssBT,ssLD,ssD,ssRD,self.shapes)
                else: case=DiscreteCase(sim,self,pT,pD,ssET,ssBT,ssLD,ssD,ssRD)
                casesDNew.append((pD,ssLD,ssD,ssRD,case))
                cases.append(case)
            casesTNew.append((pT,ssET,ssBT,casesDNew))
            
        #reachable
        DiscreteCase.reachable_batched(cases)
        
        #analyze
        if hasattr(self,'bestCase'):
            delattr(self,'bestCase')
        if hasattr(self,'bestCases'):
            delattr(self,'bestCases')
        if branch:
            self.bestCases=[None for i in sim.cost.get_capacities()]
        else: self.bestCase=None
        for case in cases:
            if not case.initialize(branch,safeDist):
                continue
            elif branch:
                for ic in range(len(case.estimatedCostReduce)):
                    if self.bestCases[ic] is None or (case.estimatedCostReduce[ic]>0 and case.estimatedCostReduce[ic]>self.bestCases[ic].estimatedCostReduce[ic]):
                        self.bestCases[ic]=case
            else:
                if self.bestCase is None or (case.estimatedCostReduce>0 and case.estimatedCostReduce>self.bestCase.estimatedCostReduce):
                    self.bestCase=case
        
        #add all
        self.casesT=[]
        for pT,ssET,ssBT,casesD in casesTNew:
            casesDNew=[]
            for pD,ssLD,ssD,ssRD,case in casesD:
                if case.forwardLmt>0.0 and case.backwardLmt>0.0:
                    casesDNew.append((pD,ssLD,ssD,ssRD,case))
            self.casesT.append((pT,ssET,ssBT,casesDNew))
    
    def display_intersect(self,i,margin):
        s=self.shapes[i]
        #draw
        gldraw.setcolor(SPECIAL_OBJECT_COLOR[0],SPECIAL_OBJECT_COLOR[1],SPECIAL_OBJECT_COLOR[2])
        s.shape.draw(margin*2,fill=True)
        #ray
        gl.glBegin(gl.GL_LINES)
        gldraw.setcolor(FORWARD_COLOR[0],FORWARD_COLOR[1],FORWARD_COLOR[2])
        for so,a_v_vv in s.intersectForward.items():
            _,a,b=a_v_vv
            gl.glVertex3f(a[0],a[1],margin)
            gl.glVertex3f(b[0],b[1],margin)
        gldraw.setcolor(BACKWARD_COLOR[0],BACKWARD_COLOR[1],BACKWARD_COLOR[2])
        for so,a_v_vv in s.intersectBackward.items():
            _,a,b=a_v_vv
            gl.glVertex3f(a[0],a[1],margin)
            gl.glVertex3f(b[0],b[1],margin)
        gl.glEnd()
        #draw cost
        s.costForward.draw(margin)
        s.costForward.draw_cost()
        s.costBackward.draw(margin)
        s.costBackward.draw_cost()

    def display_tangent_case(self,i,j,margin):
        pT,ssET,ssIT,cases=self.casesT[i]
        if j<0 or j>=len(cases):
            #draw ssT
            gldraw.setcolor(OUTSIDE_COLOR[0],OUTSIDE_COLOR[1],OUTSIDE_COLOR[2])
            for s in ssIT:
                s.shape.draw(margin*2,fill=True)
            #draw ssE
            gldraw.setcolor(SPECIAL_OBJECT_COLOR[0],SPECIAL_OBJECT_COLOR[1],SPECIAL_OBJECT_COLOR[2])
            for s in ssET:
                s.shape.draw(margin*2,fill=True)
        else:
            #draw caseT
            pD,ssLD,ssID,ssRD,caseStruct=cases[j]
            gldraw.setcolor(BACKWARD_COLOR[0],BACKWARD_COLOR[1],BACKWARD_COLOR[2])
            for s in ssLD:
                s.shape.draw(margin*2,fill=True)
            gldraw.setcolor(SPECIAL_OBJECT_COLOR[0],SPECIAL_OBJECT_COLOR[1],SPECIAL_OBJECT_COLOR[2])
            for s in ssID:
                s.shape.draw(margin*2,fill=True)
            gldraw.setcolor(FORWARD_COLOR[0],FORWARD_COLOR[1],FORWARD_COLOR[2])
            for s in ssRD:
                s.shape.draw(margin*2,fill=True)
            #draw pusher
            gldraw.setcolor(PUSHER_COLOR[0],PUSHER_COLOR[1],PUSHER_COLOR[2])
            self.draw_pusher(pT[0],pT[1],pD[0],pD[1],margin*3)
            #draw cost
            caseStruct.display_cost()
            
    def draw_pusher(self,t0,t1,d0,d1,margin):
        gl.glBegin(gl.GL_TRIANGLES)
        v00=op.add(op.mul(self.tangent,t0),op.mul(self.dir,d0))
        v10=op.add(op.mul(self.tangent,t1),op.mul(self.dir,d0))
        v11=op.add(op.mul(self.tangent,t1),op.mul(self.dir,d1))
        v01=op.add(op.mul(self.tangent,t0),op.mul(self.dir,d1))
        #t0
        gl.glVertex3f(v00[0],v00[1],margin)
        gl.glVertex3f(v10[0],v10[1],margin)
        gl.glVertex3f(v11[0],v11[1],margin)
        #t1
        gl.glVertex3f(v00[0],v00[1],margin)
        gl.glVertex3f(v11[0],v11[1],margin)
        gl.glVertex3f(v01[0],v01[1],margin)
        gl.glEnd()

    def num_cases(self):
        numCases=0
        for pT,ssET,ssBT,casesD in self.casesT:
            numCases+=len(casesD)
        return numCases

class PushPlannerContinuous(Planner):
    def __init__(self,sim,speed,speedPush,nrDir=8,entireSet=True,safeDist=1.,joint=False):
        Planner.__init__(self,sim)
        self.speed=speed
        self.speedPush=speedPush
        self.drawShape=-1
        self.drawDirId=-1
        self.drawCaseT=-1
        self.drawCase=-1
        self.joint=joint
        DiscreteSearch.ONLY_CONSIDER_ENTIRE_SET=entireSet
        
        #directions
        self.dirs=[]
        for j in range(nrDir//2):
            angle=j*math.pi*2/nrDir
            self.dirs.append((op.cos_safe(angle),op.sin_safe(angle)))
    
        #pusher geometry
        self.pusherLength=self.sim.shapePusherBox2D.longest_edge()
        self.pusherWidth=self.sim.shapePusherBox2D.shortest_edge()
        
        #safety
        self.safeDist=self.pusherWidth*safeDist
        self.pusherLength+=self.safeDist*2
        self.pusherWidth+=self.safeDist*2
    
    def keyboardfunc(self,c,x,y):
        def tolowerbyte(c):
            cl=['1','2','3','4','5','6','7','8']
            cu=['!','@','#','$','%','^','&','*']
            for l,u in zip(cl,cu):
                if l==c or u==c:
                    return l
            return None
        if tolowerbyte(c)==b'1':    #next search direction
            branch=c==b'1'
            self.drawDirId=(self.drawDirId+len(self.dirs)-1)%len(self.dirs)
            shapes=[DiscreteShape(s) for s in self.sim.get_shapes()]
            self.search=DiscreteSearch(branch,shapes,self.sim,self.dirs[self.drawDirId],self.pusherLength,self.pusherWidth)
            self.drawCaseT=-1
            self.drawCase=-1
        elif tolowerbyte(c)==b'2':  #last search direction
            branch=c==b'2'
            self.drawDirId=(self.drawDirId+1)%len(self.dirs)
            shapes=[DiscreteShape(s) for s in self.sim.get_shapes()]
            self.search=DiscreteSearch(branch,shapes,self.sim,self.dirs[self.drawDirId],self.pusherLength,self.pusherWidth)
            self.drawCaseT=-1
            self.drawCase=-1
        elif tolowerbyte(c)==b'3':  #next shape
            branch=c==b'3'
            if not hasattr(self,"search"):
                shapes=[DiscreteShape(s) for s in self.sim.get_shapes()]
                self.search=DiscreteSearch(branch,shapes,self.sim,self.dirs[self.drawDirId],self.pusherLength,self.pusherWidth)
            self.drawShape=(self.drawShape+len(self.search.shapes)-1)%len(self.search.shapes)
            self.drawCaseT=-1
            self.drawCase=-1
        elif tolowerbyte(c)==b'4':  #last shape
            branch=c==b'4'
            if not hasattr(self,"search"):
                shapes=[DiscreteShape(s) for s in self.sim.get_shapes()]
                self.search=DiscreteSearch(branch,shapes,self.sim,self.dirs[self.drawDirId],self.pusherLength,self.pusherWidth)
            self.drawShape=(self.drawShape+1)%len(self.search.shapes)
            self.drawCaseT=-1
            self.drawCase=-1
        elif tolowerbyte(c)==b'5':  #last tangent case
            branch=c==b'5'
            if not hasattr(self,"search"):
                shapes=[DiscreteShape(s) for s in self.sim.get_shapes()]
                self.search=DiscreteSearch(branch,shapes,self.sim,self.dirs[self.drawDirId],self.pusherLength,self.pusherWidth)
            self.drawCaseT=(self.drawCaseT+len(self.search.casesT)-1)%len(self.search.casesT)
            self.drawCase=-1
            self.drawShape=-1
        elif tolowerbyte(c)==b'6':  #next tangent case
            branch=c==b'6'
            if not hasattr(self,"search"):
                shapes=[DiscreteShape(s) for s in self.sim.get_shapes()]
                self.search=DiscreteSearch(branch,shapes,self.sim,self.dirs[self.drawDirId],self.pusherLength,self.pusherWidth)
            self.drawCaseT=(self.drawCaseT+1)%len(self.search.casesT)
            self.drawCase=-1
            self.drawShape=-1
        elif tolowerbyte(c)==b'7':  #last directional case
            branch=c==b'7'
            if not hasattr(self,"search"):
                shapes=[DiscreteShape(s) for s in self.sim.get_shapes()]
                self.search=DiscreteSearch(branch,shapes,self.sim,self.dirs[self.drawDirId],self.pusherLength,self.pusherWidth)
            if self.drawCaseT>=0 and self.drawCaseT<len(self.search.casesT):
                nrCaseT=len(self.search.casesT[self.drawCaseT][-1])
                self.drawCase=(self.drawCase+nrCaseT-1)%nrCaseT
        elif tolowerbyte(c)==b'8':  #next directional case
            branch=c==b'8'
            if not hasattr(self,"search"):
                shapes=[DiscreteShape(s) for s in self.sim.get_shapes()]
                self.search=DiscreteSearch(branch,shapes,self.sim,self.dirs[self.drawDirId],self.pusherLength,self.pusherWidth)
            if self.drawCaseT>=0 and self.drawCaseT<len(self.search.casesT):
                nrCaseT=len(self.search.casesT[self.drawCaseT][-1])
                self.drawCase=(self.drawCase+1)%nrCaseT
        elif c==b'9':
            self.plan(debugInfo=True)
        elif c==b'0':
            self.branch(debugInfo=True)
        
    def display(self,margin):
        gl.glPushAttrib(gl.GL_POINT_BIT|gl.GL_LINE_BIT)
        gl.glPointSize(PUSH_GRASP_SAMPLE_POINT_SIZE)
        gl.glLineWidth(PUSH_GRASP_LINE_WIDTH)
        
        if hasattr(self,"search") and self.drawShape>=0 and self.drawShape<len(self.search.shapes):
            self.search.display_intersect(self.drawShape,margin)
        if hasattr(self,"search") and self.drawCaseT>=0 and self.drawCaseT<len(self.search.casesT):
            self.search.display_tangent_case(self.drawCaseT,self.drawCase,margin)
        if hasattr(self,"bestCase"):
            self.bestCase.display_action(margin)
        if hasattr(self,"bestCases"):
            for ic in range(len(self.bestCases)):
                self.bestCases[ic].display_action(margin,branch_id=ic)
    
        gl.glPopAttrib(gl.GL_POINT_BIT|gl.GL_LINE_BIT)
        
    def plan(self,debugInfo):
        numCases=0
        self.bestCase=None
        state0=self.sim.get_state()
        gPos=self.sim.reachableTraj.get_pusher().global_pos()[1][0:2]
        #shapes=[DiscreteShape(s) for s in self.sim.get_shapes(emulated_pos_noise=0.1)] this line is for debugging
        shapes=[DiscreteShape(s) for s in self.sim.get_shapes()]
        
        boundaryOnly=True
        self.searches=[DiscreteSearch(False,shapes,self.sim,self.dirs[id],self.pusherLength,self.pusherWidth,boundaryOnly,gPos,joint=self.joint,safeDist=self.safeDist) for id in range(len(self.dirs))]
        for search in self.searches:
            numCases+=search.num_cases()
            if self.bestCase is None or search.bestCase.estimatedCostReduce>self.bestCase.estimatedCostReduce:
                self.bestCase=search.bestCase
        action=self.bestCase.build_action(state0,self.speed,self.speedPush)
        action.test(state0)
        self.sim.set_state(state0)
        if debugInfo:
            print("PushPlannerContinuous tested %d cases, estimatedCostReduce=%f, costReduce=%f!"%  \
                  (numCases,self.bestCase.estimatedCostReduce,action.costReduce))
        if self.bestCase.estimatedCostReduce>0 and action.costReduce>0:
            return action
        else: return None
        
    def branch(self,debugInfo):
        numCases=0
        self.bestCases=[None for c in self.sim.cost.get_capacities()]
        state0=self.sim.get_state()
        gPos=self.sim.reachableTraj.get_pusher().global_pos()[1][0:2]
        shapes=[DiscreteShape(s) for s in self.sim.get_shapes()]
        
        boundaryOnly=True
        self.searches=[DiscreteSearch(True,shapes,self.sim,self.dirs[id],self.pusherLength,self.pusherWidth,boundaryOnly,gPos,joint=self.joint,safeDist=self.safeDist) for id in range(len(self.dirs))]
        for search in self.searches:
            numCases+=search.num_cases()
            for ic in range(len(self.bestCases)):
                if self.bestCases[ic] is None or search.bestCases[ic].estimatedCostReduce[ic]>self.bestCases[ic].estimatedCostReduce[ic]:
                    self.bestCases[ic]=search.bestCases[ic]
        
        actions=[]
        for ic in range(len(self.bestCases)):
            action=self.bestCases[ic].build_action(state0,self.speed,self.speedPush,branch_id=ic)
            action.test(state0)
            self.sim.set_state(state0)
            if self.bestCases[ic].estimatedCostReduce[ic]>0 and action.costReduce>0:
                if debugInfo:
                    print("PushPlannerContinuous-branch%d tested %d cases, estimatedCostReduce=%f, costReduce=%f!"%  \
                          (ic,numCases,self.bestCases[ic].estimatedCostReduce[ic],action.costReduce))
                actions.append(action)
            else: 
                if debugInfo:
                    print("GraspPlannerDiscrete-branch%d empty!"%ic)
                actions.append(None)
        return actions