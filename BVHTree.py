from klampt.vis import gldraw
from visualizer import *
from utils import *
import copy

def draw_bb(bb,margin):
    gldraw.setcolor(OBJECT_COLOR[0],OBJECT_COLOR[1],OBJECT_COLOR[2])
    gl.glBegin(gl.GL_LINES)
    if len(bb[0])==2:
        #x
        gl.glVertex3f(bb[0][0],bb[0][1],margin)
        gl.glVertex3f(bb[1][0],bb[0][1],margin)
        gl.glVertex3f(bb[0][0],bb[1][1],margin)
        gl.glVertex3f(bb[1][0],bb[1][1],margin)
        #y
        gl.glVertex3f(bb[0][0],bb[0][1],margin)
        gl.glVertex3f(bb[0][0],bb[1][1],margin)
        gl.glVertex3f(bb[1][0],bb[0][1],margin)
        gl.glVertex3f(bb[1][0],bb[1][1],margin)
    elif len(bb[0])==3:
        #x
        gl.glVertex3f(bb[0][0],bb[0][1],bb[0][2])
        gl.glVertex3f(bb[1][0],bb[0][1],bb[0][2])
        gl.glVertex3f(bb[0][0],bb[1][1],bb[0][2])
        gl.glVertex3f(bb[1][0],bb[1][1],bb[0][2])
        gl.glVertex3f(bb[0][0],bb[1][1],bb[1][2])
        gl.glVertex3f(bb[1][0],bb[1][1],bb[1][2])
        gl.glVertex3f(bb[0][0],bb[0][1],bb[1][2])
        gl.glVertex3f(bb[1][0],bb[0][1],bb[1][2])
        #y
        gl.glVertex3f(bb[0][0],bb[0][1],bb[0][2])
        gl.glVertex3f(bb[0][0],bb[1][1],bb[0][2])
        gl.glVertex3f(bb[1][0],bb[0][1],bb[0][2])
        gl.glVertex3f(bb[1][0],bb[1][1],bb[0][2])
        gl.glVertex3f(bb[1][0],bb[0][1],bb[1][2])
        gl.glVertex3f(bb[1][0],bb[1][1],bb[1][2])
        gl.glVertex3f(bb[0][0],bb[0][1],bb[1][2])
        gl.glVertex3f(bb[0][0],bb[1][1],bb[1][2])
        #z
        gl.glVertex3f(bb[0][0],bb[0][1],bb[0][2])
        gl.glVertex3f(bb[0][0],bb[0][1],bb[1][2])
        gl.glVertex3f(bb[1][0],bb[0][1],bb[0][2])
        gl.glVertex3f(bb[1][0],bb[0][1],bb[1][2])
        gl.glVertex3f(bb[1][0],bb[1][1],bb[0][2])
        gl.glVertex3f(bb[1][0],bb[1][1],bb[1][2])
        gl.glVertex3f(bb[0][0],bb[1][1],bb[0][2])
        gl.glVertex3f(bb[0][0],bb[1][1],bb[1][2])
    else: 
        assert False
    gl.glEnd()

def surface_area_bb(bb):
    expand=op.sub(bb[1],bb[0])
    cost=0
    for i in range(len(expand)):
        area=1
        for j in range(len(expand)):
            if j!=i:
                area*=expand[j]
        cost+=area*2
    return cost

class BVHTree:
    def __init__(self,entities,expand,depth=None):
        self.entities=entities
        self.bb=None
        self.L=None
        self.R=None
        self.depth=0 if depth is None else depth
        self.bb=empty_bb()
        if entities is not None:
            for e in entities:
                self.bb=union_bb(self.bb,e.bb)
            self.bb=expand_bb(self.bb,expand)
            self.build_tree(entities,expand)
      
    def __deepcopy__(self,memo):
        if id(self) in memo:
            return memo[id(self)]
        result=BVHTree(None,None)
        memo[id(self)]=result
        for k,v in self.__dict__.items():
            setattr(result,k,copy.deepcopy(v,memo))
        return result
    
    @staticmethod
    def cost_split(ess,d):
        ess=sorted(ess,key=lambda e:(e.bb[0][d]+e.bb[1][d])/2)
        essR=ess[::-1]
    
        #find all cost
        bbL=[]
        bbR=[]
        costL=[]
        costR=[]
        for i in range(len(ess)):
            if i==0:
                bbL.append(ess[i].bb)
                bbR.append(essR[i].bb)
            else: 
                bbL.append(union_bb(bbL[-1],ess[i].bb))
                bbR.append(union_bb(bbR[-1],essR[i].bb))
            costL.append(surface_area_bb(bbL[-1]))
            costR.append(surface_area_bb(bbR[-1]))
    
        #find best cost        
        bestI=None
        bestCost=None
        for i in range(len(ess)-1):
            cost=costL[i]*(i+1)+costR[len(ess)-2-i]*(len(ess)-1-i)
            if bestCost is None or cost<bestCost:
                bestCost=cost
                bestI=i
    
        #fillin bestCost
        L=[]
        R=[]
        for i in range(bestI+1):
            L.append(ess[i])
        for i in range(len(ess)-1-bestI):
            R.append(essR[i])
        assert len(set(L).intersection(set(R)))==0
        assert set(L+R)==set(ess)
        return bestCost,L,R
        
    def build_tree(self,entities,expand):
        if len(entities)==0:
            return
        elif len(entities)==1:
            return
        else:
            bestCost=None
            bestL=None
            bestR=None
            for d in range(len(entities[0].bb[0])):
                cost,L,R=BVHTree.cost_split(entities,d)
                if bestCost is None or cost<bestCost:
                    bestCost=cost
                    bestL=L
                    bestR=R
            #return
            self.L=BVHTree(bestL,expand,self.depth+1)
            self.R=BVHTree(bestR,expand,self.depth+1)
            
    def display(self,margin=0.0001):
        draw_bb(self.bb,margin)
        if self.L is not None:
            self.L.display(margin)
        if self.R is not None:
            self.R.display(margin)
            
    def addSOS(self,M,entity,ZSS,expr,bigM=INFINITY_NUMBER):
        if self.L is None:
            assert self.entities[0]==entity
            return []
        else:
            if len(ZSS)<=self.depth:
                assert len(ZSS)==self.depth
                ZSS.append(M.addVar(vtype=op.GRB.BINARY))
            #add branching constraint
            ret=[]
            if entity in self.L.entities:
                ret.append(M.addConstr(expr<=ZSS[self.depth]*bigM))
                ret+=self.L.addSOS(M,entity,ZSS,expr,bigM)
            else:  
                assert entity in self.R.entities
                ret.append(M.addConstr(expr<=(1-ZSS[self.depth])*bigM))
                ret+=self.R.addSOS(M,entity,ZSS,expr,bigM)
            return ret
    
    def max_depth(self):
        if self.L is None:
            return self.depth
        else:return max(self.L.max_depth(),self.R.max_depth())
    
    def update(self):
        if self.L is not None:
            self.L.update()
            self.R.update()
            self.bb=empty_bb()
            self.bb=union_bb(self.bb,self.L.bb)
            self.bb=union_bb(self.bb,self.R.bb)
        else:
            self.bb=self.entities[0].bb