import scipy.sparse as sp
import numpy as np

class SparseBlock3x3:
    def __init__(self,nBlk):
        self.blks=[[None for i in range(nBlk)] for j in range(nBlk)]
        
    def get_elem(self,r,c):
        rB=r//3
        cB=c//3
        if self.blks[rB][cB] is None:
            return 0.0
        else: return self.blks[rB][cB][r-rB*3][c-cB*3]
        
    def add_elem(self,r,c,val):
        rB=r//3
        cB=c//3
        self.ensure_blk(rB,cB)[r-rB*3][c-cB*3]+=val
        
    def ensure_blk(self,rB,cB):
        if self.blks[rB][cB] is None:
            self.blks[rB][cB]=[[0.0 for i in range(3)] for j in range(3)]
        return self.blks[rB][cB]
    
    def n(self):
        return len(self.blks)*3
    
    def __len__(self):
        return self.n()
        
    def add_blk(self,r,c,blk):
        rB=r//3
        cB=c//3
        nR=len(blk)
        nC=len(blk[0])
        assert nR<=3 and nC<=3
        blkCurr=self.ensure_blk(rB,cB)
        for r in range(nR):
            for c in range(nC):
                blkCurr[r][c]+=blk[r][c]
        
    def sub_blk(self,r,c,blk):
        rB=r//3
        cB=c//3
        nR=len(blk)
        nC=len(blk[0])
        assert nR<=3 and nC<=3
        blkCurr=self.ensure_blk(rB,cB)
        for r in range(nR):
            for c in range(nC):
                blkCurr[r][c]-=blk[r][c]
        
    def add_diag(self,mu):
        N=self.n()
        if mu!=0.0:
            for i in range(N):
                self.add_elem(i,i,mu)
        return self
        
    def mul(self,b):
        ret=[0.0 for i in range(len(b))]
        for r in range(len(self.blks)):
            for c in range(len(self.blks)):
                blk=self.blks[r][c]
                if blk is None:
                    continue
                for rr in range(3):
                    val=0.0
                    for cc in range(3):
                        val+=blk[rr][cc]*b[c*3+cc]
                    ret[r*3+rr]+=val
        return ret
    
    def mulT(self,b):
        raise RuntimeError("SparseBlock3x3.mulT not implemented!")
    
    def to_csc(self):
        data=[]
        rows=[]
        cols=[]
        for r in range(len(self.blks)):
            for c in range(len(self.blks)):
                blk=self.blks[r][c]
                if blk is not None:
                    for rr in range(3):
                        for cc in range(3):
                            data.append(np.float64(blk[rr][cc]))
                            rows.append(np.int32(r*3+rr))
                            cols.append(np.int32(c*3+cc))
        return sp.csc_matrix((data,(rows,cols)),shape=(self.n(),self.n()))
    
    @staticmethod
    def from_dense(A):
        nBlk=(len(A)+2)//3
        assert nBlk*3==len(A)
        ret=SparseBlock3x3(nBlk)
        for r in range(len(A)):
            for c in range(len(A[0])):
                ret.add_elem(r,c,A[r][c])
        return ret