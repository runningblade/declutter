from klampt import WorldModel,Geometry3D,Simulator
from visualizer import *
from problem import *
from utils import *

def make_table(world,table_height,table_width,table_thick,table_depth):
    table=Geometry3D()
    table.loadFile(DATA_PATH+"/data/objects/cube.off")
    table.transform([table_height,0,0,0,table_width,0,0,0,table_thick],[0,0,-table_thick])
    
    legs=[]
    off=0.1
    for i in range(4):
        x=table_height*off if (i&1)==0 else table_height*(1-off)
        y=table_width*off if(i&2)==0 else table_width*(1-off)
        l=Geometry3D()
        l.loadFile(DATA_PATH+"/data/objects/cube.off")
        l.transform([table_thick,0,0,0,table_thick,0,0,0,table_depth-table_thick],[x-table_thick/2,y-table_thick/2,-table_depth])
        legs.append(l)
    
    table_geom=Geometry3D()
    table_geom.setGroup()
    for i,elem in enumerate([table]+legs):
        g=Geometry3D(elem)
        table_geom.setElement(i,g)
    table=world.makeTerrain("table")
    table.geometry().set(table_geom)
    table.appearance().setColor(TABLE_COLOR[0],TABLE_COLOR[1],TABLE_COLOR[2])
    return table

def make_floor(world,table_thick,table_depth,floor_size):
    floor_geom=Geometry3D()
    floor_geom.loadFile(DATA_PATH+"/data/objects/cube.off")
    floor_geom.transform([floor_size,0,0,0,floor_size,0,0,0,table_thick],[-floor_size/2,-floor_size/2,-table_depth-table_thick])
    
    floor=world.makeTerrain("floor")
    floor.geometry().set(floor_geom)
    floor.appearance().setColor(FLOOR_COLOR[0],FLOOR_COLOR[1],FLOOR_COLOR[2])

def make_scene(table_center=True,table_height=3,table_width=5,  \
               table_thick=0.1,table_depth=1,floor_size=100,    \
               mode='tx90',lowpoly=True,scaleBB=1.0,suffix=""):
    table_height=float(table_height)
    table_width=float(table_width)
    table_depth=float(table_depth)
    floor_size=float(floor_size)
    
    world=WorldModel()
    if mode=='TRINA':
        if lowpoly:
            robot_path=DATA_PATH+"/data/TRINA/robots/Anthrax_lowpoly"+suffix+".urdf"
        else: robot_path=DATA_PATH+"/data/TRINA/robots/Anthrax"+suffix+".urdf"
        robot=world.loadRobot(robot_path)
        bb=get_robot_bb(robot)
        q=robot.getConfig()
        q[2]=-table_depth#-table_thick
        q[0]=-table_height/2-bb[1][0]*scaleBB
        robot.setConfig(q)
    elif mode=='TRINA_CALIBRATED':
        world0,table0,robot0,_=make_scene_TRINA()
        geom=robot0.link(5).geometry().getTriangleMesh()
        T=robot0.link(5).getTransform()
        vss0=[se3.apply(T,[geom.vertices[j] for j in range(i*3,i*3+3)]) for i in range(len(geom.vertices)//3)]
        
        #get calibrated data
        import xmlrpc.client,socket,json
        socket.setdefaulttimeout(10)
        try:
            robot_path=DATA_PATH+"/data/TRINA/robots/Anthrax_lowpoly"+suffix+"_Calibrated.urdf"
            proxyCamera=xmlrpc.client.ServerProxy("http://10.0.242.158:8040/")
            URDF=proxyCamera.getURDF(vss0,False).data
            with open(robot_path,"w") as handle:
                handle.write(URDF)
        except Exception as e:
            print("Error reading URDF: %s!"%str(e))
            robot_path=DATA_PATH+"/data/TRINA/robots/Anthrax_lowpoly"+suffix+"_Calibrated.urdf"
            if not os.path.exists(robot_path):
                raise RuntimeError("Cannot find %s!"%robot_path)
            
        #load
        robot=world.loadRobot(robot_path)
        bb=get_robot_bb(robot)
        q=robot.getConfig()
        q[0]+=bb[1][0]*scaleBB
        robot.setConfig(q)
    elif mode=='tx90': 
        robot_path=DATA_PATH+"/data/robots/tx90l_push8.rob"
        robot=world.loadRobot(robot_path)
    else: raise RuntimeError("Unknown mode")
    ignore_adjacent_link_collisions(robot)
    bb=compute_bb_tight(get_robot_root_link(robot)[1].geometry())
    if table_center:
        offX=-table_height/2
    else: offX=bb[0][0]
    
    table=make_table(world,table_height,table_width,table_thick,table_depth)
    table.geometry().transform([1,0,0,0,1,0,0,0,1],[offX,-table_width/2,0])
    make_floor(world,table_thick,table_depth,floor_size)
    return world,table,robot,robot_path

def make_scene_TRINA(lowpoly=True):
    return make_scene(True,table_height=0.76,table_width=1.22,table_depth=0.735,    \
                      mode='TRINA',lowpoly=lowpoly,scaleBB=0.25,suffix="_Adjusted")

def make_scene_TRINA_CALIBRATED(lowpoly=True,suffix="_Adjusted"):
    return make_scene(True,table_height=0.76,table_width=1.22,table_depth=0.735,    \
                      mode='TRINA_CALIBRATED',lowpoly=lowpoly,scaleBB=0.5,suffix="_Adjusted")

def make_scene_tx90(table_height=3,table_width=5):
    return make_scene(True,table_height,table_width,mode='tx90')

if __name__=='__main__':
    world,table,robot,robot_path=make_scene_TRINA_CALIBRATED()
    bbt=((-0.25,-0.5,0),(0.25,0.5,0))#table
    create_boxes_same_size(world,30,0.05,0.2,bbt,robot,0.004,"move_to_table")
    
    sim=Simulator(world)
    set_simulator_margin(world,sim,0.001)
    GLVisualizer(world,table,robot,None).run()