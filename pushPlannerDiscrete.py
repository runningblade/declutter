from polygon2D import Polygon2D
from BVHTree import BVHTree
from planner import *

class PushAction(Action):
    def __init__(self,sim,idss,speed,speedMove,buildBB=True,mustExist=False,hardwareMargin=0.1):
        #height
        object=sim.world.rigidObject(0)
        bb=compute_bb_tight(object.geometry())
        hObj=bb[1][2]-bb[0][2]
        height=sim.reachableTraj.get_pusher().height_ratio(hObj*hardwareMargin)
        
        Action.__init__(self,sim,speed,speedMove)
        self.idss=idss
        sim.reachableTraj.sync()
        self.trajDOF=sim.reachableTraj.gen_traj(self.idss,speed,speedMove,startFromCurrent=False,directed=op.sub(idss[1],idss[0]),height=height)
        if self.trajDOF is None:
            if mustExist:
                self.write()
                print("Cannot find trajDOF for PushAction!")
                assert False
            return
        self.pusher=self.sim.pusher_shape()

        self.posss=[op.mul((id[0],id[1]),self.sim.reachableTraj.get_pusher().res) for id in self.idss]
        if not buildBB:
            return
        posd=op.sub(self.posss[1],self.posss[0])
        theta=atan2_zero2pi(posd[1],posd[0])+math.pi/2
        sinTheta=op.sin_safe(theta)
        cosTheta=op.cos_safe(theta)
        
        vssf=[]
        vssft=[]
        self.bb=empty_bb(2)
        for ip,p in enumerate(self.posss):
            vss=[]
            for v in self.pusher.vss:
                pos_trans=local_to_global_2D(cosTheta,sinTheta,p[0],p[1],v)
                self.bb=union_bb(self.bb,list(pos_trans))
                vss.append(pos_trans)
            if ip==0:
                vssf+=vss
            vssft+=vss
        self.bb=tuple(self.bb)
        self.shapef=Polygon2D(compute_convex_2D(vssf),0.001)
        self.shapeft=Polygon2D(compute_convex_2D(vssft),0.001)

    def display(self,margin,drawShape):
        if drawShape:
            self.shapeft.draw(raw=True)
        for i in range(len(self.posss)-1):
            gl.glVertex3f(self.posss[i+0][0],self.posss[i+0][1],margin)
            gl.glVertex3f(self.posss[i+1][0],self.posss[i+1][1],margin)

    def from_position(self,margin,i=0):
        return [self.posss[i+0][0],self.posss[i+0][1],margin]
    
    def to_position(self,margin,i=0):
        return [self.posss[i+1][0],self.posss[i+1][1],margin]

    def label_valid(self,shapes):
        for s in shapes:
            if s.intersect(self.shapef):
                self.valid=False
                return
        for s in shapes:
            if s.intersect(self.shapeft):
                self.valid=True
                return

    def test(self,state0,parallel=False):
        self.state0=state0
        if parallel:
            self.state1,self.costReduce,self.traj=copy.deepcopy(self.sim).push(state0,self.posss,self.speedMove,self.trajDOF)
        else: self.state1,self.costReduce,self.traj=self.sim.push(state0,self.posss,self.speedMove,self.trajDOF)
        
class PushActionSet:
    def __init__(self,sim,speed,speedMove,level,nrDir):
        self.sim=sim
        self.actions=[]
        for idfrom in sim.reachableTraj.get_pusher().grid:
            if idfrom[0]%level==0 and idfrom[1]%level==0:
                for j in range(nrDir):
                    angle=j*math.pi*2/nrDir
                    off=(op.cos_safe(angle)*level,op.sin_safe(angle)*level)
                    idto=op.add(idfrom,off)
                    #add operation
                    actionPush=PushAction(sim,[idfrom,idto],speed,speedMove)
                    if actionPush.trajDOF is not None:
                        self.actions.append(actionPush)
                        if len(self.actions)%100==0:
                            print("Build %d push actions!"%len(self.actions))
        #build AABB
        self.BBTree=BVHTree(self.actions,0.0)
        print("Constructed AABBTree with depth=%d for %d entities (log(%d)=%f)!"%   \
              (self.BBTree.max_depth(),len(self.actions),len(self.actions),op.log_safe(len(self.actions))))
    
    def display(self,margin,drawBB=True):
        gl.glBegin(gl.GL_LINES)
        for i,action in enumerate(self.actions):
            if hasattr(action,"valid"):
                if action.valid:
                    gldraw.setcolor(VALID_PUSH_ACTION_COLOR[0],VALID_PUSH_ACTION_COLOR[1],VALID_PUSH_ACTION_COLOR[2])
                    action.display(margin,i==0 and not drawBB)
                else: 
                    gldraw.setcolor(PUSH_ACTION_COLOR[0],PUSH_ACTION_COLOR[1],PUSH_ACTION_COLOR[2])
                    action.display(margin/2,i==0 and not drawBB)
            else:
                gldraw.setcolor(EE_DIRECTION_COLOR[0],EE_DIRECTION_COLOR[1],EE_DIRECTION_COLOR[2])
                action.display(margin/2,i==0 and not drawBB)
        gl.glEnd()
        #show bounding volume hierarchy
        gldraw.setcolor(PUSH_ACTION_COLOR[0],PUSH_ACTION_COLOR[1],PUSH_ACTION_COLOR[2])
        if drawBB:
            self.BBTree.display(margin)
                    
    def size(self):
        return len(self.actions)

    def label_valid(self):
        for action in self.actions:
            action.valid=False
        shapes=self.sim.get_shapes()
        self.label_valid_AABB(self.BBTree,shapes)
        
    def label_valid_AABB(self,aabb,shapes):
        if aabb.L is None and aabb.R is None:
            aabb.entities[0].label_valid(shapes)
        else:
            shapesIntersect=[]
            for s in shapes:
                if intersect_bb_bb(s.compute_bb(),aabb.bb):
                    shapesIntersect.append(s)
            if len(shapesIntersect)==0:
                return
            self.label_valid_AABB(aabb.L,shapesIntersect)
            self.label_valid_AABB(aabb.R,shapesIntersect)

    def plan(self,state0,debugInfo):
        self.label_valid()
        
        #perform all push actions
        numValidActions=sum([1 if action.valid else 0 for action in self.actions])
        for action in self.actions:
            if action.valid:
                action.test(state0)
            
        #find best cost-reducing action
        actionBest=None
        for action in self.actions:
            if action.valid:
                if action.costReduce>0 and (actionBest is None or action.costReduce>actionBest.costReduce):
                    actionBest=action
        if debugInfo:
            print("PushPlannerDiscrete tested %d actions, costReduce=%f!"%(numValidActions,actionBest.costReduce if actionBest is not None else 0.0))
        return actionBest
        
class PushPlannerDiscrete(Planner):
    def __init__(self,sim,speed,speedMove,minLevel=1,thresHierarchy=100,nrDir=8):
        Planner.__init__(self,sim)
        self.drawBB=True
        #add hierarchy
        self.hierarchy={}
        level=minLevel
        while True:
            actions=PushActionSet(self.sim,speed,speedMove,level,nrDir)
            if actions.size()<thresHierarchy:
                break
            print("Adding level=%d with %d push operations!"%(level,actions.size()))
            self.hierarchy[level]=actions
            level*=2
    
    def keyboardfunc(self,c,x,y):
        keys=list(self.hierarchy)
        if c==b'1':
            if hasattr(self,"displayId"):
                self.displayId=(self.displayId+1)%len(keys)
            else: self.displayId=0
        elif c==b'2':
            if hasattr(self,"displayId"):
                self.displayId=(self.displayId+len(keys)-1)%len(keys)
            else: self.displayId=0
        elif c==b'3':
            if hasattr(self,"displayId"):
                self.hierarchy[keys[self.displayId]].label_valid()
        elif c==b'4':
            self.drawBB=not self.drawBB
    
    def display(self,margin):
        gl.glPushAttrib(gl.GL_POINT_BIT|gl.GL_LINE_BIT)
        gl.glPointSize(PUSH_GRASP_SAMPLE_POINT_SIZE)
        gl.glLineWidth(PUSH_GRASP_LINE_WIDTH)
        
        keys=list(self.hierarchy)
        if hasattr(self,"displayId"):
            self.hierarchy[keys[self.displayId]].display(margin,self.drawBB)
            
        gl.glPopAttrib(gl.GL_POINT_BIT|gl.GL_LINE_BIT)
    
    def plan(self,debugInfo):
        state0=self.sim.get_state()
        maxLevel=max(list(self.hierarchy))
        while maxLevel in self.hierarchy:
            action=self.hierarchy[maxLevel].plan(state0,debugInfo)
            if action is not None:
                if debugInfo:
                    print("Best cost reduction: %f!"%action.costReduce)
                return action
            maxLevel//=2
        self.sim.set_state(state0)
        return None
        
    def get_push_actions(self):
        actions=[]
        for k,v in self.hierarchy.items():
            actions=v.actions
        return actions