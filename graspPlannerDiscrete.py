from polygon2D import Polygon2D
from pushPlannerContinuous import *
import pyDeclutter

class GraspAction(Action):
    def __init__(self,sim,idss,oid,speed,speedMove,h,mustExist=False):
        Action.__init__(self,sim,speed,speedMove)
        self.idss=idss
        self.oid=oid
        body=self.sim.world.rigidObject(oid)
        sim.reachableTraj.sync()
        self.trajDOF=sim.reachableTraj.gen_traj(self.idss,self.speed,self.speedMove,startFromCurrent=False,grasp=body,height=h)
        if self.trajDOF is None:
            if mustExist:
                self.write()
                print("Cannot find trajDOF for GraspAction!")
                assert False
            return
        
        carry=self.trajDOF[2]
        carry.tO0=body.getTransform()
        carry.sim=self.sim.sim
        #R,t=carry.tO0
        reachable=self.sim.reachableTraj.get_gripper()
        #t=[idss[-1][0]*reachable.res,idss[-1][1]*reachable.res,t[2]]
        #carry.tO1=(R,t)
        
    def display(self,margin,drawShape):
        reachable=self.sim.reachableTraj.get_gripper()
        gl.glVertex3f(self.idss[ 0][0]*reachable.res,self.idss[ 0][1]*reachable.res,margin)
        gl.glVertex3f(self.idss[-1][0]*reachable.res,self.idss[-1][1]*reachable.res,margin)

    def from_position(self,margin):
        reachable=self.sim.reachableTraj.get_gripper()
        return [self.idss[ 0][0]*reachable.res,self.idss[ 0][1]*reachable.res,margin]
    
    def to_position(self,margin):
        reachable=self.sim.reachableTraj.get_gripper()
        return [self.idss[-1][0]*reachable.res,self.idss[-1][1]*reachable.res,margin]

    def test(self,state0,parallel=False):
        reachable=self.sim.reachableTraj.get_gripper()
        pto=(self.idss[-1][0]*reachable.res,self.idss[-1][1]*reachable.res)
        self.state0=state0
        if parallel:
            self.state1,self.costReduce,self.traj=copy.deepcopy(self.sim).grasp(state0,self.oid,pto)
        else: self.state1,self.costReduce,self.traj=self.sim.grasp(state0,self.oid,pto)
    
class GraspPlannerDiscrete(Planner):
    def __init__(self,sim,speed,speedGrasp,pad=0.5):
        Planner.__init__(self,sim)
        self.speed=speed
        self.speedGrasp=speedGrasp
        self.draw=False
        
        #sample possible put locations (always use cell center)
        self.samples=[]
        sampleIdMaps=dict()
        reachable=sim.reachableTraj.get_gripper()
        self.rad=0.
        for s in sim.get_shapes():
            self.rad=max(self.rad,s.diameter()/2*(1+pad))
        offGrid=op.ceil_safe(self.rad/reachable.res)
        for c in reachable.cell:
            if c[0]%offGrid==0 and c[1]%offGrid==0:
                sampleIdMaps[c]=len(self.samples)
                self.samples.append(op.mul(op.add(c,(0.5,0.5)),reachable.res))
        
        #network
        self.build_network()
        
        #height
        object=self.sim.world.rigidObject(0)
        bb=compute_bb_tight(object.geometry())
        hObj=bb[1][2]-bb[0][2]
        self.height=reachable.height_ratio(hObj)
        
    def build_network(self):
        reachable=self.sim.reachableTraj.get_gripper()
        cells=list(reachable.cell)
        self.invCellId=dict()
        self.cellId=dict()
        def add_to_G(cc):
            if cc not in self.cellId:
                self.invCellId[len(self.cellId)]=(cc[0]+0.5,cc[1]+0.5)
                self.cellId[cc]=len(self.cellId)
                self.G.add_node(self.cellId[cc])
        import networkx
        self.G=networkx.Graph()
        for c in cells:
            add_to_G(c)
        for c in cells:
            for dx,dy in [(-1,0),(1,0),(0,-1),(0,1)]:
                co=(c[0]+dx,c[1]+dy)
                if co in cells:
                    self.G.add_edge(self.cellId[c],self.cellId[co],weight=op.norm((float(dx),float(dy))))
            
            c00=op.add(c,(0,0))
            c10=op.add(c,(1,0))
            c01=op.add(c,(0,1))
            c11=op.add(c,(1,1))
            if c00 in cells and c10 in cells and c01 in cells and c11 in cells:
               self.G.add_edge(self.cellId[c00],self.cellId[c11],weight=op.norm((float(1),float(1))))
               self.G.add_edge(self.cellId[c10],self.cellId[c01],weight=op.norm((float(1),float(1))))
            
    def route_on_network(self):
        reachable=self.sim.reachableTraj.get_gripper()
        source=self.cellId[reachable.grid_id(self.pFrom)]
        target=self.cellId[reachable.grid_id(self.pTo)]
        
        from networkx.algorithms.shortest_paths.weighted import single_source_dijkstra
        _,hist=single_source_dijkstra(self.G,source=source,target=target)
        return [tuple(op.mul(self.pFrom,1/reachable.res)),tuple(op.mul(self.pTo,1/reachable.res))]
            
    def keyboardfunc(self,c,x,y):
        if c==b'1':
            self.draw=not self.draw
        elif c==b'2':
            self.plan(True)
        elif c==b'3':
            self.branch(True)
        
    def display(self,margin):
        if not self.draw:
            return
        gl.glPushAttrib(gl.GL_POINT_BIT|gl.GL_LINE_BIT)
        gl.glPointSize(PUSH_GRASP_SAMPLE_POINT_SIZE)
        gl.glLineWidth(PUSH_GRASP_LINE_WIDTH)
        
        #draw objects
        gldraw.setcolor(SPECIAL_OBJECT_COLOR[0],SPECIAL_OBJECT_COLOR[1],SPECIAL_OBJECT_COLOR[2])
        gl.glBegin(gl.GL_LINES)
        self.shapes=[DiscreteShape(s) for s in self.sim.get_shapes()]
        for p in [s.shape.pos for s in self.shapes]:
            gl.glVertex3f(p[0],p[1],-1)
            gl.glVertex3f(p[0],p[1], 1)
        gl.glEnd()
        
        #oid/invalidObjs/action
        if hasattr(self,"oid"):
            gldraw.setcolor(SPECIAL_OBJECT_COLOR[0],SPECIAL_OBJECT_COLOR[1],SPECIAL_OBJECT_COLOR[2])
            self.shapes[self.oid].shape.draw(margin*2,fill=True)
        if hasattr(self,"invalidObjs"):
            gldraw.setcolor(SPECIAL2_OBJECT_COLOR[0],SPECIAL2_OBJECT_COLOR[1],SPECIAL2_OBJECT_COLOR[2])
            for i in self.invalidObjs:
                self.shapes[i].shape.draw(margin*2,fill=True)
        if hasattr(self,"action"):
            gldraw.setcolor(VALID_GRASP_ACTION_COLOR[0],VALID_GRASP_ACTION_COLOR[1],VALID_GRASP_ACTION_COLOR[2])
            gl.glBegin(gl.GL_LINES)
            self.action.display(margin,False)
            gl.glEnd()
        if hasattr(self,"actions"):
            for action in self.actions:
                gldraw.setcolor(VALID_GRASP_ACTION_COLOR[0],VALID_GRASP_ACTION_COLOR[1],VALID_GRASP_ACTION_COLOR[2])
                gl.glBegin(gl.GL_LINES)
                action.display(margin,False)
                gl.glEnd()
            
        #samples
        gldraw.setcolor(GRASP_ACTION_COLOR[0],GRASP_ACTION_COLOR[1],GRASP_ACTION_COLOR[2])
        gl.glBegin(gl.GL_POINTS)
        for s in self.samples:
            gl.glVertex3f(s[0],s[1],margin)
        gl.glEnd()
        if hasattr(self,"validSampleIds"):
            gldraw.setcolor(VALID_GRASP_ACTION_COLOR[0],VALID_GRASP_ACTION_COLOR[1],VALID_GRASP_ACTION_COLOR[2])
            gl.glBegin(gl.GL_POINTS)
            for id in self.validSampleIds:
                s=self.samples[id]
                gl.glVertex3f(s[0],s[1],margin*2)
            gl.glEnd()
            
        gl.glPopAttrib(gl.GL_POINT_BIT|gl.GL_LINE_BIT)
    
    def pruned_valid_samples(self,costsSampleAll):
        samples=[pyDeclutter.Vec2(s[0],s[1]) for s in self.samples]
        validitySamples=[1-v for v in self.shapes[0].env.validSamples(samples,[self.rad]*len(self.samples))]
        def govern(i,j):    #return can i govern j
            for cid in range(len(costsSampleAll)):
                #print(costsSampleAll[cid][i],costsSampleAll[cid][j])
                if costsSampleAll[cid][i]>costsSampleAll[cid][j]:
                    return False
            return True
        prunedIds=[]
        validSampleIds=[i for i,v in enumerate(validitySamples) if v==1]
        for i in validSampleIds:
            governed=False
            for j in prunedIds:
                if govern(j,i):
                    governed=True
                    break
            if not governed:
                prunedIds=[j for j in prunedIds if not govern(i,j)]+[i]
        self.validSampleIds=prunedIds
           
    def binary_and_logic(self,M,A,B):
        result=M.addVar()#result>=0
        M.addConstr(result<=A)
        M.addConstr(result<=B)
        M.addConstr(result>=(A+B-1))
        return result
          
    def model_setup(self):
        state0=self.sim.get_state()
        self.shapes=[DiscreteShape(s) for s in self.sim.get_shapes()]
        pss=[s.shape.pos for s in self.shapes]
        reachable=self.sim.reachableTraj.get_gripper()
        
        #category/cost
        category=self.sim.css()
        cluster=[[i for i in range(len(pss)) if category[i]==c] for c in range(max(category)+1)]
        cost0=self.sim.cost.calc_cost(pss,category)
        
        #calc costs
        costsPssAll=self.sim.cost.calc_costs_all(pss)
        costsSampleAll=self.sim.cost.calc_costs_all(self.samples)
        
        #find valid samples
        DiscreteShape.initialize_env(self.shapes)
        self.pruned_valid_samples(costsSampleAll)
        self.validSamples=[self.samples[id] for id in self.validSampleIds]
        costsSampleAll=[[costs[id]  for id in self.validSampleIds] for costs in costsSampleAll]
        if len(self.validSamples)==0:       #no sample is reachable
            return None
        #find invalid objects
        self.invalidObjs=[i for i,p in enumerate(pss) if not reachable.id_in_grid(reachable.grid_id(p))]
        if len(self.invalidObjs)==len(pss): #no object is reachable
            return None
        
        #solve cost transfer problem
        M=op.gp.Model("OptimalGrasp")
        M.setParam("LogToConsole",0)
        M.setParam("LogToFile","")
        capacities=self.sim.cost.get_capacities()
        zf=[[M.addVar(vtype=op.GRB.BINARY) for j in range(len(pss))] for i in range(len(capacities))]
        zt=[[M.addVar(vtype=op.GRB.BINARY) for j in range(len(self.validSamples))] for i in range(len(capacities))]
        #objective term
        zfSum=0.0
        ztSum=0.0
        objective=0.0
        for rid in range(len(capacities)):
            for pid in range(len(pss)):
                objective+=zf[rid][pid]*costsPssAll[rid][pid]
                zfSum+=zf[rid][pid]
            for sid in range(len(self.validSamples)):
                objective+=zt[rid][sid]*costsSampleAll[rid][sid]
                ztSum+=zt[rid][sid]
        #constraint from-to
        M.addConstr(zfSum==len(pss)-1)
        M.addConstr(ztSum==1)
        #constraint: only one region
        for j in range(len(pss)):
            M.addConstr(sum([zf[i][j] for i in range(len(capacities))])<=1)
        #constraint: capacity
        for ic,c in enumerate(cluster):
            isTake=len(c)-sum([sum([zf[i][j] for j in c]) for i in range(len(capacities))]) #whether the taken object is from the icth category
            for i in range(len(capacities)):
                remaining=sum([zf[i][j] for j in c])                            #number of objects that are not taken, and belong to the ith region and the icth category
                isFill=sum([zt[i][j] for j in range(len(self.validSamples))])   #will the taken object be put to the ith region
                desiredCapacity=remaining+self.binary_and_logic(M,isTake,isFill)#this is the number of objects belonging to the ith region and icth category, after grasp
                M.addConstr(desiredCapacity<=capacities[i][ic])                 #the number of objects after grasp should be smaller than capacity
        #constraint: invalid
        for j in self.invalidObjs:
            M.addConstr(sum([zf[i][j] for i in range(len(capacities))])==1)     #invalid objects are those that are out-of-reach, make sure they are not grasped
        #constraint: monotonic cost decrease
        M.addConstr(objective<=cost0)
        return state0,cost0,pss,costsPssAll,costsSampleAll,zf,zt,M,objective
            
    def plan(self,debugInfo):
        state0,cost0,pss,_,_,zf,zt,M,objective=self.model_setup()
        M.setObjective(objective,op.GRB.MINIMIZE)
        #solve the problem
        M.optimize()
        if M.status==op.GRB.OPTIMAL:
            cost1=objective.getValue()
        else: cost1=cost0
        if cost1<cost0:
            for j in range(len(pss)):
                if sum([zf[i][j].x for i in range(len(zf))])<0.5:
                    self.pFrom=pss[j]
                    self.oid=j
            for j in range(len(self.validSamples)):
                if sum([zt[i][j].x for i in range(len(zf))])>0.5:
                    self.pTo=self.validSamples[j]
            idss=[tuple(op.mul(self.pFrom,1/self.sim.reachableTraj.get_gripper().res)),
                  tuple(op.mul(self.pTo,1/self.sim.reachableTraj.get_gripper().res))]
            self.action=GraspAction(self.sim,idss,self.oid,self.speed,self.speedGrasp,self.height,mustExist=True)
            self.action.test(state0)
            self.sim.set_state(state0)
            if debugInfo:
                print("GraspPlannerDiscrete tested %d samples, costReduce=%f!"%(len(self.validSamples),cost0-cost1))
            return self.action
        else: 
            if debugInfo:
                print("GraspPlannerDiscrete empty!")
            return None

    def branch(self,debugInfo):
        state0,cost0,pss,costsPssAll,costsSampleAll,zf,zt,M,objective=self.model_setup()
        self.actions=[]
        for branch_id in range(len(zf)):
            #create an objective set focus on reducing cost of region branch_id
            objectiveBranch=0.
            for pid in range(len(pss)):
                objectiveBranch+=zf[branch_id][pid]*costsPssAll[branch_id][pid]
            for sid in range(len(self.validSamples)):
                objectiveBranch+=zt[branch_id][sid]*costsSampleAll[branch_id][sid]
            M.setObjective(objectiveBranch,op.GRB.MINIMIZE)
            #solve the problem
            M.optimize()
            if M.status==op.GRB.OPTIMAL:
                cost1=objective.getValue()
            else: cost1=cost0
            if cost1<cost0:
                for j in range(len(pss)):
                    if sum([zf[i][j].x for i in range(len(zf))])<0.5:
                        self.pFrom=pss[j]
                        self.oid=j
                for j in range(len(self.validSamples)):
                    if sum([zt[i][j].x for i in range(len(zf))])>0.5:
                        self.pTo=self.validSamples[j]
                idss=[tuple(op.mul(self.pFrom,1/self.sim.reachableTraj.get_gripper().res)),
                      tuple(op.mul(self.pTo,1/self.sim.reachableTraj.get_gripper().res))]
                action=GraspAction(self.sim,idss,self.oid,self.speed,self.speedGrasp,self.height,mustExist=True)
                action.test(state0)
                self.sim.set_state(state0)
                if debugInfo:
                    print("GraspPlannerDiscrete-branch%d tested %d samples, costReduce=%f!"%(branch_id,len(self.validSamples),cost0-cost1))
                self.actions.append(action)
            else: 
                if debugInfo:
                    print("GraspPlannerDiscrete-branch%d empty!"%branch_id)
                self.actions.append(None)
        return self.actions